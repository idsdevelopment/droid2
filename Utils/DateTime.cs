﻿using System;

namespace Utils
{
	public static class DateTime
	{
		private static readonly DateTimeOffset Epoch;

		static DateTime()
		{
			Epoch = new DateTimeOffset( new System.DateTime( 1970, 1, 1 ) ).KindUnspecified();
		}

		public const string MONDAY_SHORT = "MON",
							TUESDAY_SHORT = "TUE",
							WEDNESDAY_SHORT = "WED",
							THURSDAY_SHORT = "THU",
							FRIDAY_SHORT = "FRI",
							SATURDAY_SHORT = "SAT",
							SUNDAY_SHORT = "SUN";

		public enum WEEK_DAY : byte
		{
			UNKNOWN,
			MONDAY,
			TUESDAY,
			WEDNESDAY,
			THURSDAY,
			FRIDAY,
			SATURDAY,
			SUNDAY
		}

		public static WEEK_DAY ToWeekDay( System.DateTime time )
		{
			switch( time.DayOfWeek )
			{
			case DayOfWeek.Monday:
				return WEEK_DAY.MONDAY;
			case DayOfWeek.Tuesday:
				return WEEK_DAY.TUESDAY;
			case DayOfWeek.Wednesday:
				return WEEK_DAY.WEDNESDAY;
			case DayOfWeek.Thursday:
				return WEEK_DAY.THURSDAY;
			case DayOfWeek.Friday:
				return WEEK_DAY.FRIDAY;
			case DayOfWeek.Saturday:
				return WEEK_DAY.SATURDAY;
			case DayOfWeek.Sunday:
				return WEEK_DAY.SUNDAY;
			default:
				return WEEK_DAY.UNKNOWN;
			}
		}

		public static WEEK_DAY ToWeekDayFromShortText( string weekDay )
		{
			switch( weekDay.Trim().ToUpper() )
			{
			case MONDAY_SHORT:
				return WEEK_DAY.MONDAY;
			case TUESDAY_SHORT:
				return WEEK_DAY.TUESDAY;
			case WEDNESDAY_SHORT:
				return WEEK_DAY.WEDNESDAY;
			case THURSDAY_SHORT:
				return WEEK_DAY.THURSDAY;
			case FRIDAY_SHORT:
				return WEEK_DAY.FRIDAY;
			case SATURDAY_SHORT:
				return WEEK_DAY.SATURDAY;
			case SUNDAY_SHORT:
				return WEEK_DAY.SUNDAY;
			default:
				return WEEK_DAY.UNKNOWN;
			}
		}

		public static DateTimeOffset ServerTimeToLocalTime( this System.DateTime serverTime )
		{
			DateTimeOffset RetVal;

			if( serverTime.Date == System.DateTime.MinValue )
				RetVal = DateTimeOffset.MinValue;
			else
				RetVal = serverTime.Date < Epoch.Date ? serverTime : serverTime.ToLocalTime();

			var Temp = System.DateTime.SpecifyKind( RetVal.DateTime, DateTimeKind.Unspecified );

			RetVal = new DateTimeOffset( Temp, new TimeSpan( 0 ) );

			return RetVal;
		}

		public static System.DateTime LocalTimeToServerTime( this DateTimeOffset serverTime )
		{
			var Temp = System.DateTime.SpecifyKind( serverTime.DateTime, DateTimeKind.Local );

			return Temp < Epoch.Date ? serverTime.DateTime : Temp.ToLocalTime();
		}

		public static DateTimeOffset KindUnspecified( this DateTimeOffset time )
		{
			return new DateTimeOffset( System.DateTime.SpecifyKind( time.DateTime, DateTimeKind.Unspecified ), new TimeSpan( 0 ) );
		}

		public static DateTimeOffset NowKindUnspecified => DateTimeOffset.Now.KindUnspecified();
		public static DateTimeOffset UtcNowKindUnspecified => DateTimeOffset.UtcNow.KindUnspecified();

		public const string PACIFIC_TIME_ID = "America/Vancouver",
							EASTERN_STANDARD_TIME_ID = "GMT-5:00";

		public static System.DateTime ToEasternStandardTime( this System.DateTime deviceTime )
		{
			if( deviceTime == System.DateTime.MinValue )
				return System.DateTime.MinValue;

			var UtcTime = deviceTime.ToUniversalTime();
			var EasternZone = TimeZoneInfo.FindSystemTimeZoneById( EASTERN_STANDARD_TIME_ID );
			return TimeZoneInfo.ConvertTimeFromUtc( UtcTime, EasternZone );
		}

		public static System.DateTime ToEasternStandardTime( this DateTimeOffset deviceTime )
		{
			if( deviceTime == DateTimeOffset.MinValue )
				return System.DateTime.MinValue;

			var UtcTime = deviceTime.ToUniversalTime();
			var EasternZone = TimeZoneInfo.FindSystemTimeZoneById( EASTERN_STANDARD_TIME_ID );
			return TimeZoneInfo.ConvertTimeFromUtc( UtcTime.DateTime, EasternZone );
		}

		public static System.DateTime ToPacificTime( this System.DateTime deviceTime )
		{
			if( deviceTime == System.DateTime.MinValue )
				return System.DateTime.MinValue;

			var UtcTime = deviceTime.ToUniversalTime();
			var PacificZone = TimeZoneInfo.FindSystemTimeZoneById( PACIFIC_TIME_ID );
			return TimeZoneInfo.ConvertTimeFromUtc( UtcTime, PacificZone );
		}

		public static System.DateTime ToPacificTime( this DateTimeOffset deviceTime )
		{
			if( deviceTime == DateTimeOffset.MinValue )
				return System.DateTime.MinValue;

			var UtcTime = deviceTime.ToUniversalTime();
			var PacificZone = TimeZoneInfo.FindSystemTimeZoneById( PACIFIC_TIME_ID );
			return TimeZoneInfo.ConvertTimeFromUtc( UtcTime.DateTime, PacificZone );
		}

		public static DateTimeOffset FromPacificTime( this System.DateTime pacificTime )
		{
			if( pacificTime == System.DateTime.MinValue )
				return DateTimeOffset.MinValue;

			pacificTime = System.DateTime.SpecifyKind( pacificTime, DateTimeKind.Unspecified );
			var PacificZone = TimeZoneInfo.FindSystemTimeZoneById( PACIFIC_TIME_ID );
			var UtcTime = TimeZoneInfo.ConvertTimeToUtc( pacificTime, PacificZone );
			return UtcTime.ToLocalTime();
		}

		public static System.DateTime FromPacificTime( this DateTimeOffset pacificTime )
		{
			if( pacificTime == DateTimeOffset.MinValue )
				return System.DateTime.MinValue;

			var PacificZone = TimeZoneInfo.FindSystemTimeZoneById( PACIFIC_TIME_ID );
			var UtcTime = TimeZoneInfo.ConvertTimeToUtc( pacificTime.DateTime, PacificZone );
			return UtcTime.ToLocalTime();
		}

		public static long UnixTicksSeconds( this System.DateTime time )
		{
			return (long)( time - Epoch ).TotalSeconds;
		}

		public static long UnixTicksSeconds( this DateTimeOffset time )
		{
			return (long)( time - Epoch ).TotalSeconds;
		}

		public static long UnixTicksMilliSeconds( this System.DateTime time )
		{
			return (long)( time - Epoch ).TotalMilliseconds;
		}

		public static long UnixTicksMilliSeconds( this DateTimeOffset time )
		{
			return (long)( time - Epoch ).TotalMilliseconds;
		}

		public static System.DateTime NoSeconds( this System.DateTime time )
		{
			return time.AddSeconds( -time.Second ).AddMilliseconds( -time.Millisecond );
		}

		public static DateTimeOffset NoSeconds( this DateTimeOffset time )
		{
			return time.AddSeconds( -time.Second ).AddMilliseconds( -time.Millisecond );
		}

		public static TimeSpan TimeOfDayNoSeconds( this System.DateTime time )
		{
			return time.NoSeconds().TimeOfDay;
		}

		public static TimeSpan TimeOfDayNoSeconds( this DateTimeOffset time )
		{
			return time.NoSeconds().TimeOfDay;
		}
	}
}