﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Utils
{
    public class Tasks
    {
        private static int _MaxThreads;

        public static int MaxThreads
        {
            get
            {
                if( _MaxThreads <= 0 )
                    _MaxThreads = Environment.ProcessorCount;

                return _MaxThreads;
            }
        }

        public class Task<T>
        {
            public IList<T> Run( IList<T> itemList, Action<T> action, int maxTasks )
            {
                var Count = itemList.Count;
                if( Count > 0 )
                {
                    if( maxTasks < 1 )
                        maxTasks = 1;

                    var TaskCount = Math.Min( maxTasks, Count );
                    var Tasks = new Task[ TaskCount ];

                    var TaskNdx = 0;
                    var Ndx = 0;

                    for( ; ( Count > 0 ) && ( TaskNdx < TaskCount ); --Count )
                    {
                        var Item = itemList[ Ndx++ ];

                        Tasks[ TaskNdx++ ] = Task.Run( () => { action( Item ); } );
                    }

                    for( ; Count > 0; Count-- )
                    {
                        TaskNdx = Task.WaitAny( Tasks );

                        var Item = itemList[ Ndx++ ];

                        Tasks[ TaskNdx ] = Task.Run( () => { action( Item ); } );
                    }

                    Task.WaitAll( Tasks );
                }
                return itemList;
            }

            public IList<T> Run( IList<T> itemList, Action<T> action )
            {
                return Run( itemList, action, MaxThreads );
            }
        }

        public class ConnectionTask<T> : Task<T>
        {
            public new IList<T> Run( IList<T> itemList, Action<T> action )
            {
                return Run( itemList, action, ServicePointManager.DefaultConnectionLimit * 3 / 4 ); // Use up 3/4 of the connections
            }
        }
    }
}