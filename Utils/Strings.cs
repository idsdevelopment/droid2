﻿using System;

namespace Utils
{
	public static class Strings
	{
		public static string Between( this string text, string first, string last, StringComparison culture = StringComparison.Ordinal )
		{
			var First = text.IndexOf( first, culture );
			if( First >= 0 )
			{
				First += first.Length;

				var Last = text.LastIndexOf( last, culture );
				if( ( Last >= 0 ) && ( Last > First ) )
					return text.Substring( First, Last - First );
			}
			return null;
		}

		public static bool IsNullOrEmpty(this string value )
		{
			return string.IsNullOrEmpty( value );
		}
	}

}