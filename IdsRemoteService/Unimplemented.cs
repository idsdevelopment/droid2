using System;
using System.Collections.Generic;
using System.Linq;
using RemoteService.Model;
#if !ANDROID
using IdsRemoteService.IdsRemoteServiceReference;

#else
using IdsRemoteService.org.internetdispatcher.jbtest2;
#endif

namespace IdsRemote
{
    public partial class IdsService
    {
        public void SignOut()
        {
        }

        public void ErrorLog( string errorMessage )
        {
        }

        public void AuditTrailNewOperation( string program, string text, string comment )
        {
        }

        public void AuditTrailModifyOperation( string program, string text, string comment )
        {
        }

        public void AuditTrailDeleteOperation( string program, string text, string comment )
        {
        }

        public void AuditTrailInformOperation( string program, string text, string comment )
        {
        }
    }
}