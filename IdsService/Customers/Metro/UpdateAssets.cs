﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdsService.org.internetdispatcher.jbtest2;
using Service.Abstracts;

namespace IdsService
{
	public partial class IdsService : AIService<Client, AuthToken>
	{
		public override Task<bool> RbhUpdateAssets( AuthToken authToken, string accountId, string tripId, IEnumerable<string> assetBarcodes )
		{
			return Task.Run( () =>
			                 {
				                 try
				                 {
					                 var Cl = NewClient;

					                 if( authToken is not null )
					                 {
						                 Cl.updateAssetTrips( new updateAssetTrips
						                                      {
							                                      authToken     = authToken.Token,
							                                      accountId     = accountId,
							                                      assetBarcodes = assetBarcodes.ToArray(),
							                                      tripId        = tripId
						                                      } );
						                 return true;
					                 }
				                 }
				                 catch
				                 {
				                 }
				                 return false;
			                 } );
		}
	}
}