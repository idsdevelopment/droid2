﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdsService.org.internetdispatcher.jbtest2;
using Service.Abstracts;

namespace IdsService
{
	public partial class IdsService : AIService<Client, AuthToken>
	{
		public override Task<bool> RbhLoadingTrailer( AuthToken authToken, string proBill, string pop, int sigHeight, int sigWidth, string signature, List<string> references )
		{
			return Task.Run( () =>
			                 {
				                 if( OnLine )
				                 {
					                 try
					                 {
						                 var Cl = NewClient;

						                 var LoadingTrailer = new rbhLoadingTrailer
						                                      {
							                                      authToken                = authToken.Token,
							                                      barcodesInReferenceField = references.ToArray(),
							                                      popName                  = pop,
							                                      signature                = signature,
							                                      probillNumber            = proBill,
							                                      signatureHeight          = sigHeight,
							                                      signatureWidth           = sigWidth,
							                                      signatureHeightSpecified = true,
							                                      signatureWidthSpecified  = true
						                                      };

						                 Cl.rbhLoadingTrailer( LoadingTrailer );

						                 return true;
					                 }
					                 catch
					                 {
					                 }
				                 }

				                 return false;
			                 } );
		}

		public override Task<List<string>> RbhUnloadTrailerFindBins( AuthToken authToken, string proBill )
		{
			return Task.Run( () =>
			                 {
				                 var Cl = NewClient;

				                 var FindBins = new rbhUnloadTrailerFindBins
				                                {
					                                authToken     = authToken.Token,
					                                probillNumber = proBill
				                                };

				                 return Cl.rbhUnloadTrailerFindBins( FindBins ).ToList();
			                 } );
		}

		public override Task RbhUnloadTrailer( AuthToken authToken, List<string> references )
		{
			return Task.Run( () =>
			                 {
				                 var Cl = NewClient;

				                 Cl.rbhUnloadTrailer( new rbhUnloadTrailer
				                                      {
					                                      authToken                = authToken.Token,
					                                      barcodesInReferenceField = references.ToArray()
				                                      } );
			                 } );
		}

		public override Task<bool> RbhDestructionTrailer( AuthToken authToken, string trailer, string pop, int sigHeight, int sigWidth, string signature, List<string> references )
		{
			return Task.Run( () =>
			                 {
				                 try
				                 {
					                 var Cl = NewClient;

					                 Cl.rbhScanningOntoDestructionTrailer( new rbhScanningOntoDestructionTrailer
					                                                       {
						                                                       authToken                = authToken.Token,
						                                                       signature                = signature,
						                                                       signatureWidth           = sigWidth,
						                                                       signatureHeight          = sigHeight,
						                                                       signatureHeightSpecified = true,
						                                                       signatureWidthSpecified  = true,
						                                                       POD                      = pop,
						                                                       barcodesInReferenceField = references.ToArray(),
						                                                       trailerNumber            = trailer
					                                                       } );

					                 return true;
				                 }
				                 catch
				                 {
				                 }

				                 return false;
			                 } );
		}
	}
}