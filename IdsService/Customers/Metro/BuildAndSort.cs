﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdsService.org.internetdispatcher.jbtest2;
using Service.Abstracts;

namespace IdsService
{
	public partial class IdsService : AIService<Client, AuthToken>
	{
		public override Task<List<Missing>> RbhFindMissingShipments( AuthToken authToken, List<string> references )
		{
			return Task.Run( () =>
			                 {
				                 if( OnLine )
				                 {
					                 try
					                 {
						                 var Cl = NewClient;

						                 var Fm = new rbhFindMissingShipments
						                          {
							                          authToken        = authToken.Token,
							                          referenceNumbers = references.ToArray()
						                          };

						                 var Temp = Cl.rbhFindMissingShipments( Fm );

						                 if( Temp is not null )
						                 {
							                 static string FixId( string mercuryId )
							                 {
								                 var P = mercuryId.IndexOf( '|' );

								                 return P >= 0 ? mercuryId[ (P + 1).. ] : mercuryId;
							                 }

							                 const string TERMINATOR = "-@@@-",
							                              UNKNOWN    = "Unknown";

							                 var Result = ( from S in Temp
							                                where S.Contains( TERMINATOR )
							                                select S.Split( "-@@@-", StringSplitOptions.RemoveEmptyEntries )
							                                into Fields
							                                where ( Fields.Length == 4 ) && !references.Contains( Fields[ 0 ] )
							                                select new Missing
							                                       {
								                                       Reference     = Fields[ 0 ],
								                                       CompanyName   = Fields[ 1 ],
								                                       TripId        = Fields[ 2 ],
								                                       MercuryTripId = FixId( Fields[ 3 ] )
							                                       } ).ToList();

							                 Result.AddRange( from U in Temp
							                                  where !U.Contains( TERMINATOR )
							                                  select new Missing
							                                         {
								                                         Reference     = U,
								                                         CompanyName   = UNKNOWN,
								                                         TripId        = UNKNOWN,
								                                         MercuryTripId = UNKNOWN
							                                         } );

							                 return Result;
						                 }

						                 return null;
					                 }
					                 catch
					                 {
					                 }
				                 }

				                 return null;
			                 } );
		}

		public override Task RbhUpdateReturnsWithBin( AuthToken authToken, List<string> references, string bin )
		{
			return Task.Run( () =>
			                 {
				                 var Cl = NewClient;

				                 Cl.rbhUpdateReturnsWithBin( new rbhUpdateReturnsWithBin
				                                             {
					                                             authToken        = authToken.Token,
					                                             referenceNumbers = references.ToArray(),
					                                             binId            = bin
				                                             } );
			                 } );
		}

		public override Task RbhUpdateEditCreateWithBin( AuthToken authToken, List<string> references, string bin )
		{
			return Task.Run( () =>
			                 {
				                 var Cl = NewClient;

				                 Cl.rbhEditingExistingBinsCreateIfNecessary( new rbhEditingExistingBinsCreateIfNecessary
				                                                             {
					                                                             authToken                = authToken.Token,
					                                                             barcodesInReferenceField = references.ToArray()
				                                                             } );
			                 } );
		}
	}
}