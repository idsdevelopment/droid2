﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IdsService.org.internetdispatcher.jbtest2;
using Service.Abstracts;

namespace IdsService
{
	public partial class IdsService : AIService<Client, AuthToken>
	{
		public override Task<bool> RbhForeignWaybill( AuthToken authToken, List<string> waybills, List<string> bins )
		{
			return Task.Run( () =>
			                 {
				                 try
				                 {
					                 var Cl = NewClient;

					                 Cl.rbhReturnCageToDestructionTrailer( new rbhReturnCageToDestructionTrailer
					                                                       {
						                                                       authToken      = authToken.Token,
						                                                       binIds         = bins.ToArray(),
						                                                       waybillNumbers = waybills.ToArray()
					                                                       } );

					                 return true;
				                 }
				                 catch
				                 {
				                 }

				                 return false;
			                 } );
		}
	}
}