﻿using IdsService.org.internetdispatcher.jbtest2;
using IdsService.org.internetdispatcher.jbtest21;

namespace IdsService
{
	[System.Web.Services.WebServiceBindingAttribute( Name = "IDSWSBinding", Namespace = "http://remote.server.id.com/" )]
	public class Client : IDSWSService
	{
		public Client()
		{
			Timeout = Globals.OperationTimeoutInSeconds * 1000;
			Url = Globals.EndPoints.UseTestingServer ? Globals.EndPoints.JBTEST2 : Globals.EndPoints.CurrentServer.Main;
		}
	}

	[System.Web.Services.WebServiceBindingAttribute( Name = "PeekManagerWSBinding", Namespace = "http://remote.server.id.com/" )]
	internal class GpsClient : PeekManagerWSService
	{
		public GpsClient()
		{
			Timeout = Globals.OperationTimeoutInSeconds * 1000;
			Url = Globals.EndPoints.UseTestingServer ? Globals.EndPoints.DebugServers[ 0 ].Peek : Globals.EndPoints.CurrentServer.Peek;
		}
	}
}