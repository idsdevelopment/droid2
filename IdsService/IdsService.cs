﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CommsDb.Abstracts;
using IdsService.org.internetdispatcher.jbtest2;
using Service.Abstracts;
using Service.Interfaces;
using Utils;
using remoteGpsData = IdsService.org.internetdispatcher.jbtest21.remoteGpsData;
using updateDriverLocation = IdsService.org.internetdispatcher.jbtest21.updateDriverLocation;

namespace IdsService
{
	public class AuthToken
	{
		public string Token      = "",
		              DriverId   = "",
		              CarrierId  = "",
		              DriverName = "",
		              AccountId;

		public override string ToString() => Token;
	}

	public class User : IUser
	{
		public string UserName        { get; set; }
		public string CarrierId       { get; set; }
		public ushort PollingInterval { get; set; }
		public byte   StartHour       { get; set; }
		public byte   EndHour         { get; set; }
		public byte[] ActiveDays      { get; set; }
		public string Fax             { get; set; }
	}

	public partial class IdsService : AIService<Client, AuthToken>
	{
		public const string RESELLER_ID = "RESELLER_ID";

		private const int DEFAULT_POLLING_PERIOD_IN_SECONDS = 15,
		                  SERVER_PING_PERIOD_IN_SECONDS     = 30,
		                  DEFAULT_START_HOUR                = 7,
		                  DEFAULT_END_HOUR                  = 17;

		public class EndpointState
		{
			internal uint                         AveragePing;
			internal Globals.EndPoints.ServerPair EndPoint;
		}

		internal class RemoteTrip : AITrip<remoteTrip>
		{
			internal RemoteTrip( remoteTrip r, remoteAddress p, remoteAddress d )
			{
				OriginalObject = r;

				TripId           = r.tripId;
				Status           = (STATUS)r.status;
				Driver           = r.driver;
				ReceivedByDevice = r.carCharge;
				ReadByDriver     = r.redirect;

				ServiceLevel = r.serviceLevel ?? "";
				PackageType  = r.packageType ?? "";
				AccountId    = r.accountId ?? "";

				CallerEmailAddress = r.callerEmail ?? "";

				PickupCompanyName  = r.pickupCompany ?? "";
				PickupSuite        = p.suite ?? "";
				PickupAddressLine1 = p.street ?? "";
				PickupCity         = p.city ?? "";
				PickupRegion       = p.province ?? "";
				PickupPostalCode   = p.pc ?? "";
				PickupCountry      = p.country ?? "";
				PickupPhone        = r.pickupPhone ?? "";
				PickupMobile       = r.pickupPhone ?? "";
				PickupEmailAddress = r.pickupEmail ?? "";
				PickupContact      = r.pickupContact ?? "";

				DeliveryCompanyName  = r.deliveryCompany ?? "";
				DeliverySuite        = d.suite ?? "";
				DeliveryAddressLine1 = d.street ?? "";
				DeliveryCity         = d.city ?? "";
				DeliveryRegion       = d.province ?? "";
				DeliveryPostalCode   = d.pc ?? "";
				DeliveryCountry      = d.country ?? "";
				DeliveryPhone        = r.deliveryPhone ?? "";
				DeliveryMobile       = r.deliveryPhone ?? "";
				DeliveryEmailAddress = r.deliveryEmail ?? "";
				DeliveryContact      = r.deliveryContact ?? "";

				PickupTime          = r.pickupTime.ServerTimeToLocalTime();
				DeliveryTime        = r.deliveredTime.ServerTimeToLocalTime();
				DueTime             = r.deadlineTime.ServerTimeToLocalTime();
				ReadyTime           = r.readyTime.ServerTimeToLocalTime();
				UpdatedTime         = new DateTime( r.lastUpdated );
				PickupArrivalTime   = r.pickupArriveTime.ServerTimeToLocalTime();
				DeliveryArrivalTime = r.deliveryArriveTime.ServerTimeToLocalTime();

				Weight = (decimal)r.weight;
				Pieces = OriginalPieceCount = r.pieces;

				CurrentZone  = r.currentZone ?? "";
				PickupZone   = r.pickupZone ?? "";
				DeliveryZone = r.deliveryZone ?? "";
				PodName      = r.podName ?? "";

				BillingNote  = r.billingNotes ?? "";
				PickupNote   = r.pickupNotes ?? "";
				DeliveryNote = r.deliveryNotes ?? "";

				ChargeAmount = (decimal)r.carChargeAmount;
				Pallets      = r.pallets ?? "";
				Reference    = r.clientReference ?? "";

				OriginalPickupAddressId   = r.pickupAddressId ?? "";
				OriginalDeliveryAddressId = r.deliveryAddressId ?? "";

				BillingAccount = r.billingCompany;
			}
		}

		public class Zone : IZone
		{
			public string ZoneName { get; set; }
		}

		public static readonly Dictionary<string, EndpointState> AveragePings = new();

		private readonly Dictionary<string, remoteAddress> AddressCache     = new();
		private readonly object                            RemoteLockObject = new();

		private bool InFindTripsForDriver;

		private bool InPing;
		private bool InPingTimer;

		// ReSharper disable once NotAccessedField.Local
		private Timer PingTimer;

		public override async Task<PING_STATUS> Ping()
		{
			if( !InPing && !InPingTimer )
			{
				InPing = true;

				try
				{
					PingServers();

					return await Task.Run( () =>
					                       {
						                       try
						                       {
							                       lock( RemoteLockObject )
							                       {
								                       using var Client = NewClient;

								                       Client.Timeout = 2000;
								                       var RetVal = Client.ping( new ping() );

								                       return RetVal.@return == "OK" ? PING_STATUS.ONLINE : PING_STATUS.OFFLINE;
							                       }
						                       }
						                       catch( Exception Exception )
						                       {
							                       Console.WriteLine( Exception );

							                       return PING_STATUS.OFFLINE;
						                       }
					                       }
					                     );
				}
				finally
				{
					InPing = false;
				}
			}

			return PING_STATUS.UNKNOWN;
		}

		public override async Task<(CONNECTION_STATUS Status, AuthToken AuthToken, IUser User)> Connect( string carrierId, string userName, string password )
		{
			Globals.EndPoints.Reseller = carrierId;

			return await Task.Run( () =>
			                       {
				                       using var Client = NewClient;

				                       try
				                       {
					                       var AToken = carrierId + '-' + userName + '-' + password;

					                       var Usr = Client.findUserForLogin( new findUserForLogin
					                                                          {
						                                                          authToken    = AToken,
						                                                          accountId    = carrierId,
						                                                          userIdToFind = userName,
						                                                          userPass     = password
					                                                          } );

					                       var Return = Usr?.@return;

					                       if( Return != null )
					                       {
						                       ushort PollingInterval = DEFAULT_POLLING_PERIOD_IN_SECONDS;

						                       byte StartHour = DEFAULT_START_HOUR,
						                            EndHour   = DEFAULT_END_HOUR;

						                       var ActiveDays = new byte[ 7 ];

						                       if( Return.pager != null )
						                       {
							                       var Periods = Return.pager.Split( new[] { ',' }, StringSplitOptions.RemoveEmptyEntries );

							                       var L = Periods.Length;

							                       if( L >= 1 )
							                       {
								                       ushort.TryParse( Periods[ 0 ], out PollingInterval );

								                       if( L >= 2 )
									                       byte.TryParse( Periods[ 1 ], out StartHour );

								                       if( L >= 3 )
									                       byte.TryParse( Periods[ 2 ], out EndHour );

								                       for( int I = 3,
								                                J = 0;
								                            I < L; )
									                       ActiveDays[ J++ ] = (byte)DateTimeExtensions.ToWeekDayFromShortText( Periods[ I++ ] );
							                       }
						                       }

						                       return ( Status: CONNECTION_STATUS.OK,
						                                AuthToken: new AuthToken
						                                           {
							                                           DriverId   = Return.userId ?? "",
							                                           Token      = AToken,
							                                           CarrierId  = Return.resellerId,
							                                           DriverName = Return.userId,
							                                           AccountId  = Return.accountId
						                                           },
						                                User: new User
						                                      {
							                                      UserName        = Return.userId,
							                                      CarrierId       = Return.resellerId,
							                                      StartHour       = StartHour,
							                                      PollingInterval = PollingInterval,
							                                      EndHour         = EndHour,
							                                      ActiveDays      = ActiveDays,
							                                      Fax             = ( Return.fax ?? "" ).Trim()
						                                      } );
					                       }
				                       }
				                       catch
				                       {
				                       }

				                       return ( Status: CONNECTION_STATUS.BAD, AuthToken: null, User: null );
			                       } );
		}

		public override Task<Dictionary<string, string>> GetPreferences( AuthToken authToken )
		{
			return Task.Run( () =>
			                 {
				                 var RetVal = new Dictionary<string, string> { { RESELLER_ID, authToken.CarrierId } };

				                 using var Client = NewClient;

				                 for( var I = 0; I++ < 10; )
				                 {
					                 try
					                 {
						                 var Response = Client.getResellerPrefs( new getResellerPrefs { authToken = authToken.Token } );
						                 var Result   = Response;

						                 if( Result != null )
						                 {
							                 foreach( var Temp in Result.Select( val => val.Split( '|' ) ).Where( temp => temp.Length >= 2 ) )
								                 RetVal.Add( Temp[ 0 ].Trim(), Temp[ 1 ] );
						                 }

						                 break;
					                 }
					                 catch
					                 {
						                 Thread.Sleep( 10000 );
					                 }
				                 }

				                 return RetVal;
			                 } );
		}

		public override Task<(List<ITrip> Trips, bool Ok)> GetTrips( AuthToken authToken, bool ignoreAddress = false )
		{
			return Task.Run( () =>
			                 {
				                 lock( AddressCache )
					                 AddressCache.Clear();

				                 var Result = new List<ITrip>();

				                 var Retval = ( Trips: Result, Ok: false );

				                 if( InFindTripsForDriver )
					                 return Retval;

				                 InFindTripsForDriver = true;

				                 try
				                 {
					                 remoteTrip[] Trips;

					                 using( var Client = NewClient )
						                 Trips = Client.findTripsForDriver( new findTripsForDriver { authToken = authToken.Token, driverId = authToken.DriverId } );

					                 if( Trips is not null )
					                 {
						                 var Atoken = authToken.Token;

						                 Trips = ( from T in Trips
						                           where T.status < (int)STATUS.VERIFIED
						                           select T ).ToArray();

						                 new Tasks.Task<remoteTrip>().Run( Trips, trip =>
						                                                          {
							                                                          var (Trip, Ok) = ConvertTrip( Atoken, trip, ignoreAddress ).Result;

							                                                          if( Ok )
							                                                          {
								                                                          try
								                                                          {
									                                                          lock( Result )
										                                                          Result.Add( Trip );
								                                                          }
								                                                          catch( Exception E )
								                                                          {
									                                                          Console.WriteLine( E );
								                                                          }
							                                                          }
						                                                          }, 2 );
						                 Retval.Ok = true;
					                 }
				                 }
				                 catch( Exception E )
				                 {
					                 Console.WriteLine( E );
				                 }
				                 finally
				                 {
					                 InFindTripsForDriver = false;
				                 }

				                 return Retval;
			                 } );
		}

		public override Task<(ITrip Trip, bool Ok)> GetTrip( AuthToken authToken, string tripId, bool ignoreAddress = false )
		{
			return Task.Run( async () =>
			                 {
				                 using var Client = NewClient;

				                 try
				                 {
					                 var Trip = Client.findTrip( new findTrip
					                                             {
						                                             authToken = authToken.Token,
						                                             tripId    = tripId
					                                             } ).@return;

					                 if( Trip is not null )
					                 {
						                 var (RemoteTrip, Ok) = await ConvertTrip( authToken.Token, Trip, ignoreAddress );

						                 if( Ok )
							                 return ( Trip: RemoteTrip, Ok: true );
					                 }
				                 }
				                 catch( Exception Exception )
				                 {
					                 Console.WriteLine( Exception );
				                 }

				                 return ( Trip: (ITrip)null, Ok: false );
			                 } );
		}


		public override Task<UPDATE_STATUS> UpdateTrip( AuthToken authToken, ITrip t, UPDATE_STATUS previousUpdateStatus )
		{
			return Task.Run( () =>
			                 {
				                 try
				                 {
					                 var Now = DateTimeExtensions.NowKindUnspecified.DateTime;

					                 var Stat = (int)STATUS.NEW;

					                 if( t.UndeliverableMode == UNDELIVERABLE_MODE.NONE )
					                 {
						                 switch( t.Status )
						                 {
						                 case STATUS.CREATE_NEW_TRIP_AS_DISPATCHED:
							                 t.Status = STATUS.DISPATCHED;
							                 Stat     = (int)STATUS.DISPATCHED;

							                 goto CreateNewTrip;

						                 case STATUS.CREATE_NEW_TRIP_AS_PICKED_UP:
							                 t.Status = STATUS.PICKED_UP;
							                 Stat     = (int)STATUS.PICKED_UP;

							                 goto CreateNewTrip;

						                 case STATUS.CREATE_NEW_TRIP:
							                 t.Status = STATUS.NEW;

							                 CreateNewTrip:

							                 var N = new remoteTrip
							                         {
								                         accountId  = t.AccountId,
								                         resellerId = authToken.CarrierId,

								                         driver = t.Driver,
								                         status = Stat,

								                         tripId     = null,
								                         internalId = null,

								                         deliveryAddressId = t.OriginalDeliveryAddressId,
								                         deliveryCompany   = t.DeliveryCompanyName,
								                         pickupCompany     = t.PickupCompanyName,

								                         callerPhone                 = "",
								                         billingNotes                = "",
								                         callTakerId                 = t.CallerEmailAddress ?? "",
								                         callTime                    = Now,
								                         callTimeSpecified           = true,
								                         caller                      = "",
								                         callerEmail                 = "",
								                         clientReference             = "",
								                         currentZone                 = "",
								                         deadlineTime                = Now,
								                         deliveredTime               = DateTime.MinValue,
								                         deliveryArriveTime          = DateTime.MinValue,
								                         deliveryArriveTimeSpecified = true,
								                         deliveryContact             = "",
								                         deliveryEmail               = t.DeliveryEmailAddress ?? "",
								                         deliveryNotes               = "",
								                         deliveryPhone               = "",
								                         deliveryZone                = "",
								                         driverAssignTime            = Now,
								                         driverAssignTimeSpecified   = true,
								                         packageType                 = "",
								                         pallets                     = "",
								                         pickupArriveTime            = Now,
								                         pickupArriveTimeSpecified   = true,
								                         pickupContact               = "",
								                         pickupEmail                 = t.PickupEmailAddress ?? "",
								                         pickupTime                  = Now,
								                         pickupNotes                 = "",
								                         pickupPhone                 = "",
								                         pickupTimeSpecified         = true,
								                         pickupZone                  = "",
								                         podName                     = "",
								                         readyTimeString             = "",
								                         readyTime                   = Now,
								                         readyTimeSpecified          = true,
								                         receivedByIdsRouteDate      = Now,
								                         serviceLevel                = "",
								                         sigFilename                 = "",
								                         sortOrder                   = "",
								                         stopGroupNumber             = "",
								                         modifiedTripDate            = Now,

								                         dangerousGoods = true
							                         };

							                 // Set Pickup Address
							                 if( t.PickupAddressId.IsNotNullOrWhiteSpace() )
							                 {
								                 N.pickupAddressId = t.PickupAddressId;
								                 N.pickupCompany   = t.PickupCompanyName;
							                 }

							                 if( t.DeliveryAddressId.IsNotNullOrWhiteSpace() )
							                 {
								                 N.deliveryAddressId = t.DeliveryAddressId;
								                 N.deliveryCompany   = t.DeliveryCompanyName;
							                 }

							                 t.OriginalObject = N;

							                 break;
						                 }
					                 }

					                 if( t.OriginalObject is remoteTrip T )
					                 {
						                 var DoEmail         = false;
						                 var DoUndeliverable = false;

						                 var Status = (int)t.Status;

						                 switch( t.UndeliverableMode )
						                 {
						                 case UNDELIVERABLE_MODE.STATUS_CHANGE:
							                 t.Status       = STATUS.ACTIVE;
							                 t.ServiceLevel = "UNDELIVERABLE";
							                 t.BillingNote  = $"UNDELIVERABLE {DateTime.Now:yyyy-MM-dd hh:mm}\r\n{t.BillingNote}";

							                 break;

						                 case UNDELIVERABLE_MODE.NEW_TRIP:
							                 DoUndeliverable = true;

							                 break;
						                 }

						                 if( T.status != Status )
						                 {
							                 DoEmail = t.Status switch
							                           {
								                           STATUS.PICKED_UP => true,
								                           STATUS.VERIFIED  => true,
								                           _                => false
							                           };
						                 }

						                 T.stopGroupNumber ??= "";

						                 T.status    = Status;
						                 T.carCharge = t.ReceivedByDevice;
						                 T.driver    = t.Driver;
						                 T.redirect  = t.ReadByDriver;

						                 T.serviceLevel = t.ServiceLevel;
						                 T.packageType  = t.PackageType;

						                 T.pickupTime       = t.PickupTime.LocalTimeToServerTime();
						                 T.pickupArriveTime = t.PickupArrivalTime.LocalTimeToServerTime();

						                 T.deliveredTime      = t.DeliveryTime.LocalTimeToServerTime();
						                 T.deliveryArriveTime = t.DeliveryArrivalTime.LocalTimeToServerTime();

						                 T.lastUpdated = Now.Ticks;

						                 T.deadlineTime = t.DueTime.LocalTimeToServerTime();
						                 T.readyTime    = t.ReadyTime.LocalTimeToServerTime();
						                 T.currentZone  = t.CurrentZone;

						                 T.pieces = (int)t.Pieces;
						                 T.weight = (float)t.Weight;

						                 T.podName         = t.PodName;
						                 T.clientReference = t.Reference;
						                 T.sigFilename     = t.Signature;

						                 T.pallets = t.Pallets;

						                 T.pickupNotes   = t.PickupNote;
						                 T.deliveryNotes = t.DeliveryNote;
						                 T.billingNotes  = t.BillingNote;

						                 T.deliveryEmail = t.DeliveryEmailAddress;

						                 var Parts = ( t.Signature ?? "" ).Split( new[] { '^' }, StringSplitOptions.RemoveEmptyEntries );

						                 if( Parts.Length >= 3 )
						                 {
							                 T.sigHeight   = int.Parse( Parts[ 0 ] );
							                 T.sigWidth    = int.Parse( Parts[ 1 ] );
							                 T.sigFilename = Parts[ 2 ];
						                 }
						                 else
						                 {
							                 T.sigHeight   = T.sigWidth = 0;
							                 T.sigFilename = "";
						                 }

						                 if( !DoUndeliverable )
						                 {
							                 using var Client = NewClient;

							                 if( previousUpdateStatus is UPDATE_STATUS.UNKNOWN or UPDATE_STATUS.FAIL1 )
							                 {
								                 if( ( T.status == (int)STATUS.NEW ) && t.UseUpdateTripDetailedQuickForNew )
								                 {
									                 Client.updateTripDetailedQuick( new updateTripDetailedQuick
									                                                 {
										                                                 authToken = authToken.Token,
										                                                 _tripVal = new remoteTripDetailed
										                                                            {
											                                                            deliveryResellerId = t.DeliveryResellerId,
											                                                            pickupResellerId   = t.PickupResellerId,
											                                                            deliveryAccountId  = t.DeliveryAccountId,
											                                                            pickupAccountId    = t.PickupAccountId,

											                                                            status                            = T.status,
											                                                            billingNotes                      = T.billingNotes,
											                                                            deliveryCompany                   = T.deliveryCompany,
											                                                            deliveryAddressId                 = T.deliveryAddressId,
											                                                            pickupCompany                     = T.pickupCompany,
											                                                            pickupAddressId                   = T.pickupAddressId,
											                                                            POD                               = T.POD,
											                                                            accountId                         = T.accountId,
											                                                            board                             = T.board,
											                                                            callTakerId                       = T.callTakerId,
											                                                            callTime                          = T.callTime,
											                                                            callTimeSpecified                 = T.callTimeSpecified,
											                                                            caller                            = T.caller,
											                                                            callerEmail                       = T.callerEmail,
											                                                            callerPhone                       = T.callerPhone,
											                                                            carCharge                         = T.carCharge,
											                                                            carChargeAmount                   = T.carChargeAmount,
											                                                            clientReference                   = T.clientReference,
											                                                            currentZone                       = T.currentZone,
											                                                            dangerousGoods                    = T.dangerousGoods,
											                                                            deadlineTime                      = T.deadlineTime,
											                                                            deadlineTimeSpecified             = T.deadlineTimeSpecified,
											                                                            declaredValueAmount               = T.declaredValueAmount,
											                                                            deliveredTime                     = T.deliveredTime,
											                                                            deliveredTimeSpecified            = T.deliveredTimeSpecified,
											                                                            deliveryAmount                    = T.deliveryAmount,
											                                                            deliveryArriveTime                = T.deliveryArriveTime,
											                                                            deliveryArriveTimeSpecified       = T.deliveryArriveTimeSpecified,
											                                                            deliveryCity                      = "",
											                                                            deliveryContact                   = T.deliveryContact,
											                                                            deliveryCountry                   = "",
											                                                            deliveryDescription               = "",
											                                                            deliveryEmail                     = T.deliveryEmail,
											                                                            deliveryNotes                     = T.deliveryNotes,
											                                                            deliveryPc                        = "",
											                                                            deliveryPhone                     = T.deliveryPhone,
											                                                            deliveryProvince                  = "",
											                                                            deliveryStreet                    = "",
											                                                            deliverySuite                     = "",
											                                                            deliveryZone                      = T.deliveryZone,
											                                                            disbursementAmount                = T.disbursementAmount,
											                                                            discountAmount                    = T.discountAmount,
											                                                            disputed                          = T.disputed,
											                                                            dontFinalizeDate                  = T.dontFinalizeDate,
											                                                            dontFinalizeDateSpecified         = T.dontFinalizeDateSpecified,
											                                                            dontFinalizeFlag                  = T.dontFinalizeFlag,
											                                                            dontFinalizeFlagSpecified         = T.dontFinalizeFlagSpecified,
											                                                            driver                            = T.driver,
											                                                            driverAssignTime                  = T.driverAssignTime,
											                                                            driverAssignTimeSpecified         = T.driverAssignTimeSpecified,
											                                                            driverPaidCommissionDate          = T.driverPaidCommissionDate,
											                                                            driverPaidCommissionDateSpecified = T.driverPaidCommissionDateSpecified,
											                                                            height                            = T.height,
											                                                            insuranceAmount                   = T.insuranceAmount,
											                                                            internalId                        = "",
											                                                            invoiceId                         = T.invoiceId,
											                                                            invoiced                          = T.invoiced,
											                                                            isCashTrip                        = T.isCashTrip,
											                                                            isCashTripReconciled              = T.isCashTripReconciled,
											                                                            isDriverPaid                      = T.isDriverPaid,
											                                                            lastUpdated                       = T.lastUpdated,
											                                                            length                            = T.length,
											                                                            mailDrop                          = T.mailDrop,
											                                                            miscAmount                        = T.miscAmount,
											                                                            modified                          = T.modified,
											                                                            modifiedTripDate                  = T.modifiedTripDate,
											                                                            modifiedTripDateSpecified         = T.modifiedTripDateSpecified,
											                                                            modifiedTripFlag                  = T.modifiedTripFlag,
											                                                            modifiedTripFlagSpecified         = T.modifiedTripFlagSpecified,
											                                                            noGoodsAmount                     = T.noGoodsAmount,
											                                                            packageType                       = T.packageType,
											                                                            pallets                           = T.pallets,
											                                                            pickupArriveTime                  = T.pickupArriveTime,
											                                                            pickupArriveTimeSpecified         = T.pickupArriveTimeSpecified,
											                                                            pickupCity                        = "",
											                                                            pickupContact                     = T.pickupContact,
											                                                            pickupCountry                     = "",
											                                                            pickupDescription                 = "",
											                                                            pickupEmail                       = T.pickupEmail,
											                                                            pickupNotes                       = T.pickupNotes,
											                                                            pickupPc                          = "",
											                                                            pickupPhone                       = T.pickupPhone,
											                                                            pickupProvince                    = "",
											                                                            pickupStreet                      = "",
											                                                            pickupSuite                       = "",
											                                                            pickupTime                        = T.pickupTime,
											                                                            pickupTimeSpecified               = T.pickupTimeSpecified,
											                                                            pickupZone                        = T.pickupZone,
											                                                            pieces                            = T.pieces,
											                                                            podName                           = T.podName,
											                                                            priorityInvoiceId                 = T.priorityInvoiceId,
											                                                            priorityInvoiceIdSpecified        = T.priorityInvoiceIdSpecified,
											                                                            priorityStatus                    = T.priorityStatus,
											                                                            priorityStatusSpecified           = T.priorityStatusSpecified,
											                                                            processedByIdsRouteDate           = T.processedByIdsRouteDate,
											                                                            processedByIdsRouteDateSpecified  = T.processedByIdsRouteDateSpecified,
											                                                            readyTime                         = T.readyTime,
											                                                            readyTimeSpecified                = T.readyTimeSpecified,
											                                                            readyTimeString                   = T.readyTimeString,
											                                                            readyToInvoiceFlag                = T.readyToInvoiceFlag,
											                                                            readyToInvoiceFlagSpecified       = T.readyToInvoiceFlagSpecified,
											                                                            receivedByIdsRouteDate            = T.receivedByIdsRouteDate,
											                                                            receivedByIdsRouteDateSpecified   = T.receivedByIdsRouteDateSpecified,
											                                                            redirect                          = T.redirect,
											                                                            redirectAmount                    = T.redirectAmount,
											                                                            resellerId                        = T.resellerId,
											                                                            returnTrip                        = T.returnTrip,
											                                                            serviceLevel                      = T.serviceLevel,
											                                                            sigFilename                       = T.sigFilename,
											                                                            skipUpdateCommonAddress           = T.skipUpdateCommonAddress,
											                                                            sortOrder                         = T.sortOrder,
											                                                            stopGroupNumber                   = T.stopGroupNumber,
											                                                            totalAmount                       = T.totalAmount,
											                                                            totalFixedAmount                  = T.totalFixedAmount,
											                                                            totalPayrollAmount                = T.totalPayrollAmount,
											                                                            totalTaxAmount                    = T.totalTaxAmount,
											                                                            tripId                            = T.tripId,
											                                                            waitTimeAmount                    = T.waitTimeAmount,
											                                                            waitTimeMins                      = T.waitTimeMins,
											                                                            weight                            = T.weight,
											                                                            weightAmount                      = T.weightAmount,
											                                                            weightOrig                        = T.weightOrig,
											                                                            width                             = T.width
										                                                            },
										                                                 broadcastUpdateMessage = false
									                                                 } );
								                 }
								                 else
								                 {
									                 Client.updateTripQuickBroadcast( new updateTripQuickBroadcast
									                                                  {
										                                                  authToken        = authToken.Token,
										                                                  _tripVal         = T,
										                                                  updateDriverZone = t.ReceivedByDevice && t.ReadByDriver && ( t.Status >= STATUS.PICKED_UP )
									                                                  } );
								                 }
							                 }

							                 if( DoEmail && !string.IsNullOrEmpty( t.CallerEmailAddress ) && !string.IsNullOrEmpty( T.tripId ) )
							                 {
								                 try
								                 {
									                 Client.sendTripStatusMessageNew( new sendTripStatusMessageNew
									                                                  {
										                                                  authToken = authToken.Token,
										                                                  tripId    = t.TripId,
										                                                  status    = Status,
										                                                  isNew     = false
									                                                  } );
								                 }
								                 catch( Exception E )
								                 {
									                 Console.WriteLine( E );
									                 return UPDATE_STATUS.FAIL2;
								                 }
							                 }
						                 }
						                 else
						                 {
							                 var UpdateUndel = new updateTripWithUndeliverables
							                                   {
								                                   authToken               = authToken.Token,
								                                   trip                    = T,
								                                   reason                  = t.UndeliverableReason?.Trim() ?? "",
								                                   undeliverablePieceCount = (int)t.OriginalPieceCount - (int)t.Pieces,
								                                   updateDriverZone        = true
							                                   };

							                 using var Client = NewClient;
							                 Client.updateTripWithUndeliverables( UpdateUndel );
						                 }

						                 return UPDATE_STATUS.OK;
					                 }
				                 }
				                 catch( Exception Exception )
				                 {
					                 Console.WriteLine( Exception );
				                 }

				                 return UPDATE_STATUS.FAIL1;
			                 } );
		}

		public override Task<List<IZone>> GetZones( AuthToken authToken )
		{
			return Task.Run( () =>
			                 {
				                 var Result = new List<IZone>();

				                 try
				                 {
					                 using var Client = NewClient;

					                 var Message  = new getZonesForReseller { authToken = authToken.Token };
					                 var Response = Client.getZonesForReseller( Message );

					                 if( Response is not null )
					                 {
						                 Result.AddRange( from Z in Response
						                                  orderby Z.zoneAbbrev
						                                  select new Zone { ZoneName = Z.zoneAbbrev } );
					                 }
				                 }
				                 catch( Exception Exception )
				                 {
					                 Console.WriteLine( Exception );
				                 }

				                 return Result;
			                 } );
		}

		public override Task<bool> UpdateTripStatus( AuthToken authToken, List<ITripStatus> tripStatus )
		{
			return Task.Run( () =>
			                 {
				                 try
				                 {
					                 var L           = tripStatus.Count;
					                 var TripIds     = new string[ L ];
					                 var UpDateTimes = new DateTime[ L ];
					                 var Ndx         = 0;

					                 foreach( var TripStatus in tripStatus )
					                 {
						                 TripIds[ Ndx ]       = TripStatus.TripId;
						                 UpDateTimes[ Ndx++ ] = TripStatus.UpdateTime.LocalTimeToServerTime();
					                 }

					                 var Message = new setTripsToPickedUp { authToken = authToken.Token, trips = TripIds, puDates = UpDateTimes };

					                 try
					                 {
						                 using var Client = NewClient;

						                 Client.setTripsToPickedUp( Message );

						                 return true;
					                 }
					                 catch( Exception E )
					                 {
						                 Console.WriteLine( E );
						                 Thread.Sleep( 10 * 1000 );
					                 }
				                 }
				                 catch( Exception Exception )
				                 {
					                 Console.WriteLine( Exception );
				                 }

				                 return false;
			                 } );
		}

		public override Task<bool> CreateTripFromBarcode( AuthToken authToken, string barcode, decimal quantity, DateTimeOffset pickupTime, string packageType, string serviceLevel )
		{
			return Task.Run( () =>
			                 {
				                 var Message = new createTripFromBarcode
				                               {
					                               authToken           = authToken.Token,
					                               barcode             = barcode.Trim(),
					                               tote                = "",
					                               pickupTime          = pickupTime.LocalTimeToServerTime(),
					                               pickupTimeSpecified = true,
					                               packageType         = packageType,
					                               serviceLevel        = serviceLevel
				                               };

				                 try
				                 {
					                 using var Client = NewClient;

					                 Client.createTripFromBarcode( Message );

					                 return true;
				                 }
				                 catch( Exception Exception )
				                 {
					                 Console.WriteLine( Exception );
				                 }

				                 return false;
			                 } );
		}

		public override Task<(List<ITrip> Trips, string ContainerId, CONTAINER_STATUS Status)> GetTripsInContainer( AuthToken authToken, string containerCode )
		{
			return Task.Run( () =>
			                 {
				                 var Result = ( Trips: new List<ITrip>(), ContainerId: "", Status: CONTAINER_STATUS.BAD );

				                 try
				                 {
					                 using var Client = NewClient;

					                 Client.Timeout = 1200000;

					                 var Container = Client.findTripByClientReferenceIdExact( new findTripByClientReferenceIdExact
					                                                                          {
						                                                                          authToken       = authToken.Token,
						                                                                          clientReference = containerCode
					                                                                          } ).@return;

					                 if( Container is not null )
					                 {
						                 Result.ContainerId = Container.tripId;

						                 if( (STATUS)Container.status >= STATUS.DELETED )
							                 Result.Status = CONTAINER_STATUS.DELETED;
						                 else
						                 {
							                 var Message = new findTripsByPalletsField
							                               {
								                               authToken    = authToken.Token,
								                               accountId    = Container.accountId,
								                               palletsField = Container.pallets
							                               };

							                 var Temp = Client.findTripsByPalletsField( Message );

							                 if( Temp is not null && ( Temp.Length > 0 ) )
							                 {
								                 var Ra = new remoteAddress();

								                 foreach( var RemoteTrip in Temp )
								                 {
									                 if( ( (STATUS)RemoteTrip.status != STATUS.DELETED ) && !string.Equals( RemoteTrip.packageType, "Satchel", StringComparison.InvariantCultureIgnoreCase ) )
										                 Result.Trips.Add( new RemoteTrip( RemoteTrip, Ra, Ra ) );
								                 }

								                 Result.Status = Result.Trips.Count > 0 ? CONTAINER_STATUS.OK : CONTAINER_STATUS.EMPTY;
							                 }
						                 }
					                 }
				                 }
				                 catch( Exception Exception )
				                 {
					                 Console.WriteLine( Exception );
				                 }

				                 return Result;
			                 } );
		}

		public override Task<List<ITrip>> GetTripsAtLocation( AuthToken authToken, string locationBarcode )
		{
			var Result = new List<ITrip>();

			Task GetByStatus( STATUS status )
			{
				return Task.Run( async () =>
				                 {
					                 try
					                 {
						                 using var Client = NewClient;

						                 var Trips = Client.findTripsByLocationBarcode( new findTripsByLocationBarcode
						                                                                {
							                                                                authToken       = authToken.Token,
							                                                                isPickupBarcode = true,
							                                                                locationCode    = locationBarcode,
							                                                                statusFilter    = (int)status
						                                                                } );

						                 if( Trips is not null )
						                 {
							                 foreach( var RemoteTrip in Trips )
							                 {
								                 var (Trip, Ok) = await ConvertTrip( authToken.Token, RemoteTrip, false );

								                 if( Ok )
								                 {
									                 lock( Result )
										                 Result.Add( Trip );
								                 }
							                 }
						                 }
					                 }
					                 catch
					                 {
					                 }
				                 } );
			}

			return Task.Run( () =>
			                 {
				                 Task.WaitAll( GetByStatus( STATUS.ACTIVE ),
				                               GetByStatus( STATUS.DISPATCHED ) );

				                 return Result;
			                 } );
		}

		public override Task<bool> UpdateClaim( IClaims claims )
		{
			return Task.Run( () =>
			                 {
				                 try
				                 {
					                 var Specified = !string.IsNullOrEmpty( claims.Signature ) && ( claims.SignatureHeight > 0 ) && ( claims.SignatureWidth > 0 );

					                 var Message = new claimTrips
					                               {
						                               authToken                        = claims.DriverAuthToken,
						                               tripIds                          = claims.TripIds.ToArray(),
						                               optionalPOD                      = claims.Pod,
						                               optionalSignature                = claims.Signature,
						                               optionalSignatureHeight          = claims.SignatureHeight,
						                               optionalSignatureWidth           = claims.SignatureWidth,
						                               optionalSignatureHeightSpecified = Specified,
						                               optionalSignatureWidthSpecified  = Specified,
						                               pickupTime                       = claims.PickupTime.LocalTimeToServerTime(),
						                               pickupTimeSpecified              = true
					                               };

					                 using var Client = NewClient;

					                 Client.Timeout = 5 * 60 * 1000; // 5 minute timeout
					                 Client.claimTrips( Message );

					                 return true;
				                 }
				                 catch( Exception Exception )
				                 {
					                 Console.WriteLine( Exception );
				                 }

				                 return false;
			                 } );
		}

		public override Task<bool> RbhUpdateRcClaim( IClaims claims )
		{
			return Task.Run( () =>
			                 {
				                 try
				                 {
					                 var Specified = !string.IsNullOrEmpty( claims.Signature ) && ( claims.SignatureHeight > 0 ) && ( claims.SignatureWidth > 0 );

					                 var Message = new rbhReceiveBinsAtReturnsCage
					                               {
						                               authToken                = claims.DriverAuthToken,
						                               barcodesInReferenceField = claims.TripIds.ToArray(),
						                               signatureWidth           = claims.SignatureWidth,
						                               signatureHeight          = claims.SignatureHeight,
						                               signatureHeightSpecified = Specified,
						                               signatureWidthSpecified  = Specified,
						                               POP                      = claims.Pod,
						                               signature                = claims.Signature
					                               };

					                 using var Client = NewClient;

					                 Client.Timeout = 5 * 60 * 1000; // 5 minute timeout
					                 Client.rbhReceiveBinsAtReturnsCage( Message );

					                 return true;
				                 }
				                 catch( Exception Exception )
				                 {
					                 Console.WriteLine( Exception );
				                 }

				                 return false;
			                 } );
		}

		public override Task<bool> UpdateGps( AuthToken authToken, IList<GpsPoint> points )
		{
			return Task.Run( () =>
			                 {
				                 if( authToken is not null )
				                 {
					                 using var Client = new GpsClient();

					                 try
					                 {
						                 var Offset = TimeZoneInfo.Local.BaseUtcOffset;

						                 var RemoteGpsData = points.Select( gpsPoint => new remoteGpsData
						                                                                {
							                                                                time      = gpsPoint.Time.Add( -Offset ).UnixTicksMilliSeconds(),
							                                                                latitude  = gpsPoint.Latitude.ToString( CultureInfo.InvariantCulture ),
							                                                                longitude = gpsPoint.Longitude.ToString( CultureInfo.InvariantCulture )
						                                                                } ).ToArray();

						                 Client.updateDriverLocation( new updateDriverLocation { authToken = authToken.Token, driverId = authToken.DriverId, gpsData = RemoteGpsData } );

						                 return true;
					                 }
					                 catch( Exception E )
					                 {
						                 Console.WriteLine( E );
					                 }
				                 }

				                 return false;
			                 } );
		}

		private void PingServers()
		{
			PingTimer ??= new Timer( _ =>
			                         {
				                         if( !InPingTimer )
				                         {
					                         InPingTimer = true;

					                         try
					                         {
						                         lock( RemoteLockObject )
						                         {
							                         var Servers = Globals.EndPoints.Servers;

							                         foreach( var Server in Servers )
							                         {
								                         if( !AveragePings.TryGetValue( Server.Main, out var EndpointState ) )
								                         {
									                         EndpointState = new EndpointState { EndPoint = Server };
									                         AveragePings.Add( Server.Main, EndpointState );
								                         }
							                         }

							                         if( AveragePings.Count > 1 )
							                         {
								                         foreach( var AveragePing in AveragePings )
								                         {
									                         var Value = AveragePing.Value;

									                         try
									                         {
										                         using var Client = NewClient;
										                         Client.Timeout = 1000;
										                         Client.Url     = Value.EndPoint.Main;

										                         var Timer = new Stopwatch();
										                         Timer.Start();

										                         var RetVal = Client.ping( new ping() );
										                         Timer.Stop();

										                         if( RetVal.@return == "OK" )
										                         {
											                         Value.AveragePing = ( Value.AveragePing + (uint)Timer.Elapsed.Milliseconds ) / 2;

											                         continue;
										                         }
									                         }
									                         catch( Exception E )
									                         {
										                         Console.WriteLine( E );
										                         Value.AveragePing = uint.MaxValue;

										                         continue;
									                         }

									                         Value.AveragePing *= 2;
								                         }

								                         EndpointState Min = null;

								                         foreach( var (_, EndpointState) in AveragePings )
								                         {
									                         if( ( Min == null ) || ( EndpointState.AveragePing < Min.AveragePing ) )
										                         Min = EndpointState;
								                         }

								                         if( Min != null )
									                         Globals.EndPoints.CurrentServer = Min.EndPoint;
							                         }
							                         else
								                         Globals.EndPoints.CurrentServer = AveragePings.FirstOrDefault().Value.EndPoint;
						                         }
					                         }
					                         catch
					                         {
					                         }
					                         finally
					                         {
						                         InPingTimer = false;
					                         }
				                         }
			                         }, null, SERVER_PING_PERIOD_IN_SECONDS * 1000, SERVER_PING_PERIOD_IN_SECONDS * 1000 );
		}

		private async Task<(remoteAddress Address, bool Ok)> GetAddress( string addressId, string authToken )
		{
			return await Task.Run( () =>
			                       {
				                       var RetVal = ( Address: new remoteAddress(), Ok: true );

				                       lock( AddressCache )
				                       {
					                       if( AddressCache.TryGetValue( addressId, out var Address ) )
					                       {
						                       RetVal.Address = Address;

						                       return RetVal;
					                       }
				                       }

				                       try
				                       {
					                       using var C1 = NewClient;

					                       var Address = C1.findAddress( new findAddress
					                                                     {
						                                                     authToken = authToken,
						                                                     addressId = addressId
					                                                     } )?.@return ?? new remoteAddress();

					                       lock( AddressCache )
					                       {
						                       try
						                       {
							                       AddressCache.Add( addressId, Address );
						                       }
						                       catch // Probably just added by another thread
						                       {
						                       }
					                       }

					                       RetVal.Address = Address;
				                       }
				                       catch( Exception Exception )
				                       {
					                       Console.WriteLine( Exception );
					                       RetVal.Ok = false;
				                       }

				                       return RetVal;
			                       } );
		}

		private async Task<(RemoteTrip Trip, bool Ok )> ConvertTrip( string authToken, remoteTrip trip, bool ignoreAddress )
		{
			var Result = ( Trip: (RemoteTrip)null, Ok: true );

			if( !ignoreAddress )
			{
				var (PAddress, POk) =  await GetAddress( trip.pickupAddressId, authToken );
				Result.Ok           &= POk;

				var (DAddress, DOk) =  await GetAddress( trip.deliveryAddressId, authToken );
				Result.Ok           &= DOk;

				Result.Trip = new RemoteTrip( trip, PAddress, DAddress );
			}
			else
			{
				var Adr = new remoteAddress();
				Result.Trip = new RemoteTrip( trip, Adr, Adr );
			}

			return Result;
		}
	}
}