﻿namespace IdsService
{
	public static class Globals
	{
		public static class EndPoints
		{
			public class ServerPair
			{
				public string Main, Peek;
			}

			internal class ClientServers
			{
				public string ResellerId;

				// ReSharper disable once MemberHidesStaticFromOuterClass
				public ServerPair[] Servers;
			}

			public const string
				JBTEST = "http://jbtest.internetdispatcher.org:8080/ids-beans/IDSWS",
				JBTEST2 = "http://jbtest2.internetdispatcher.org:8080/ids-beans/IDSWS", // 198.50.187.106:8080
				TEST = "http://test.internetdispatcher.org:8080/ids-beans/IDSWS",
				APP = "http://app.internetdispatcher.org:8080/ids-beans/IDSWS",
				USERS = "http://users.internetdispatcher.org:8080/ids-beans/IDSWS",
				REPORTS = "http://reports.internetdispatcher.org:8080/ids-beans/IDSWS",
				DRIVERS = "http://drivers.internetdispatcher.org:8080/ids-beans/IDSWS",
				IDS = "http://ids.internetdispatcher.org:8080/ids-beans/IDSWS",
				OZ = "http://oz.internetdispatcher.org:8080/ids-beans/IDSWS",
				OZ_MIRROR = "http://ozmirror.internetdispatcher.org:8080/ids-beans/IDSWS",
				CUSTOM = "http://custom.internetdispatcher.org:8080/ids-beans/IDSWS",

				//
				PEEK_JBTEST2 = "http://jbtest2.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
				PEEK_TEST = "http://test.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
				PEEK_APP = "http://app.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
				PEEK_USERS = "http://users.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
				PEEK_REPORTS = "http://reports.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
				PEEK_DRIVERS = "http://drivers.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
				PEEK_IDS = "http://ids.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
				PEEK_OZ = "http://oz.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
				PEEK_OZ_MIRROR = "http://ozmirror.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
				PEEK_CUSTOM = "http://custom.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS";

			public static bool UseTestingServer;

			public static readonly ServerPair TestServerPair = new() { Main = APP, Peek = PEEK_APP };

			public static readonly ServerPair[] StandardServers =
			{
				new() { Main = DRIVERS, Peek = PEEK_DRIVERS },
				new() { Main = APP, Peek     = PEEK_APP }
			};

			public static readonly ServerPair[] DebugServers =
			{
				new() { Main = JBTEST2, Peek = PEEK_APP }
//				new ServerPair { Main = OZ, Peek = PEEK_OZ }
			};


			public static readonly ServerPair[] UsersServer =
			{
				new() { Main = USERS, Peek = PEEK_USERS }
			};

			public static readonly ServerPair[] CustomServer =
			{
				new() { Main = CUSTOM, Peek = PEEK_CUSTOM }
			};

            public static readonly ServerPair[] PmlOzServers =
			{
				new() { Main = OZ, Peek = PEEK_OZ }
//				new ServerPair { Main = OZ_MIRROR, Peek = PEEK_OZ_MIRROR }
			};

/*
			public static readonly ServerPair[] PriorityCustomServers =
			{
				new ServerPair { Main = CUSTOM, Peek = PEEK_CUSTOM }
//				new ServerPair { Main = OZ_MIRROR, Peek = PEEK_OZ_MIRROR }
			};
*/
			public static ServerPair[] Servers = StandardServers;

			private static readonly ClientServers[] ClientServersArray =
			{
				new() { ResellerId = "PMLTEST", Servers  = PmlOzServers },
				new() { ResellerId = "PML", Servers      = PmlOzServers },
                new() { ResellerId = "PRIORITY", Servers = StandardServers },
                new() { ResellerId = "LINFOX", Servers   = PmlOzServers }
            };

			private static string _Reseller;

			private static readonly object LockObject = new();

			private static ServerPair _CurrentServer = Servers[ 0 ];

			internal static string Reseller
			{
				get => _Reseller;
				set
				{
					value = value.ToUpper();
					_Reseller = value;

					IdsService.AveragePings.Clear();

					if( UseTestingServer )
					{
						Servers = DebugServers;
						return;
					}

					foreach( var ClientServer in ClientServersArray )
					{
						if( ClientServer.ResellerId == value )
						{
							Servers = ClientServer.Servers;
							CurrentServer = ClientServer.Servers[ 0 ];
							return;
						}
					}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					Servers = StandardServers;
				}
			}

			public static ServerPair CurrentServer
			{
				get
				{
					lock( LockObject )
						return _CurrentServer;
				}

				set
				{
					lock( LockObject )
						_CurrentServer = value;
				}
			}
		}

		public const int DEFAULT_TIMEOUT_IN_SECONDS = 60;

		public static int OperationTimeoutInSeconds = DEFAULT_TIMEOUT_IN_SECONDS;
	}
}