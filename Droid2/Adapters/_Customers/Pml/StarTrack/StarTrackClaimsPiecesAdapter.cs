﻿using Android.App;
using Android.Widget;
using Droid2.Fragments._Customers.Pml.StarTrack;

namespace Droid2.Adapters._Customers.Pml.StarTrack
{
	public class StarTrackClaimsPiecesAdapter : PmlClaimsPiecesAdapter
	{
		protected override bool ValidateType( PmlScannedTrip trip )
		{
			return StarTrackPickups.IsStarTrack( trip );
		}

		protected override void AfterNewUpliftedTrip( PmlScannedTrip trip )
		{
			trip.Balance = 9999;
		}

		public StarTrackClaimsPiecesAdapter( Activity context, ListView owner ) : base( context, owner )
		{
		}
	}
}