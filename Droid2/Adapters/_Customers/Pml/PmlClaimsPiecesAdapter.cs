﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Views;
using Android.Widget;
using Droid2.Adapters.Customers.Metro;
using Droid2.Fragments.Customers.Metro;
using Droid2.Fragments.Customers.Pml;
using Service.Interfaces;
using Math = Java.Lang.Math;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters._Customers.Pml
{
	public class PmlScannedTrip : MetroScannedTrip
	{
		public static  decimal CurrentScanCount;
		private static decimal _UpperScanLimit;

		public string AurtNumber => BillingNote.Trim();

		internal override bool ShowErrorBackground => CannotPickup || base.ShowErrorBackground;

		public override string TripIdAsText => Reference;

		public override bool CannotPickup { get; set; }

		public override bool AllowUndeliverable => MetroDsdDeliveries.IsReturnTrip( this );

		public static decimal UpperScanLimit
		{
			get => _UpperScanLimit;
			set => _UpperScanLimit = (decimal)Math.Min( (double)_UpperScanLimit, (double)value );
		}

		public override string DeliveryZone
		{
			get => ServiceLevel;
			set => ServiceLevel = value;
		}

		internal override bool ScanCompleted
		{
			get
			{
				var Sv = ServiceLevel.ToUpper();

				if( ( CurrentScanCount < UpperScanLimit ) || ( Sv == MetroDsdDeliveries.RETURN ) )
				{
					switch( Sv )
					{
					case MetroDsdDeliveries.RETURN:
						if( MetroDsdDeliveries.DoingReturns )
							return !MetroDsdDeliveries.IsReturnTrip( this ) || ( Balance == 0 ) || CannotPickup;

						return Balance == 0;

					case PmlDsdDeliveries.UPLIFT:
					case PmlDsdDeliveries.UPPACK:
					case PmlDsdDeliveries.UPPC:
						return Scanned > 0;

					case PmlDsdDeliveries.UPTOTAL:
						return false;

					default:
						return PmlDsdDeliveries.IsUpliftedTrip( this ) || ( Balance == 0 );
					}
				}

				return true;
			}
		}

		public Dictionary<string, int> ProductTypes = new();

		public static void ResetLimits()
		{
			_UpperScanLimit  = int.MaxValue;
			CurrentScanCount = 0;
		}

		internal override ScannedTrip Clone( Trip trip ) => new PmlScannedTrip( trip );

		public PmlScannedTrip()
		{
		}

		public PmlScannedTrip( Trip trip ) : base( trip )
		{
		}

		public PmlScannedTrip( PmlScannedTrip trip ) : base( trip )
		{
			ProductTypes = trip.ProductTypes;

			// ReSharper disable once VirtualMemberCallInConstructor
			CannotPickup       = trip.CannotPickup;
			CannotPickupReason = trip.CannotPickupReason;
			Scanned            = trip.Scanned;
			Balance            = trip.Balance;
		}
	}


	public class PmlUndeliverable : MetroUndeliverable
	{
		public override string UndeliverableTripsAsText => TripsNotPickedUpAsText;
		public override string TripsNotPickedUpAsText   => $"Totes Not Picked Up: {TripsNotPickedUp}";
	}


	public class PmlClaimsPiecesAdapter : TMetroClaimsPiecesAdapter<PmlScannedTrip>
	{
		private static readonly (string Barcode, string Description)[] Barcodes =
		{
			( Barcode: "9310704914932", Description: "RYO Craftsman Sailors Rum 25g 5Pk (CT)" ),
			( Barcode: "9310704911306", Description: "RYO Craftsman Sailors Rum 25g 5Pk (PK)" ),
			( Barcode: "9310704914949", Description: "RYO Craftsman Brunswick Blend 25g 5Pk (CT)" ),
			( Barcode: "9310704911320", Description: "RYO Craftsman Brunswick Blend 25g 5Pk (PK)" ),
			( Barcode: "9310704914956", Description: "RYO Craftsman Sailors Rum 15g 5Pk (CT)" ),
			( Barcode: "9310704912464", Description: "RYO Craftsman Sailors Rum 15g 5Pk (PK)" ),
			( Barcode: "9310704914963", Description: "RYO Craftsman Brunswick Blend 15g 5Pk (CT)" ),
			( Barcode: "9310704912488", Description: "RYO Craftsman Brunswick Blend 15g 5Pk (PK)" ),
			( Barcode: "9310704914970", Description: "RYO Choice Original Blue 25g 5Pk (CT)" ),
			( Barcode: "9310704907354", Description: "RYO Choice Original Blue 25g 5Pk (PK)" ),
			( Barcode: "9310704914987", Description: "RYO Choice Rich Gold 25g 5Pk (CT)" ),
			( Barcode: "9310704909174", Description: "RYO Choice Rich Gold 25g 5Pk (PK)" ),
			( Barcode: "9310704914994", Description: "RYO Choice Full Red 25g 5Pk (CT)" ),
			( Barcode: "9310704909198", Description: "RYO Choice Full Red 25g 5Pk (PK)" ),
			( Barcode: "9310704915021", Description: "RYO Craftsman Expresso 15g 5Pk (CT)" ),
			( Barcode: "9310704913102", Description: "RYO Craftsman Expresso 15g 5Pk (PK)" ),
			( Barcode: "9310704915038", Description: "RYO Craftsman Expresso 25g 5Pk (CT)" ),
			( Barcode: "9310704912341", Description: "RYO Craftsman Expresso 25g 5Pk (PK)" ),
			( Barcode: "9310704915076", Description: "RYO Longbeach Rich ZL 25g 5Pk (CT)" ),
			( Barcode: "9310704912266", Description: "RYO Longbeach Rich ZL 25g 5Pk (PK)" ),
			( Barcode: "9310704915083", Description: "RYO LB Original Yellow ZL 25g 5Pk (CT)" ),
			( Barcode: "9310704912280", Description: "RYO LB Original Yellow ZL 25g 5Pk (PK)" ),
			( Barcode: "9310704915090", Description: "RYO Peter Jackson Blue ZL 15g 5Pk (CT)" ),
			( Barcode: "9310704913041", Description: "RYO Peter Jackson Blue ZL 15g 5Pk (PK)" ),
			( Barcode: "9310704915106", Description: "RYO Peter Jackson Blue ZL 25g 5PK (CT)" ),
			( Barcode: "9310704913409", Description: "RYO Peter Jackson Blue ZL 25g 5PK (PK)" ),
			( Barcode: "9310704913812", Description: "Bond Street P21 Red 15g 5PK (CT)" ),
			( Barcode: "9310704913805", Description: "Bond Street P21 Red 15g 5PK (PK)" ),
			( Barcode: "9310704913836", Description: "Bond Street P21 Blue 15g 5Pk (CT)" ),
			( Barcode: "9310704913829", Description: "Bond Street P21 Blue 15g 5Pk (PK)" ),
			( Barcode: "9310704914123", Description: "Bond Street P21 Gold 15g 5Pk (CT)" ),
			( Barcode: "9310704914116", Description: "Bond Street P21 Gold 15g 5Pk (PK)" ),
			( Barcode: "9310704915328", Description: "RYO Longbeach Orig Yellow 45g (CT)" ),
			( Barcode: "9310704915311", Description: "RYO Longbeach Orig Yellow 45g (PK)" ),
			( Barcode: "9310704915342", Description: "RYO Longbeach Rich 45g (CT)" ),
			( Barcode: "9310704915335", Description: "RYO Longbeach Rich 45g (PK)" ),
			( Barcode: "9310704908818", Description: "RYO Longbeach Orig Yellow 55g (CT)" ),
			( Barcode: "9310704908801", Description: "RYO Longbeach Orig Yellow 55g (PK)" ),
			( Barcode: "9310704909952", Description: "RYO Longbeach Rich 55g (CT)" ),
			( Barcode: "9310704909846", Description: "RYO Longbeach Rich 55g (PK)" )
		};

		public override bool HasScannedTrips => true;

		public bool HideAllButReturns
		{
			get => _HideAllButReturns;
			set
			{
				_HideAllButReturns = value;

				foreach( var Trip in Trips )
				{
					var Hidden = value && !MetroDsdDeliveries.IsReturnTrip( Trip );
					Trip.Hidden = Hidden;
				}

				NotifyDataSetChanged();
			}
		}

		protected override int  LayoutId => Resource.Layout.Pml_ListViewRow_ClaimPieces;
		private            bool _HideAllButReturns;

		protected (string Barcode, string Description, bool Found) SearchBarcodes( (string Barcode, string Description)[] barcodesArray, string barcode )
		{
			barcode = barcode.Trim();

			return ( from B in barcodesArray
			         where B.Barcode == barcode
			         select ( B.Barcode, B.Description, Found: true ) ).FirstOrDefault();
		}

		public virtual (string Barcode, string Description, bool Found ) GetBarcodeDetails( string barcode ) => SearchBarcodes( Barcodes, barcode );

		public override BaseTripAdapter Add( List<Trip> trips )
		{
			var RetVal = base.Add( trips );
			ShowHidden        = false;
			HideAllButReturns = true;

			return RetVal;
		}

		protected override bool IsScanCompleted( List<Trip> trips )
		{
			var Trps = ( from T in trips
			             let Trp = T as PmlScannedTrip
			             where Trp is not null
			             select Trp ).ToList();

/*
			foreach( var PmlScannedTrip in Trps )
			{
				if( PmlDsdDeliveries.IsSatchel( PmlScannedTrip ) && ( PmlScannedTrip.Balance > 0 ) )
					return false;
			}
*/
			var Completed = true;

			foreach( var PmlScannedTrip in Trps )
			{
				if( !PmlScannedTrip.CannotPickup && !PmlDsdDeliveries.IsUpTotal( PmlScannedTrip ) )
				{
					Completed = false;

					break;
				}
			}

			if( Completed )
				return true;

			foreach( var Trip in Trps )
			{
				if( MetroDsdDeliveries.IsReturnTrip( Trip ) && !Trip.ScanCompleted )
					return false;
			}

			foreach( var Trip in Trps )
			{
				if( Trip.ScanCompleted )
					return true;
			}

			return false;
		}

		protected virtual bool ValidateType( PmlScannedTrip trip ) => PmlDsdDeliveries.IsUpLift( trip );


		protected virtual void AfterNewUpliftedTrip( PmlScannedTrip trip )
		{
		}


		public override decimal UpdateCount( string scannedId, Trip trip, int count = 1 )
		{
			if( PmlDsdDeliveries.IsUpTotal( trip ) )
				return -1;

			var Trps = ( from T in Trips
			             let Trp = (PmlScannedTrip)T
			             where Trp.Balance > 0
			             select Trp ).ToList();

			decimal RetVal = -1;

			decimal UpdateTrip( PmlScannedTrip t )
			{
				RetVal = base.UpdateCount( t.TripId, t, count );

				if( RetVal >= 0 )
				{
					var PTypes = t.ProductTypes;

					if( PTypes.TryGetValue( scannedId, out var ItemCount ) )
						PTypes[ scannedId ] = ItemCount + count;

					else
						PTypes.Add( scannedId, count );
				}

				return RetVal;
			}

			while( true )
			{
				try
				{
					var Found = false;

					//Satchel delivery
					foreach( var Trip in Trps )
					{
						if( ( Trip.Reference == scannedId ) && PmlDsdDeliveries.IsSatchel( Trip ) && !Trip.ScanCompleted )
						{
							if( UpdateTrip( Trip ) >= 0 )
							{
								Found = true;

								break;
							}
						}
					}

					if( Found )
						break;

					//Check all returns before pieces and not affected by uptotal
					foreach( var Trip in Trps )
					{
						if( ( Trip.Reference == scannedId ) && MetroDsdDeliveries.IsReturnTrip( Trip ) && !Trip.ScanCompleted )
						{
							if( UpdateTrip( Trip ) >= 0 )
							{
								Found = true;

								break;
							}
						}
					}

					if( Found )
						break;

					foreach( var Trip in Trps )
					{
						if( ( Trip.Reference == scannedId ) && PmlDsdDeliveries.IsUpPack( Trip ) )
						{
							if( UpdateTrip( Trip ) >= 0 )
							{
								Found = true;

								break;
							}
						}
					}

					if( Found )
						break;

					if( PmlScannedTrip.CurrentScanCount >= PmlScannedTrip.UpperScanLimit )
						return -1;

					//Check all pieces before uplifts
					foreach( var Trip in Trps )
					{
						if( ( Trip.Reference == scannedId ) && PmlDsdDeliveries.IsUppc( Trip ) )
						{
							if( UpdateTrip( Trip ) >= 0 )
							{
								PmlScannedTrip.CurrentScanCount += count;
								Found                           =  true;

								break;
							}
						}
					}

					if( Found )
						break;

					var Rand = new Random();

					var CheckTrips = new List<Trip>( Trps );

					var (_, Description, BarcodeFound) = GetBarcodeDetails( scannedId );

					foreach( var Trip in Trps )
					{
						if( ValidateType( Trip ) && BarcodeFound )
						{
							var Len = scannedId.Length;

							var Prefix = Len switch
							             {
								             8  => scannedId[ ..3 ],
								             13 => scannedId[ ..7 ],
								             _  => ""
							             };

							switch( Prefix )
							{
						#if DEBUG
							case "":
						#endif
							case "9310704":
							case "8710632":
							case "932":
							case "933":
							case "934":
								PmlScannedTrip? UTrip = null;

								foreach( var CheckTrip in CheckTrips )
								{
									var St = (PmlScannedTrip)CheckTrip;

									if( PmlDsdDeliveries.IsUpliftedTrip( St ) && ( St.Reference == scannedId ) )
									{
										UTrip = St;

										break;
									}
								}

								var DoAdd = UTrip is null;

								if( DoAdd )
								{
									var Now = DateTime.UtcNow;

									UTrip = new PmlScannedTrip( Trip )
									        {
										        TripId       = $"UP{Now.Year:D4}{Now.Month:D2}{Now.Day:D2}{Now.Hour:D2}{Now.Minute:D2}{Now.Second:D2}{Rand.Next( 99999 ):D4}",
										        ServiceLevel = PmlDsdDeliveries.UPLIFTED,
										        Status       = STATUS.CREATE_NEW_TRIP_AS_PICKED_UP,
										        PackageType  = Description,
										        Reference    = scannedId,
										        Balance      = Trip.Pieces,
										        Hidden       = true
									        };

									AfterNewUpliftedTrip( UTrip );
								}

								if( UTrip != null )
								{
									RetVal = base.UpdateCount( UTrip.TripId, UTrip );

									if( DoAdd )
									{
										Trips.Add( UTrip );
										DoScanCompleted();
									}
								}

								break;
							}
						}
					}

					break;
				}
				finally
				{
					if( RetVal < 0 )
					{
						Globals.Dialogues.Error( "Product Not Authorized" );
						RetVal = 0; // Stop beep
					}
				}
			}

			return RetVal;
		}

		protected override MetroUndeliverable NewTripData() => new PmlUndeliverable { TotalTrips = Trips.Count, TripIds = "", UndeliverableTripIds = "" };

		protected override void OnGetView( View view, Trip trip )
		{
			base.OnGetView( view, trip );
			var Tv = view.FindViewById<TextView>( Resource.Id.packageType );

			if( Tv is not null )
				Tv.Text = trip.PackageType;
		}

		public PmlClaimsPiecesAdapter( Activity context, ListView owner ) : base( context, owner )
		{
			AutoShow = false;
		}
	}
}