﻿using System.Collections.Generic;
using Android.App;
using Android.Widget;
using CommsDb.Database;
using IdsService;
using IdsService.org.internetdispatcher.jbtest2;

namespace Droid2.Adapters._Customers.Pml
{
	internal class PmlAuditAdapter : ClaimsPiecesAdapter
	{
		public PmlAuditAdapter( Activity context, ListView owner, bool showUndeliverable = false ) : base( context, owner, showUndeliverable )
		{
		}

		protected override int LayoutId => Resource.Layout.Pml_ListViewRow_AuditPieces;

		public override BaseTripAdapter Add( List<Db<Client, AuthToken, remoteTrip>.Trip> trips )
		{
			var Temp = new List<Db<Client, AuthToken, remoteTrip>.Trip>();
			foreach( var Trip in trips )
				Temp.Add( new PmlAuditScannedTrip( Trip ) );

			return InternalAdd( Temp );
		}
	}

	public class PmlAuditScannedTrip : ScannedTrip
	{
		public PmlAuditScannedTrip( Db<Client, AuthToken, remoteTrip>.Trip t ) : base( t )
		{
		}

		internal override ScannedTrip Clone( Db<Client, AuthToken, remoteTrip>.Trip trip )
		{
			return new PmlScannedTrip( trip );
		}

		public override string TripIdAsText => Reference;
	}
}