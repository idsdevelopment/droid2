using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using Droid2.Fragments.Customers.Metro;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters.Customers.Metro
{
	public class MetroScannedTrip : ScannedTrip
	{
		public bool BrokenSeal;

		public MetroScannedTrip()
		{
		}

		public MetroScannedTrip( Trip trip ) : base( trip )
		{
		}

		internal override string AsText
		{
			get
			{
				var RetVal = BrokenSeal ? "Broken Seal" : "";
				var Temp = base.AsText ?? "";

				if( !string.IsNullOrWhiteSpace( Temp ) )
					RetVal += "\r\n";
				return RetVal + Temp;
			}
		}

		internal override bool ShowErrorBackground => BrokenSeal || base.ShowErrorBackground || CannotPickup;

		internal override bool ScanCompleted
		{
			get
			{
				if( MetroDsdDeliveries.DoingReturns )
					return !MetroDsdDeliveries.IsReturnTrip( this ) || ( Balance == 0 );
				return Balance == 0;
			}
		}

		internal override ScannedTrip Clone( Trip trip )
		{
			return new MetroScannedTrip( trip );
		}
	}

	public class MetroUndeliverable : BaseTripAdapter.TripCountData
	{
		public override string TripsAsText => $"Totes: {TotalTrips}";
		public override string UndeliverableTripsAsText => $"Undeliverable Totes: {UndeliverableTrips}";
		public override string UndeliverablePiecesAsText => "";
	}

	public class MetroClaimsPiecesAdapter : TMetroClaimsPiecesAdapter<MetroScannedTrip>
	{
		public MetroClaimsPiecesAdapter( Activity context, ListView owner ) : base( context, owner )
		{
		}
	}


	// ReSharper disable once InconsistentNaming
	public class TMetroClaimsPiecesAdapter<TSt> : ABaseClaimsPiecesAdapter<TSt, MetroUndeliverable> where TSt : MetroScannedTrip, new()
	{
		private readonly Drawable ReturnsBackground, NonReturnsBackground, DeletedBackground;
/*
		public Action<ScannedTrip, MetroOptionsActivity.SCAN_ACTION> Callback;
*/

		public TMetroClaimsPiecesAdapter( Activity context, ListView owner ) : base( context, owner )
		{
			ReturnsBackground = ContextCompat.GetDrawable( context, Resource.Drawable.ReturnsIcon );
			NonReturnsBackground = ContextCompat.GetDrawable( context, Resource.Drawable.ClaimsIcon );
			DeletedBackground = ContextCompat.GetDrawable( context, Resource.Drawable.DeletedIcon );

			OnRowClick = async ( view, o, arg3 ) =>
			             {
				             if( !ShowHidden ) // Delivery Accept, Kill options
				             {
					             MetroOptionsActivity.ScanAction = MetroOptionsActivity.SCAN_ACTION.BUSY;

					             MetroOptionsActivity.SelectedTrip = Trips[ (int)view.Tag ] as MetroScannedTrip;

					             var Intent = new Intent( Context, typeof( MetroOptionsActivity ) );
					             Context.StartActivity( Intent );

					             await Task.Run( () =>
					                             {
						                             while( MetroOptionsActivity.ScanAction == MetroOptionsActivity.SCAN_ACTION.BUSY )
							                             Thread.Sleep( 100 );
					                             } );

					             var Trp = MetroOptionsActivity.SelectedTrip;
					             if( Trp != null )
					             {
						             switch( MetroOptionsActivity.ScanAction )
						             {
						             case MetroOptionsActivity.SCAN_ACTION.BROKEN_SEAL:
							             Trp.BrokenSeal = true;
							             NotifyDataSetChanged();
							             break;

						             case MetroOptionsActivity.SCAN_ACTION.RESET_SCAN:
							             Trp.Balance = Trp.Pieces;
							             Trp.Scanned = 0;
							             Trp.Hidden = false;
							             Trp.Undeliverable = false;
							             Trp.CannotPickup = false;
							             Trp.NotForThisLocation = false;
							             Trp.BrokenSeal = false;
							             NotifyDataSetChanged();
							             break;
						             }
					             }
				             }
			             };
		}

		protected override string GetCompanyName( Trip trip )
		{
			return trip is ScannedTrip { IsReturn: true } ? trip.PickupCompanyName : trip.DeliveryCompanyName;
		}

		protected override void OnGetView( View view, Trip trip )
		{
			if( trip is ScannedTrip STrip )
			{
				var Indicator = view.FindViewById<FrameLayout>( Resource.Id.indicator );
				Indicator.Background = STrip.IsReturn ? ( STrip.CannotPickup ? DeletedBackground : ReturnsBackground ) : NonReturnsBackground;
			}
		}
	}
}