#nullable enable

using System;
using System.Collections.Generic;
using Android.App;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Droid2.Fragments.Customers.Metro;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters.Customers.Metro
{
	public class MetroTripListAdapter : PickupDeliveriesAdapter
	{
		public MetroTripListAdapter( int layout, Activity context, ListView owner, List<Trip> trips, Action<Trip, bool> onTripClick )
			: base( layout, context, owner, trips, onTripClick )
		{
		}

		public MetroTripListAdapter( Activity context, ListView owner, List<Trip> trips, Action<Trip, bool> onTripClick ) : base( context, owner, trips, onTripClick )
		{
		}

		private readonly Color Blue  = Color.DeepSkyBlue,
		                       White = Color.White;

		public override View GetView( int position, View convertView, ViewGroup parent )
		{
			var View = base.GetView( position, convertView, parent );
			View.FindViewById<TextView>( Resource.Id.tripViewCompanyName )?.SetTextColor( MetroDsdDeliveries.IsCWDTrip( CurrentTrip ) ? Blue : White );
			return View;
		}
	}
}