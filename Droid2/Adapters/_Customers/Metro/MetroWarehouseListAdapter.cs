using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Views;
using Android.Widget;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters.Customers.Metro
{
    public class ResidualTrip
    {
        public Trip Trip;
        public decimal Residual;
        public string BadScanReason = "";

        internal ResidualTrip()
        {
        }

        public ResidualTrip( Trip trip )
        {
            Trip = trip;
            Residual = trip.Pieces;
        }

        public static explicit operator Trip( ResidualTrip trip )
        {
            return trip.Trip;
        }
    }

    public class ResidualList : List<ResidualTrip>
    {
        public ResidualList()
        {
        }

        public ResidualList( IEnumerable<Trip> trips )
        {
            foreach( var Trip in trips )
                Add( new ResidualTrip( Trip ) );
        }

        public bool RemoveDeleted( List<Trip> trips )
        {
            var Changed = false;
            for( var Ndx = Count - 1; Ndx >= 0; Ndx-- )
            {
                var ResidualTrip = this[ Ndx ];

                var Found = trips.Any( trip => trip.TripId == ResidualTrip.Trip.TripId );

                if( !Found )
                {
                    Changed = true;
                    RemoveAt( Ndx );
                }
            }
            return Changed;
        }
    }

    public class MetroWarehouseListAdapter : BaseAdapter<ResidualTrip>
    {
        private readonly Activity Context;
        public int Background;

        public ResidualList Trips { get; private set; }

        public override long GetItemId( int position )
        {
            return position;
        }

        public Action<ResidualTrip, int> OnClick;

        public override View GetView( int position, View convertView, ViewGroup parent )
        {
            if( convertView == null ) // re-use an existing view, if one is available
            {
                convertView = Context.LayoutInflater.Inflate( Resource.Layout.Metro_ListViewRow_WarehouseScan, null );
                convertView.SetBackgroundResource( Background );
                convertView.Click += ( sender, args ) =>
                {
                    var Position = (int)convertView.Tag;
                    OnClick?.Invoke( Trips[ Position ], Position );
                };
            }

            convertView.Tag = position;

            var ResidualTrip = Trips[ position ];
            var T = (Trip)ResidualTrip;

            convertView.FindViewById<TextView>( Resource.Id.tripId ).Text = T.TripId;
            convertView.FindViewById<TextView>( Resource.Id.residualPieces ).Text = ResidualTrip.Residual.ToString( "####" );
            convertView.FindViewById<TextView>( Resource.Id.pieces ).Text = T.Pieces.ToString( "###0" );
            convertView.FindViewById<TextView>( Resource.Id.weight ).Text = T.Weight.ToString( "###,###.##" );

            convertView.FindViewById<TextView>( Resource.Id.companyName ).Text = T.DeliveryCompanyName;
            convertView.FindViewById<TextView>( Resource.Id.companyAddress ).Text = T.DeliveryAddressLine1;

            convertView.FindViewById<TextView>( Resource.Id.badScan ).Text = ResidualTrip.BadScanReason;

            return convertView;
        }

        public override int Count => Trips.Count;


        public override ResidualTrip this[ int position ] => Trips[ position ];

        public void UpdateTrips( List<Trip> trips )
        {
            Trips = new ResidualList( trips );
            Context.RunOnUiThread( NotifyDataSetChanged );
        }

        public void UpdateTrips( ResidualList trips )
        {
            Trips = trips;
            Context.RunOnUiThread( NotifyDataSetChanged );
        }

        public void AddRange( IEnumerable<Trip> trips )
        {
            Trips.AddRange( new ResidualList( trips ) );
            Context.RunOnUiThread( NotifyDataSetChanged );
        }

        public void Add( string tripId, string reason )
        {
            Trips.Add( new ResidualTrip { Residual = -1, BadScanReason = reason, Trip = new Trip { TripId = tripId } } );
            Context.RunOnUiThread( NotifyDataSetChanged );
        }

        public MetroWarehouseListAdapter( Activity context, IEnumerable<Trip> trips )
        {
            Context = context;
            Background = Resource.Color.Black;
            Trips = new ResidualList( trips );
        }
    }
}