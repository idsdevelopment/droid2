using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;

namespace Droid2.Adapters.Customers.Metro
{
	public class MissingReference
	{
		public string CompanyName,
		              TripId,
		              MercuryId,
		              Barcode;
	}

	public class ReferenceList : List<MissingReference>
	{
	}

	public class MetroMissingListAdapter : BaseAdapter<MissingReference>
	{
		private ReferenceList References = new();

		public override int Count => References.Count;
		private readonly Activity Context;
		public int Background;

		public Action<MissingReference, int> OnClick;

		public override long GetItemId( int position )
		{
			return position;
		}

		public override View GetView( int position, View convertView, ViewGroup parent )
		{
			if( convertView == null ) // re-use an existing view, if one is available
			{
				convertView = Context.LayoutInflater.Inflate( Resource.Layout.Metro_ListViewRow_Missing, null );
				convertView.SetBackgroundResource( Background );

				convertView.Click += ( sender, args ) =>
				                     {
					                     var Position = (int)convertView.Tag;
					                     OnClick?.Invoke( References[ Position ], Position );
				                     };
			}

			convertView.Tag = position;

			var MissingReference = References[ position ];
			var M                = MissingReference;

			convertView.FindViewById<TextView>( Resource.Id.barcode ).Text     = M.Barcode;
			convertView.FindViewById<TextView>( Resource.Id.tripId ).Text      = M.TripId;
			convertView.FindViewById<TextView>( Resource.Id.companyName ).Text = M.CompanyName;
			convertView.FindViewById<TextView>( Resource.Id.mercuryId ).Text   = M.MercuryId;

			return convertView;
		}

		public void Clear()
		{
			References.Clear();
		}

		public void UpdateTrips( List<MissingReference> missing )
		{
			( References = new ReferenceList() ).AddRange( missing );
			Context.RunOnUiThread( NotifyDataSetChanged );
		}

		public void UpdateTrips( ReferenceList missing )
		{
			References = missing;
			Context.RunOnUiThread( NotifyDataSetChanged );
		}

		public void AddRange( IEnumerable<MissingReference> missing )
		{
			References.AddRange( missing );
			Context.RunOnUiThread( NotifyDataSetChanged );
		}


		public override MissingReference this[ int position ] => References[ position ];

		public MetroMissingListAdapter( Activity context )
		{
			Context    = context;
			Background = Resource.Color.Black;
		}
	}
}