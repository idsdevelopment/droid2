using System.Collections.Generic;
using Android.App;
using Droid2.Fragments.Standard.PickupDeliveries;

namespace Droid2.Adapters._Customers.Priority
{
    internal class PriorityLocationSelectionAdapter : RadioButtonListAdapter
    {
        public PriorityLocationSelectionAdapter( Activity context, IEnumerable<DriverPickup.LocationCompany> items ) : base( context )
        {
            BeginUpdate();
            foreach( var LocationCompany in items )
                Add( LocationCompany.LocationBarcode );
            EndUpdate();
        }
    }
}