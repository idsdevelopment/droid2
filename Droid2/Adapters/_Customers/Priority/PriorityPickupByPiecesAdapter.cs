using Android.App;

namespace Droid2.Adapters._Customers.Priority
{
    internal class PriorityPickupByPiecesAdapter : StringAdapter
    {
        public PriorityPickupByPiecesAdapter( Activity context ) : base( context )
        {
        }

        public bool Add( string location, string barcode )
        {
//	        var Code = $"({location.Trim()}){barcode}";
	        var Code = $" {barcode}";
            var RetVal = !Contains( Code );
            if( RetVal )
                Add( Code );
            return RetVal;
        }
    }
}