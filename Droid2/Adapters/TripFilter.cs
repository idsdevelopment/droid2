using System.Collections.Generic;
using Service.Interfaces;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters
{
    public static class TripFilter
    {

	public static List<Trip> FilterTrips( this List<Trip> trips, Globals.Filters.VISIBILITY vis )
        {
            return FilterTripList( trips, vis );
        }

        public static List<Trip> FilterTripList( List<Trip> trips, Globals.Filters.VISIBILITY visibility )
        {
            var TripList = new List<Trip>();

            foreach( var T in trips )
            {
                var Status = T.Status;

                if( Status < STATUS.VERIFIED )
                {
                    switch( visibility )
                    {
                    case Globals.Filters.VISIBILITY.DELIVERY:
                        switch( Status )
                        {
                        case STATUS.PICKED_UP:
                            TripList.Add( T );
                            break;
                        }
                        break;

                    case Globals.Filters.VISIBILITY.NEW when !T.ReadByDriver:
                        TripList.Add( T );
                        break;

                    case Globals.Filters.VISIBILITY.PICKUP:
                        switch( Status )
                        {
                        case STATUS.ACTIVE:
                        case STATUS.DISPATCHED:
                            TripList.Add( T );
                            break;
                        }
                        break;

                    case Globals.Filters.VISIBILITY.ALL:
                        TripList.Add( T );
                        break;
                    }
                }
            }
            return TripList;
        }
    }
}