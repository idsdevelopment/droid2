#nullable enable

using System;
using System.Collections.Generic;
using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using Service.Interfaces;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters
{
	public class PickupDeliveriesAdapter : BaseTripAdapter
	{
		public override  bool       HasScannedTrips => false;
		private readonly int        Layout;
		private          List<Trip> SavedTrips = null!;

		protected readonly Drawable Yellow      = new ColorDrawable( Color.Yellow ),
		                            VeryDimGray = Globals.Color( Resource.Color.VeryDimGray );

		public Action<Trip, bool> OnTripClick;
		public Action<Trip>       OnTripLongClick = null!;

		public virtual void UpdateTrips( List<Trip> trips )
		{
			ReplaceTrips( trips );
			NotifyDataSetChanged();
		}

		public override decimal UpdateCount( string scannedId, Trip trip, int count = 1 ) => 0;

		public override BaseTripAdapter MarkUndeliverables() => this;

		public override BaseTripAdapter UnMarkUndeliverables() => this;

		public override long GetItemId( int position ) => position;


		public static string BuildAddress( string suite, string addr, string city, string postalCode )
		{
			var Suite = "";
			suite = suite.Trim();

			if( !string.IsNullOrEmpty( suite ) )
				Suite = $"{suite} ";

			return $"{Suite}{addr.Trim()}, {city.Trim()}, {postalCode.Trim()}";
		}


		public override View GetView( int position, View? convertView, ViewGroup? parent )
		{
			var T = Trips[ position ];
			CurrentTrip = T;

			if( convertView is null ) // re-use an existing view, if one is available
			{
				convertView = Context.LayoutInflater?.Inflate( Layout, null );

				if( convertView is { } )
				{
					convertView.Click += ( sender, _ ) =>
					                     {
						                     if( sender is View { Tag: { } } Sender )
						                     {
							                     CurrentTrip = Trips[ (int)Sender.Tag ];
							                     OnTripClick( CurrentTrip, false );
						                     }
					                     };

					convertView.LongClick += ( sender, _ ) =>
					                         {
						                         if( sender is View { Tag: { } } Sender )
						                         {
							                         CurrentTrip = Trips[ (int)Sender.Tag ];
							                         OnTripLongClick( CurrentTrip );
						                         }
					                         };
				}
			}

			convertView!.Tag = position;

			Drawable FColour;
			Color    Colour;

			if( !T.ReadByDriver )
			{
				FColour = Yellow;
				Colour  = Color.Black;
			}
			else
			{
				FColour = VeryDimGray;
				Colour  = Color.White;
			}

			convertView.FindViewById<TableRow>( Resource.Id.detailsRow )!.Background = FColour;

			var ServiceLevel = convertView.FindViewById<TextView>( Resource.Id.tripViewServiceLevel );
			ServiceLevel!.SetTextColor( Colour );
			ServiceLevel.Text = T.ServiceLevel;

			var PackageType = convertView.FindViewById<TextView>( Resource.Id.tripViewPackageType );
			PackageType!.SetTextColor( Colour );
			PackageType.Text = T.PackageType;

			var Pieces = convertView.FindViewById<TextView>( Resource.Id.tripViewPieces );
			Pieces!.SetTextColor( Colour );
			Pieces.Text = T.Pieces.ToString( "####" );

			var Weight = convertView.FindViewById<TextView>( Resource.Id.tripViewWeight );
			Weight!.SetTextColor( Colour );

			var W = T.Weight;
			Weight.Text = W.ToString( W >= 100 ? "####" : "##.##" );

			var TripId = convertView.FindViewById<TextView>( Resource.Id.tripId );
			TripId!.Text = T.TripId.Last( 13 );

			DateTime TripTime;

			int StatusBar;

			string CoName,
			       Address;

			if( T.Status == STATUS.PICKED_UP )
			{
				StatusBar = Resource.Drawable.DeliverBar;
				CoName    = T.DeliveryCompanyName;
				Address   = BuildAddress( T.DeliverySuite, T.DeliveryAddressLine1, T.DeliveryCity, T.DeliveryPostalCode );
				TripTime  = T.DueTime.DateTime;
			}
			else
			{
				StatusBar = Resource.Drawable.PickUpBar;
				CoName    = T.PickupCompanyName;
				Address   = BuildAddress( T.PickupSuite, T.PickupAddressLine1, T.PickupCity, T.PickupPostalCode );
				TripTime  = T.ReadyTime.DateTime;
			}

			var DisplayTime = convertView.FindViewById<TextView>( Resource.Id.tripViewDueTime );
			DisplayTime!.SetTextColor( Colour );
			DisplayTime.Text = TripTime.ToShortTimeString();

			convertView.FindViewById<ImageView>( Resource.Id.tripViewImageBar )!.SetImageResource( StatusBar );
			convertView.FindViewById<TextView>( Resource.Id.tripViewCompanyName )!.Text    = CoName;
			convertView.FindViewById<TextView>( Resource.Id.tripViewCompanyAddress )!.Text = Address;

			var Status = convertView.FindViewById<TextView>( Resource.Id.status );
			Status!.Text = T.UndeliverableMode != UNDELIVERABLE_MODE.NONE ? "Undeliverable" : "";

			return convertView;
		}

		protected PickupDeliveriesAdapter( int layout, Activity context, ListView owner, List<Trip> trips, Action<Trip, bool> onTripClick ) : base( context, owner )
		{
			Layout      = layout;
			OnTripClick = onTripClick;

			ReplaceTrips( trips );
			Globals.Debug.Activity.TripCount = trips.Count;

			Globals.Filters.OnFilterChange     = ( _, _ ) => { NotifyDataSetChanged(); };
			Globals.Filters.OnVisibilityChange = ShowTrips;
		}

		public PickupDeliveriesAdapter( Activity context, ListView owner, List<Trip> trips, Action<Trip, bool> onTripClick )
			: this( Resource.Layout.ListViewRow_Trip, context, owner, trips, onTripClick )
		{
		}

		public PickupDeliveriesAdapter( Activity context, ListView owner, List<Trip> trips, Action<Trip, bool> onTripClick, Action<Trip> onTripLongClick )
			: this( Resource.Layout.ListViewRow_Trip, context, owner, trips, onTripClick )
		{
			OnTripLongClick = onTripLongClick;
		}

		private void ShowTrips( Globals.Filters.VISIBILITY vis )
		{
			Clear().Add( SavedTrips.FilterTrips( vis ).SortTrips() );
		}

		private void ReplaceTrips( List<Trip> trips )
		{
			SavedTrips = trips;
			ShowTrips( Globals.Filters.Visibility );
		}
	}
}