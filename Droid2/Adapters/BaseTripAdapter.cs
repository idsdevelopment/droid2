using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Widget;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;
// ReSharper disable InconsistentNaming

namespace Droid2.Adapters
{
	public abstract class BaseTripAdapter : BaseAdapter<Trip>
	{
		public class TripCountData
		{
			public string TripIds, UndeliverableTripIds;

			public decimal UndeliverableTrips, TripsNotPickedUp, UndeliverablePieces, UndeliverableWeight, TotalTrips, TotalPieces, TotalWeight;

			public TripCountData()
			{
			}

			public TripCountData( TripCountData data )
			{
				UndeliverableTrips = data.UndeliverableTrips;
				UndeliverablePieces = data.UndeliverablePieces;
				UndeliverableWeight = data.UndeliverableWeight;
				TotalTrips = data.TotalTrips;
				TotalPieces = data.TotalPieces;
				TotalWeight = data.TotalWeight;
				TripIds = data.TripIds;
				UndeliverableTripIds = data.UndeliverableTripIds;
			}

			public decimal DeliverablePieces => TotalPieces - UndeliverablePieces;

			public bool HasUndeliverable => UndeliverablePieces > 0;

			public virtual string TripsAsText => $"Total Trips: {TotalTrips}";
			public virtual string UndeliverableTripsAsText => $"Undeliverable Trips: {UndeliverableTrips}";
			public virtual string TripsNotPickedUpAsText => $"Trips Not Picked Up: {TripsNotPickedUp}";

			public virtual string PiecesAsText => $"Total Pieces: {TotalPieces}";
			public virtual string UndeliverablePiecesAsText => $"Undeliverable Pieces: {UndeliverablePieces}";
		}

		protected readonly Activity Context;
		protected readonly object   LockObject = new();
		protected readonly ListView Owner;

		private bool _ShowHidden;

		public Action<bool> ScanCompletedCallback;

		private int UpdateDepth, HiddenCount;

		protected BaseTripAdapter( Activity context, ListView owner )
		{
			Context = context;
			Owner = owner;
		}

		public List<Trip> Trips { get; protected set; } = new();

		public virtual TripCountData TripInfo => new();

		public abstract bool HasScannedTrips { get; }

		public Trip CurrentTrip { get; protected set; }

		public virtual bool AllHidden => HiddenCount >= Trips.Count;

		public virtual void DoScanCompleted()
		{
		}


		public override int Count
		{
			get
			{
				lock( LockObject )
				{
					return Trips?.Count - HiddenCount ?? 0;
				}
			}
		}

		public override Trip this[ int position ]
		{
			get
			{
				lock( LockObject )
					return Trips[ position ];
			}
		}

		public bool ShowHidden
		{
			get => _ShowHidden;
			set
			{
				if( _ShowHidden != value )
				{
					_ShowHidden = value;
					NotifyDataSetChanged();
				}
			}
		}

		protected virtual bool UseExternalTripCompare => false;

		public abstract decimal UpdateCount( string scannedId, Trip trip, int count = 1 );

		public abstract BaseTripAdapter MarkUndeliverables();
		public abstract BaseTripAdapter UnMarkUndeliverables();

		public void BeginUpdate()
		{
			++UpdateDepth;
		}

		public void EndUpdate()
		{
			if( UpdateDepth > 0 )
			{
				if( --UpdateDepth == 0 )
					NotifyDataSetChanged();
			}
		}

		public override long GetItemId( int position )
		{
			return position;
		}

		public BaseTripAdapter Clear()
		{
			lock( LockObject )
				Trips.Clear();

			return NotifyDataSetChanged();
		}

		public BaseTripAdapter Replace( List<Trip> tripList )
		{
			Trips = tripList;
			NotifyDataSetChanged();
			return this;
		}

		protected virtual int TripSortCompare( Trip t1, Trip t2 )
		{
			if( !t1.Hidden && !t2.Hidden )
				return string.CompareOrdinal( t1.TripId, t2.TripId );
			if( t1.Hidden == t2.Hidden )
				return 0;
			return t1.Hidden ? 1 : -1;
		}

		public new BaseTripAdapter NotifyDataSetChanged()
		{
			if( Trips != null )
			{
				// All must run on Ui occasional adapter modified errors
				Context.RunOnUiThread( () =>
				                       {
					                       if( UpdateDepth == 0 )
					                       {
						                       HiddenCount = 0;

						                       if( !ShowHidden )
						                       {
							                       foreach( var AdapterTrip in Trips )
							                       {
								                       if( AdapterTrip.Hidden )
									                       HiddenCount++;
							                       }
						                       }

						                       if( UseExternalTripCompare )
							                       Trips.Sort( TripSortCompare );
						                       else
							                       Trips.SortTrips();

						                       base.NotifyDataSetChanged();
					                       }
				                       } );
			}

			return this;
		}

		private BaseTripAdapter ShowHideTrip( Trip trip, bool hide )
		{
			trip.Hidden = hide;
			NotifyDataSetChanged();
			return this;
		}

		private BaseTripAdapter ShowHideTrip( string tripId, bool hide )
		{
			if( HasTrip( tripId, out var Trp ) )
				ShowHideTrip( Trp, hide );
			return this;
		}

		public BaseTripAdapter HideTrip( Trip trip )
		{
			return ShowHideTrip( trip, true );
		}

		public BaseTripAdapter HideTrip( string trip )
		{
			return ShowHideTrip( trip, true );
		}

		public BaseTripAdapter ShowTrip( Trip trip )
		{
			return ShowHideTrip( trip, false );
		}

		public BaseTripAdapter ShowTrip( string trip )
		{
			return ShowHideTrip( trip, false );
		}

		public BaseTripAdapter ShowAllTrips()
		{
			foreach( var Trip in Trips )
				Trip.Hidden = false;

			return NotifyDataSetChanged();
		}

		public BaseTripAdapter InvertAllHiddenTrips()
		{
			foreach( var Trip in Trips )
				Trip.Hidden = !Trip.Hidden;

			return NotifyDataSetChanged();
		}

		public BaseTripAdapter Add( Trip trip )
		{
			lock( LockObject )
				Trips.Add( trip );

			return NotifyDataSetChanged();
		}

		public BaseTripAdapter Append( List<Trip> trips )
		{
			lock( LockObject )
				Trips.AddRange( trips );

			return NotifyDataSetChanged();
		}

		protected BaseTripAdapter InternalAdd( List<Trip> trips )
		{
			lock( LockObject )
			{
				Trips.Clear();
				Append( trips );
			}

			return this;
		}

		public virtual BaseTripAdapter Add( List<Trip> trips )
		{
			return InternalAdd( trips );
		}

		public BaseTripAdapter Remove( int ndx )
		{
			lock( LockObject )
				Trips.RemoveAt( ndx );

			return NotifyDataSetChanged();
		}

		public BaseTripAdapter Remove( Trip trip )
		{
			lock( LockObject )
			{
				var TripId = trip.TripId;
				var FoundTrip = Trips.FirstOrDefault( adapterTrip => adapterTrip.TripId == TripId );
				if( FoundTrip != null )
				{
					Trips.Remove( FoundTrip );
					return NotifyDataSetChanged();
				}
			}

			return this;
		}

		public BaseTripAdapter Remove( List<Trip> trips )
		{
			BeginUpdate();
			try
			{
				lock( LockObject )
				{
					foreach( var Trip in trips )
						Remove( Trip );
				}
			}
			finally
			{
				EndUpdate();
			}

			return this;
		}

		public bool HasTrip( string tripId, out Trip trip )
		{
			lock( LockObject )
			{
				foreach( var Trip in Trips )
				{
					if( Trip.TripId == tripId )
					{
						trip = Trip;
						return true;
					}
				}
			}

			trip = null;
			return false;
		}

		public bool HasTrip( string tripId )
		{
			return HasTrip( tripId, out _ );
		}

		public int IndexOf( string tripId )
		{
			lock( LockObject )
			{
				var Ndx = 0;
				foreach( var Trip in Trips )
				{
					if( Trip.TripId == tripId )
						return Ndx;
					Ndx++;
				}

				return -1;
			}
		}

		public BaseTripAdapter ScrollToTripId( string tripId )
		{
			lock( LockObject )
			{
				var Ndx = IndexOf( tripId );
				if( Ndx >= 0 )
					Context.RunOnUiThread( () => { Owner.SetSelection( Ndx ); } );
			}

			return this;
		}
	}
}