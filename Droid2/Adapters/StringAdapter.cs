using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Views;
using Android.Widget;

namespace Droid2.Adapters
{
	internal class StringAdapter : BaseAdapter<string>
	{
		public override  int      Count => StringList.Count;
		private readonly Activity Context;

		private         bool         DisableDataSetChanged;
		public readonly List<string> StringList = new();

		public override long GetItemId( int position ) => position;

		public override View GetView( int position, View convertView, ViewGroup parent )
		{
			if( Context.LayoutInflater is not null )
				convertView ??= Context.LayoutInflater.Inflate( Resource.Layout.ListViewRow_String, null );

			var View = convertView?.FindViewById<TextView>( Resource.Id.barcode );

			if( View is not null )
				View.Text = StringList[ position ];

			return convertView;
		}

		public override void NotifyDataSetChanged()
		{
			if( !DisableDataSetChanged )
				Context.RunOnUiThread( () => { base.NotifyDataSetChanged(); } );
		}

		public bool Contains( string str )
		{
			return StringList.Any( s => s == str );
		}

		public void BeginUpdate()
		{
			DisableDataSetChanged = true;
		}

		public void EndUpdate()
		{
			DisableDataSetChanged = false;
			NotifyDataSetChanged();
		}


		public void Clear()
		{
			StringList.Clear();
			NotifyDataSetChanged();
		}

		public void Add( string str )
		{
			StringList.Add( str );
			NotifyDataSetChanged();
		}

		public override string this[ int position ] => StringList[ position ];

		internal StringAdapter( Activity context )
		{
			Context = context;
		}
	}
}