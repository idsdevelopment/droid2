using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;

namespace Droid2.Adapters
{
	internal class CheckBoxListAdapter : BaseAdapter<string>
	{
		internal class ListItem
		{
			public int Index;
			public string Text;
		}

		private readonly Activity Context;

		public readonly List<ListItem> ItemList;

		private bool DisableDataSetChanged;
		public Action<CheckBox, int> OnLongClick = null;

		public Action<int> OnSelectionChanged = i => { };

		internal CheckBoxListAdapter( Activity context, IEnumerable<string> items )
		{
			Context = context;
			ItemList = new List<ListItem>();

			var Ndx = 0;
			foreach( var Item1 in items )
				ItemList.Add( new ListItem { Index = Ndx++, Text = Item1 } );
		}

		internal CheckBoxListAdapter( Activity context ) : this( context, new List<string>() )
		{
		}

		public List<int> SelectedIndices { get; set; } = new();

		public override int Count => ItemList.Count;

		public override string this[ int position ] => ItemList[ position ].Text;

		public override long GetItemId( int position )
		{
			return position;
		}

		public override View GetView( int position, View convertView, ViewGroup parent )
		{
			if( convertView == null )
			{
				convertView = Context.LayoutInflater.Inflate( Resource.Layout.ListViewRow_CheckBoxes, null );
				var Check = convertView.FindViewById<CheckBox>( Resource.Id.checkBox );
				Check.Click += ( sender, args ) =>
				               {
					               SelectedIndices.Clear();

					               for( var Index = 0; Index < parent.ChildCount; Index++ )
					               {
						               var V = parent.GetChildAt( Index );
						               if( V.FindViewById( Resource.Id.checkBox ) is CheckBox CheckBox )
						               {
							               if( CheckBox.Checked )
								               SelectedIndices.Add( (int)CheckBox.Tag );
						               }
					               }

					               var SelectedIndex = sender is CheckBox Box ? (int)Box.Tag : -1;

					               OnSelectionChanged?.Invoke( SelectedIndex );
				               };

				Check.LongClick += ( sender, args ) =>
				                   {
					                   if( OnLongClick != null )
					                   {
						                   if( sender is CheckBox CheckBox )
							                   OnLongClick( CheckBox, (int)CheckBox.Tag );
					                   }
				                   };
			}

			var CBox = convertView.FindViewById<CheckBox>( Resource.Id.checkBox );
			CBox.Text = ItemList[ position ].Text;
			CBox.Tag = position;

			return convertView;
		}

		public override void NotifyDataSetChanged()
		{
			if( !DisableDataSetChanged )
				Context.RunOnUiThread( () => { base.NotifyDataSetChanged(); } );
		}

		public void BeginUpdate()
		{
			DisableDataSetChanged = true;
		}

		public void EndUpdate()
		{
			DisableDataSetChanged = false;
			NotifyDataSetChanged();
		}

		public void Clear()
		{
			ItemList.Clear();
			NotifyDataSetChanged();
		}

		private void ReIndex()
		{
			var Ndx = 0;
			foreach( var ListItem in ItemList )
				ListItem.Index = Ndx++;
		}

		public void Add( string str )
		{
			ItemList.Add( new ListItem { Text = str } );
			ReIndex();
			NotifyDataSetChanged();
		}

		public void Add( List<string> items )
		{
			foreach( var Item1 in items )
				ItemList.Add( new ListItem { Text = Item1 } );

			ReIndex();

			NotifyDataSetChanged();
		}
	}
}