using System;
using System.Collections.Generic;
using Service.Interfaces;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters
{
	public static class TripSort
	{
		public static List<Trip> SortTrips( this List<Trip> trips )
		{
			SortTripList( trips );

			return trips;
		}

		public static int Compare( Globals.Filters.SORT sort, Trip trip, Trip trip1 )
		{
			// Put hidden ones last
			if( !trip.Hidden && !trip1.Hidden )
			{
				// Always put undeliverable first;
				if( ( trip.UndeliverableMode != UNDELIVERABLE_MODE.NONE )
				    || ( trip1.UndeliverableMode != UNDELIVERABLE_MODE.NONE ) )
					return trip1.UndeliverableMode - trip.UndeliverableMode;

				switch( sort )
				{
				case Globals.Filters.SORT.ACCOUNT_ID:
					return string.CompareOrdinal( trip.AccountId, trip1.AccountId );

				case Globals.Filters.SORT.ACCOUNT_NUMBER:
					return string.CompareOrdinal( trip.AccountNumber, trip1.AccountNumber );

				case Globals.Filters.SORT.DUE:
					return DateTime.Compare( trip.DueTime.DateTime, trip1.DueTime.DateTime );

				case Globals.Filters.SORT.COMPANY:
					return trip.Status == STATUS.DELIVERED
						       ? string.CompareOrdinal( trip.PickupCompanyName, trip1.PickupCompanyName )
						       : string.CompareOrdinal( trip.DeliveryCompanyName, trip1.DeliveryCompanyName );

				case Globals.Filters.SORT.ID:
					return string.CompareOrdinal( trip.TripId, trip1.TripId );

				case Globals.Filters.SORT.NEW:
					return trip.Status == STATUS.NEW ? 0 : -1;

				case Globals.Filters.SORT.REFERENCE:
					return string.CompareOrdinal( trip.Reference, trip1.Reference );

				case Globals.Filters.SORT.STATUS:
					if( trip.Status < trip1.Status )
						return -1;

					return trip.Status > trip1.Status ? 1 : 0;

				case Globals.Filters.SORT.SERVICE_LEVEL:
					return string.CompareOrdinal( trip.ServiceLevel ?? "", trip1.ServiceLevel ?? "" );
				}

				return 0;
			}

			if( trip.Hidden == trip1.Hidden )
				return 0;

			return trip.Hidden ? 1 : -1;
		}

		public static void SortTripList( List<Trip> trips )
		{
			var PSort = Globals.Filters.PrimarySort;
			var SSort = Globals.Filters.SecondarySort;

			trips.Sort( ( trip, trip1 ) =>
			            {
				            var RetVal = Compare( PSort, trip, trip1 );

				            return RetVal != 0 ? RetVal : Compare( SSort, trip, trip1 );
			            } );
		}
	}
}