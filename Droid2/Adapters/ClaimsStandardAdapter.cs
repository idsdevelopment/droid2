using System;
using Android.App;
using Android.Views;
using Android.Widget;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters
{
	public class ClaimsStandardAdapter : BaseTripAdapter
	{
		private readonly Action<Trip> OnTripClicked;

		public ClaimsStandardAdapter( Activity context, ListView owner, Action<Trip> onTripClicked ) : base( context, owner )
		{
			OnTripClicked = onTripClicked;
		}

		public override View GetView( int position, View convertView, ViewGroup parent )
		{
			var T = Trips[ position ];
			CurrentTrip = T;

			if( convertView == null ) // re-use an existing view, if one is available
			{
				convertView = Context.LayoutInflater.Inflate( Resource.Layout.ListViewRow_ClaimStandard, null );
				convertView.FindViewById<LinearLayout>( Resource.Id.tripListBase ).Click += ( sender, args ) =>
				{
					if( sender is View Sender )
					{
						CurrentTrip = Trips[ (int)Sender.Tag ];
						OnTripClicked?.Invoke( CurrentTrip );
					}
				};
			}

			convertView.Tag = position;

			convertView.FindViewById<TextView>( Resource.Id.tripId ).Text = T.TripId;
			convertView.FindViewById<TextView>( Resource.Id.pieces ).Text = T.Pieces.ToString( "####" );
			return convertView;
		}

		public ClaimsStandardAdapter Add( string tripId )
		{
			if( !HasTrip( tripId, out var T ) )
				Add( new Trip { TripId = tripId, Pieces = 1 } );
			else
			{
				T.Pieces++;
				NotifyDataSetChanged();
			}

			return this;
		}

		public override decimal UpdateCount( string scannedId, Trip trip, int count = 1 )
		{
			return 0;
		}

		public override bool HasScannedTrips => false;

		public override BaseTripAdapter MarkUndeliverables()
		{
			return this;
		}

		public override BaseTripAdapter UnMarkUndeliverables()
		{
			return this;
		}
	}
}