using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;

namespace Droid2.Adapters
{
	internal class RadioButtonListAdapter : BaseAdapter<string>
	{
		public int SelectedIndex { get; private set; }

		public readonly List<string> ItemList;
		private readonly Activity Context;

		public Action<int> OnSelectionChanged = i => { };
		public Action<RadioButton, int> OnLongClick = null;

		internal RadioButtonListAdapter( Activity context, List<string> items )
		{
			Context = context;
			ItemList = items;
		}

		internal RadioButtonListAdapter( Activity context ) : this( context, new List<string>() )
		{
		}


		public override long GetItemId( int position )
		{
			return position;
		}

		public override View GetView( int position, View convertView, ViewGroup parent )
		{
			if( convertView == null )
			{
				convertView = Context.LayoutInflater.Inflate( Resource.Layout.ListViewRow_RadioButtons, null );
				var Rad = convertView.FindViewById<RadioButton>( Resource.Id.radioButton );
				Rad.Click += ( sender, args ) =>
				             {
					             for( var Index = 0; Index < parent.ChildCount; Index++ )
					             {
						             var V = parent.GetChildAt( Index );
						             if( V.FindViewById( Resource.Id.radioButton ) is RadioButton RButton )
							             RButton.Checked = false;
					             }

					             if( sender is RadioButton RadButton )
					             {
						             RadButton.Checked = true;
						             SelectedIndex = (int)RadButton.Tag;
					             }
					             else
						             SelectedIndex = -1;

					             OnSelectionChanged?.Invoke( SelectedIndex );
				             };

				Rad.LongClick += ( sender, args ) =>
				                 {
					                 if( OnLongClick != null )
					                 {
						                 if( sender is RadioButton RadButton )
							                 OnLongClick( RadButton, (int)RadButton.Tag );
					                 }
				                 };
			}

			var Button = convertView.FindViewById<RadioButton>( Resource.Id.radioButton );
			Button.Text = ItemList[ position ];
			Button.Tag = position;

			return convertView;
		}

		public override int Count => ItemList.Count;

		public override string this[ int position ] => ItemList[ position ];

		private bool DisableDataSetChanged;

		public override void NotifyDataSetChanged()
		{
			if( !DisableDataSetChanged )
				Context.RunOnUiThread( () => { base.NotifyDataSetChanged(); } );
		}

		public void BeginUpdate()
		{
			DisableDataSetChanged = true;
		}

		public void EndUpdate()
		{
			DisableDataSetChanged = false;
			NotifyDataSetChanged();
		}

		public void Clear()
		{
			ItemList.Clear();
			NotifyDataSetChanged();
		}

		public void Add( string str )
		{
			ItemList.Add( str );
			NotifyDataSetChanged();
		}

		public void Add( List<string> items )
		{
			ItemList.AddRange( items );
			NotifyDataSetChanged();
		}
	}
}