﻿using System.Collections.Generic;
using Android.App;
using Android.Widget;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters
{
	internal class ClaimsByPieceNumberAdapter : ClaimsPiecesAdapter
	{
		internal readonly Dictionary<string, bool> PiecesLookup = new();

		private readonly bool IsPickup;

		public ClaimsByPieceNumberAdapter( Activity context, ListView owner, bool isPickup, bool showUndeliverable = false ) : base( context, owner, showUndeliverable )
		{
			IsPickup = isPickup;
		}

		public override BaseTripAdapter Add( List<Trip> trips )
		{
			if( IsPickup )
			{
				foreach( var Trip in trips )
					Trip.MissingPieces = new byte[ 255 ];
			}

			base.Add( trips );

			PiecesLookup.Clear();

			foreach( var Trip in trips )
			{
				var Id = Trip.TripId;
				var C = Trip.OriginalPieceCount;

				for( var I = 0; I++ < C; )
					PiecesLookup.Add( $"{Id}-{I}", false );
			}
			return this;
		}

		public override decimal UpdateCount( string scannedId, Trip trip, int count = 1 )
		{
			var P = scannedId.LastIndexOf( '-' );
			if( P < 0 )
				scannedId += "-1";

			if( PiecesLookup.TryGetValue( scannedId, out var Scanned ) )
			{
				if( !Scanned )
				{
					PiecesLookup[ scannedId ] = true;
					return base.UpdateCount( trip.TripId, trip, count );
				}
			}
			return -1; // Error
		}
	}
}