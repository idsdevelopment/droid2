using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Android.App;
using Android.Graphics;
using Android.Icu.Text;
using Android.Views;
using Android.Widget;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Adapters
{
	public class ScannedTrip : Trip
	{
		public virtual bool CannotPickup       { get; set; } // Pml
		public virtual bool AllowUndeliverable => true;

		public virtual string TripIdAsText => TripId;

		public new virtual string DeliveryZone
		{
			get => base.DeliveryZone;
			set => base.DeliveryZone = value;
		}

		internal virtual bool ShowErrorBackground => NotForThisLocation || Undeliverable || ( Balance < 0 );

		internal virtual Color TextColor => NotForThisLocation ? Color.Yellow : Color.White;

		internal virtual string AsText
		{
			get
			{
				if( CannotPickup )
					return CannotPickupReason;

				if( Undeliverable )
					return $"{Balance} Undeliverable";

				if( NotForThisLocation ) // Must be before overscan
					return "Not for this location";

				return Balance < 0 ? "Overscan" : "";
			}
		}

		internal virtual bool ScanCompleted => ( Balance == 0 ) || CannotPickup;

		public string CannotPickupReason = "";

		public bool NotForThisLocation,
		            Undeliverable,
		            IsReturn;

		public decimal Scanned,
		               Balance;

		internal virtual ScannedTrip Clone( Trip trip )
		{
			return new( trip );
		}

		internal virtual ScannedTrip Clone( ScannedTrip trip )
		{
			return new( trip );
		}

		public ScannedTrip()
		{
		}

		public ScannedTrip( Trip trip ) : base( trip )
		{
			Balance = Pieces;
		}

		public ScannedTrip( ScannedTrip trip ) : base( trip )
		{
			Balance            = trip.Balance;
			Scanned            = trip.Scanned;
			NotForThisLocation = trip.NotForThisLocation;
			Undeliverable      = trip.Undeliverable;
		}
	}

	public abstract class ABaseClaimsPiecesAdapter<TSt, TD> : BaseTripAdapter where TSt : ScannedTrip, new()
	                                                                          where TD : BaseTripAdapter.TripCountData, new()
	{
		protected override bool UseExternalTripCompare => true;

		protected virtual int LayoutId => Resource.Layout.ListViewRow_ClaimPieces;

		public override TripCountData TripInfo
		{
			get
			{
				var Retval = NewTripData();

				foreach( var Trip in Trips )
				{
					if( Retval.TripIds != "" )
						Retval.TripIds += ',';
					Retval.TripIds += Trip.TripId;

					Retval.TotalPieces += Trip.Pieces;
					Retval.TotalWeight += Trip.Weight;

					if( Trip is ScannedTrip STrip && STrip.AllowUndeliverable && ( STrip.Balance > 0 ) )
					{
						if( Retval.UndeliverableTripIds != "" )
							Retval.UndeliverableTripIds += ',';

						Retval.UndeliverableTripIds += Trip.TripId;

						Retval.UndeliverableTrips++;
						Retval.UndeliverablePieces += STrip.Balance;

						if( STrip.CannotPickup )
							Retval.TripsNotPickedUp++;
					}
				}

				return Retval;
			}
		}

		public override bool HasScannedTrips
		{
			get { return Trips.OfType<ScannedTrip>().Any( sTrip => ( sTrip.Scanned > 0 ) || sTrip.Undeliverable ); }
		}

		public bool HasBalance
		{
			get { return Trips.OfType<ScannedTrip>().Any( sTrip => sTrip.Balance != 0 ); }
		}

		public bool HasUnderScan
		{
			get { return Trips.OfType<ScannedTrip>().Any( sTrip => sTrip.Balance > 0 ); }
		}

		public bool HasOverscan
		{
			get { return Trips.OfType<ScannedTrip>().Any( sTrip => sTrip.Scanned > sTrip.Pieces ); }
		}

		protected internal bool AutoShow = true;

		public Action<View, object, EventArgs> OnRowClick,
		                                       OnRowLongClick;

		public bool ShowMinimalOptions;

		protected virtual TD NewTripData()
		{
			return new() { TotalTrips = Trips.Count, TripIds = "", UndeliverableTripIds = "" };
		}

		// Put overscan at top of list
		protected override int TripSortCompare( Trip t1, Trip t2 )
		{
			var B1 = ( (TSt)t1 ).Balance;
			var B2 = ( (TSt)t2 ).Balance;

			if( ( B1 < 0 ) && ( B2 < 0 ) )
				return string.CompareOrdinal( t1.TripId, t2.TripId );

			if( B1 < 0 )
				return -1;

			return B2 < 0 ? 1 : base.TripSortCompare( t1, t2 );
		}

		protected virtual void OnGetView( View view, Trip trip )
		{
		}

		protected virtual string GetCompanyName( Trip trip )
		{
			return trip.DeliveryCompanyName;
		}

		public override View GetView( int position, View convertView, ViewGroup parent )
		{
			var T = Trips[ position ] as TSt;
			CurrentTrip = T;

			if( convertView == null ) // re-use an existing view, if one is available
			{
				convertView = Context.LayoutInflater.Inflate( LayoutId, null );

				convertView.Click += ( sender, args ) =>
				                     {
					                     CurrentTrip = Trips[ position ] as TSt;
					                     OnRowClick?.Invoke( convertView, sender, args );
				                     };

				convertView.LongClick += ( sender, args ) =>
				                         {
					                         CurrentTrip = Trips[ position ] as TSt;
					                         OnRowLongClick?.Invoke( convertView, sender, args );
				                         };
			}

			convertView.Tag = position;

			if( T != null )
			{
				convertView.FindViewById<TextView>( Resource.Id.tripId ).Text = T.TripIdAsText;

				var CompanyName = convertView.FindViewById<TextView>( Resource.Id.companyName );
				CompanyName.Text = GetCompanyName( T );

				convertView.SetBackgroundResource( T.ShowErrorBackground ? Resource.Drawable.RedGradient : Resource.Color.Black );
				CompanyName.SetTextColor( T.TextColor );

				convertView.FindViewById<TextView>( Resource.Id.zone ).Text = T.DeliveryZone;

				convertView.FindViewById<TextView>( Resource.Id.pieces ).Text = T.Pieces.ToString( "###0" );

				var Scanned = convertView.FindViewById<TextView>( Resource.Id.scanned );
				Scanned.Text = T.Scanned.ToString( "###0" );
				Scanned.SetTextColor( T.Scanned > 0 ? Color.Yellow : Color.White );

				var Balance = convertView.FindViewById<TextView>( Resource.Id.balance );
				Balance.Text = T.Balance.ToString( "###0" );
				Balance.SetTextColor( T.Balance < 0 ? Color.OrangeRed : Color.LimeGreen );

				convertView.FindViewById<TextView>( Resource.Id.status ).Text = T.AsText;
			}

			OnGetView( convertView, T );

			return convertView;
		}

		public override BaseTripAdapter MarkUndeliverables()
		{
			foreach( var Trip in Trips )
			{
				if( Trip is ScannedTrip STrip && ( STrip.Balance > 0 ) )
					STrip.Undeliverable = true;
			}

			DoScanCompleted();

			return NotifyDataSetChanged();
		}

		public override BaseTripAdapter UnMarkUndeliverables()
		{
			foreach( var Trip in Trips )
			{
				if( Trip is ScannedTrip STrip )
					STrip.Undeliverable = false;
			}

			DoScanCompleted();

			return NotifyDataSetChanged();
		}

		public new ABaseClaimsPiecesAdapter<TSt, TD> Add( Trip trip )
		{
			base.Add( new TSt().Clone( trip ) );

			return this;
		}

		public ABaseClaimsPiecesAdapter<TSt, TD> Add( TSt trip )
		{
			base.Add( new TSt().Clone( trip ) );

			return this;
		}

		public ABaseClaimsPiecesAdapter<TSt, TD> Update( List<Trip> trips )
		{
			var ToUpdate = ConvertToAdapterType( trips.SortTrips() );

			foreach( var T in ToUpdate )
			{
				foreach( var A in Trips )
				{
					var S = (ScannedTrip)A;

					if( A.TripId == T.TripId )
					{
						T.Scanned = S.Scanned;
						T.Balance = S.Balance;

						break;
					}
				}
			}

			Trips = ( from T in ToUpdate
			          select (Trip)T ).ToList();

			NotifyDataSetChanged();

			return this;
		}

		public List<TSt> ConvertToAdapterType( List<Trip> trips )
		{
			return trips.Select( trip => (TSt)new TSt().Clone( trip ) ).ToList();
		}

		public override BaseTripAdapter Add( List<Trip> trips )
		{
			base.Add( trips.Select( trip => (Trip)new TSt().Clone( trip ) ).ToList() ); // Convert to type T and add
			Trips = Trips.SortTrips();

			return this;
		}

		public ABaseClaimsPiecesAdapter<TSt, TD> Append( List<TSt> trips )
		{
			lock( LockObject )
				Trips.AddRange( trips );

			NotifyDataSetChanged();

			return this;
		}

		public virtual ABaseClaimsPiecesAdapter<TSt, TD> AddUnknownTrip( Trip trip )
		{
			var St = new TSt().Clone( trip );
			St.DeliveryCompanyName = "UNKNOWN";
			St.NotForThisLocation  = true;
			St.Balance             = -1;
			St.Scanned             = 1;
			base.Add( St );

			return this;
		}

		public ABaseClaimsPiecesAdapter<TSt, TD> AddUnknownTrip( string tripId )
		{
			return AddUnknownTrip( new Trip { TripId = tripId } );
		}

		public ABaseClaimsPiecesAdapter<TSt, TD> AddUndeliverableTrip( Trip trip )
		{
			var St = new TSt().Clone( trip );
			St.Undeliverable = true;
			base.Add( St );

			return this;
		}

		protected virtual bool IsScanCompleted( List<Trip> trips )
		{
			while( true )
			{
				try
				{
					foreach( var Trip in Trips )
					{
						if( Trip is TSt T )
						{
							if( !T.ScanCompleted )
								return false;
						}
					}

					break;
				}
				catch( InvalidOperationException ) // Being changed by something else
				{
					Thread.Sleep( 100 );
				}
			}

			return true;
		}

		public override void DoScanCompleted()
		{
			var AllOk = Trips.Count > 0;

			if( AllOk )
				AllOk = IsScanCompleted( Trips );

			ScanCompletedCallback?.Invoke( AllOk );
		}

		public override decimal UpdateCount( string scannedId, Trip trip, int count = 1 )
		{
			try
			{
				if( trip is TSt Trp )
				{
					Trp.Balance -= count;
					Trp.Scanned += count;

					if( Trp.Balance == 0 )
						Trp.Hidden = true;
					else if( AutoShow )
						Trp.Hidden = Trp.Balance == 0;

					NotifyDataSetChanged();

					return Trp.Balance;
				}

				return -1; // Error
			}
			finally
			{
				DoScanCompleted();
			}
		}

		protected ABaseClaimsPiecesAdapter( Activity context, ListView owner ) : base( context, owner )
		{
		}
	}
}