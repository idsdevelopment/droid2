using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Widget;
using Droid2.Fragments.Standard;
using Service.Interfaces;

namespace Droid2.Adapters
{
	public class ClaimsPiecesAdapter : ABaseClaimsPiecesAdapter<ScannedTrip, BaseTripAdapter.TripCountData>
	{
		public Action<ScannedTrip, PickupDeliveriesOptionsActivity.SCAN_STATUS, string> Callback;

		public Action ResetScanCallback;

		public ClaimsPiecesAdapter( Activity context, ListView owner, bool showUndeliverable = false ) : base( context, owner )
		{
			OnRowClick = async ( view, o, args ) =>
			             {
				             PickupDeliveriesOptionsActivity.Status = PickupDeliveriesOptionsActivity.SCAN_STATUS.BUSY;

				             if( Trips[ (int)view.Tag ] is ScannedTrip T )
				             {
					             PickupDeliveriesOptionsActivity.SelectedTrip = T;

					             var Intent = new Intent( Context, typeof( PickupDeliveriesOptionsActivity ) );
					             Intent.PutExtra( PickupDeliveriesOptionsActivity.MINIMAL, ShowMinimalOptions );

					             if( T.NotForThisLocation )
						             Intent.PutExtra( PickupDeliveriesOptionsActivity.SHOW_REMOVE, true );

					             Intent.PutExtra( PickupDeliveriesOptionsActivity.SHOW_UNDELIVERABLE, showUndeliverable );
					             Intent.PutExtra( PickupDeliveriesOptionsActivity.ZONE_FILTER, T.Status == STATUS.PICKED_UP ? T.DeliveryZone : T.PickupZone );

					             Context.StartActivity( Intent );

					             await Task.Run( () =>
					                             {
						                             while( PickupDeliveriesOptionsActivity.Status == PickupDeliveriesOptionsActivity.SCAN_STATUS.BUSY )
							                             Thread.Sleep( 100 );
					                             } );

					             if( PickupDeliveriesOptionsActivity.Status != PickupDeliveriesOptionsActivity.SCAN_STATUS.CANCEL )
					             {
						             switch( PickupDeliveriesOptionsActivity.Status )
						             {
						             case PickupDeliveriesOptionsActivity.SCAN_STATUS.FILTER:
							             var EmptyFilter = string.IsNullOrWhiteSpace( PickupDeliveriesOptionsActivity.ZoneFilter );

							             foreach( var Trip in Trips )
							             {
								             if( Trip is ScannedTrip STrip )
									             STrip.Hidden = !EmptyFilter && ( STrip.DeliveryZone.Trim().ToUpper() != PickupDeliveriesOptionsActivity.ZoneFilter );
							             }

							             if( Trips.All( trip => trip.Hidden ) )
							             {
								             Globals.Dialogues.Inform( "No trips matching zone", () =>
								                                                                 {
									                                                                 PickupDeliveriesOptionsActivity.ZoneFilter = "";

									                                                                 foreach( var Trip in Trips )
										                                                                 Trip.Hidden = false;

									                                                                 NotifyDataSetChanged();
								                                                                 } );
							             }
							             else
								             NotifyDataSetChanged();

							             break;

						             case PickupDeliveriesOptionsActivity.SCAN_STATUS.RESET_SCAN:
							             var Trp = PickupDeliveriesOptionsActivity.SelectedTrip;

							             if( Trp != null )
							             {
								             Trp.Balance            = Trp.Pieces;
								             Trp.Scanned            = 0;
								             Trp.Hidden             = false;
								             Trp.Undeliverable      = false;
								             Trp.NotForThisLocation = false;
								             NotifyDataSetChanged();
							             }

							             ResetScanCallback?.Invoke();

							             break;

						             default:
							             Callback?.Invoke( PickupDeliveriesOptionsActivity.SelectedTrip, PickupDeliveriesOptionsActivity.Status, PickupDeliveriesOptionsActivity.ZoneFilter );

							             break;
						             }
					             }
				             }
			             };
		}
	}
}