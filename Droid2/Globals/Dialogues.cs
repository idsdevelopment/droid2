#nullable enable

using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Views;
using Android.Widget;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Dialogues
		{
			public const int SORT       = 0;
			public const int RESOLVE    = 1;
			public const int DO_DOZE    = 2;
			public const int VISIBILITY = 3;

			private const string CONFIRM = "Confirm";

			private const string UNDELIVERABLE = "Are you sure you wish to make the trip UNDELIVERABLE?";

			private const string WARNING = "Warning";

			public static void AskExitIds( Action onOk, Action? onCancel = null )
			{
				Dialogue( "Exit IDS", ( builder, action ) =>
				                      {
					                      builder.SetMessage( "This will exit IDS, are you sure?" );

					                      builder.SetPositiveButton( Preferences.YesString, ( _, _ ) =>
					                                                                        {
						                                                                        action();
						                                                                        onOk.Invoke();
					                                                                        } );

					                      builder.SetNegativeButton( Preferences.CancelString, ( _, _ ) =>
					                                                                           {
						                                                                           action();
						                                                                           onCancel?.Invoke();
					                                                                           } );
				                      } );
			}

			public static void Error( string text, bool beep = true, Activity? activity = null, Action? closedCallback = null )
			{
				if( beep )
					Sounds.PlayBadScanSound();

				Dialogue( Preferences.ErrorString, ( builder, action ) =>
				                                   {
					                                   builder.SetMessage( text );

					                                   builder.SetNegativeButton( Preferences.CancelString, ( _, _ ) =>
					                                                                                        {
						                                                                                        action();
						                                                                                        closedCallback?.Invoke();
					                                                                                        } );
				                                   }, activity );
			}

			public static void ConfirmBase( string heading, string text, string yesString, Action onOk, Action? onCancel = null, bool beep = true, Activity? activity = null )
			{
				if( beep )
					Sounds.PlayBadScanSound();

				Dialogue( heading, ( builder, action ) =>
				                   {
					                   builder.SetMessage( text );

					                   builder.SetPositiveButton( yesString, ( _, _ ) =>
					                                                         {
						                                                         action();
						                                                         onOk();
					                                                         } );

					                   if( onCancel is not null )
					                   {
						                   builder.SetNegativeButton( Preferences.NoString, ( _, _ ) =>
						                                                                    {
							                                                                    action();
							                                                                    onCancel.Invoke();
						                                                                    } );
					                   }
				                   }, activity );
			}

			public static async Task<bool> ConfirmBase( string heading, string text, string yesString, bool beep = true, Activity? activity = null, bool showCancel = true )
			{
				var Result = false;

				await Task.Run( async () =>
				                {
					                var Waiting = true;

					                Action? OnCancel;

					                if( showCancel )
						                OnCancel = () => { Waiting = false; };
					                else
						                OnCancel = null;

					                ConfirmBase( heading, text, yesString, () =>
					                                                       {
						                                                       Result  = true;
						                                                       Waiting = false;
					                                                       }, OnCancel, beep, activity );

					                await Task.Run( () =>
					                                {
						                                while( Waiting )
							                                Thread.Sleep( 100 );
					                                } );
				                } );

				return Result;
			}

			public static void Confirm( string text, Action onOk, Action? onCancel = null, bool beep = true, Activity? activity = null )
			{
				ConfirmBase( CONFIRM, text, Preferences.YesString, onOk, onCancel, beep, activity );
			}

			public static bool Confirm( string text, bool beep = true, Activity? activity = null ) => ConfirmBase( CONFIRM, text, Preferences.YesString, beep, activity ).Result;

			public static void ConfirmUndeliverable( Action onOk, Action? onCancel = null, bool beep = true, Activity? activity = null )
			{
				ConfirmBase( CONFIRM, UNDELIVERABLE, Preferences.YesString, onOk, onCancel, beep, activity );
			}

			public static bool ConfirmUndeliverable( bool beep = false, Activity? activity = null ) => ConfirmBase( CONFIRM, UNDELIVERABLE, Preferences.YesString, beep, activity ).Result;

			public static void Warning( string text, Action onOk, Action? onCancel = null, bool beep = true, Activity? activity = null )
			{
				ConfirmBase( WARNING, text, Preferences.WarningString, onOk, onCancel, beep, activity );
			}

			public static bool Warning( string text, bool beep = true, Activity? activity = null ) => ConfirmBase( WARNING, text, Preferences.WarningString, beep, activity, false ).Result;

			public static void Inform( string text, bool beep = false, Activity? activity = null )
			{
				Inform( text, () => { }, beep, activity );
			}

			public static void Inform( string text, Action onOk, bool beep = false, Activity? activity = null )
			{
				if( beep )
					Sounds.PlayBadScanSound();

				Dialogue( "Information", ( builder, action ) =>
				                         {
					                         builder.SetMessage( text );

					                         builder.SetPositiveButton( Preferences.OkString, ( _, _ ) =>
					                                                                          {
						                                                                          action();
						                                                                          onOk();
					                                                                          } );
				                         }, activity );
			}

			public static void InformUnderneath( View view, string text )
			{
				var Loc = new int[ 2 ];
				view.GetLocationOnScreen( Loc );

				var Text = Toast.MakeText( view.Context, text, ToastLength.Short );

				if( Text is not null )
				{
					Text.SetGravity( GravityFlags.Left | GravityFlags.Top, Loc[ 0 ], Loc[ 1 ] + view.Height );
					Text.Show();
				}
			}

			public static void MapDialer( bool isPickup, string companyName, string address, string phoneNumber, Activity? activity = null )
			{
				Dialogue( isPickup ? "View Pickup" : "View Delivery", ( builder, action ) =>
				                                                      {
					                                                      builder.SetMessage( $"Action for {companyName}" );

					                                                      builder.SetPositiveButton( "Map", ( _, _ ) =>
					                                                                                        {
						                                                                                        action();
						                                                                                        GoogleMaps.ShowMapFromAddress( companyName, address );
					                                                                                        } );

					                                                      builder.SetNegativeButton( Preferences.CancelString, ( _, _ ) => { action(); } );

					                                                      if( !string.IsNullOrWhiteSpace( phoneNumber ) )
					                                                      {
						                                                      phoneNumber = phoneNumber.Trim();

						                                                      builder.SetNeutralButton( "Dial", ( _, _ ) =>
						                                                                                        {
							                                                                                        action();
							                                                                                        Phone.Dial( phoneNumber );
						                                                                                        } );
					                                                      }
				                                                      }, activity );
			}

			public static void NotForThisLocation()
			{
				Error( "Item is not for this location" );
			}

			private static async void Dialogue( string heading, Action<AlertDialog.Builder, Action> builderCallback, Activity? activity = null )
			{
				var Act     = activity ?? Debug.Activity;
				var Builder = new AlertDialog.Builder( Act );
				Builder.SetTitle( heading );

				var Closed = false;

				builderCallback( Builder, () => { Closed = true; } );

				Act.RunOnUiThread( () =>
				                   {
					                   var Dlg = Builder.Show();

					                   if( Dlg is not null && Act.Resources is not null )
					                   {
						                   var TitleId = Act.Resources.GetIdentifier( "alertTitle", "id", "android" );

						                   var Title = Dlg.FindViewById<TextView>( TitleId );

						                   var Parent = (ViewGroup?)Title?.Parent?.Parent;

						                   if( Parent is not null )
						                   {
							                   var Px4 = Android.ConvertDpToPixels( 4 );
							                   var Px3 = Android.ConvertDpToPixels( 3 );
							                   Parent.SetPadding( Px3, Px4, Px4, Px3 );
						                   }

						                   var MPx = Android.ConvertDpToPixels( 10 );

						                   if( Title is not null )
						                   {
							                   Title.SetPadding( MPx, 0, 0, 0 );

							                   Title.SetBackgroundResource( Resource.Drawable.BorderBottomBackgroundGray );
							                   Title.TextAlignment = TextAlignment.Center;
						                   }
					                   }
				                   } );

				await Task.Run( () =>
				                {
					                while( !Closed )
						                Thread.Sleep( 200 );
				                } );
			}
		}
	}
}