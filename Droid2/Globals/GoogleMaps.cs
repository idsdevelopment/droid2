using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.Content;
using Java.Net;
using Newtonsoft.Json.Linq;
using Uri = Android.Net.Uri;

namespace Droid2
{
    public static partial class Globals
    {
        public static class GoogleMaps
        {
            public struct Point
            {
                public double Latitude, Longitude;
            }

            public static Point GetLatLocationFromAddress( string address )
            {
                return Task.Run( () =>
                {
                    double Lat = 0, Long = 0;

                    try
                    {
                        address = WebUtility.UrlEncode( address );
                        var Url = new URL( "http://maps.google.com/maps/api/geocode/json?address=" + address + "&ka&sensor=false" );

                        var Connection = (HttpURLConnection)Url.OpenConnection();
                        Connection.RequestMethod = "GET";
                        Connection.DoInput = true;
                        Connection.Connect();
                        var Stream = Connection.InputStream;
                        if( Stream != null )
                        {
                            var Buffer = new byte[ 10000 ];
                            var Count = 0;
                            var Ndx = 0;
                            do
                            {
                                Count = Stream.Read( Buffer, Ndx, 1 );
                                Ndx += Count;
                            } while( ( Count > 0 ) && ( Ndx < Buffer.Length ) );


                            var RetVal = WebUtility.UrlDecode( Encoding.ASCII.GetString( Buffer ) );
                            var Json = JObject.Parse( RetVal );

                            var Loc = Json.SelectToken( "results[ 0 ].geometry.location" );

                            Lat = (double)Loc.SelectToken( "lat" );
                            Long = (double)Loc.SelectToken( "lng" );
                        }
                    }
                    catch( Exception E )
                    {
                        Console.WriteLine( E );
                    }

                    return new Point { Longitude = Long, Latitude = Lat };
                } ).Result;
            }

            public static void ShowMapFromAddress( string companyName, string address )
            {
                var Point = GetLatLocationFromAddress( address );
                var GeoUri = Uri.Parse( $"geo:0,0,?q={Point.Latitude},{Point.Longitude}({companyName}\r\n{address})" );

                var MapIntent = new Intent( Intent.ActionView, GeoUri );
                MapIntent.SetPackage( "com.google.android.apps.maps" );
                Debug.Activity.StartActivity( MapIntent );
            }
        }
    }
}