using Android.Content;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Background
		{
			public static Intent ServiceIntent;
			public static IdsDroidServiceConnection Connection;
		}
	}
}