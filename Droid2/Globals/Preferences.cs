using System.Collections.Generic;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Preferences
		{
			public static string CancelString => Login.LoggedIn ? Modifications.Preferences.CancelString : "Cancel";

			public static string OkString => Login.LoggedIn ? Modifications.Preferences.OkString : "OK";

			public static string ErrorString => Login.LoggedIn ? Modifications.Preferences.ErrorString : "Error";

			public static string YesString => Login.LoggedIn ? Modifications.Preferences.YesString : "Yes";

			public static string NoString => Login.LoggedIn ? Modifications.Preferences.NoString : "No";

			public static string WarningString => Login.LoggedIn ? Modifications.Preferences.WarningString : "OK";

			private const string CARRIER_PREF_SCAN_LOCATION_BARCODE_PICKUPS_DELIVERIES = "CARRIER_PREF_SCAN_LOCATION_BARCODE_PICKUPS_DELIVERIES",
			                     CARRIER_PREF_ADVANCED_REQUIRE_PODNAME = "CARRIER_PREF_ADVANCED_REQUIRE_PODNAME",
			                     CARRIER_PREF_ADVANCED_REQUIRE_PODSIG = "CARRIER_PREF_ADVANCED_REQUIRE_PODSIG",
			                     CARRIER_PREF_ADVANCED_SHOW_CHARGES = "CARRIER_PREF_ADVANCED_SHOW_CHARGES",
			                     CARRIER_PREF_DEVICE_EDITABLE_WGT_PCS_REF = "CARRIER_PREF_DEVICE_EDITABLE_WGT_PCS_REF",
			                     CLAIM_ACTION_BATCH = "CLAIM_ACTION_BATCH",
			                     CARRIER_PREF_ADVANCED_BARCODE_BATCH = "CARRIER_PREF_ADVANCED_BARCODE_BATCH",
			                     CARRIER_PREF_ADVANCED_BARCODE_PIECES = "CARRIER_PREF_ADVANCED_BARCODE_PIECES",
			                     CARRIER_PREF_CROSS_DOCK = "CARRIER_PREF_CROSS_DOCK",
			                     CARRIER_PREF_UNDELIVERABLE_KEEP_TRIPID = "CARRIER_PREF_UNDELIVERABLE_KEEP_TRIPID",
			                     CARRIER_PREF_UNDELIVERABLE_TOGGLE = "CARRIER_PREF_UNDELIVERABLE_TOGGLE",
			                     CARRIER_PREF_SHOW_PALLETS = "carrier_pref_show_pallets",
			                     CARRIER_PREF_TRIPID_PLUS_PIECE_COUNT = "CARRIER_PREF_TRIPID_PLUS_PIECE_COUNT";

			private static Dictionary<string, string> __Preferences;

			private static Dictionary<string, string> Prefs => __Preferences ??= RemoteService.Preferences;

			public static void ClearPreferences()
			{
				__Preferences = null;
			}

			private static bool BoolPreference( string preference )
			{
				if( Prefs.TryGetValue( preference, out var Value ) && !string.IsNullOrWhiteSpace( Value ) )
				{
					if( float.TryParse( Value, out var FValue ) )
						return (int)FValue != 0;

					return Value.Trim().ToUpper()[ 0 ] == 'T';
				}

				return false;
			}

			public static bool UseLocationBarcodes => Modifications.Preferences.UseLocationBarcodes || BoolPreference( CARRIER_PREF_SCAN_LOCATION_BARCODE_PICKUPS_DELIVERIES );
			public static bool RequiresPodName => Modifications.Preferences.RequiresPodName || BoolPreference( CARRIER_PREF_ADVANCED_REQUIRE_PODNAME );
			public static bool RequiresSignature => Modifications.Preferences.RequiresSignature || BoolPreference( CARRIER_PREF_ADVANCED_REQUIRE_PODSIG );
			public static bool RequiresReference => Modifications.Preferences.RequiresReference;

			public static bool ShowCharges => Modifications.Preferences.ShowCharges || BoolPreference( CARRIER_PREF_ADVANCED_SHOW_CHARGES );
			public static bool ShowPallets => Modifications.Preferences.ShowPallets || BoolPreference( CARRIER_PREF_SHOW_PALLETS );

			public static bool EditPiecesWeightReference => Modifications.Preferences.EditPiecesWeightReference || BoolPreference( CARRIER_PREF_DEVICE_EDITABLE_WGT_PCS_REF );

			public static bool PickupDeliveryByPieces => Modifications.Preferences.PickupDeliveryByPieces || BoolPreference( CARRIER_PREF_ADVANCED_BARCODE_PIECES );
			public static bool PickupDeliveryByPieceNumber => Modifications.Preferences.PickupDeliveryByPieceNumber || BoolPreference( CARRIER_PREF_TRIPID_PLUS_PIECE_COUNT );

			public static bool BatchMode => Modifications.Preferences.BatchMode || BoolPreference( CLAIM_ACTION_BATCH ) || BoolPreference( CARRIER_PREF_ADVANCED_BARCODE_BATCH );

			public static bool ScanBarcodeForUndeliverableReason => Modifications.Preferences.ScanBarcodeForUndeliverableReason;

			public static bool CrossDock => Login.LoggedIn && ( Modifications.Preferences.CrossDock || BoolPreference( CARRIER_PREF_CROSS_DOCK ) );
			public static bool UndeliverableKeepTripId => Modifications.Preferences.UndeliverableKeepTripId || BoolPreference( CARRIER_PREF_UNDELIVERABLE_KEEP_TRIPID );
			public static bool HideUndeliverableButton => Modifications.Preferences.HideUndeliverableButton || BoolPreference( CARRIER_PREF_UNDELIVERABLE_TOGGLE );

			public static bool HidePiecesAndWeight => Modifications.Preferences.HidePiecesAndWeight;
			public static bool HideReference => Modifications.Preferences.HideReference;
		}
	}
}