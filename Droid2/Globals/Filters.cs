using System;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Filters
		{
			public enum OPTION
			{
				VISABILITY,
				PRIMARY_SORT,
				SECONDARY_SORT
			}

			public enum VISIBILITY
			{
				ALL,
				PICKUP,
				DELIVERY,
				NEW,
				UNDEFINED
			}

			public enum SORT
			{
				STATUS,
				ID,
				DUE,
				ACCOUNT_ID,
				ACCOUNT_NUMBER,
				COMPANY,
				REFERENCE,
				NEW,
				SERVICE_LEVEL,
				UNDEFINED
			}

			public static Action<OPTION, SORT> OnFilterChange;
			public static Action<VISIBILITY> OnVisibilityChange;

			private static VISIBILITY _Visibility = VISIBILITY.UNDEFINED;

			public static VISIBILITY Visibility
			{
				get => _Visibility == VISIBILITY.UNDEFINED ? _Visibility = (VISIBILITY)Database.Config.VisibilityFilter : _Visibility;

				set
				{
					_Visibility = value;
					var Config = Database.Config;
					Config.VisibilityFilter = (byte)value;
					Database.Config = Config;

					OnVisibilityChange?.Invoke( value );
				}
			}

			private static SORT _PrimarySort = SORT.UNDEFINED;

			public static SORT PrimarySort
			{
				get => _PrimarySort == SORT.UNDEFINED ? _PrimarySort = (SORT)Database.Config.Sort1Filter : _PrimarySort;

				set
				{
					_PrimarySort = value;
					var Config = Database.Config;
					Config.Sort1Filter = (byte)value;
					Database.Config = Config;

					OnFilterChange?.Invoke( OPTION.PRIMARY_SORT, value );
				}
			}

			private static SORT SortSecondarySort = SORT.UNDEFINED;

			public static SORT SecondarySort
			{
				get => SortSecondarySort == SORT.UNDEFINED ? SortSecondarySort = (SORT)Database.Config.Sort2Filter : SortSecondarySort;

				set
				{
					SortSecondarySort = value;
					var Config = Database.Config;
					Config.Sort2Filter = (byte)value;
					Database.Config = Config;
					OnFilterChange?.Invoke( OPTION.SECONDARY_SORT, value );
				}
			}
		}
	}
}