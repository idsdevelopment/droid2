namespace Droid2
{
    public static partial class Globals
    {
        public static class Debug
        {
            public static MainActivity Activity;

            public static string Text
            {
                get
                {
#if DEBUG
                    return Activity.DebugText;
#else
                    return "";
#endif
                }

                // ReSharper disable once ValueParameterNotUsed
                set
                {
#if DEBUG
                    Activity.DebugText = value;
#endif
                }
            }

            public static void Beep()
            {
#if DEBUG
                Sounds.DebugBeep();
#endif
            }
        }
    }
}