﻿namespace Droid2
{
	public static partial class Globals
	{
		public static class Priority
		{
			public static class Switches
			{
				public static bool ClosePickupByBarcode;

				public static string SelectedPharmacy = "",
				                     SelectedRoute = "",
				                     SelectedLocation = "",
				                     SelectedServiceLevel;
			}
		}
	}
}