using CommsDb.Database;
using IdsService;
using IdsService.org.internetdispatcher.jbtest2;

namespace Droid2
{
	public static partial class Globals
	{
		public class IdsDatabase : Db<Client, AuthToken, remoteTrip>
		{
			internal const string DB_NAME = "IdsDatabase";

			public IdsDatabase( string dbName = DB_NAME ) : base( dbName )
			{
			}
		}

		private static IdsDatabase _Database;

		public static IdsDatabase Database => _Database ??= new IdsDatabase();
	}
}