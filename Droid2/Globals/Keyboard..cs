using System;
using Android.Content;
using Android.Views.InputMethods;
using WeakEvent;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Keyboard
		{
			private static InputMethodManager InputManager;

			public static bool Show
			{
				set
				{
					var Activity = Debug.Activity;

					Activity.RunOnUiThread( () =>
					                        {
						                        InputManager ??= (InputMethodManager)Activity.GetSystemService( Context.InputMethodService );

						                        var Focus          = Activity.Window.CurrentFocus;
						                        var FocusIsNotNull = Focus is not null;

						                        if( FocusIsNotNull )
							                        InputManager.HideSoftInputFromWindow( Focus.WindowToken, value ? HideSoftInputFlags.ImplicitOnly : HideSoftInputFlags.None );
/*						                        else
						                        {
//							                        InputManager.ToggleSoftInput( ShowFlags.Forced, value ? HideSoftInputFlags.ImplicitOnly : HideSoftInputFlags.None );
												}
*/
											} );
				}
			}

			public static bool Hide
			{
				set => Show = !value;
			}

		#region KeyPress
			public class OnKeyPressData
			{
				public bool Handled;
				public char Key;
			}

			public static readonly WeakEventSource<OnKeyPressData> _OnKeyPress = new();

			public static event EventHandler<OnKeyPressData> OnKeyPress
			{
				add => _OnKeyPress.Subscribe( value );
				remove => _OnKeyPress.Unsubscribe( value );
			}
		#endregion
		}
	}
}