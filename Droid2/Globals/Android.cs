using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;

// ReSharper disable UnusedVariable

namespace Droid2
{
	public static partial class Globals
	{
		public static class Android
		{
			private static int _MajorVersion = -1, _MinorVersion = -1, _Build = -1, AsInt = -1;

			// ReSharper disable once MemberHidesStaticFromOuterClass
			public static string Version
			{
				get
				{
					var Vsn = global::Android.OS.Build.VERSION.Release;
					var Temp = Vsn.Split( new[] { '.' }, StringSplitOptions.RemoveEmptyEntries );
					_MajorVersion = int.Parse( Temp[ 0 ] );
					_MinorVersion = ( Temp.Length > 1 ) ? int.Parse( Temp[ 1 ] ) : 0;
					_Build = _MinorVersion;
					return Vsn;
				}
			}

			public static int MajorVersion
			{
				get
				{
					if( _MajorVersion == -1 )
					{
						var V = Version;
					}
					return _MajorVersion;
				}
			}

			public static int MinorVersion
			{
				get
				{
					if( _MinorVersion == -1 )
					{
						var V = Version;
					}
					return _MinorVersion;
				}
			}

			public static int Build
			{
				get
				{
					if( _Build == -1 )
					{
						var V = Version;
					}
					return _Build;
				}
			}

			public static int VersionAsInt
			{
				get
				{
					if( AsInt == -1 ) // Needs {} for scope
						AsInt = int.Parse( Version.Replace( ".", "" ).Trim() );

					return AsInt;
				}
			}

			public static int ConvertPixelsToDp( float pixelValue )
			{
				return (int)( pixelValue / Debug.Activity.Resources.DisplayMetrics.Density );
			}

			public static int ConvertDpToPixels( float pixelValue )
			{
				return (int)( pixelValue * Debug.Activity.Resources.DisplayMetrics.Density );
			}

			public static void Restart()
			{
				Globals.Version.CheckForUpdates = false;

				Task.Run( () =>
				          {
					          Fragment.KillCurrentFragment();

					          OperatingMode.Mode = OperatingMode.OPERATING_MODE.UNDEFINED;

					          var Act = Debug.Activity;
					          if( Act != null )
					          {
						          Act.StoppingAll = true;
						          Act.StopService( Background.ServiceIntent );

						          var Service = Act.Binder.Service;
						          Service.StopService();
						          while( Service.ServiceStarted )
							          Thread.Sleep( 100 );

						          var Main = new Intent( Act, typeof( MainActivity ) );
						          Main.AddFlags( ActivityFlags.NewTask | ActivityFlags.ClearTop );
						          Act.StartActivity( Main );
					          }
				          } );
			}
		}
	}
}