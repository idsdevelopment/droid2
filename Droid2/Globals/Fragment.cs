#nullable enable

using System;
using System.Threading.Tasks;
using Droid2.Fragments.Standard;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Fragment
		{
			public static volatile AFragment? CurrentFragment,
			                                  PreviousFragment;

			[Obsolete]
			public static object ExecuteFragment( AFragment fragment, int loadAreaResourceId, object? arg = null ) => Debug.Activity.ExecuteFragment( fragment, loadAreaResourceId, arg );

			public static void ExecuteFragment( AFragment fragment, object? arg = null )
			{
				Debug.Activity.RunMenuOption( fragment, arg );
			}

			public static void ExecuteFragment( OperatingMode.OPERATING_MODE mode, object? arg = null )
			{
				Debug.Activity.RunMenuOption( mode, arg );
			}

			public static async void KillCurrentFragment()
			{
				await Task.Run( () =>
				                {
					                var Act = Debug.Activity;

					                if( Act is not null && CurrentFragment is not null )
						                Act.ClearLoadFrame();
				                } );
			}
		}
	}
}