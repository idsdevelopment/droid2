namespace Droid2
{
	public static partial class Globals
	{
		private static IdsService.IdsService _RemoteService;
		public static IdsService.IdsService RemoteService => _RemoteService ??= new IdsService.IdsService();
	}
}