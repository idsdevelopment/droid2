using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;

namespace Droid2
{
    public static partial class Globals
    {
        public static class Colours
        {
            /**
  * Color matrix that flips the components (<code>-1.0f * c + 255 = 255 - c</code>)
  * and keeps the alpha intact.
  */

            private static readonly float[] Negative =
            {
                -1.0f,
                0,
                0,
                0,
                255, // red
                0,
                -1.0f,
                0,
                0,
                255, // green
                0,
                0,
                -1.0f,
                0,
                255, // blue
                0,
                0,
                0,
                1.0f,
                0 // alpha  
            };

            public static void SetInvertedColours( Drawable drawable )
            {
                drawable.SetColorFilter( new ColorMatrixColorFilter( Negative ) );
            }
        }

        public static bool IsEqual( this ColorDrawable c1, ColorDrawable c2 )
        {
            var C1 = c1.Color;
            var C2 = c2.Color;
            return ( C1.A == C2.A ) && ( C1.R == C2.R ) && ( C1.G == C2.G ) && ( C1.B == C2.B );
        }

        public static Drawable Color( int resourceColour )
        {
            return ContextCompat.GetDrawable( Debug.Activity, resourceColour );
        }
    }
}