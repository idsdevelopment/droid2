using System;
using CommsDb.Database;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Distance
		{
		#if DEBUG
			public const double MIN_DISTANCE = 5; // 5 Metres
			public const int MAX_STATIONARY_MINUTES = 1;
		#else
			public const double MIN_DISTANCE = 50; // 50 Metres
			public const int MAX_STATIONARY_MINUTES = 2;
		#endif
			public const int MAX_UPDATE_POINTS = 100;

			private static readonly Points   LastPoints = new();
			private static          DateTime LastTime;
			private static readonly object   LockObject = new();

			public static bool IsDistanceOk( double latitude, double longitude )
			{
				lock( LockObject )
				{
					if( LastPoints.IsZero )
					{
						var F = LastPoints.From;
						F.Latitude = latitude;
						F.Longitude = longitude;
						return true;
					}

					var T = LastPoints.To;
					T.Latitude = latitude;
					T.Longitude = longitude;

					var Dist = Math.Abs( LastPoints.DistanceTo( Points.MEASUREMENT.METRES ) );

					Debug.Text = Dist.ToString( "N" );
					var Now = DateTime.Now;

					if( ( Dist >= MIN_DISTANCE ) || ( ( Now - LastTime ).TotalMinutes >= MAX_STATIONARY_MINUTES ) )
					{
						LastTime = Now;

						// Make this to next from
						var Temp = LastPoints.From;
						LastPoints.From = T;
						LastPoints.To = Temp;
						return true;
					}

					return false;
				}
			}
		}
	}
}