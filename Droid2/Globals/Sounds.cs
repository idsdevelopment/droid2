#nullable enable

using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Media;
using Android.Support.V4.Content;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Sounds
		{
			public static async void PlaySound( int sound, bool killSound )
			{
				if( !killSound )
				{
					await Task.Run( () =>
					                {
						                var Player = MediaPlayer.Create( Debug.Activity, sound );

						                if( Player != null )
						                {
							                Player.Start();

							                do
								                Thread.Sleep( 50 );
							                while( Player.IsPlaying );

							                Debug.Activity.RunOnUiThread( () =>
							                                              {
								                                              Player.Release();
								                                              Player = null;
							                                              } );
						                }
					                } );
				}
			}

			[Obsolete]
			public static void PlayNewTripSound()
			{
				PlayNotification( DisableNewTripSound, "New trip available", 0, Resource.Color.Yellow );
			}

			[Obsolete]
			public static void PlayDeletedTripSound()
			{
				PlayNotification( DisableDeletedTripSound, "Trip deleted", 1, Resource.Color.DarkViolet );
			}

			public static void PlayBadScanSound()
			{
				PlaySound( Resource.Raw.BadScan, false );
			}

			public static void DebugBeep()
			{
				PlaySound( Resource.Raw.DebugBeep, false );
			}

			public static void WarehouseNotifySound()
			{
				PlaySound( Resource.Raw.WarehouseNotify, false );
			}

			[Obsolete]
			private static void PlayNotification( bool killNotification, string text, int notificationId, int lightColour )
			{
				if( !DisableNotifications && !killNotification )
				{
					var Ctx = Application.Context;

					Debug.Activity.RunOnUiThread( async () =>
					                              {
						                              var NotifySound = RingtoneManager.GetActualDefaultRingtoneUri( Ctx, RingtoneType.Notification );
						                              var R           = RingtoneManager.GetRingtone( Ctx, NotifySound )!;
						                              R.Play();

						                              await Task.Run( () =>
						                                              {
							                                              while( R.IsPlaying )
								                                              Thread.Sleep( 100 );
						                                              } );
					                              } );

					var Act         = Debug.Activity;
					var LightColour = new Color( ContextCompat.GetColor( Act, lightColour ) ).ToArgb();

					var PIntent = PendingIntent.GetActivity( Act, notificationId, Act.Intent, PendingIntentFlags.OneShot );

 #pragma warning disable 8602
					var Notification = new Notification.Builder( Act )
					                   .SetContentTitle( "IDS" )
					                   .SetContentText( text )
					                   .SetSmallIcon( Resource.Drawable.ic_drive_eta_white_24dp )
					                   .SetDefaults( NotificationDefaults.Sound | NotificationDefaults.Vibrate )
					                   .SetSound( RingtoneManager.GetDefaultUri( RingtoneType.Notification ) )
					                   .SetLights( LightColour, 500, 500 )
					                   .SetContentIntent( PIntent )
					                   .Build();
#pragma warning restore 8602

					Notification!.Flags |= NotificationFlags.AutoCancel;

					var NotificationManager = Act.GetSystemService( Context.NotificationService ) as NotificationManager;
					NotificationManager?.Notify( notificationId, Notification );
				}
			}
		}

		public static bool DisableNewTripSound,
		                   DisableDeletedTripSound,
		                   DisableNotifications;

		public static bool DisableTripSounds
		{
			set
			{
				DisableDeletedTripSound = value;
				DisableNewTripSound     = value;
			}
		}
	}
}