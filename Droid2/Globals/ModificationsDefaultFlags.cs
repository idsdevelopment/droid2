using System;
using Android.Views;
using Android.Widget;
using Droid2.Fragments.Standard;
using Droid2.Fragments.Standard.Claims.CrossDockFragment;
using Droid2.Fragments.Standard.Claims.StandardFragment;
using Droid2.Fragments.Standard.PickupDeliveries;

namespace Droid2
{
	public static partial class Globals
	{
		public class DriverPickupSelector : Modifications.ProgramEntry
		{
			public override Type Program
			{
				get => Preferences.PickupDeliveryByPieceNumber ? typeof( DeliveriesByPieceNumber ) : typeof( DriverPickup );
				set { }
			}
		}

		public static partial class Modifications
		{
			public const int DEFAULT_LAYOUT = Resource.Id.standardOptions;

			private static readonly ProgramEntry[] DefaultPrograms =
			{
				new DriverPickupSelector
				{
					Mode = OperatingMode.OPERATING_MODE.DRIVER_PICKUP,
					Program = typeof(DriverPickup),
					RadioButton = Resource.Id.driverPickupButton
				},
				new()
				{
					      Mode = OperatingMode.OPERATING_MODE.CLAIMS_STANDARD,
					      Program = typeof( ClaimsStandard ),
					      RadioButton = Resource.Id.claimsStandardButton
				      },
				new()
				{
					      Mode = OperatingMode.OPERATING_MODE.CROSS_DOCK,
					      Program = typeof( CrossDock ),
					      RadioButton = Resource.Id.crossDockButton
				      }

			};

			private static readonly Action OnLoggedIn = () =>
			{
				var RButton = Debug.Activity.FindViewById<RadioButton>( Resource.Id.crossDockButton );
				if( RButton != null )
					RButton.Visibility = Globals.Preferences.CrossDock ? ViewStates.Visible : ViewStates.Gone;
			};

			// For Program Execution
			// ReSharper disable once UnusedMember.Local
			private static Property DefaultProgramsProperty = new( "", DEFAULT_LAYOUT, "",
			                                                       new PreferenceOverrides(),
			                                                       DefaultPrograms,
			                                                       OnLoggedIn );

			public static Property IsTesting = new( "AppDemo", DEFAULT_LAYOUT, "AppDemo",
			                                        new PreferenceOverrides(),
			                                        DefaultPrograms,
			                                        OnLoggedIn );

			public static Property IsCoral = new( "Coral", DEFAULT_LAYOUT, "Coral",
			                                      new PreferenceOverrides(),
			                                      DefaultPrograms,
			                                      OnLoggedIn );

			public static Property IsDemo => IsTesting;
		}
	}
}