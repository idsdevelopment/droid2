using System.Threading;
using System.Threading.Tasks;

namespace Droid2
{
    public static partial class Globals
    {
        public static async void Wait( int delayMilliseconds )
        {
            await Task.Run( () => { Thread.Sleep( delayMilliseconds ); } );
        }
    }
}