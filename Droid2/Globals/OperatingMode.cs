#nullable enable

using Droid2.Fragments.Standard;

namespace Droid2
{
	public static partial class Globals
	{
		public static class OperatingMode
		{
			public enum MANUAL_SCAN : byte
			{
				AUTOMATIC_SCAN,
				MANUAL_SCAN,
				UNDEFINED
			}

			public enum OPERATING_MODE : byte
			{
				UNDEFINED,
				DRIVER_PICKUP,
				CLAIMS_STANDARD,
				METRO_WAREHOUSE_SCAN,
				METRO_DRIVER_VERIFICATION,
				METRO_DSD_DELIVERIES,
				METRO_CLAIMS,
				PRIORITY_PICKUP_BY_BARCODE,
				PRIORITY_DELIVERIES,
				CROSS_DOCK,
				METRO_RETURNS,
				INTERMOUNTAIN_PICKUP,
				PML_RETURNS,
				PML_CLAIMS,
				PML_CROSS_DOCK,
				PML_AUDIT,
				BLANK,
				METRO_BUILD,
				METRO_LOAD,
				METRO_UNLOAD,
				METRO_CREATE_EDIT,
				METRO_DESTRUCTION_TRAILER,
				METRO_FOREIGN_WAYBILL,
				METRO_RC_CLAIM,
				PML_STAR_TRACK,
				PML_STAR_TRACK_PICKUPS,
				PML_STAR_TRACK_AUDIT,
				LINFOX_SCAN1_SCAN2,
				LINFOX_STAR_TRACK_PICKUPS,
				LINFOX_STAR_TRACK_AUDIT,
				METRO_TOTE_PICKUP
			}

			private static OPERATING_MODE _Mode = OPERATING_MODE.UNDEFINED;

			private static MANUAL_SCAN _ManualScan = MANUAL_SCAN.UNDEFINED;
			public static AFragment Program => Modifications.Program( Mode );

			public static OPERATING_MODE Mode
			{
				get
				{
					var RetVal = _Mode == OPERATING_MODE.UNDEFINED ? _Mode = (OPERATING_MODE)Database.Config.Mode : _Mode;

					if( ( RetVal == OPERATING_MODE.UNDEFINED ) && Login.LoggedIn )
						_Mode = RetVal = Modifications.DefaultProgramMode;

					return RetVal;
				}

				set
				{
					_Mode = value;
					var Config = Database.Config;
					Config.Mode     = (byte)value;
					Database.Config = Config;
					Modifications.SetMode( value );
				}
			}

			public static MANUAL_SCAN ManualScan
			{
				get => _ManualScan == MANUAL_SCAN.UNDEFINED ? _ManualScan = (MANUAL_SCAN)Database.Config.ManualScan : _ManualScan;
				set
				{
					_ManualScan = value;
					var Config = Database.Config;
					Config.ManualScan = (byte)value;
					Database.Config   = Config;
				}
			}
		}
	}
}