using System;
using System.Linq;
using static CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>;
using DateTime = Utils.DateTimeExtensions;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Settings
		{
			public static int PollingInterval = 15;

			private static readonly DateTime.WEEK_DAY[] TwoWeeks =
			{
				DateTime.WEEK_DAY.MONDAY,
				DateTime.WEEK_DAY.TUESDAY,
				DateTime.WEEK_DAY.WEDNESDAY,
				DateTime.WEEK_DAY.THURSDAY,
				DateTime.WEEK_DAY.FRIDAY,
				DateTime.WEEK_DAY.SATURDAY,
				DateTime.WEEK_DAY.SUNDAY,
				DateTime.WEEK_DAY.MONDAY,
				DateTime.WEEK_DAY.TUESDAY,
				DateTime.WEEK_DAY.WEDNESDAY,
				DateTime.WEEK_DAY.THURSDAY,
				DateTime.WEEK_DAY.FRIDAY,
				DateTime.WEEK_DAY.SATURDAY,
				DateTime.WEEK_DAY.SUNDAY
			};

			public static bool PickupDeliveriesLandscape
			{
				get => Database.Config.PickupDeliveriesLandscape;
				set
				{
					var Config = Database.Config;
					Config.PickupDeliveriesLandscape = value;
					Database.Config                  = Config;
				}
			}

			public static bool ClaimsLandscape
			{
				get => Database.Config.ClaimsLandscape;
				set
				{
					var Config = Database.Config;
					Config.ClaimsLandscape = value;
					Database.Config        = Config;
				}
			}


			public static bool PmlAuditLandscape
			{
				get => Database.Config.PmlAuditLandscape;
				set
				{
					var Config = Database.Config;
					Config.PmlAuditLandscape = value;
					Database.Config          = Config;
				}
			}

			public static void GetLoginDefaults( out string accountId, out string userName )
			{
				var Config = Database.Config;
				accountId = Config.AccountId;
				userName  = Config.UserName;
			}

			public static void GetLoginDefaults( out string accountId, out string userName, out string password )
			{
				var Config = Database.Config;
				accountId = Config.AccountId;
				userName  = Config.UserName;
				password  = Config.Password;
			}

			public static void GetLoginAlternateDefaults( out string accountId, out string userName )
			{
				var Config = Database.Config;
				accountId = Config.AccountId;
				userName  = Config.AlternateUserName;
			}

			public static void SaveLogin( string accountId, string userName, string password, ushort pollingInterval, byte startHour, byte endHour, byte[] activeDays )
			{
				var Config = Database.Config;
				Config.AccountId       = accountId;
				Config.UserName        = userName;
				Config.Password        = password;
				Config.PollingInterval = pollingInterval;
				Config.StartHour       = startHour;
				Config.EndHour         = endHour;
				Config.ByteActiveDays  = activeDays;
				Database.Config        = Config;
			}

			public static void SaveAlternateLogin( string userName )
			{
				var Config = Database.Config;
				Config.AlternateUserName = userName;
				Database.Config          = Config;
			}

			public static bool InActivePeriod( Configuration config, out TimeSpan timeRemaining )
			{
				var Now  = System.DateTime.Now;
				var Hour = Now.Hour;
				var Day  = DateTime.ToWeekDay( Now );

				if( ( Hour >= config.StartHour ) && ( Hour <= config.EndHour ) )
				{
					if( config.ActiveDays.Any( d => d == Day ) )
					{
						timeRemaining = TimeSpan.MinValue;

						return true;
					}
				}

				// Find Current Day
				var CurrentDayNdx = 0;

				for( ; CurrentDayNdx < TwoWeeks.Length; CurrentDayNdx++ )
				{
					if( Day == TwoWeeks[ CurrentDayNdx ] )
						break;
				}

				// Find Next Allowed Day In Config
				for( var NextDayNdx = CurrentDayNdx + 1; NextDayNdx < TwoWeeks.Length; NextDayNdx++ )
				{
					var NextDay = TwoWeeks[ NextDayNdx ];

					if( config.ActiveDays.Any( activeDay => activeDay == NextDay ) )
					{
						var DiffHours = ( ( NextDayNdx - CurrentDayNdx ) * 24 ) + config.StartHour;
						var Diff      = TimeSpan.FromHours( DiffHours ) - Now.TimeOfDay;
						timeRemaining = Diff;

						return false;
					}
				}

				timeRemaining = TimeSpan.MaxValue;

				return false;
			}

			public static bool InActivePeriod( Configuration config )
			{
				return InActivePeriod( config, out _ );
			}

			public static bool InActivePeriod()
			{
				return InActivePeriod( Database.Config );
			}
		}
	}
}