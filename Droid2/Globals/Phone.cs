using Android.Content;
using Android.Net;

namespace Droid2
{
    public static partial class Globals
    {
        public static class Phone
        {
            public static void Dial( string phoneNumber )
            {
                var PhoneNumber = Uri.Parse( $"tel:{phoneNumber.Trim()}" );
                var Intent = new Intent( global::Android.Content.Intent.ActionDial, PhoneNumber );
                Debug.Activity.StartActivity( Intent );
            }
        }
    }
}