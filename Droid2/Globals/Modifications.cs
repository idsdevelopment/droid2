using System;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using Android.Widget;
using Droid2.Fragments.Standard;

namespace Droid2
{
	public static partial class Globals
	{
		public static partial class Modifications
		{
			private const string ALWAYS_HIDDEN = "~Always Hidden~";

			public class PreferenceOverrides
			{
				public string CancelString  = "Cancel",
				              OkString      = "OK",
				              ErrorString   = "Error",
				              YesString     = "Yes",
				              NoString      = "No",
				              WarningString = "OK";

				public bool EnableFilter;

				public bool UseLocationBarcodes,
				            RequiresPodName,
				            RequiresSignature,
				            ShowCharges,
				            EditPiecesWeightReference,
				            PickupDeliveryByPieces,
				            PickupDeliveryByPieceNumber,
				            RequireAcceptanceForDeliveries,
				            BatchMode,
				            ScanBarcodeForUndeliverableReason,
				            CrossDock,
				            UndeliverableKeepTripId,
				            HideUndeliverableButton,
				            ShowPallets,
				            RequiresReference,
				            HidePiecesAndWeight,
				            HideReference;
			}

			public class ProgramEntry
			{
				// ReSharper disable once MemberHidesStaticFromOuterClass
				public virtual Type Program { get; set; }

				// ReSharper disable once MemberHidesStaticFromOuterClass
				public string Filter = "";

				public bool IsDefault;

				public OperatingMode.OPERATING_MODE Mode;

				public int RadioButton;
			}

			public class Property
			{
				private static bool HasNoMods,
				                    HasNoModsValue;

				public bool Value
				{
					get
					{
						if( !HasProperty )
						{
							HasProperty = true;
							_Value      = UpperCaseResellerId == ResellerId;

							if( _Value )
							{
								var CurrentMode = OperatingMode.Mode;

								if( AllowedPrograms.All( allowedMode => allowedMode.Mode != CurrentMode ) )
									OperatingMode.Mode = AllowedPrograms[ 0 ].Mode;
							}
						}

						return _Value;
					}
				}

				// ReSharper disable once MemberHidesStaticFromOuterClass
				public static bool NoModifications
				{
					get
					{
						if( !HasNoMods )
						{
							HasNoMods      = true;
							HasNoModsValue = !Properties.Any( prop => prop );
						}

						return HasNoModsValue;
					}
				}

				// First Mode Is Default
				public Property( string resellerId, int optionsPanel, string updateLocation, PreferenceOverrides preferenceOverrides, ProgramEntry[] allowed = null,
				                 Action onLogin = null )
				{
					ResellerId     = resellerId.Trim().ToUpper();
					UpdateLocation = updateLocation.Trim();
					Properties.Add( this );
					PreferenceOverrides = preferenceOverrides;
					OptionsPanel        = optionsPanel;
					AllowedPrograms     = allowed ?? DefaultPrograms;
					OnLogin             = onLogin;
				}

				private           bool           HasProperty;
				private           bool           _Value;
				public readonly   ProgramEntry[] AllowedPrograms;
				internal readonly int            OptionsPanel;

				internal readonly PreferenceOverrides PreferenceOverrides;

				// ReSharper disable once MemberHidesStaticFromOuterClass
				internal readonly string ResellerId;

				// ReSharper disable once MemberHidesStaticFromOuterClass
				internal readonly string UpdateLocation;

				// ReSharper disable once MemberHidesStaticFromOuterClass
				internal Action OnLogin;

				public static implicit operator bool( Property prop ) => prop.Value;
			}

			private static string _ResellerId;

			private static List<Property> _Properties; // Don't use static initialisation throws TypeInitialization exception 

			// ReSharper disable once MemberHidesStaticFromOuterClass
			private static PreferenceOverrides _Preferences;

			public static string Filter = "",
			                     Fax    = "";

			private static List<Property> Properties => _Properties ??= new List<Property>();

			public static string ResellerId
			{
				get
				{
					if( string.IsNullOrWhiteSpace( _ResellerId ) )
						_ResellerId = RemoteService.Preferences[ IdsService.IdsService.RESELLER_ID ].Trim();

					return _ResellerId;
				}
			}

			public static string UpperCaseResellerId => ResellerId.ToUpper();

			private static Property ResellerProperty => GetResellerProperty( UpperCaseResellerId ) ?? GetResellerProperty( "" );

			public static string UpdateLocation => ResellerProperty.UpdateLocation;

			public static bool LoggedIn
			{
				set
				{
					if( value )
					{
						Debug.Activity.RunOnUiThread( () =>
						                              {
							                              foreach( var Property in Properties )
								                              Property.OnLogin?.Invoke();
						                              } );
					}
				}
			}

			// ReSharper disable once MemberHidesStaticFromOuterClass
			public static PreferenceOverrides Preferences
			{
				get
				{
					if( _Preferences != null )
						return _Preferences;

					var Prop = ResellerProperty;
					_Preferences = Prop.PreferenceOverrides ?? new PreferenceOverrides();

					return _Preferences;
				}
			}

			public static OperatingMode.OPERATING_MODE DefaultProgramMode
			{
				get
				{
					var Prop = ResellerProperty;

					if( Prop is not null && ( Prop.AllowedPrograms.Length > 0 ) )
					{
						var Default = ( from A in Prop.AllowedPrograms
						                where A.IsDefault
						                select A ).FirstOrDefault() ?? Prop.AllowedPrograms[ 0 ];

						return Default.Mode;
					}

					return OperatingMode.OPERATING_MODE.UNDEFINED;
				}
			}

			public static bool NoModifications => Property.NoModifications;

			private static Property GetResellerProperty( string resellerId )
			{
				var Props = Properties;
				return Props.FirstOrDefault( property => resellerId == property.ResellerId );
			}

			private static void RadioButtonOnClick( object sender, EventArgs eventArgs )
			{
				if( sender is RadioButton CheckBox )
					Debug.Activity.RunMenuOption( (OperatingMode.OPERATING_MODE)(int)CheckBox.Tag );
			}

			public static AFragment Program( OperatingMode.OPERATING_MODE mode ) =>
				(
					from AllowedMode in ResellerProperty.AllowedPrograms
					where AllowedMode.Mode == mode
					select Activator.CreateInstance( AllowedMode.Program ) as AFragment ).FirstOrDefault();

			public static void SetMode( OperatingMode.OPERATING_MODE mode )
			{
				Debug.Activity.RunOnUiThread( () =>
				                              {
					                              foreach( var Property in Properties )
					                              {
						                              foreach( var ProgramEntry in Property.AllowedPrograms )
						                              {
							                              if( ProgramEntry.Mode == mode )
							                              {
								                              var Panel = Debug.Activity.FindViewById( Property.OptionsPanel );

								                              if( Panel is not null )
								                              {
									                              var Button = Panel.FindViewById<RadioButton>( ProgramEntry.RadioButton );

									                              if( Button is not null )
										                              Button.Checked = true;

									                              break;
								                              }
							                              }
						                              }
					                              }
				                              } );
			}

			public static void HideAll()
			{
				Debug.Activity.RunOnUiThread( () =>
				                              {
					                              Debug.Activity.FindViewById( DEFAULT_LAYOUT ).Visibility = ViewStates.Gone;

					                              foreach( var Property in Properties )
					                              {
						                              var Layout = Debug.Activity.FindViewById( Property.OptionsPanel );

						                              if( Layout != null )
							                              Layout.Visibility = ViewStates.Gone;
					                              }
				                              } );
			}

			public static void EnableModifications()
			{
				Debug.Activity.RunOnUiThread( () =>
				                              {
					                              View           Layout = null;
					                              ProgramEntry[] Modes  = null;

					                              foreach( var Property in Properties )
					                              {
						                              if( Property )
						                              {
							                              Layout = Debug.Activity.FindViewById( Property.OptionsPanel );
							                              Modes  = Property.AllowedPrograms;

							                              break;
						                              }
					                              }

					                              if( Layout is null )
					                              {
						                              Layout = Debug.Activity.FindViewById( DEFAULT_LAYOUT );
						                              Modes  = DefaultPrograms;
					                              }

					                              Layout.Visibility = ViewStates.Visible;

					                              var CurrentMode = OperatingMode.Mode;

					                              var ModeFound = false;

					                              static string[] SplitFilter( string filter )
					                              {
						                              return ( from F in filter.ToLower().Split( ',' )
						                                       select F.Trim() ).ToArray();
					                              }

					                              var Filters  = SplitFilter( Filter );
					                              var DoFilter = Preferences.EnableFilter;

					                              foreach( var Mode in Modes )
					                              {
						                              var Button = Debug.Activity.FindViewById<RadioButton>( Mode.RadioButton );

						                              if( Button is not null )
						                              {
							                              if( !DoFilter || Filters.Intersect( SplitFilter( Mode.Filter ) ).Any() )
							                              {
								                              Button.Click += RadioButtonOnClick;
								                              Button.Tag   =  (int)Mode.Mode;
								                              var Match = CurrentMode == Mode.Mode;
								                              Button.Checked = Match;

								                              if( Match )
									                              ModeFound = true;
							                              }
							                              else
								                              Button.Visibility = ViewStates.Gone;
						                              }
					                              }

					                              if( !ModeFound )
						                              OperatingMode.Mode = Modes[ 0 ].Mode;
				                              } );
			}
		}
	}
}