using System.Linq;
using System.Text;

namespace Droid2
{
    public static class Extensions
    {
        public static bool IsInt( this string str )
        {
            return int.TryParse( str, out _ );
        }

        public static bool IndexOfAll( this string str, char[] matchAll )
        {
            return matchAll.All( matchChar => str.IndexOf( matchChar ) >= 0 );
        }

        public static string PriorityBarcodeFilter( this string str )
        {
            var Temp = new StringBuilder();
            foreach( var C in str )
            {
                if( ( ( C >= 'A' ) && ( C <= 'Z' ) ) || ( ( C >= 'a' ) && ( C <= 'z' ) ) || ( ( C >= '0' ) && ( C <= '9' ) ) || ( C == '+' ) || ( C == '-' ) )
                    Temp.Append( C );
            }

            return Temp.ToString();
        }

        public static string Last( this string str, int len )
        {
            var L = str.Length;
            return L > len ? str[ (L - len).. ] : str;
        }
    }
}