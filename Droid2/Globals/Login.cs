using System.Globalization;
using IdsService;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Login
		{
			private static bool _LoggedIn;

			public static bool LoggedIn
			{
				get => _LoggedIn;
				set
				{
					_LoggedIn = value;
					Modifications.LoggedIn = value;
					if( Database != null )
						Database.LoggedIn = value;
					if( !value )
					{
						RemoteService.ClearPreferences();
						Preferences.ClearPreferences();
						GpsEnabled = false;
					}
				}
			}

			public static string UserName { get; private set; } = "";
			public static string CarrierId { get; private set; } = "";

			public static bool Execute()
			{
				if( Debug.Activity.ExecuteFragment( new Fragments.Standard.Login() ) is User User )
				{
					UserName = CultureInfo.InvariantCulture.TextInfo.ToTitleCase( User.UserName.ToLower() );
					CarrierId = CultureInfo.InvariantCulture.TextInfo.ToTitleCase( User.CarrierId.ToLower() );
					return true;
				}

				return false;
			}

			public static async void ReLogin()
			{
				Database.DeleteAllTripsAsync().Wait();
				Settings.GetLoginDefaults( out var AId, out var UName, out var Pw );
				await RemoteService.Connect( AId, UName, Pw );
			}
		}
	}
}