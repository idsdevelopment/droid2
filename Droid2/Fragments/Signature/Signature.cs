using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Xamarin.Controls;
using PointF = System.Drawing.PointF;

namespace Droid2.Signature
{
	public class SignatureResult
	{
		public string Points;
		public int Height, Width;
	}

    [Obsolete]
    public class SignatureFragment : Fragment
	{
		private const int MARGIN = 5,
		                  QUALITY = 250;

		private RelativeLayout SignatureLayout;

		private SignaturePadView SignaturePad;

		private string SignaturePoints;
		private int Height, Width;

		private bool StopWaitingSignature;

		public override View OnCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
		{
			return inflater.Inflate( Resource.Layout.Fragment_Signature, container, false );
		}

		private bool Created;

		public override void OnResume()
		{
			base.OnResume();

			if( Created )
				return;

			Created = true;
			var V = View;

			SignaturePad = V.FindViewById<SignaturePadView>( Resource.Id.signatureView );

			SignatureLayout = V.FindViewById<RelativeLayout>( Resource.Id.signatureLayout );
			SignatureLayout.Touch += ( sender, args ) => { args.Handled = true; };

			SignatureLayout.FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
			                                                                          {
				                                                                          SignaturePoints = null;
				                                                                          StopWaitingSignature = true;
			                                                                          };


			// Ok Button
			SignatureLayout.FindViewById<Button>( Resource.Id.okButton ).Click += ( sender, args ) =>
			                                                                      {
				                                                                      var Points = SignaturePad.Points;
				                                                                      float MaxX = float.NegativeInfinity,
				                                                                            MaxY = float.NegativeInfinity,
				                                                                            MinX = float.PositiveInfinity,
				                                                                            MinY = float.PositiveInfinity;

				                                                                      foreach( var Point in Points )
				                                                                      {
					                                                                      MaxX = Math.Max( MaxX, Point.X );
					                                                                      MaxY = Math.Max( MaxY, Point.Y );

					                                                                      MinX = Math.Min( MinX, Point.X );
					                                                                      MinY = Math.Min( MinY, Point.Y );
				                                                                      }


				                                                                      Width = (int)( MaxX - MinX );
				                                                                      Height = (int)( MaxY - MinY );

				                                                                      var Longest = Math.Max( Width, Height );
				                                                                      var Scale = Longest >= QUALITY ? (float)QUALITY / Longest : 1;

				                                                                      Width = (int)( Width * Scale ) + 2 * MARGIN;
				                                                                      Height = (int)( Height * Scale ) + 2 * MARGIN;

				                                                                      var XAdj = (int)Math.Round( -MinX * Scale, MidpointRounding.AwayFromZero ) + MARGIN;
				                                                                      var YAdj = (int)Math.Round( -MinY * Scale, MidpointRounding.AwayFromZero ) + MARGIN;

				                                                                      var SigPoints = new StringBuilder();
				                                                                      var First = true;

				                                                                      void PointString( bool mouseDown, PointF point )
				                                                                      {
					                                                                      if( !First )
						                                                                      SigPoints.Append( ',' );
					                                                                      else
						                                                                      First = false;

					                                                                      var StrX = ( (int)point.X + XAdj ).ToString();
					                                                                      var StrY = ( (int)point.Y + YAdj ).ToString();
					                                                                      SigPoints.Append( mouseDown ? "1:" : "0:" ).Append( StrX ).Append( ':' ).Append( StrY );
				                                                                      }

				                                                                      var NeedMoveTo = true;
				                                                                      var Last = PointF.Empty;

				                                                                      foreach( var Point in Points )
				                                                                      {
					                                                                      var P = Point;

					                                                                      // If empty discontinuous line
					                                                                      if( P.IsEmpty )
						                                                                      NeedMoveTo = true;
					                                                                      else
					                                                                      {
						                                                                      P.X = (float)Math.Round( P.X * Scale, MidpointRounding.AwayFromZero );
						                                                                      P.Y = (float)Math.Round( P.Y * Scale, MidpointRounding.AwayFromZero );

						                                                                      if( NeedMoveTo )
						                                                                      {
							                                                                      NeedMoveTo = false;
							                                                                      if( !Last.IsEmpty )
								                                                                      PointString( false, Last ); // Move from
							                                                                      PointString( false, P );        // Move To
							                                                                      PointString( true, P );
						                                                                      }
						                                                                      else if( ( (int)Last.X != (int)P.X ) || ( (int)Last.Y != (int)P.Y ) ) // Skip Duplicates
							                                                                      PointString( true, P );
					                                                                      }

					                                                                      Last = P;
				                                                                      }

				                                                                      if( !Last.IsEmpty )
					                                                                      PointString( false, Last ); // Force Mouse Up At End

				                                                                      SignaturePoints = SigPoints.ToString();

				                                                                      StopWaitingSignature = true;
			                                                                      };


			Activity.RunOnUiThread( () =>
			                        {
				                        SignaturePad.Caption.Text = "Signature";
				                        SignaturePad.Caption.SetTypeface( Typeface.Serif, TypefaceStyle.BoldItalic );
				                        SignaturePad.Caption.SetTextSize( ComplexUnitType.Sp, 16f );

				                        SignaturePad.SignaturePrompt.Text = " ";

				                        /*
				                                SignaturePad.SignaturePrompt.SetTypeface( Typeface.SansSerif, TypefaceStyle.Normal );
				                                SignaturePad.SignaturePrompt.SetTextSize( ComplexUnitType.Sp, 32f );
				                        */

				                        SignaturePad.BackgroundColor = Color.Rgb( 255, 255, 200 ); // a light yellow.
				                        SignaturePad.StrokeColor = Color.Black;
				                        SignaturePad.StrokeWidth = Globals.Android.ConvertDpToPixels( 2 );

				                        /*
				                                SignaturePad.BackgroundImageView.SetAdjustViewBounds( true );
				                                var Layout = new RelativeLayout.LayoutParams( 0, 0 );
				                                Layout.AddRule( LayoutRules.CenterInParent );
				                                SignaturePad.BackgroundImageView.LayoutParameters = Layout;
				                        */

				                        Globals.Keyboard.Hide = true;
			                        } );

			base.OnResume();
		}

		public async Task<SignatureResult> CaptureSignature()
		{
			Globals.Keyboard.Hide = true;

			StopWaitingSignature = false;
			SignaturePoints = null;
			SignaturePad.Clear();
			return await Task.Run( () =>
			                       {
				                       while( !StopWaitingSignature )
					                       Thread.Sleep( 250 );

				                       return new SignatureResult { Points = SignaturePoints, Width = Width, Height = Height };
			                       } );
		}
	}
}