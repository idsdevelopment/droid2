#nullable enable

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V4.Content;
using Android.Views;
using CommsDb.Database;
using Droid2.Barcode;
using IdsService;
using IdsService.org.internetdispatcher.jbtest2;
using Service.Interfaces;

namespace Droid2.Fragments.Standard
{
#pragma warning disable 618
	public abstract class AFragment : Fragment
#pragma warning restore 618
	{
		public string FragmentType => GetType().Name;

		protected new View?   View      { get; private set; }
		public        bool    Destroyed { get; private set; }
		public        object? Result    { get; protected set; }

		protected Action NotifyOutFifoEmpty
		{
			get => Globals.Database.NotifyOutFifoEmpty!;
			set => Globals.Database.NotifyOutFifoEmpty = value;
		}

		protected bool Gps
		{
			get => Globals.GpsEnabled;
			set => Globals.GpsEnabled = value;
		}

		protected bool IgnoreTripAddress
		{
			get => Globals.Database.IgnoreTripAddress;
			set => Globals.Database.IgnoreTripAddress = value;
		}

		protected int TripCount
		{
			set
			{
				if( Executing )
					Activity.TripCount = value;
			}
		}

		protected int PieceCount
		{
			set
			{
				if( Executing )
					Activity.PieceCount = value;
			}
		}

		protected int ScannedCount
		{
			set
			{
				if( Executing )
					Activity.ScannedCount = value;
			}
		}

		public bool Online
		{
			get => _OnLine;
			set
			{
				if( Executing )
				{
					_OnLine = value;
					OnPing( value );
				}
			}
		}

		public new int Id { get; }

		protected ScreenOrientation Orientation
		{
			get => _Orientation;
			set
			{
				_Orientation                  = value;
				Activity.RequestedOrientation = value;
			}
		}

		public new MainActivity     Activity => Globals.Debug.Activity;
		public     IdsDroidService? Service  => Activity.Binder?.Service;

		public bool Executing => _Executing;

		public bool Created { get; private set; }

		[Obsolete]
		protected new FragmentManager ChildFragmentManager => ( Globals.Android.VersionAsInt < 420 ? Activity.FragmentManager : base.ChildFragmentManager )!;

		protected static STATUS[] TripStatusFilterSet
		{
			get => Globals.Database.StatusFilterSet;
			set => Globals.Database.StatusFilterSet = value;
		}

		protected bool LookForTrips
		{
			get => _LookForTrips;
			set
			{
				_LookForTrips = value;
				var Svs = Service;

				if( Svs != null )
				{
					if( value )
					{
						Globals.Keyboard.Hide = true;

						Svs.OnTripsChanged = OnTripsChanged;

						if( !Globals.Database.InCheckForTrips )
						{
							Globals.Database.DeleteAllLocalTripsAsync();
							CheckForTrips();
						}
					}
					else
						Svs.OnTripsChanged = null;
				}
			}
		}

		private struct FragmentEntry
		{
			internal int OwnerId;
		#pragma warning disable 618
			internal Fragment Fragment;
		#pragma warning restore 618
		}

		private readonly List<FragmentEntry> ActiveFragments = new();

		private readonly bool SaveAllowBackButton;

		private volatile bool _Executing = true;

		private bool _LookForTrips;

		private bool _OnLine;

		private ScreenOrientation _Orientation = ScreenOrientation.Unspecified;
		private Color             DarkViolet;

		private bool Destroying;

	#pragma warning disable 618
		private FragmentTransaction? Transaction;
	#pragma warning restore 618

		protected abstract void OnExecute( object? arg = null );

		protected abstract void OnCreateFragment();
		protected abstract void OnDestroyFragment();

		[Obsolete]
		protected abstract void OnCreateDynamicFragments();

		protected virtual int OverrideLayoutId( int id ) => id;

		[Obsolete]
		protected override void Dispose( bool disposing )
		{
			Destroy();
			base.Dispose( disposing );
		}

		protected void CheckForTrips()
		{
			Globals.Database.DisablePolling = false;
			Globals.Database.CheckForTrips();
		}

		protected static void Wait( int delayMilliseconds )
		{
			Globals.Wait( delayMilliseconds );
		}

		protected T? FindViewById<T>( int id ) where T : View => View?.FindViewById<T>( id );

		[Obsolete]
		~AFragment()
		{
			Destroy();
		}

		protected static void PlayBadScanSound()
		{
			Globals.Sounds.PlayBadScanSound();
		}

		protected static void WarehouseNotifySound()
		{
			Globals.Sounds.WarehouseNotifySound();
		}

		protected static void Error( string text, bool beep = true )
		{
			Globals.Dialogues.Error( text, beep );
		}

		protected static void Confirm( string text, Action onOk, Action? onCancel = null )
		{
			Globals.Dialogues.Confirm( text, onOk, onCancel );
		}

		protected static bool Confirm( string text, bool beep = true, Activity? activity = null ) => Globals.Dialogues.Confirm( text, beep, activity );

		protected static void ConfirmUndeliverable( Action onOk, Action? onCancel = null, bool beep = false )
		{
			Globals.Dialogues.ConfirmUndeliverable( onOk, onCancel, beep );
		}

		public static bool ConfirmUndeliverable( bool beep = false, Activity? activity = null ) => Globals.Dialogues.ConfirmUndeliverable( beep, activity );

		protected static void Warning( string text, Action onOk, Action onCancel, bool beep = false, Activity? activity = null )
		{
			Globals.Dialogues.Warning( text, onOk, onCancel, beep, activity );
		}

		protected static bool Warning( string text, bool beep = false, Activity? activity = null ) => Globals.Dialogues.Warning( text, beep, activity );

		protected static void Inform( string text, Action onOk, bool beep = true, Activity? activity = null )
		{
			Globals.Dialogues.Inform( text, onOk, beep, activity );
		}

		[Obsolete]
		public void KillFragment()
		{
			Destroy();
		}

		[Obsolete]
		public object ExecuteFragment( AFragment fragment, int loadAreaResourceId, object? arg = null ) => Globals.Fragment.ExecuteFragment( fragment, loadAreaResourceId, arg );

		public void RunFragment( object? arg = null )
		{
			try
			{
			#if ALLOW_ROTATE
				Orientation = ScreenOrientation.Unspecified;
			#else
				Orientation = ScreenOrientation.Portrait;
			#endif

				TripStatusFilterSet = Db<Client, AuthToken, remoteTrip>.DefaultStatusFilter;

				OnExecute( arg );

				Task.Run( () =>
				          {
					          while( _Executing && !Destroyed )
						          Thread.Sleep( 100 );
				          } ).Wait();
			}
			catch( Exception E )
			{
				Log( E );
			}
		}

		public void Return()
		{
			_Executing = false;
		}

		public void Return( Globals.OperatingMode.OPERATING_MODE programToExecute )
		{
			Globals.Fragment.ExecuteFragment( programToExecute );
			Return();
		}

		public void Return( object result )
		{
			Result = result;
			Return();
		}

		protected virtual void OnPing( bool online )
		{
		}

		public override void OnResume()
		{
			base.OnResume();

			if( !Created )
			{
				foreach( var Frag in ActiveFragments )
				{
					var Container = FindViewById<View>( Frag.OwnerId );

					if( Container?.Background is ColorDrawable Bc && Bc.Color.Equals( DarkViolet ) )
						RunOnUiThread( () => { Container.SetBackgroundColor( Color.Transparent ); } );
				}

				Created = true;
			}
		}

		public override void OnDetach()
		{
#pragma warning disable CS0612 // Type or member is obsolete
			Destroy();
#pragma warning restore CS0612 // Type or member is obsolete
			base.OnDetach();
		}

		[Obsolete]
		protected void AddDynamicFragment( int id, Fragment fragment )
		{
			ActiveFragments.Add( new FragmentEntry { OwnerId = id, Fragment = fragment } );

			Transaction ??= ChildFragmentManager.BeginTransaction();
			Transaction?.Replace( id, fragment );
		}

		[Obsolete]
		protected void AddDynamicFragment( View container, Fragment fragment )
		{
			AddDynamicFragment( container.Id, fragment );
		}

		public override View OnCreateView( LayoutInflater? inflater, ViewGroup? container, Bundle? savedInstanceState )
		{
			if( View is null )
			{
				DarkViolet = new Color( ContextCompat.GetColor( container?.Context, Resource.Color.DarkViolet ) );

				View = inflater?.Inflate( Id, container, false );
				OnCreateFragment();
			}

			return View!;
		}

		[Obsolete]
		public override void OnViewCreated( View? view, Bundle? savedInstanceState )
		{
			OnCreateDynamicFragments();

			try
			{
				Transaction?.CommitAllowingStateLoss();
			}
			catch // Maybe already committed
			{
			}
		}

		[Obsolete]
		public void Destroy()
		{
			lock( this )
			{
				if( Destroying )
				{
					Thread.Sleep( 250 );
					return;
				}

				Destroying = true; // Leave Destroying always true
				_Executing = false;
			}

			if( !Destroyed )
			{
				try
				{
					if( Globals.Fragment.CurrentFragment == this )
						Globals.Fragment.CurrentFragment = null;

					LookForTrips      = false;
					IgnoreTripAddress = false;

					BarcodeInputFragment.ScannerAbort  = true;
					Globals.Keyboard.Hide              = true;
					MainActivity.OnBackKey             = null;
					Globals.DisableNotifications       = false;
					Orientation                        = ScreenOrientation.Unspecified;
					IgnoreTripAddress                  = false;
					TripStatusFilterSet                = Db<Client, AuthToken, remoteTrip>.DefaultStatusFilter;
					Globals.Filters.OnFilterChange     = null;
					Globals.Filters.OnVisibilityChange = null;
					TripCount                          = -1;
					OnDestroyFragment();

					var Act = Globals.Debug.Activity;

					if( Act is { IsFinishing: false } )
					{
						var FragmentTransaction = ChildFragmentManager.BeginTransaction();

						if( FragmentTransaction is not null )
						{
							foreach( var ActiveFragment in ActiveFragments )
								FragmentTransaction.Remove( ActiveFragment.Fragment );

							FragmentTransaction.CommitAllowingStateLoss();
						}
					}
				}
				catch // Bug in fragment manager (sometimes thinks activity is destroyed)
				{
				}
				finally
				{
					try
					{
						MainActivity.AllowBackButton = SaveAllowBackButton;
						Destroyed                    = true; // Must be last
					}
					catch
					{
					}
				}
			}
		}

		public async void RunOnUiThread( Action action, bool wait = false )
		{
			var Act = Globals.Debug.Activity;

			if( Act is { IsFinishing: false } )
			{
				Act.RunOnUiThread( action );

				if( wait )
				{
					var Completed = false;

					Act.RunOnUiThread( () => { Completed = true; } );

					await Task.Run( () =>
					                {
						                while( !Completed )
							                Thread.Sleep( 1 );
					                } );
				}
			}
		}

		protected virtual void OnTripsChanged( int totalTrips, bool hasNew, bool hasDeleted, bool hasChanged )
		{
		}

		protected virtual void OnFilterChange( Globals.Filters.OPTION option, Globals.Filters.SORT sort )
		{
		}

		protected AFragment( int id )
		{
			// ReSharper disable once VirtualMemberCallInConstructor
			Id = OverrideLayoutId( id );

			SaveAllowBackButton            = MainActivity.AllowBackButton;
			Globals.Filters.OnFilterChange = OnFilterChange;
			_OnLine                        = Globals.RemoteService.OnLine;
		}

		protected static void Log( string text )
		{
		}

		protected static void Log( Exception _ )
		{
		}
	}
}