using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;

namespace Droid2.Fragments.Standard.Claims.PiecesFragment
{
    [ Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
          Label = "ClaimsPiecesActivity",
          AlwaysRetainTaskState = true,
          ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Keyboard
      ) ]
    public class ClaimsPiecesOptionsActivity : Activity
    {
        public enum SCAN_STATUS
        {
            CANCEL,
            FILTER,
            UNDELIVERABLE,
            RESET_SCAN,
            REMOVE,
            BUSY // For Thread
        }

        public static volatile SCAN_STATUS Status = SCAN_STATUS.CANCEL;
        public static ScannedTrip SelectedTrip;
        public static string ZoneFilter;


        private EditText ZoneFilterExit;
        private Button UndeliverableButton;

        protected override void OnCreate( Bundle savedInstanceState )
        {
            base.OnCreate( savedInstanceState );

            if( SelectedTrip == null )
                Finish();
            else
            {
                var Trip = SelectedTrip;

                SetContentView( Resource.Layout.Activity_ClaimsPiecesOptions );
                FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
                {
                    Status = SCAN_STATUS.CANCEL;
                    Finish();
                };

                UndeliverableButton = FindViewById<Button>( Resource.Id.undeliverableButton );
                UndeliverableButton.Click += ( sender, args ) =>
                {
                    Status = SCAN_STATUS.UNDELIVERABLE;
                    Finish();
                };

                RunOnUiThread( () => { FindViewById<TextView>( Resource.Id.tripId ).Text = Trip.TripId; } );

                var RemoveButton = FindViewById<Button>( Resource.Id.removeButton );
                RemoveButton.Click += ( sender, args ) =>
                {
                    Status = SCAN_STATUS.REMOVE;
                    Finish();
                };

                var ResetButton = FindViewById<Button>( Resource.Id.resetScanButton );
                ResetButton.Click += ( sender, args ) =>
                {
                    Status = SCAN_STATUS.RESET_SCAN;
                    Finish();
                };

                var Minimal = Intent.GetBooleanExtra( "Minimal", false );
                if( Minimal )
                {
                    RunOnUiThread( () =>
                                   {
                                       RemoveButton.Visibility = Intent.GetBooleanExtra( "ShowRemove", false ) ? ViewStates.Visible : ViewStates.Gone;
                                       ResetButton.Visibility = Intent.GetBooleanExtra( "HideReset", false ) ?  ViewStates.Gone : ViewStates.Visible;

                                       UndeliverableButton.Visibility = Intent.GetBooleanExtra( "ShowUndeliverable", false ) ? ViewStates.Visible : ViewStates.Gone;
                                       FindViewById<LinearLayout>( Resource.Id.zoneFilterLayout ).Visibility = ViewStates.Gone;
                                   } );
                }
                else
                {
                    ZoneFilterExit = FindViewById<EditText>( Resource.Id.zoneFilter );
                    var Config = Globals.Database.Db.Configuration;
                    ZoneFilterExit.Text = Config.ClaimsZoneFilter;

                    FindViewById<Button>( Resource.Id.filterButton ).Click += ( sender, args ) =>
                    {
                        var Zone = ZoneFilterExit.Text.Trim().ToUpper();
                        RunOnUiThread( () => { ZoneFilterExit.Text = Zone; } );

                        if( !Globals.Database.Db.HasZone( Zone ) )
                            Globals.Dialogues.Error( "Not A Valid Zone", true, this );
                        else
                        {
                            Config.ClaimsZoneFilter = Zone;
                            Globals.Database.Db.Configuration = Config;
                            ZoneFilter = Zone;
                            Status = SCAN_STATUS.FILTER;
                            Finish();
                        }
                    };

                    FindViewById<Button>( Resource.Id.resetFilterButton ).Click += ( sender, args ) =>
                    {
                        ZoneFilter = "";
                        Status = SCAN_STATUS.FILTER;
                        Finish();
                    };
                }
            }
        }
    }
}