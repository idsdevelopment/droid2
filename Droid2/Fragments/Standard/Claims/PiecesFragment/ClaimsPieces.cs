using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Database;
using RemoteService.Constants;
using DateTime = System.DateTime;

namespace Droid2.Fragments.Standard.Claims.PiecesFragment
{
    public class ClaimsPieces : AFragment
    {
        private Login LoginFragment;

        private ListView ScannedListView;
        private ClaimsPiecesAdapter ListAdapter;
        private LinearLayout MainLayout;
        private FrameLayout LoginLayout;
        private BarcodeInputFragment Scanner;
        private Button NextButton;

        private readonly List<ScannedTrip> ScannedTrips = new List<ScannedTrip>();

        private volatile bool Completed;

        private void EnableNext( bool enable ) { RunOnUiThread( () => { NextButton.Enabled = enable; } ); }
        private void EnableNext() { EnableNext( ListAdapter.Trips.Count <= 0 ); }

        public ClaimsPieces() : base( Resource.Layout.Fragment_ClaimsPieces ) { }

        protected override void OnCreateDynamicFragments()
        {
            LoginFragment = new Login { IsAlternateLogin = true };
            AddDynamicFragment( Resource.Id.loginFrame, LoginFragment );

            Scanner = new BarcodeInputFragment();
            AddDynamicFragment( Resource.Id.scannerLayout, Scanner );
        }

        protected override void OnCreateFragment()
        {
            LoginLayout = FindViewById<FrameLayout>( Resource.Id.loginFrame );
            MainLayout = FindViewById<LinearLayout>( Resource.Id.mainLayout );

            RunOnUiThread( () => { MainLayout.Visibility = ViewStates.Gone; } );

            ScannedListView = FindViewById<ListView>( Resource.Id.claimsListView );
            ListAdapter = new ClaimsPiecesAdapter( Activity, ScannedListView );
            ScannedListView.Adapter = ListAdapter;

            NextButton = FindViewById<Button>( Resource.Id.nextButton );
        }

        protected override void OnTripsChanged( bool hasNew, bool hasDeleted, bool hasChanged )
        {
            var Trips = Globals.Database.Db.Trips;
            ListAdapter.Add( Trips );
            TripCount = Trips.Count;
        }

        protected override async void OnExecute( object arg = null )
        {
#pragma warning disable 4014
            Task.Run( () => { Globals.Database.Db.UpdateZones( Globals.RemoteService.Ids.GetZones() ); } );
#pragma warning restore 4014

            ExecuteFragment( LoginFragment, Resource.Id.loginFrame );
            LoginFragment.Abort();

            TripStatusFilterSet = new[] { Trips.STATUS.PICKED_UP };

            LookForTrips = true;

            RunOnUiThread( () =>
                           {
                               LoginLayout.Visibility = ViewStates.Gone;
                               MainLayout.Visibility = ViewStates.Visible;
                           } );

            NextButton.Click += ( sender, args ) => { };

            if( Globals.OperatingMode.ManualScan == Globals.OperatingMode.MANUAL_SCAN.MANUAL_SCAN )
                Scanner.OnAccept = s => { }; // Just Show Button

            ListAdapter.TripOptionsCallback = ( trip, action, zoneFilter ) =>
            {
                switch( action )
                {
                case ClaimsPiecesOptionsActivity.SCAN_STATUS.UNDELIVERABLE:
                    trip.Undeliverable = true;
                    ListAdapter.Remove( trip );
                    ScannedTrips.Add( trip );
                    ListAdapter.NotifyDataSetChanged();
                    break;

                case ClaimsPiecesOptionsActivity.SCAN_STATUS.REMOVE:
                    ListAdapter.Remove( trip );
                    break;
                }
                EnableNext();
            };

            EnableNext( false );

            NextButton.Click += async ( sender, args ) =>
            {
                ClaimPiecesFinaliseActivity.Trips = ScannedTrips;
                Activity.StartActivity( new Intent( Activity, typeof( ClaimPiecesFinaliseActivity ) ) );
                Activity.OverridePendingTransition( Resource.Animation.PullInLeft, 0 );
                await Task.Run( () =>
                                {
                                    for( ClaimPiecesFinaliseActivity.Action = ClaimPiecesFinaliseActivity.FINALISE_ACTION.BUSY;
                                         ClaimPiecesFinaliseActivity.Action == ClaimPiecesFinaliseActivity.FINALISE_ACTION.BUSY; )
                                        Thread.Sleep( 100 );

                                    if( ClaimPiecesFinaliseActivity.Action != ClaimPiecesFinaliseActivity.FINALISE_ACTION.CANCEL )
                                    {
                                        var RemoveList = new List<ScannedTrip>();
                                        var Undeliverable = new List<Trip>();

                                        foreach( var Trip in ScannedTrips )
                                            if( Trip.Undeliverable )
                                            {
                                                RemoveList.Add( Trip );

                                                var Trp = new Trip( Trip ) { Status = (sbyte)IdsRemote.Trip.TRIP_STATUS.UNDELIVERABLE, UpdateServer = true };
                                                Undeliverable.Add( Trp );
                                            }

                                        var Deliverable = ( from Trip in ScannedTrips where !Trip.Undeliverable select Trip.TripId ).ToArray();

                                        var Sig = ClaimPiecesFinaliseActivity.SignaturePoints;
                                        var Pod = ClaimPiecesFinaliseActivity.PodName;

                                        var Config = Globals.Database.Db.Configuration;

                                        // Make sure re-login as original driver before returning
                                        Globals.RemoteService.Ids.LogInAsDriver( Config.AccountId, Config.UserName, Config.Password );
                                        Globals.Database.Db.UpdateTrips( Undeliverable ); // Update before deleting trips

                                        Task.Run( () =>
                                                  {
                                                      var Now = DateTime.Now;

                                                      if( Sig == null )
                                                          Globals.RemoteService.Ids.UpdateClaim( Deliverable, "", 0, 0, Pod, Now );
                                                      else
                                                          Globals.RemoteService.Ids.UpdateClaim( Deliverable, Sig.Points, Sig.Height, Sig.Width, Pod, Now );
                                                  } );
                                    }
                                } );

                Completed = true;
                Scanner.Abort();
            };

            while( Executing )
            {
                var Barcode = await Scanner.Scan();
                if( Completed )
                {
                    Globals.OperatingMode.Mode = Globals.OperatingMode.OPERATING_MODE.DRIVER_PICKUP;

#pragma warning disable 4014
                    Globals.Database.Db.DeleteTripsAsync();
#pragma warning restore 4014

                    // Delay finish to remove unwanted display
                    ClaimPiecesFinaliseActivity.Instance?.Finish();
                    ClaimPiecesFinaliseActivity.Instance = null;

                    Return();
                    return;
                }

                if( !string.IsNullOrWhiteSpace( Barcode ) ) // Probably back key
                {
                    var FoundTrip = ListAdapter.Trips.FirstOrDefault( Trip => Trip.TripId == Barcode );
                    if( FoundTrip != null )
                    {
                        var STrip = (ScannedTrip)FoundTrip;
                        STrip.Scanned += 1;
                        STrip.Balance -= 1;

                        if( STrip.Balance == 0 )
                        {
                            ScannedTrips.Add( STrip );
                            ListAdapter.Remove( FoundTrip );
                        }
                        else
                            ListAdapter.NotifyDataSetChanged();
                    }
                    else
                    {
                        // Maybe overscan
                        var STrip1 = ScannedTrips.FirstOrDefault( trip => trip.TripId == Barcode );
                        if( STrip1 != null )
                        {
                            STrip1.Scanned += 1;
                            STrip1.Balance -= 1;

                            ScannedTrips.Remove( STrip1 );
                            ListAdapter.Add( STrip1 );
                        }
                        else //Unknown Trip
                        {
                            STrip1 = new ScannedTrip( new Trip
                                                      {
                                                          TripId = Barcode
                                                      } );

                            ListAdapter.AddUnknownTrip( STrip1 );
                        }
                        Globals.Sounds.PlayBadScanSound();
                    }
                    EnableNext();
                }
            }
            Return();
        }

        protected override void OnDestroyFragment() { Scanner.Abort(); }
    }
}