using System;
using System.Linq;
using Android.Content.PM;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Signature;

namespace Droid2.Fragments.Standard.Claims.StandardFragment
{
    public class ClaimsStandard : AFragment
    {
        private BarcodeInputFragment Scanner;

        private SignatureFragment Signature;
        private FrameLayout SignatureLayout;
        private ViewGroup.LayoutParams SignatureLayoutParams;

        private Button NextButton;

        private ListView ClaimsListView;
        private ClaimsStandardAdapter Adapter;
        private LinearLayout FinaliseLayout;
        private ViewGroup.LayoutParams FinaliseLayoutParams;

        private Animation FinaliseLayoutPullInRight;
        private Animation FinaliseLayoutPushOutRight;

        private Animation SignatureLayoutPullInLeft;
        private Animation SignatureLayoutPushOutLeft;

        private SigantureResult SignaturePoints = new SigantureResult();
        private EditText PodName;

        private void EnableNext() { RunOnUiThread( () => { NextButton.Enabled = Adapter.Trips.Count > 0; } ); }

        public ClaimsStandard() : base( Resource.Layout.Fragment_ClaimsStandard ) { }

        private void GoHome() { RunOnUiThread( () => { FinaliseLayout.StartAnimation( FinaliseLayoutPushOutRight ); } ); }

        protected override void OnCreateFragment()
        {
            NextButton = FindViewById<Button>( Resource.Id.nextButton );
            NextButton.Click += ( sender, args ) =>
            {
                RunOnUiThread( () =>
                {
                    FinaliseLayout.Visibility = ViewStates.Visible;

                    FinaliseLayoutParams.Width = ViewGroup.LayoutParams.MatchParent;
                    FinaliseLayoutParams.Height = ViewGroup.LayoutParams.MatchParent;

                    FinaliseLayout.LayoutParameters = FinaliseLayoutParams;

                    FinaliseLayout.StartAnimation( FinaliseLayoutPullInRight );
                } );
            };

            FindViewById<Button>( Resource.Id.backButton ).Click += ( sender, args ) => { GoHome(); };

            ClaimsListView = FindViewById<ListView>( Resource.Id.claimsListView );
            ClaimsListView.Adapter = Adapter = new ClaimsStandardAdapter( Activity,
                                                                          ClaimsListView,
                                                                          trip =>
                                                                          {
                                                                              Globals.Dialogues.Confirm( $"Remove trip:\r\n{Adapter.CurrentTrip.TripId}",
                                                                                                         () =>
                                                                                                         {
                                                                                                             Adapter.Remove( trip );
                                                                                                             EnableNext();
                                                                                                         }, null, false );
                                                                          } );

            FinaliseLayout = FindViewById<LinearLayout>( Resource.Id.finaliseLayout );

            RunOnUiThread( () => { FinaliseLayout.Visibility = ViewStates.Gone; } );
            FinaliseLayoutParams = FinaliseLayout.LayoutParameters;
            FinaliseLayoutPullInRight = AnimationUtils.LoadAnimation( Activity, Resource.Animation.PullInRight );
            FinaliseLayoutPushOutRight = AnimationUtils.LoadAnimation( Activity, Resource.Animation.PushOutRight );
            FinaliseLayoutPushOutRight.AnimationEnd += ( sender, args ) =>
            {
                FinaliseLayoutParams.Width = 0;
                FinaliseLayout.LayoutParameters = FinaliseLayoutParams;
                FinaliseLayout.Visibility = ViewStates.Gone;
            };
            FinaliseLayout.Touch += ( sender, args ) => { args.Handled = true; };

            SignatureLayout = FindViewById<FrameLayout>( Resource.Id.signatureLayout );
            RunOnUiThread( () => { SignatureLayout.Visibility = ViewStates.Gone; } );
            SignatureLayoutParams = SignatureLayout.LayoutParameters;
            SignatureLayoutPullInLeft = AnimationUtils.LoadAnimation( Activity, Resource.Animation.PullInLeft );
            SignatureLayoutPushOutLeft = AnimationUtils.LoadAnimation( Activity, Resource.Animation.PushOutLeft );
            SignatureLayoutPushOutLeft.AnimationEnd += ( sender, args ) =>
            {
                SignatureLayoutParams.Width = 0;
                SignatureLayout.LayoutParameters = SignatureLayoutParams;
                SignatureLayout.Visibility = ViewStates.Gone;
            };

            FindViewById<Button>( Resource.Id.signButton ).Click += async ( sender, args ) =>
            {
                Activity.RequestedOrientation = ScreenOrientation.Landscape;

                SignatureLayout.Visibility = ViewStates.Visible;
                SignatureLayoutParams.Width = ViewGroup.LayoutParams.MatchParent;
                SignatureLayoutParams.Height = ViewGroup.LayoutParams.MatchParent;

                SignatureLayout.LayoutParameters = SignatureLayoutParams;
                SignatureLayout.StartAnimation( SignatureLayoutPullInLeft );

                SignaturePoints = await Signature.CaptureSignature();

                Activity.RequestedOrientation = ScreenOrientation.Unspecified;
                SignatureLayout.StartAnimation( SignatureLayoutPushOutLeft );
            };

            PodName = FindViewById<EditText>( Resource.Id.podName );
            FindViewById<Button>( Resource.Id.finaliseButton ).Click += ( sender, args ) =>
            {
                var TripIds = Adapter.Trips.Select( Trip => Trip.TripId ).ToList();
                RunOnUiThread( () =>
                {
                    Adapter.Clear();

                    GoHome();

                    var PName = PodName.Text;
                    PodName.Text = "";
                    SignaturePoints = new SigantureResult();
                    Globals.Database.Db.AddClaims( TripIds, PName, SignaturePoints, DateTime.Now );
                    EnableNext();
                } );
            };
        }

        protected override void OnDestroyFragment()
        {
            Scanner?.Abort();
            Scanner = null;
        }

        protected override void OnCreateDynamicFragments()
        {
            Scanner = new BarcodeInputFragment();
            AddDynamicFragment( Resource.Id.scannerLoadFrame, Scanner );

            Signature = new SignatureFragment();
            AddDynamicFragment( Resource.Id.signatureLayout, Signature );
        }

        protected override async void OnExecute( object arg = null )
        {
            while( Executing )
            {
                EnableNext();
                var TripId = await Scanner.Scan();
                Adapter.Add( TripId ).ScrollToTripId( TripId );
            }
        }
    }
}