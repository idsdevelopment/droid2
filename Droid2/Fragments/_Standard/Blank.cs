﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Droid2.Fragments.Standard;

namespace Droid2.Fragments._Standard
{
    public  class Blank : AFragment
    {
	    public Blank() : base( Resource.Layout._Blank )
	    {
	    }

	    protected override void OnExecute( object arg = null )
	    {
		    while( Executing )
		    {
			    Thread.Sleep( 250  );
		    }
		    
	    }

	    protected override void OnCreateFragment()
	    {
	    }

	    protected override void OnDestroyFragment()
	    {
	    }

	    protected override void OnCreateDynamicFragments()
	    {
	    }
    }
}