using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Droid2.Adapters;
using Utils;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard.PickupDeliveries
{
	public partial class DriverPickup
	{
		private const string UNKNOWN_LOC = "Unknown location";

		public class LocationCompany
		{
			protected internal string Company;
			protected internal string LocationBarcode;

			protected internal List<Trip> Trips           = new(),
			                              UnfilteredTrips = new();

			public LocationCompany()
			{
			}

			public LocationCompany( LocationCompany locationCompany )
			{
				LocationBarcode = locationCompany.LocationBarcode;
				Company         = locationCompany.Company;
				Trips.AddRange( locationCompany.Trips );
			}
		}

		protected virtual List<ScannedTrip> FilterLocationTrips( List<ScannedTrip> trips ) => trips;

		protected virtual List<Trip> FilterLocationTrips( List<Trip> trips ) => trips;

		protected virtual bool ValidateLocation( string locationCode, out LocationCompany location ) => Locations.TryGetValue( locationCode, out location );

		protected virtual bool TryGetLocation( out LocationCompany location, out string locationCode )
		{
			var RetVal = false;

			if( UseLocationBarcodes )
			{
				var LocationCode = "";

				location = Task.Run( async () =>
				                     {
					                     var Loc = await Scan( "Location Barcode" );

					                     LocationCompany Location = null;

					                     if( !Scanner.WasBackKey && ( Loc != "" ) && Executing )
						                     RetVal = ValidateLocation( Loc, out Location );

					                     LocationCode = Loc;

					                     return Location;
				                     } ).Result;

				locationCode = LocationCode;
			}
			else
			{
				if( PickupDeliveryByPieces )
				{
					HavePiecesLocation = false;

					location = Task.Run( () =>
					                     {
						                     while( Executing && !HavePiecesLocation )
							                     Thread.Sleep( 100 );

						                     return PiecesLocation;
					                     } ).Result;

					locationCode = location != null ? location.Company : ""; //Happen during forced abort
					RetVal       = true;
				}
				else
				{
					location     = null;
					locationCode = null;
				}
			}

			return RetVal;
		}

		protected virtual ( string CompanyName, string Barcode, bool Ok ) GetBarcodeFromCompanyName( string companyName )
		{
			if( !companyName.IsNullOrEmpty() )
			{
				companyName = companyName.Trim();

				var BCode = companyName.Between( "(", ")" );

				if( !string.IsNullOrEmpty( BCode ) )
				{
					var Company = companyName.Replace( $"({BCode})", "" ).Replace( "  ", " " ).Trim();

					return ( CompanyName: Company, Barcode: BCode, Ok: true );
				}

				return companyName.IsInt() ? ( CompanyName: companyName, Barcode: companyName, Ok: true ) : ( CompanyName: "", Barcode: "", Ok: false );
			}

			return ( CompanyName: "", Barcode: "", false );
		}

		protected LocationCompany GetBarcodeFromCompanyName( string companyName, Trip trip )
		{
			var (CompanyName, Barcode, Ok) = GetBarcodeFromCompanyName( companyName );

			if( Ok )
			{
				var RetVal = new LocationCompany
				             {
					             Company         = CompanyName,
					             LocationBarcode = Barcode
				             };
				RetVal.Trips.Add( trip );

				return RetVal;
			}

			return null;
		}

		protected virtual void AddCompanyBarcode( string companyName, Trip trip, bool isDelivery )
		{
			var Barcode = GetBarcodeFromCompanyName( companyName, trip );

			if( Barcode != null )
			{
				var BCode = Barcode.LocationBarcode;

				if( !Locations.TryGetValue( BCode, out var Location ) )
					Locations.Add( BCode, Barcode );
				else
				{
					var LTrips = Location.Trips;

					foreach( var BarcodeTrip in Barcode.Trips )
					{
						var Found = false;

						for( var Ndx = 0; Ndx < LTrips.Count; Ndx++ )
						{
							var LTrip = LTrips[ Ndx ];

							if( BarcodeTrip.TripId == LTrip.TripId )
							{
								LTrips[ Ndx ] = BarcodeTrip;
								Found         = true;

								break;
							}
						}

						if( !Found )
							LTrips.Add( BarcodeTrip );
					}
				}
			}
		}
	}
}