using System.Threading;
using Android.App;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using Droid2.Barcode;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard.PickupDeliveries
{
    [ Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
        Label = "UndeliverableActivity",
        AlwaysRetainTaskState = true,
        ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Keyboard
    ) ]
    public class UndeliverableReasonActivity : Activity
    {
        public enum SCAN_ACTION
        {
            CANCEL,
            OK,
            BUSY // For Thread
        }

        public static volatile SCAN_ACTION ScanAction = SCAN_ACTION.CANCEL;
        public static Trip SelectedTrip;
        public static string Reason;

        private BarcodeInputFragment Scanner;

        [System.Obsolete]
        protected override void OnCreate( Bundle savedInstanceState )
        {
            base.OnCreate( savedInstanceState );

            if( SelectedTrip == null )
                Finish();
            else
            {
                var Trip = SelectedTrip;

                Scanner = new BarcodeInputFragment();

                var Trans = FragmentManager!.BeginTransaction();
                Trans.Add( Resource.Id.scannerLayout, Scanner );
                Trans.CommitAllowingStateLoss();

                SetContentView( Resource.Layout.Activity_UndeliverableReason );
                FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
                {
                    ScanAction = SCAN_ACTION.CANCEL;
                    Finish();
                };

                RunOnUiThread( () =>
                {
                    FindViewById<FrameLayout>( Resource.Id.scannerLayout )!.SetBackgroundColor( Color.Transparent );
                    FindViewById<TextView>( Resource.Id.tripId )!.Text = Trip.TripId;
                } );
            }
        }

        private bool Created;

        protected override void OnResume()
        {
            if( !Created )
            {
                Created = true;
                base.OnResume();

                new Timer( async state =>
                {
                    Scanner.ForceManualAccept = !Globals.Modifications.Preferences.ScanBarcodeForUndeliverableReason;

                    Scanner.Placeholder = "Reason";
                    Reason = await Scanner.Scan();
                    ScanAction = SCAN_ACTION.OK;
                    Finish();
                }, null, 100, Timeout.Infinite );
            }
        }
    }
}