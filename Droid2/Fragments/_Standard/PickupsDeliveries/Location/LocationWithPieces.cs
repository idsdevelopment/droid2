#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Views;
using CommsDb.Database;
using Droid2.Adapters;
using IdsService;
using IdsService.org.internetdispatcher.jbtest2;
using Service.Interfaces;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard.PickupDeliveries
{
	public partial class DriverPickup
	{
		protected        EventHandler AcceptEventFunc { get; private set; } = null!;
		private readonly object       UpdateLockObject = new();
		protected        bool         InAcceptDelivery;

		protected string SelectedLocation = "";
		protected virtual bool BeforeAcceptButton() => true;

		protected virtual async Task<string> Scan( string placeholder )
		{
			Scanner.Placeholder = placeholder;
			return ( await Scanner.Scan() ).Trim();
		}

		protected virtual ScannedTrip? MatchTrip( List<Trip> scannerTrips, string scannedId )
		{
			return scannerTrips.OfType<ScannedTrip>().FirstOrDefault( sTrip => sTrip.TripId == scannedId )!;
		}

		protected virtual PiecesTripCountData GetPiecesTripCountData( BaseTripAdapter.TripCountData undel ) => new(undel);

		protected virtual EventHandler OverrideAcceptEvent( EventHandler acceptFunc ) => acceptFunc;

		protected virtual void OnScanComplete( bool completed )
		{
			RunOnUiThread( () => { AcceptButton.Enabled = completed || PickupDeliveryByPieces; } );
		}

		protected virtual List<Trip> OnFilterPickupDeliveryTrips( List<Trip> trips )
		{
			List<Trip> Trips;

			if( IsPickup )
			{
				Trips = ( from T in trips
				          where T.Status <= STATUS.DISPATCHED
				          select T ).ToList();
			}
			else
			{
				Trips = ( from T in trips
				          where T.Status == STATUS.PICKED_UP
				          select T ).ToList();
			}

			return Trips;
		}

		protected virtual List<Trip> GetTripsForAcceptDisplay() => ( from T in ClaimsPiecesAdapter.Trips
		                                                             where T is ScannedTrip
		                                                             select T ).ToList();

		protected virtual void OnCancelButtonClick()
		{
		}

		protected decimal BaseAfterUpdatePieceCount()
		{
			decimal Scanned = 0;
			var     Trips   = ClaimsPiecesAdapter.Trips;

			if( Trips is not null )
			{
				foreach( var Trip in Trips )
				{
					if( Trip is ScannedTrip T )
						Scanned += T.Scanned;
				}

				RunOnUiThread( () => { TotalScanned.Text = Scanned.ToString( "###0" ); } );
			}

			return Scanned;
		}

		protected virtual decimal AfterUpdatePieceCount() => BaseAfterUpdatePieceCount();

		protected virtual string OnCancelBtnGetText( bool inAccept ) => inAccept ? "Back" : "Cancel";

		private async Task LocationWithPieces()
		{
			InAcceptDelivery = false;

			void UpdateAccept( bool inAccept )
			{
				InAcceptDelivery = inAccept;

				ClaimsPiecesAdapter.ShowHidden = inAccept;

				RunOnUiThread( () =>
				               {
					               CancelBtn.Text = OnCancelBtnGetText( inAccept );

					               if( !inAccept )
					               {
						               AcceptButton.Text        = "Next";
						               AcceptButton.Enabled     = true;
						               ScannerLayout.Visibility = ViewStates.Visible;
					               }
					               else
					               {
						               AcceptButton.Text = IsPickup ? "Accept Pickup" : "Accept Delivery";
						               var Enabled = ClaimsPiecesAdapter!.HasScannedTrips;
						               AcceptButton.Enabled     = Enabled;
						               CancelBtn.Visibility     = ViewStates.Visible;
						               ScannerLayout.Visibility = ViewStates.Gone;
					               }
				               } );
			}

			CancelBtn.Click += ( _, _ ) =>
			                   {
				                   OnCancelButtonClick();

				                   if( !InAcceptDelivery )
				                   {
					                   Globals.Dialogues.Confirm( IsPickup ? CANCEL_PICKUP : CANCEL_DELIVERY, () =>
					                                                                                          {
						                                                                                          ShowScannedTotalOnly = false;
						                                                                                          CurrentTrip          = null;
						                                                                                          CancelDelivery       = true;
						                                                                                          Scanner.Abort();
					                                                                                          }, () => { } );
				                   }
				                   else
				                   {
					                   ClaimsPiecesAdapter.UnMarkUndeliverables();
					                   UpdateAccept( false );
					                   Scanner.Abort();
				                   }
			                   };

			async void LocalAcceptEventFunc( object o, EventArgs eventArgs )
			{
				if( !InAcceptDelivery )
				{
					if( BeforeAcceptButton() )
					{
						HideTripDetails = true;

						var Undel = ClaimsPiecesAdapter.TripInfo;

						if( PickupDeliveryByPieces && IsPickup )
							Undel = GetPiecesTripCountData( Undel );

						if( Undel.HasUndeliverable && ( Undel.UndeliverablePieces > 0 ) )
						{
							bool Waiting = true,
							     Ok      = false;

							var Text  = Undel.UndeliverableTripsAsText;
							var Text1 = Undel.UndeliverablePiecesAsText;

							if( Text1 != "" )
								Text += "\r\n" + Text1;

							Text = Text.Trim();

							Globals.Dialogues.Confirm( Text, () =>
							                                 {
								                                 ClaimsPiecesAdapter.MarkUndeliverables();
								                                 Ok      = true;
								                                 Waiting = false;
							                                 }, () => { Waiting = false; } );

							await Task.Run( () =>
							                {
								                while( Waiting )
									                Thread.Sleep( 100 );
							                } );

							if( !Ok )
								return;
						}

						ClaimsPiecesAdapter.Replace( GetTripsForAcceptDisplay() );

						UpdateAccept( true );
					}
				}
				else
				{
					var Undel = ClaimsPiecesAdapter.TripInfo;

					HideTripDetails     = true;
					HideDeliveryButtons = false;
					ShowPod             = true;

					RunOnUiThread( () =>
					               {
						               SignButton.Visibility = ViewStates.Visible;
						               PodLayout.Visibility  = ViewStates.Visible;
					               } );

					ProcessPiecesPickup = true;

					OnTripClick( new Trip( ClaimsPiecesAdapter.Trips[ 0 ] ) // Copy Addresses
					             {
						             TripId          = Undel.TripIds,
						             Pieces          = Undel.DeliverablePieces,
						             DeliveryNote    = "",
						             PickupNote      = "",
						             DeliveryContact = "",
						             PickupContact   = ""
					             }, true );
				}
			}

			AcceptEventFunc    =  LocalAcceptEventFunc;
			AcceptButton.Click += OverrideAcceptEvent( LocalAcceptEventFunc );

			while( Executing )
			{
				DisableUpdateDisplayFromDatabase = false;
				CancelDelivery                   = false;
				InAcceptDelivery                 = false;
				InLocationScan                   = false;
				ShowPod                          = false;
				ProcessPiecesPickup              = false;

				HideDeliveryButtons = true;
				HideTripDetails     = false;

				TripStatusFilterSet = Db<Client, AuthToken, remoteTrip>.DefaultStatusFilter;

				// Hides undeliverable button
				WaitAnimation();

				// ReSharper disable once ImplicitlyCapturedClosure
				RunOnUiThread( () =>
				               {
					               MainButtonLayout.Visibility    = ViewStates.Gone;
					               UndeliverableLayout.Visibility = ViewStates.Visible;

					               ReferenceLayout.Visibility = ViewStates.Visible;
					               WeightLayout.Visibility    = ViewStates.Visible;

					               DeliveryButtonsLayout.Visibility = ViewStates.Visible;
					               SignButton.Visibility            = ViewStates.Gone;

					               if( ListView.Adapter != TripListAdapter )
						               ListView.Adapter = TripListAdapter;

					               AcceptButton.Text    = "Next";
					               AcceptButton.Enabled = false;
					               CancelBtn.Visibility = ViewStates.Gone;

					               ScannerLayout.Visibility = UseLocationBarcodes ? ViewStates.Visible : ViewStates.Gone;
				               } );

				SelectedLocation = "";

				if( TryGetLocation( out var Location, out var LocationCode ) )
				{
					SelectedLocation = LocationCode;

					if( !Executing )
						return;

					InLocationScan = true;

					RunOnUiThread( () =>
					               {
						               ReferenceLayout.Visibility     = ViewStates.Gone;
						               WeightLayout.Visibility        = ViewStates.Gone;
						               UndeliverableLayout.Visibility = ViewStates.Gone;
						               ScannerLayout.Visibility       = ViewStates.Visible;
					               } );

					DisableUpdateDisplayFromDatabase = true;

					IsPickup = true;
					var Trips = OnFilterPickupDeliveryTrips( Location.Trips );

					if( Trips.Count == 0 )
					{
						IsPickup = false;
						Trips    = OnFilterPickupDeliveryTrips( Location.Trips );

						if( Trips.Count == 0 )
						{
							Error( "Nothing For This Location" );
							continue;
						}
					}

					TripCount = Trips.Count;

					if( PickupDeliveryByPieces )
					{
						Globals.Database.DisablePolling = true;

						lock( UpdateLockObject )
							ClaimsPiecesAdapter = GetClaimsPiecesAdapter;

						ClaimsPiecesAdapter.ScanCompletedCallback = OnScanComplete;

						var Trips1 = Trips;

						RunOnUiThread( () =>
						               {
							               MainButtonLayout.Visibility = ViewStates.Visible;

							               ListView.Adapter = ClaimsPiecesAdapter;
							               ClaimsPiecesAdapter.Add( Trips1 );
							               AcceptButton.Enabled    = true;
							               AcceptButton.Visibility = ViewStates.Visible;
							               CancelBtn.Enabled       = true;
							               FilterLocationTrips( ClaimsPiecesAdapter.Trips );
						               } );

						AfterUpdatePieceCount();

						DisableUpdateDisplayFromDatabase = false;

						while( Executing && !CancelDelivery )
						{
							var ScannedId = await Scan( PickupDeliveryByPieces ? ScannerText : "Item Barcode" );

							if( !Executing )
								return;

							if( Scanner.WasBackKey )
								break;

							if( ScannedId != "" )
							{
								var ScannerTrips = ClaimsPiecesAdapter.Trips;

								var Match = MatchTrip( ScannerTrips, ScannedId );

								#pragma warning disable 4014
								Task.Run( () =>
								          {
									          lock( UpdateLockObject )
									          {
										          var Enable = true;
										          var Adp    = ClaimsPiecesAdapter as ClaimsPiecesAdapter;

										          if( ClaimsPiecesAdapter.UpdateCount( ScannedId, Match ) < 0 )
										          {
											          PlayBadScanSound();

											          if( Adp is not null )
											          {
												          Adp.ResetScanCallback = () => { RunOnUiThread( () => { AcceptButton.Enabled = !Adp.HasOverscan; } ); };
												          Enable                = !Adp.HasOverscan;
											          }
										          }
										          else
										          {
											          AfterUpdatePieceCount();

											          if( Adp is not null )
												          Enable = !Adp.HasOverscan;
										          }

										          RunOnUiThread( () => { AcceptButton.Enabled = Enable; } );
									          }
								          } );
								#pragma warning restore 4014
							}
						}

						UpdateDisplayFromDatabase();
					}
				}
				else
				{
					if( !string.IsNullOrWhiteSpace( LocationCode ) )
						Globals.Dialogues.Error( UNKNOWN_LOC );
				}
			}
		}
	}
}