﻿using Droid2.Adapters;

namespace Droid2.Fragments.Standard.PickupDeliveries
{
    public partial class DriverPickup
    {
        public class PiecesTripCountData : BaseTripAdapter.TripCountData
        {
            public PiecesTripCountData( BaseTripAdapter.TripCountData data ) : base( data )
            {
            }

            public override string UndeliverableTripsAsText => "";
            public override string UndeliverablePiecesAsText => $"Missing Pieces: {UndeliverablePieces}";
        }
    }
}