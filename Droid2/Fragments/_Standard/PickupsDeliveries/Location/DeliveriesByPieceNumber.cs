﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Droid2.Adapters;
using Droid2.Fragments.Standard.PickupDeliveries;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard
{
	public class DeliveriesByPieceNumber : DriverPickup
	{
		public class PiecesScannedTrip : ScannedTrip
		{
			internal override string AsText => Undeliverable ? $"{Balance} Not picked-up" : base.AsText;

			internal override ScannedTrip Clone( ScannedTrip trip ) => new PiecesScannedTrip( trip );

			public PiecesScannedTrip( ScannedTrip trip ) : base( trip )
			{
			}
		}

		internal class PieceNumberScannedTrip : PiecesScannedTrip
		{
			internal override string AsText
			{
				get
				{
					if( Balance > 0 )
					{
						var IsDelivery = Owner.IsDelivery;

						if( Undeliverable || IsDelivery )
						{
							var Txt = IsDelivery ? "delivered" : "picked-up";
							return DoPieces( $"{Balance} Piece(s) not {Txt}" ).ToString();
						}
					}
					return base.AsText;
				}
			}

			private readonly DeliveriesByPieceNumber    Owner;
			internal         ClaimsByPieceNumberAdapter Adapter;

			public StringBuilder DoPieces( string txt )
			{
				var RetVal = new StringBuilder( txt );

				var BArray = new BitArray( MissingPieces );
				var C      = BArray.Count;

				var First = true;

				for( var I = 0; I < C; I++ )
				{
					if( BArray[ I ] )
					{
						if( First )
						{
							First = false;
							RetVal.Append( "\r\n" );
						}
						else
							RetVal.Append( ", " );

						RetVal.Append( $"-{I}" );
					}
				}
				return RetVal;
			}

			public PieceNumberScannedTrip( DeliveriesByPieceNumber owner, ClaimsByPieceNumberAdapter adapter, ScannedTrip trip ) : base( trip )
			{
				Owner   = owner;
				Adapter = adapter;
			}
		}

		internal class PieceNumberTripCountData : PiecesTripCountData
		{
			internal ClaimsByPieceNumberAdapter Adapter;

			public PieceNumberTripCountData( ClaimsByPieceNumberAdapter adapter, BaseTripAdapter.TripCountData data ) : base( data )
			{
				Adapter = adapter;
			}
		}

		protected override bool UpdatePiecesToScanned => IsDelivery;

		protected override BaseTripAdapter GetClaimsPiecesAdapter =>
			Adapter = new ClaimsByPieceNumberAdapter( Activity, ListView, !IsDelivery );

		protected override string Title => $"{base.Title} (By Piece Number)";

		private ClaimsByPieceNumberAdapter Adapter;


		private static void UpdateBillingNote( PieceNumberScannedTrip sTrip, ICollection found )
		{
			sTrip.BillingNote = $"{DateTime.Now:g}\r\n{sTrip.AsText}\r\n\r\n{sTrip.BillingNote}";

			if( found != null )
			{
				var AnyFound = found.Cast<bool>().Any( b => b );

				if( AnyFound )
				{
					found.CopyTo( sTrip.MissingPieces, 0 );
					sTrip.BillingNote = $"{sTrip.DoPieces( "Found piece(s)" )}\r\n\r\n{sTrip.BillingNote}";
				}
			}
		}

		private BitArray MakeMissingPiecesArray( string tripId )
		{
			var BArray = new BitArray( new byte[ 255 ] );

			foreach( var (K, Value) in Adapter.PiecesLookup )
			{
				var Key = K;

				if( Key.StartsWith( tripId ) )
				{
					var Ndx = Key.LastIndexOf( '-' );

					if( Ndx >= 0 )
					{
						Key = Key[ (Ndx + 1).. ];

						if( !int.TryParse( Key, out Ndx ) )
							Ndx = 1;
					}
					else
						Ndx = 1;

					BArray[ Ndx ] = !Value;
				}
			}
			return BArray;
		}

		protected override ScannedTrip MatchTrip( List<Trip> scannerTrips, string scannedId )
		{
			var P = scannedId.LastIndexOf( '-' );

			if( P >= 0 )
				scannedId = scannedId[ ..P ];

			return base.MatchTrip( scannerTrips, scannedId );
		}

		protected override PiecesTripCountData GetPiecesTripCountData( BaseTripAdapter.TripCountData undel ) => new PieceNumberTripCountData( Adapter, undel );

		protected virtual ScannedTrip ConvertScannedTrip( ScannedTrip strip ) => new PieceNumberScannedTrip( this, Adapter, strip );

		protected override void PickupByPiecesUpdateNotes( Trip trip )
		{
			if( trip is ScannedTrip PnTrip )
			{
				var Bal = PnTrip.Balance;

				if( Bal > 0 )
				{
					if( Bal == PnTrip.Pieces )
						PnTrip.CurrentZone = "NOTHING TO PICK-UP";
					MakeMissingPiecesArray( PnTrip.TripId ).CopyTo( trip.MissingPieces, 0 );

					if( ConvertScannedTrip( PnTrip ) is PieceNumberScannedTrip ConvertedTrip )
					{
						UpdateBillingNote( ConvertedTrip, null );
						trip.BillingNote = ConvertedTrip.BillingNote;
					}
				}
			}
		}

		protected override void DeliveryByPiecesUpdateNotes( Trip trip )
		{
			if( trip is ScannedTrip STrip )
			{
				var OldMissing = new BitArray( STrip.MissingPieces );
				var NewMissing = MakeMissingPiecesArray( STrip.TripId );
				var AllMissing = ( (BitArray)NewMissing.Clone() ).Or( OldMissing ); // One could be a found missing piece from pickup
				var Found      = ( (BitArray)AllMissing.Clone() ).Xor( NewMissing );
				( (BitArray)NewMissing.Clone() ).Xor( OldMissing ).CopyTo( STrip.MissingPieces, 0 );

				if( ConvertScannedTrip( STrip ) is PieceNumberScannedTrip ConvertedTrip )
				{
					UpdateBillingNote( ConvertedTrip, Found );
					trip.BillingNote = ConvertedTrip.BillingNote;
				}
			}
		}
	}
}