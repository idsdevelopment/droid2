using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Service.Interfaces;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard.PickupDeliveries
{
	public partial class DriverPickup
	{
		protected Button CancelDeliveryButton;

		private void AskCancel()
		{
			Globals.Dialogues.Confirm( CANCEL_DELIVERY, () =>
			                                            {
				                                            DisableUpdateDisplayFromDatabase = false;
				                                            UpdateDisplayFromDatabase();
				                                            CancelDelivery = true; //Must be before Scanner.Abort();
				                                            Scanner.Abort();
				                                            TripListAdapter.OnTripClick = OnTripClick;
			                                            }, () =>
			                                               {
			                                               } );
		}

		protected virtual void LocationWithoutPiecesEnableNext()
		{
			RunOnUiThread( () =>
			               {
				               AcceptButton.Enabled = TripListAdapter.AllHidden;
			               } );
		}

		private async Task LocationWithoutPieces()
		{
			CancelDeliveryButton = FindViewById<Button>( Resource.Id.cancelDeliveryButton );

			CancelDeliveryButton.Click += ( sender, args ) =>
			                              {
				                              AskCancel();
			                              };

			RunOnUiThread( () =>
			               {
				               AcceptButton.Text = "Next";
				               Reference.Focusable = false;
				               Weight.Focusable = false;
				               UndeliverableLayout.Visibility = ViewStates.Gone;
			               } );

			AcceptButton.Click += OverrideAcceptEvent( ( sender, args ) =>
			                                           {
				                                           var Refs = new List<string>();
				                                           decimal TotalPieces = 0;
				                                           decimal TotalWeight = 0;

				                                           foreach( var Trip in TripListAdapter.Trips )
				                                           {
					                                           TotalPieces += Trip.Pieces;
					                                           TotalWeight += Trip.Weight;

					                                           var Temp = Trip.Reference?.Trim() ?? "";

					                                           if( Refs.All( Ref => Ref != Temp ) )
						                                           Refs.Add( Temp );
				                                           }

				                                           string CombinedRef = null;

				                                           foreach( var Ref in Refs )
				                                           {
					                                           if( CombinedRef != null )
						                                           CombinedRef += ", ";
					                                           CombinedRef += Ref;
				                                           }

				                                           CurrentTrip = null;

				                                           SetPiecesWeightReference( new Trip
				                                                                     {
					                                                                     Reference = CombinedRef,
					                                                                     Pieces = TotalPieces,
					                                                                     Weight = TotalWeight,
					                                                                     Status = STATUS.PICKED_UP
				                                                                     } );

				                                           ReferenceLayout.Visibility = ViewStates.Visible;
				                                           WeightLayout.Visibility = ViewStates.Visible;
				                                           Reference.Text = CombinedRef;
				                                           PodName.Text = "";

				                                           HideDeliveryButtons = false;
				                                           MainActivity.OnBackKey = CloseDetails;

				                                           IsDelivery = true;

				                                           ShowDetails();
			                                           } );

			while( Executing )
			{
				await Task.Run( () =>
				                {
					                RunOnUiThread( () =>
					                               {
						                               HideDeliveryButtons = true;
						                               MainButtonLayout.Visibility = ViewStates.Visible;
						                               CancelDeliveryButton.Enabled = false;
						                               AcceptButton.Enabled = false;
						                               ScannerLayout.Visibility = UseLocationBarcodes ? ViewStates.Visible : ViewStates.Gone;
					                               } );
				                } );

				if( TryGetLocation( out var Location, out var LocationCode ) )
				{
					if( !Executing )
						return;

					var SaveOnTripClick = TripListAdapter.OnTripClick;
					var SaveOnLongClick = TripListAdapter.OnTripLongClick;
					TripListAdapter.OnTripLongClick = null;
					MainActivity.OnBackKey = AskCancel;

					try
					{
						TripListAdapter.OnTripClick = async ( trip, isFinalising ) =>
						                              {
							                              if( trip is ScannedTrip STrip )
							                              {
								                              PickupDeliveriesOptionsActivity.Status = PickupDeliveriesOptionsActivity.SCAN_STATUS.BUSY;
								                              PickupDeliveriesOptionsActivity.SelectedTrip = STrip;
								                              var Intent = new Intent( Activity, typeof( PickupDeliveriesOptionsActivity ) );
								                              Intent.PutExtra( PickupDeliveriesOptionsActivity.MINIMAL, true );
								                              Intent.PutExtra( PickupDeliveriesOptionsActivity.SHOW_UNDELIVERABLE, true );
								                              Intent.PutExtra( PickupDeliveriesOptionsActivity.HIDE_RESET, true );
								                              Intent.PutExtra( PickupDeliveriesOptionsActivity.SHOW_SINGLE_ITEM, true );

								                              Activity.StartActivity( Intent );

								                              await Task.Run( async () =>
								                                              {
									                                              while( PickupDeliveriesOptionsActivity.Status == PickupDeliveriesOptionsActivity.SCAN_STATUS.BUSY )
										                                              Thread.Sleep( 100 );

									                                              switch( PickupDeliveriesOptionsActivity.Status )
									                                              {
									                                              case PickupDeliveriesOptionsActivity.SCAN_STATUS.UNDELIVERABLE when ConfirmUndeliverable():
										                                              UndeliverableReasonActivity.SelectedTrip = trip;
										                                              UndeliverableReasonActivity.ScanAction = UndeliverableReasonActivity.SCAN_ACTION.BUSY;

										                                              Intent = new Intent( Activity, typeof( UndeliverableReasonActivity ) );
										                                              StartActivity( Intent );

										                                              while( UndeliverableReasonActivity.ScanAction == UndeliverableReasonActivity.SCAN_ACTION.BUSY )
											                                              Thread.Sleep( 100 );

										                                              STrip.UndeliverableReason = UndeliverableReasonActivity.ScanAction == UndeliverableReasonActivity.SCAN_ACTION.OK
											                                                                          ? UndeliverableReasonActivity.Reason
											                                                                          : "";

										                                              STrip.Undeliverable = true;
										                                              STrip.Pieces = 0;
										                                              TripListAdapter.HideTrip( STrip );
										                                              LocationWithoutPiecesEnableNext();

										                                              break;

									                                              case PickupDeliveriesOptionsActivity.SCAN_STATUS.SINGLE_ITEM:
										                                              IsDelivery = true;
										                                              CurrentTrip = STrip;

										                                              if( await OnShowDetail( STrip, false, false, true ) )
										                                              {
											                                              SetPiecesWeightReference( STrip );
											                                              ShowDetails();

											                                              RunOnUiThread( () =>
											                                                             {
												                                                             DeliveryButtonsLayout.Visibility = ViewStates.Visible;
												                                                             UndeliverableLayout.Visibility = ViewStates.Gone;
											                                                             } );

											                                              await Task.Run( () =>
											                                                              {
												                                                              while( DetailsVisible )
													                                                              Thread.Sleep( 250 );

												                                                              if( CurrentTrip == null ) // Trip Processed
												                                                              {
                                                                                                                  RunOnUiThread( () => 
                                                                                                                  {
                                                                                                                      TripListAdapter.Remove( STrip );

                                                                                                                      STrip.Status = STATUS.PICKED_UP;
                                                                                                                      Globals.Database.UpdateTrip( STrip );

                                                                                                                      var Id = STrip.TripId;

                                                                                                                      foreach( var T in Location.Trips )
                                                                                                                      {
                                                                                                                          if( T.TripId == Id)
                                                                                                                          {
                                                                                                                              T.Status = STATUS.PICKED_UP;
                                                                                                                              break;
                                                                                                                          }
                                                                                                                      }

                                                                                                                      if( TripListAdapter.Trips.Count <= 0 )
                                                                                                                      {
                                                                                                                          Globals.Dialogues.Inform( "All items processed.", () =>
                                                                                                                          {
                                                                                                                              CancelDelivery = true;
                                                                                                                              Scanner.Abort();
                                                                                                                          } );
                                                                                                                      }
                                                                                                                  } );
												                                                              }
											                                                              } );
										                                              }

										                                              break;
									                                              }
								                                              } );
							                              }
						                              };

						var Trips = ( from St in FilterLocationTrips( Location.Trips.Select( locationTrip => new ScannedTrip( locationTrip ) ).ToList() )
						              select (Trip)St ).ToList();

						DisableUpdateDisplayFromDatabase = true;

						TripListAdapter.Clear();
						TripListAdapter.Add( Trips );
						TripCount = Trips.Count;

						RunOnUiThread( () =>
						               {
							               CancelDeliveryButton.Enabled = true;
							               AcceptButton.Enabled = false;
						               } );

						CancelDelivery = false;

						// Has to be here to avoid display transition
						HideTripDetails = true;

						TripListAdapter.OnTripLongClick = trip =>
						                                  {
							                                  Scanner.InjectBarcode( trip.TripId );
						                                  };

						while( Executing && !CancelDelivery )
						{
							Scanner.Placeholder = "Trip Id";
							var Trip = await Scanner.Scan();

							if( CancelDelivery )
								break;

							if( !Scanner.WasBackKey && !string.IsNullOrWhiteSpace( Trip ) )
							{
								Trip = XlateTripId( Trip );

								if( TripListAdapter.HasTrip( Trip ) )
									TripListAdapter.HideTrip( Trip );
								else
									Globals.Dialogues.NotForThisLocation();

								LocationWithoutPiecesEnableNext();
							}
						}
					}
					finally
					{
						MainActivity.OnBackKey = null;
						TripListAdapter.OnTripClick = SaveOnTripClick;
						TripListAdapter.OnTripLongClick = SaveOnLongClick;

						DisableUpdateDisplayFromDatabase = false;
						HideTripDetails = false;
					}
				}
				else
				{
					if( !string.IsNullOrWhiteSpace( LocationCode ) )
						Globals.Dialogues.Error( UNKNOWN_LOC );
				}
			}
		}
	}
}