using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;

namespace Droid2.Fragments.Standard
{
	[ Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
		Label = "ClaimsPiecesActivity",
		AlwaysRetainTaskState = true,
		ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Keyboard
	) ]
	public class PickupDeliveriesOptionsActivity : Activity
	{
		public enum SCAN_STATUS
		{
			CANCEL,
			FILTER,
			UNDELIVERABLE,
			RESET_SCAN,
			REMOVE,
			SINGLE_ITEM,
			BUSY // For Thread
		}

		public const string ZONE_FILTER = "ZoneFilter",
							SHOW_REMOVE = "ShowRemove",
							MINIMAL = "Minimal",
							SHOW_UNDELIVERABLE = "ShowUndeliverable",
							HIDE_RESET = "HideReset",
							SHOW_SINGLE_ITEM = "ShowSingle";

		public static volatile SCAN_STATUS Status = SCAN_STATUS.CANCEL;
		public static ScannedTrip SelectedTrip;
		public static string ZoneFilter;

		private EditText ZoneFilterEdit;
		private Button UndeliverableButton;

		public override void OnBackPressed()
		{
			Status = SCAN_STATUS.CANCEL;
			Finish();
		}

		protected override void OnCreate( Bundle savedInstanceState )
		{
			base.OnCreate( savedInstanceState );

			if( SelectedTrip == null )
				Finish();
			else
			{
				var Trip = SelectedTrip;

				SetContentView( Resource.Layout.Activity_PickupDeliveriesOptions );
				FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
				{
					Status = SCAN_STATUS.CANCEL;
					Finish();
				};

				UndeliverableButton = FindViewById<Button>( Resource.Id.undeliverableButton );
				UndeliverableButton.Click += ( sender, args ) =>
				{
					Status = SCAN_STATUS.UNDELIVERABLE;
					Finish();
				};

				RunOnUiThread( () => { FindViewById<TextView>( Resource.Id.tripId ).Text = Trip.TripId; } );

				var RemoveButton = FindViewById<Button>( Resource.Id.removeButton );
				RemoveButton.Click += ( sender, args ) =>
				{
					Status = SCAN_STATUS.REMOVE;
					Finish();
				};

				var ResetButton = FindViewById<Button>( Resource.Id.resetScanButton );
				ResetButton.Click += ( sender, args ) =>
				{
					Status = SCAN_STATUS.RESET_SCAN;
					Finish();
				};

				var SingleItemLayout = FindViewById<FrameLayout>( Resource.Id.singleItemLayout );
				var SingleItemButton = SingleItemLayout.FindViewById<Button>( Resource.Id.singleItemButton );
				SingleItemButton.Click += ( sender, args ) =>
				{
					MainActivity.OnBackKey = null;
					Status = SCAN_STATUS.SINGLE_ITEM;
					Finish();
				};

				RunOnUiThread( () =>
				{
					SingleItemLayout.Visibility = Intent.GetBooleanExtra( SHOW_SINGLE_ITEM, false ) ? ViewStates.Visible : ViewStates.Gone;
					UndeliverableButton.Visibility = Intent.GetBooleanExtra( SHOW_UNDELIVERABLE, false ) ? ViewStates.Visible : ViewStates.Gone;
				} );

				var Minimal = Intent.GetBooleanExtra( MINIMAL, false );
				if( Minimal )
				{
					RunOnUiThread( () =>
					{
						RemoveButton.Visibility = Intent.GetBooleanExtra( SHOW_REMOVE, false ) ? ViewStates.Visible : ViewStates.Gone;
						ResetButton.Visibility = Intent.GetBooleanExtra( HIDE_RESET, false ) ? ViewStates.Gone : ViewStates.Visible;

						FindViewById<LinearLayout>( Resource.Id.zoneFilterLayout ).Visibility = ViewStates.Gone;
					} );
				}
				else
				{
					ZoneFilterEdit = FindViewById<EditText>( Resource.Id.zoneFilter );
					var Config = Globals.Database.Config;

					var ZText = Intent.GetStringExtra( ZONE_FILTER );
					if( string.IsNullOrEmpty( ZText ) )
						ZText = Config.ClaimsZoneFilter;

					RunOnUiThread( () => { ZoneFilterEdit.Text = ZText; } );

					FindViewById<Button>( Resource.Id.filterButton ).Click += ( sender, args ) =>
					{
						var Zone = ZoneFilterEdit.Text.Trim().ToUpper();
						RunOnUiThread( () => { ZoneFilterEdit.Text = Zone; } );

						if( !Globals.Database.HasZone( Zone ) )
						{
							Globals.Sounds.PlayBadScanSound();
							ZoneFilterEdit.Text = "";
						}
						else
						{
							Config = Globals.Database.Config;
							Config.ClaimsZoneFilter = Zone;
							Globals.Database.Config = Config;
							ZoneFilter = Zone;
							Status = SCAN_STATUS.FILTER;
							Finish();
						}
					};

					FindViewById<Button>( Resource.Id.resetFilterButton ).Click += ( sender, args ) =>
					{
						ZoneFilter = "";
						Status = SCAN_STATUS.FILTER;
						Finish();
					};
				}
			}
		}
	}
}