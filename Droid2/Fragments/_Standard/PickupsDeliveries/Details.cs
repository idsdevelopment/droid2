#nullable enable

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Signature;
using Service.Interfaces;
using Utils;
using Timer = System.Timers.Timer;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard.PickupDeliveries
{
	public partial class DriverPickup
	{
		protected Button UpdateTripButton { get; private set; } = null!;

		public virtual UNDELIVERABLE_MODE UndeliverableMode => Globals.Preferences.UndeliverableKeepTripId ? UNDELIVERABLE_MODE.STATUS_CHANGE : UNDELIVERABLE_MODE.NEW_TRIP;

		protected virtual bool NeedPodOrSignatureOnPickup => false;

		protected bool DetailsVisible
		{
			get
			{
				var HaveDetails = false;
				var RetVal      = false;

				RunOnUiThread( () =>
				               {
					               RetVal      = DetailsLayout.Visibility == ViewStates.Visible;
					               HaveDetails = true;
				               } );

				while( !HaveDetails )
					Thread.Sleep( 1 );

				return RetVal;
			}
		}

		protected bool HideDeliveryButtons
		{
			get => _HideDeliveryButtons;
			set
			{
				RunOnUiThread( () =>
				               {
					               _HideDeliveryButtons = value;
					               var Vis = value ? ViewStates.Gone : ViewStates.Visible;
					               DeliveryButtonsLayout.Visibility = Vis;
					               PodLayout.Visibility             = Vis;
					               UndeliverableText.Visibility     = Vis;
				               } );
			}
		}

		protected bool HideTripDetails
		{
			get => _HideTripDetails;
			set
			{
				_HideTripDetails = value;

				RunOnUiThread( () =>
				               {
					               var Vis = value ? ViewStates.Gone : ViewStates.Visible;
					               TripDetailsLayout.Visibility = Vis;
					               TripIdLayout.Visibility      = Vis;
				               } );
			}
		}

		protected virtual bool UpdatePiecesToScanned => true;

		private readonly ColorDrawable LightYellow = new(Color.LightYellow),
		                               Transparent = new(Color.Transparent);

		private bool _HideDeliveryButtons;

		private bool _HideTripDetails;

		private ViewGroup.LayoutParams DetailsLayoutParams      = null!;
		private Animation              DetailsLayoutPullInLeft  = null!;
		private Animation              DetailsLayoutPushOutLeft = null!;

		private Timer? PocketTimer;

		private bool SaveUpdateTripButtonEnable,
		             SaveReferenceEnable,
		             SavePiecesEnable,
		             SaveWeightEnable,
		             SavePodEnable,
		             SaveSignEnable,
		             SaveDetailsEnable;

		private Animation SignatureLayoutPullInLeft  = null!,
		                  SignatureLayoutPushOutLeft = null!;

		private SignatureResult SignaturePoints = null!;

		private Button SignButton = null!;

		private TextView UndeliverableText = null!,
		                 BillingAccount    = null!;

		protected Trip? CurrentTrip;

		protected string? CustomerSignature;

		protected LinearLayout DetailsLayout         = null!,
		                       DeliveryButtonsLayout = null!,
		                       TripIdLayout          = null!,
		                       PodLayout             = null!,
		                       PalletsLayout         = null!,
		                       PiecesWeightLayout    = null!;

		protected Button FutileButton = null!;

		protected bool     IsDelivery;
		protected EditText PodName = null!;

		protected bool ProcessingDeliveries;

		protected TextView Reference            = null!,
		                   Pieces               = null!,
		                   Weight               = null!,
		                   Pallets              = null!,
		                   TripId               = null!,
		                   ServiceLevel         = null!,
		                   PackageType          = null!,
		                   DueTime              = null!,
		                   PickupCompany        = null!,
		                   PickupCompanyAddress = null!,
		                   PickupNotes          = null!,
		                   PickupContact        = null!,
		                   DeliveryCompany      = null!,
		                   DeliveryAddress      = null!,
		                   DeliveryNotes        = null!,
		                   DeliveryContact      = null!,
		                   PodNameText          = null!;

		protected bool ShowPod = true;

		protected Button UndeliverableButton = null!,
		                 CancelBtn           = null!;

		protected LinearLayout UndeliverableLayout = null!;

		protected virtual bool EnableUpdateTrip()
		{
			var Enab = EnablePickupDeliver
			           && ( ( !IsDelivery && !NeedPodOrSignatureOnPickup )
			                || ( PickupDeliveryByPieces && !InAcceptDelivery )
			                || ( ( !RequiresPod || ( PodName.Text!.Length > 0 ) )
			                     && ( !RequiresSig || !string.IsNullOrWhiteSpace( CustomerSignature ) ) ) );

			if( Enab )
				Enab = !RequiresRef || ( RequiresRef && ( Reference.Text!.Trim().Length > 0 ) );

			return Enab;
		}

		protected void UpdateTripButtonOnClick( object sender, EventArgs eventArgs )
		{
			RunOnUiThread( () => { UpdateTripButton.Enabled = false; } );

			if( !UseLocationBarcodes && PickupDeliveryByPieces && !ProcessPiecesPickup )
				PickupByPieces();
			else
			{
				if( InLocationScan )
				{
					DoUpdateTrip();

					RunOnUiThread( () =>
					               {
						               var Adp = GetPickupDeliveriesAdapter;
						               ListView.Adapter = Adp;

						               DetailsLayout.Visibility = ViewStates.Gone;
						               MainLayout.Visibility    = ViewStates.Visible;
						               MainLayout.BringToFront();
					               } );
				}
				else
					ConfirmUpdateTrip( DoUpdateTrip, () => { RunOnUiThread( () => { UpdateTripButton.Enabled = true; } ); } );
			}
			Globals.Keyboard.Hide = true;
		}

		protected void CloseDetails()
		{
			var Trp = CurrentTrip;

			if( Trp is not null )
			{
				RunOnUiThread( async () =>
				               {
					               if( await OnShowDetail( Trp, false, false, false ) )
					               {
						               MainActivity.OnBackKey = null;

						               if( !ProcessingDeliveries && !Trp.ReadByDriver ) // Don't Change processed trips (InLocationScan)
						               {
							               Trp.ReadByDriver = true;
							               UpdateTrip( Trp );
						               }

						               RunOnUiThread( () =>
						                              {
							                              BillingAccount.Text       = "";
							                              BillingAccount.Visibility = ViewStates.Gone;

							                              if( Scanner != null )
							                              {
								                              ScannerLayout.Visibility = UseLocationBarcodes ? ViewStates.Visible : ViewStates.Gone;
								                              Scanner.Abort();
							                              }

							                              HideDetails();
						                              } );
					               }

					               ProcessingDeliveries = false;
				               } );
			}
		}

		protected virtual void SetUpdateTripButtonText()
		{
			UpdateTripButton.Text = CurrentTrip is null ? "" : CurrentTrip.Status < STATUS.PICKED_UP ? "Pick up" : "Deliver";
		}


		protected virtual void ShowDetails( bool hideUndeliverable = false )
		{
			RunOnUiThread( () =>
			               {
				               MainActivity.OnBackKey ??= CloseDetails;

				               PiecesWeightLayout.Visibility = Globals.Preferences.HidePiecesAndWeight ? ViewStates.Gone : ViewStates.Visible;
				               ReferenceLayout.Visibility    = Globals.Preferences.HideReference ? ViewStates.Gone : ViewStates.Visible;

				               // Current trip null from LocationWithoutPieces
				               var Vis = ( CurrentTrip == null ) || ( CurrentTrip.Status == STATUS.PICKED_UP ) ? ViewStates.Visible : ViewStates.Gone;
				               UndeliverableLayout.Visibility = Globals.Preferences.HideUndeliverableButton || hideUndeliverable ? ViewStates.Gone : Vis;
				               PodLayout.Visibility           = Vis;
				               PodName.Text                   = "";

				               SetUpdateTripButtonText();

				               var Allow = Globals.Preferences.EditPiecesWeightReference;

				               Reference.Focusable            = Allow;
				               Reference.FocusableInTouchMode = Allow; // Appears to require both

				               Weight.Focusable            = Allow;
				               Weight.FocusableInTouchMode = Allow;

				               Pieces.Focusable            = Allow;
				               Pieces.FocusableInTouchMode = Allow;

				               UpdateTripButton.Enabled = EnableUpdateTrip();

				               DetailsLayout.Visibility = ViewStates.Visible;

				               var T = new Timer( 500 );

				               T.Elapsed += ( _, _ ) =>
				                            {
					                            T.Stop();

					                            RunOnUiThread( () =>
					                                           {
						                                           var Lp = DetailsLayoutParams;
						                                           Lp.Width = ViewGroup.LayoutParams.MatchParent;
						                                           var Dl = DetailsLayout;
						                                           Dl.LayoutParameters = Lp;
						                                           Dl.StartAnimation( DetailsLayoutPullInLeft );
					                                           } );
					                            T.Dispose();
				                            };
				               T.Start();
			               } );
		}

		protected virtual bool AllowReferenceEdit( bool allowEdit ) => allowEdit;

		protected virtual async void OnTripClick( Trip trip, bool finalising )
		{
			if( await OnShowDetail( trip, true, finalising, true ) )
			{
				CustomerSignature      = "";
				MainActivity.OnBackKey = CloseDetails;

				CurrentTrip = trip;

				RunOnUiThread( () =>
				               {
					               ChargeLayout.Visibility = ViewStates.Gone;

					               if( Globals.Preferences.ShowCharges )
					               {
						               var Text = new StringBuilder();

						               if( trip.ShowChargeAmount && ( trip.ChargeAmount != 0 ) )
							               Text.Append( $"Charge amount: {trip.ChargeAmount:C})\r\n" );

						               foreach( var Charge in Globals.Database.GetChargesForTrip( trip.TripId ) )
							               Text.Append( $"{Charge.Label}: {Charge.Value:C}\r\n" );

						               if( Text.Length > 0 )
						               {
							               ChargeAmount.Text       = Text.ToString();
							               ChargeLayout.Visibility = ViewStates.Visible;
						               }
					               }

					               if( !HideDeliveryButtons )
					               {
						               if( trip.UndeliverableMode != UNDELIVERABLE_MODE.NONE )
						               {
							               UndeliverableButton.Text     = "Process Undeliverable";
							               UndeliverableText.Visibility = ViewStates.Visible;
						               }
						               else
						               {
							               UndeliverableText.Visibility = ViewStates.Gone;

							               if( ( trip.Status != STATUS.PICKED_UP ) || ( ( trip.Status == STATUS.PICKED_UP ) && PickupDeliveryByPieces ) )
								               UndeliverableLayout.Visibility = ViewStates.Gone;
							               else
							               {
								               UndeliverableButton.Text       = "Undeliverable";
								               UndeliverableLayout.Visibility = Globals.Preferences.HideUndeliverableButton ? ViewStates.Gone : ViewStates.Visible;
							               }
						               }
					               }
					               else if( Globals.Preferences.HideUndeliverableButton )
						               UndeliverableLayout.Visibility = ViewStates.Gone;

					               TripId.Text = trip.TripId;
					               var TempBillingAccount = trip.BillingAccount is null ? "" : trip.BillingAccount.Trim();
					               BillingAccount.Text       = TempBillingAccount;
					               BillingAccount.Visibility = TempBillingAccount.IsNotNullOrWhiteSpace() ? ViewStates.Visible : ViewStates.Gone;

					               PodName.Text      = trip.PodName;
					               ServiceLevel.Text = trip.ServiceLevel;
					               PackageType.Text  = trip.PackageType;

					               SetPiecesWeightReference( trip );

					               PickupCompany.Text        = trip.PickupCompanyName;
					               PickupCompanyAddress.Text = PickupDeliveriesAdapter.BuildAddress( trip.PickupSuite, trip.PickupAddressLine1, trip.PickupCity, trip.PickupPostalCode );

					               PickupNotes.Text   = trip.PickupNote;
					               PickupContact.Text = trip.PickupContact;

					               DeliveryCompany.Text = trip.DeliveryCompanyName;
					               DeliveryAddress.Text = PickupDeliveriesAdapter.BuildAddress( trip.DeliverySuite, trip.DeliveryAddressLine1, trip.DeliveryCity, trip.DeliveryPostalCode );
					               DeliveryNotes.Text   = trip.DeliveryNote;
					               DeliveryContact.Text = trip.DeliveryContact;

					               var Delivery = trip.Status == STATUS.PICKED_UP;

					               string BText,
					                      PodText;

					               DateTime ReadyTime;

					               if( Delivery )
					               {
						               IsDelivery = true;
						               BText      = "Deliver";
						               PodText    = "POD Name:";

						               PickupCompany.Background = Transparent;
						               PickupCompany.SetTextColor( Color.White );
						               PickupCompany.Typeface = Typeface.Default;

						               DeliveryCompany.Background = LightYellow;
						               DeliveryCompany.SetTextColor( Color.Black );
						               DeliveryCompany.Typeface = Typeface.DefaultBold;

						               ReadyTime = trip.DueTime.DateTime;
					               }
					               else
					               {
						               IsDelivery = false;
						               BText      = "Pick up";
						               PodText    = "POP Name:";

						               PickupCompany.Background = LightYellow;
						               PickupCompany.SetTextColor( Color.Black );
						               PickupCompany.Typeface = Typeface.DefaultBold;

						               DeliveryCompany.Background = Transparent;
						               DeliveryCompany.SetTextColor( Color.White );
						               DeliveryCompany.Typeface = Typeface.Default;

						               ReadyTime = trip.ReadyTime.DateTime;
					               }

					               UpdateTripButton.Text = BText;
					               PodNameText.Text      = PodText;
					               DueTime.Text          = ReadyTime.ToString( "f" );

					               var P = trip.Pallets?.Trim();

					               if( !string.IsNullOrEmpty( P ) )
					               {
						               Pallets.Text             = trip.Pallets;
						               PalletsLayout.Visibility = ViewStates.Visible;
					               }
					               else
						               PalletsLayout.Visibility = ViewStates.Gone;

					               ShowDetails();

					               if( PickupDeliveryByPieces && InAcceptDelivery )
						               UndeliverableLayout.Visibility = ViewStates.Gone;

					               PodLayout.Visibility = ShowPod ? ViewStates.Visible : ViewStates.Gone;
				               } );
			}
		}

		protected virtual void UpdateNotes( Trip trip )
		{
			if( PickupDeliveryByPieces )
			{
				if( IsPickup )
					PickupByPiecesUpdateNotes( trip );
				else
					DeliveryByPiecesUpdateNotes( trip );
			}
		}

		protected virtual bool ProcessSingleTrip( bool processMany ) => !processMany;

		protected virtual bool IsTripPickup( ScannedTrip t ) => PickupDeliveryByPieces && IsPickup;

		protected virtual void AfterUpdateTrips()
		{
		}

		protected virtual void BeforeUpdateTrips()
		{
		}

		protected virtual void UpdateTripTimes( STATUS originalStatus, DateTimeOffset originalPickupTime, DateTimeOffset originalDeliveryTime, DateTimeOffset currentTime, Trip trip )
		{
		}

		protected virtual bool IgnoreTrip( ScannedTrip trip ) => trip.Scanned <= 0; // Ignore zero scanned

		protected virtual Task? OnUpdateTrip()
		{
			BeforeUpdateTrips();

			ProcessingDeliveries = true;

			var BatchMode = Globals.Preferences.BatchMode;
			var AllowEdit = Globals.Preferences.EditPiecesWeightReference && !BatchMode && UpdatePiecesToScanned;

			var Time = DateTimeExtensions.NowKindUnspecified;

			Task? Tsk = null;

			void PiecesWeight( ITrip t )
			{
				if( !decimal.TryParse( Weight.Text!.Trim(), out var Wght ) )
					Wght = 0;
				t.Weight = Wght;

				if( !decimal.TryParse( Pieces.Text!.Trim(), out var Pcs ) )
					Pcs = 0;
				t.Pieces = Pcs;
			}

			void UpdateAdapter()
			{
				var Tps = ( from T in Globals.Database.Trips
				            where T.Status < STATUS.VERIFIED
				            select T ).ToList();

				RunOnUiThread( () =>
				               {
					               TripListAdapter.UpdateTrips( Tps );
					               TripCount = Tps.Count;
				               } );

				AfterUpdateTrips();
				Globals.Database.DisablePolling = false;
			}

			if( !ProcessSingleTrip( ( PickupDeliveryByPieces && InLocationScan ) || BatchMode ) ) // !InLocationScan == Forced delivery
			{
				var Trps = new List<Trip>( BatchMode ? TripListAdapter.Trips : ClaimsPiecesAdapter.Trips ); // Adapter can be updated outside of foreach look

				var Pod = PodName.Text;
				var Sig = CustomerSignature;
				CustomerSignature = null;

				Tsk = Task.Run( () =>
				                {
					                foreach( var Trip in Trps )
					                {
						                var Undeliverable = false;

						                void Details()
						                {
							                Trip.PodName   = Pod;
							                Trip.Signature = Sig ?? "";

							                // ReSharper disable once AccessToModifiedClosure
							                if( AllowEdit && !Undeliverable )
							                {
								                Trip.Reference = Reference.Text;

								                if( !PickupDeliveryByPieces )
									                PiecesWeight( Trip );
							                }

							                Trip.UpdatedTime = Time;
						                }

						                if( Trip is ScannedTrip STrip )
						                {
							                if( STrip.Status != STATUS.DELETED )
							                {
								                var OriginalPickupTime   = STrip.PickupTime;
								                var OriginalDeliveryTime = STrip.DeliveryTime;
								                var OriginalStatus       = STrip.Status;

								                if( IsTripPickup( STrip ) && ( STrip.Status != STATUS.PICKED_UP ) )
								                {
									                var Sc = STrip.Scanned;

									                if( Sc > 0 )
									                {
										                if( UpdatePiecesToScanned || PickupDeliveryByPieces )
											                Trip.Pieces = Sc;

										                Trip.Status = STATUS.PICKED_UP;
									                }

									                Trip.CurrentZone = Trip.PickupZone;

									                Trip.PickupArrivalTime = Time;
									                Trip.PickupTime        = Time.AddMinutes( 2 );

									                Details();
								                }
								                else
								                {
									                if( IgnoreTrip( STrip ) )
										                continue;

									                Undeliverable = STrip.Undeliverable;

									                if( Undeliverable )
									                {
										                STrip.UndeliverableMode = UndeliverableMode;

										                if( UpdatePiecesToScanned )
											                STrip.Pieces = STrip.Scanned;
									                }
									                else
									                {
										                Trip.Status      = STATUS.VERIFIED;
										                Trip.CurrentZone = Trip.DeliveryZone;

										                Trip.DeliveryArrivalTime = Time;
										                Trip.DeliveryTime        = Time;
										                Details();
									                }
								                }

								                UpdateTripTimes( OriginalStatus, OriginalPickupTime, OriginalDeliveryTime, Time, STrip );
							                }
						                }

						                UpdateNotes( Trip );
						                Globals.Database.UpdateTrip( Trip, Trip.Status is STATUS.VERIFIED or STATUS.DELETED );
					                }

					                UpdateAdapter();
				                } );
			}
			else
			{
				var Trip = CurrentTrip;

				if( Trip != null )
				{
					CurrentTrip = null;

					if( Trip.Status != STATUS.DELETED )
					{
						if( Trip.Status == STATUS.PICKED_UP )
						{
							Trip.Status      = STATUS.VERIFIED;
							Trip.CurrentZone = Trip.PickupZone;

							Trip.DeliveryArrivalTime = Time;
							Trip.DeliveryTime        = Time;
						}
						else
						{
							Trip.Status      = STATUS.PICKED_UP;
							Trip.CurrentZone = Trip.DeliveryZone;

							Trip.PickupArrivalTime = Time;
							Trip.PickupTime        = Time;
						}

						Trip.UpdatedTime = Time;

						Trip.PodName = PodName.Text!.Trim();

						Trip.Signature    = CustomerSignature ?? "";
						CustomerSignature = null;

						if( AllowEdit )
						{
							Trip.Reference = Reference.Text;
							PiecesWeight( Trip );
						}

						Trip.UpdatedTime = Time;
					}

					UpdateNotes( Trip );
					UpdateTrip( Trip );
					AfterUpdateTrips();
					Globals.Database.DisablePolling = false;
				}
			}

			return Tsk;
		}

		protected virtual bool OnUndeliverable( Trip trip ) => true;

		protected virtual void ConfirmUpdateTrip( Func<Task?> onOk, Action onCancel )
		{
			onOk();
		}

		private void DoPocketLockout( View.TouchEventArgs @event )
		{
			if( PocketTimer is not null )
			{
				PocketTimer.Stop();
				PocketTimer.Dispose();
				PocketTimer = null;
			}

			if( @event.Event is not null )
			{
				switch( @event.Event.Action )
				{
				case MotionEventActions.Down:
					RunOnUiThread( () =>
					               {
						               SaveUpdateTripButtonEnable = UpdateTripButton.Enabled;
						               SaveReferenceEnable        = Reference.Enabled;
						               SavePiecesEnable           = Pieces.Enabled;
						               SaveWeightEnable           = Weight.Enabled;
						               SavePodEnable              = PodName.Enabled;
						               SaveSignEnable             = SignButton.Enabled;
						               SaveDetailsEnable          = DetailsLayout.Enabled;

						               UpdateTripButton.Enabled = false;
						               DetailsLayout.Enabled    = false;
						               SignButton.Enabled       = false;

						               Reference.Enabled = false;
						               Pieces.Enabled    = false;
						               Weight.Enabled    = false;
						               PodName.Enabled   = false;
					               } );
					goto case MotionEventActions.Up;

				case MotionEventActions.Up:
					PocketTimer = new Timer( 1000 );

					PocketTimer.Elapsed += ( _, _ ) =>
					                       {
						                       RunOnUiThread( () =>
						                                      {
							                                      UpdateTripButton.Enabled = SaveUpdateTripButtonEnable;
							                                      DetailsLayout.Enabled    = SaveDetailsEnable;
							                                      SignButton.Enabled       = SaveSignEnable;

							                                      Reference.Enabled = SaveReferenceEnable;
							                                      Pieces.Enabled    = SavePiecesEnable;
							                                      Weight.Enabled    = SaveWeightEnable;
							                                      PodName.Enabled   = SavePodEnable;
						                                      } );

						                       PocketTimer.Dispose();
						                       PocketTimer = null;
					                       };
					PocketTimer.Start();

					break;
				}
			}
		}

		private void DetailsBackButtonClick( object? sender, EventArgs? e )
		{
			CloseDetails();
		}

		private void DeliveryOnClick( object sender, EventArgs eventArgs )
		{
			var T = CurrentTrip;

			if( T is not null )
				Globals.Dialogues.MapDialer( false, T.DeliveryCompanyName, T.DeliveryAddressLine1, T.DeliveryPhone );
		}

		private void PickupOnClick( object sender, EventArgs eventArgs )
		{
			var T = CurrentTrip;

			if( T is not null )
				Globals.Dialogues.MapDialer( true, T.PickupCompanyName, T.PickupAddressLine1, T.PickupPhone );
		}

		private void HideDetails()
		{
			RunOnUiThread( () =>
			               {
				               UndeliverableLayout.Visibility = ViewStates.Gone;
				               DetailsLayout.Enabled          = false; // Enabled by event timer
				               UpdateTripButton.Enabled       = false;

				               AcceptButton.Enabled = false;

				               DetailsLayout.Invalidate();
				               DetailsLayout.ClearAnimation();
				               DetailsLayout.StartAnimation( DetailsLayoutPushOutLeft );
			               } );
		}

		private void SetPiecesWeightReference( ITrip trip )
		{
			var AllowEdit = !HideDeliveryButtons && Globals.Preferences.EditPiecesWeightReference;

			RunOnUiThread( () =>
			               {
				               Reference.Text = trip.Reference;
				               var Allow = AllowReferenceEdit( AllowEdit );

				               Reference.Focusable            = Allow;
				               Reference.FocusableInTouchMode = Allow; // Appears to require both

				               AllowEdit &= !PickupDeliveryByPieces;
				               var W = trip.Weight;
				               Weight.Text = W.ToString( W >= 100 ? "#####" : "##.##" );

				               Weight.Focusable            = AllowEdit;
				               Weight.FocusableInTouchMode = AllowEdit;

				               Pieces.Text = trip.Pieces.ToString( CultureInfo.InvariantCulture );

				               AllowEdit                   &= UpdatePiecesToScanned;
				               Pieces.Focusable            =  AllowEdit;
				               Pieces.FocusableInTouchMode =  AllowEdit;

				               var P = trip.Pallets?.Trim();

				               if( !string.IsNullOrEmpty( P ) )
				               {
					               Pallets.Text             = trip.Pallets;
					               PalletsLayout.Visibility = ViewStates.Visible;
				               }
				               else
					               PalletsLayout.Visibility = ViewStates.Gone;
			               } );
		}

		private void UpdateTrip( ITrip trip, bool delete = false )
		{
			Globals.Database.UpdateTrip( trip, delete );
			var Trips = Globals.Database.Trips;

			RunOnUiThread( () =>
			               {
				               TripListAdapter.UpdateTrips( ( from T in Trips
				                                              where T.Status < STATUS.VERIFIED
				                                              select (Trip)new ScannedTrip( T ) ).ToList() );
			               } );
		}

		private Task? DoUpdateTrip()
		{
			var Task = OnUpdateTrip();
			DetailsBackButtonClick( null, null );
			CancelDelivery = true;
			return Task;
		}

		private async void UndeliverableButtonOnClick( object sender, EventArgs eventArgs )
		{
			if( UndeliverableLayout.Visibility == ViewStates.Visible )
			{
				var Trp = CurrentTrip;

				if( Trp is not null )
				{
					await Task.Run( async () =>
					                {
						                var IsUndeliverable = false;

						                if( Trp.UndeliverableMode == UNDELIVERABLE_MODE.NONE )
						                {
							                var Completed = false;

							                ConfirmUndeliverable( async () =>
							                                      {
								                                      IsUndeliverable = await Task.Run( () => OnUndeliverable( Trp ) );
								                                      Completed       = true;
							                                      },
							                                      () => { Completed = true; } );

							                await Task.Run( () =>
							                                {
								                                while( !Completed )
									                                Thread.Sleep( 100 );
							                                } );
						                }
						                else
							                IsUndeliverable = OnUndeliverable( Trp );

						                if( IsUndeliverable )
						                {
							                //    if( PickupDeliveryByPieces || !Globals.Preferences.BatchMode)
							                Trp.Pieces = 0; // All Undeliverable

							                Trp.UndeliverableMode = UndeliverableMode;

							                Globals.Database.UpdateTrip( Trp );
							                var Trips = Globals.Database.Trips;

							                RunOnUiThread( () =>
							                               {
								                               TripListAdapter.UpdateTrips( ( from T in Trips
								                                                              where T.Status < STATUS.VERIFIED
								                                                              select T ).ToList() );
							                               } );

							                CloseDetails();
						                }
					                } );
				}
			}
		}
	}
}