#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using Droid2.Adapters;
using Service.Interfaces;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard.PickupDeliveries
{
	public partial class DriverPickup
	{
		private bool            HavePiecesLocation;
		private LocationCompany PiecesLocation = null!;
		private string          ScannerText    = null!;

		private bool IsPickup,
		             ProcessPiecesPickup;

		protected virtual void PickupByPiecesUpdateNotes( Trip trip )
		{
			if( trip is ScannedTrip STrip )
			{
				var Bal = STrip.Balance;

				if( Bal > 0 )
				{
					if( Bal == STrip.Pieces )
						STrip.CurrentZone = "NOTHING TO PICK-UP";

					STrip.BillingNote = $"{DateTime.Now:g}\r\nPieces not picked up: {Bal}\r\n\r\n{STrip.BillingNote}";
				}
			}
		}

		protected virtual void DeliveryByPiecesUpdateNotes( Trip trip )
		{
		}

		protected virtual bool PiecesTripFilter( Trip trip, bool isPickup ) => true;

		protected virtual (List<Trip> Trips, bool IsPickup, string CompanyName, string Barcode ) FilterLocation( Trip currentTrip, List<Trip> trips )
		{
			var Pickup = CurrentTrip!.Status != STATUS.PICKED_UP;

			string CompanyName,
			       Barcode;

			if( Pickup )
			{
				CompanyName = CurrentTrip.PickupCompanyName;
				Barcode     = CurrentTrip.PickupBarcode;
			}
			else
			{
				CompanyName = CurrentTrip.DeliveryCompanyName;
				Barcode     = CurrentTrip.DeliveryBarcode;
			}

			var Trps = ( from Trip in TripListAdapter.Trips
			             where ( ( Pickup ? Trip.PickupCompanyName : Trip.DeliveryCompanyName ) == CompanyName ) && PiecesTripFilter( Trip, Pickup )
			             select Trip ).ToList();

			return ( Trps, Pickup, CompanyName, Barcode );
		}

		private void PickupByPieces()
		{
			ProcessPiecesPickup = false;
			var UnfilteredTrips = TripListAdapter.Trips;

			var (Trips, Pickup, CompanyName, Barcode) = FilterLocation( CurrentTrip!, UnfilteredTrips );
			IsPickup                                  = Pickup;

			ScannerText = "Scan Item Barcode";

			PiecesLocation = new LocationCompany
			                 {
				                 Company         = CompanyName,
				                 Trips           = Trips,
				                 LocationBarcode = Barcode,
				                 UnfilteredTrips = new List<Trip>( UnfilteredTrips )
			                 };
			CloseDetails();
			HavePiecesLocation = true;
			RunOnUiThread( () => { CancelBtn.Visibility = ViewStates.Visible; } );
		}
	}
}