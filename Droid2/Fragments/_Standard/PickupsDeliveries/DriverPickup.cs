#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content.PM;
using Android.Graphics;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Signature;
using Service.Interfaces;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard.PickupDeliveries
{
	public partial class DriverPickup : AFragment
	{
		public const string CANCEL_DELIVERY = "Are you sure that you wish to cancel the delivery?",
		                    CANCEL_PICKUP   = "Are you sure that you wish to cancel the pick-up?";

		protected bool LandscapeMode
		{
			get
			{
				var Temp = Globals.Settings.PickupDeliveriesLandscape;
				Orientation = Temp ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;

				return Temp;
			}

			set
			{
				Orientation                                = value ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;
				Globals.Settings.PickupDeliveriesLandscape = value;
			}
		}

		protected PickupDeliveriesAdapter TripListAdapter { get; private set; } = null!;

		protected ListView ListView { get; private set; } = null!;

		protected BarcodeInputFragment Scanner { get; private set; } = null!;

		protected virtual BaseTripAdapter         GetClaimsPiecesAdapter     => new ClaimsPiecesAdapter( Activity, ListView ) { ShowMinimalOptions = true };
		protected virtual PickupDeliveriesAdapter GetPickupDeliveriesAdapter => new(Activity, ListView, new List<Trip>(), OnTripClick);

		protected virtual bool EnablePickupDeliver => true;

		protected bool InLocationScan { get; private set; }

		protected virtual bool WaitingForTrips => !HaveTrips;

		public bool ShowScannedTotalOnly
		{
			get => _ShowScannedTotalOnly;
			set
			{
				_ShowScannedTotalOnly = value;

				RunOnUiThread( () => { TotalScannedLayout.Visibility = value ? ViewStates.Visible : ViewStates.Gone; } );
			}
		}


		private bool _ShowScannedTotalOnly;

		private bool HaveTrips;

		private bool InUpdate;

		private RelativeLayout MainLayout            = null!,
		                       WaitingForTripsLayout = null!;

 #pragma warning disable 612
		private SignatureFragment Signature = null!;
 #pragma warning restore 612

		private ViewGroup.LayoutParams SignatureLayoutParams = null!;

		private bool UseLocationBarcodes,
		             PickupDeliveryByPieces,
		             RequiresPod,
		             RequiresSig,
		             RequiresRef,
		             LocationOrPieces,
		             ShowPallets,
		             InAnimation;

		private TextView WaitingTripsTitle = null!,
		                 MainTitle         = null!,
		                 ChargeAmount      = null!,
		                 TotalScanned      = null!;

		protected readonly Dictionary<string, LocationCompany> Locations = new();

		protected Button AcceptButton = null!;


		protected bool            CancelDelivery;
		protected BaseTripAdapter ClaimsPiecesAdapter = null!;
		public    bool            DisableUpdateDisplayFromDatabase;

		protected RelativeLayout ScannerLayout = null!;

		protected LinearLayout SignatureLayout    = null!,
		                       TripDetailsLayout  = null!,
		                       ReferenceLayout    = null!,
		                       WeightLayout       = null!,
		                       MainButtonLayout   = null!,
		                       ChargeLayout       = null!,
		                       TotalScannedLayout = null!;

		protected virtual Task<bool> OnShowDetail( ITrip trip, bool isExamine, bool finalising, bool showing ) // Returning false stops action
		{
			HideDeliveryButtons = false;

			return Task.FromResult( true );
		}

		[Obsolete]
		protected override void OnCreateDynamicFragments()
		{
			if( LocationOrPieces )
			{
				Scanner = new BarcodeInputFragment();
				AddDynamicFragment( Resource.Id.scannerLayout, Scanner );
			}

			Signature = new SignatureFragment();
			AddDynamicFragment( Resource.Id.signatureLayout, Signature );
		}

		protected override void OnDestroyFragment()
		{
			Scanner.Abort();
		}

		public void WaitAnimation()
		{
			while( InAnimation )
				Thread.Sleep( 100 );

			Thread.Sleep( 300 );
		}

		protected virtual bool PodValid( string pod, out string validText )
		{
			validText = pod;

			return true;
		}

		protected void UpdateTitle()
		{
			RunOnUiThread( () => { MainTitle.Text = Title; } );
		}

		[Obsolete]
		protected override void OnCreateFragment()
		{
			BillingAccount = FindViewById<TextView>( Resource.Id.billingAccount )!;

			UseLocationBarcodes    = Globals.Preferences.UseLocationBarcodes;
			PickupDeliveryByPieces = Globals.Preferences.PickupDeliveryByPieces || Globals.Preferences.PickupDeliveryByPieceNumber;

			LocationOrPieces = UseLocationBarcodes || PickupDeliveryByPieces;

			RequiresPod = Globals.Preferences.RequiresPodName;
			RequiresSig = Globals.Preferences.RequiresSignature;
			RequiresRef = Globals.Preferences.RequiresReference;

			ShowPallets = Globals.Preferences.ShowPallets;

			PodNameText = FindViewById<TextView>( Resource.Id.podNameText )!;

			WaitingForTripsLayout = FindViewById<RelativeLayout>( Resource.Id.waitingTripsLayout )!;
			MainLayout            = FindViewById<RelativeLayout>( Resource.Id.mainLayout )!;
			MainButtonLayout      = FindViewById<LinearLayout>( Resource.Id.mainButtonLayout )!;
			DeliveryButtonsLayout = FindViewById<LinearLayout>( Resource.Id.deliveryButtonsLayout )!;
			ChargeLayout          = FindViewById<LinearLayout>( Resource.Id.chargeLayout )!;
			ChargeAmount          = FindViewById<TextView>( Resource.Id.chargeAmount )!;
			TripIdLayout          = FindViewById<LinearLayout>( Resource.Id.tripIdLayout )!;

			TotalScannedLayout = FindViewById<LinearLayout>( Resource.Id.totalScannedLayout )!;
			TotalScanned       = FindViewById<TextView>( Resource.Id.totalScanned )!;

			ScannerLayout   = FindViewById<RelativeLayout>( Resource.Id.scannerLayout )!;
			AcceptButton    = FindViewById<Button>( Resource.Id.acceptButton )!;
			ReferenceLayout = FindViewById<LinearLayout>( Resource.Id.referenceLayout )!;
			WeightLayout    = FindViewById<LinearLayout>( Resource.Id.weightLayout )!;

			UndeliverableLayout = FindViewById<LinearLayout>( Resource.Id.undeliverableLayout )!;
			UndeliverableButton = FindViewById<Button>( Resource.Id.undeliverableButton )!;

			UndeliverableButton.Click += UndeliverableButtonOnClick;

			UndeliverableText = FindViewById<TextView>( Resource.Id.undeliverableText )!;

			WaitingTripsTitle = FindViewById<TextView>( Resource.Id.waitingTitle )!;
			MainTitle         = FindViewById<TextView>( Resource.Id.title )!;

			RunOnUiThread( () =>
			               {
				               BillingAccount.Text = "";
				               var T = Title;
				               WaitingTripsTitle.Text = T;
				               MainTitle.Text         = T;

				               var Vis = UseLocationBarcodes ? ViewStates.Visible : ViewStates.Gone;
				               ScannerLayout.Visibility = Vis;
				               AcceptButton.Visibility  = Vis;
			               } );

			var Dtl = DetailsLayout = FindViewById<LinearLayout>( Resource.Id.detailsLayout )!;

			TripId                 =  Dtl.FindViewById<TextView>( Resource.Id.tripId )!;
			Reference              =  Dtl.FindViewById<TextView>( Resource.Id.detailReference )!;
			Reference!.TextChanged += ( _, _ ) => EnableUpdateTrip();

			PiecesWeightLayout = Dtl.FindViewById<LinearLayout>( Resource.Id.piecesWeightLayout )!;
			Pieces             = Dtl.FindViewById<TextView>( Resource.Id.detailPieces )!;
			Weight             = Dtl.FindViewById<TextView>( Resource.Id.detailWeight )!;

			Pallets                   = Dtl.FindViewById<TextView>( Resource.Id.pallets )!;
			PalletsLayout             = Dtl.FindViewById<LinearLayout>( Resource.Id.palletsLayout )!;
			PalletsLayout!.Visibility = ShowPallets ? ViewStates.Visible : ViewStates.Gone;

			ServiceLevel         = Dtl.FindViewById<TextView>( Resource.Id.tripViewServiceLevel )!;
			PackageType          = Dtl.FindViewById<TextView>( Resource.Id.tripViewPackageType )!;
			DueTime              = Dtl.FindViewById<TextView>( Resource.Id.tripViewDueTime )!;
			PickupCompany        = Dtl.FindViewById<TextView>( Resource.Id.pickupCompanyName )!;
			PickupCompanyAddress = Dtl.FindViewById<TextView>( Resource.Id.pickupCompanyAddress )!;
			PickupNotes          = Dtl.FindViewById<TextView>( Resource.Id.pickupNotes )!;
			PickupContact        = Dtl.FindViewById<TextView>( Resource.Id.pickupContact )!;
			DeliveryCompany      = Dtl.FindViewById<TextView>( Resource.Id.deliveryCompanyName )!;
			DeliveryAddress      = Dtl.FindViewById<TextView>( Resource.Id.deliveryCompanyAddress )!;
			DeliveryNotes        = Dtl.FindViewById<TextView>( Resource.Id.deliveryNotes )!;
			DeliveryContact      = Dtl.FindViewById<TextView>( Resource.Id.deliveryContact )!;

			CancelBtn = FindViewById<Button>( Resource.Id.cancelDeliveryButton )!;

			PodLayout = FindViewById<LinearLayout>( Resource.Id.podLayout )!;
			PodName   = Dtl.FindViewById<EditText>( Resource.Id.podName )!;

			FutileButton = FindViewById<Button>( Resource.Id.futileButton )!;

			FutileButton.Visibility = ViewStates.Gone;

			var InTextChange = false;

			PodName.TextChanged += ( _, _ ) =>
			                       {
				                       if( !InTextChange )
				                       {
					                       InTextChange = true;

					                       try
					                       {
						                       if( PodValid( PodName.Text ?? "", out var ValidText ) )
							                       UpdateTripButton.Enabled = EnableUpdateTrip();
						                       else
						                       {
							                       PodName.Text = ValidText;
							                       Globals.Sounds.PlayBadScanSound();
						                       }
					                       }
					                       finally
					                       {
						                       InTextChange = false;
					                       }
				                       }
			                       };

			DetailsLayout.FindViewById<LinearLayout>( Resource.Id.detailsLayout )!.Touch += ( _, args ) => { DoPocketLockout( args ); };

			SignButton = FindViewById<Button>( Resource.Id.signButton )!;

			SignButton!.Click += async ( _, _ ) =>
			                     {
				                     var SaveOrientation = Orientation;

				                     Orientation = ScreenOrientation.Landscape;

				                     SignatureLayoutParams.Width  = ViewGroup.LayoutParams.MatchParent;
				                     SignatureLayoutParams.Height = ViewGroup.LayoutParams.MatchParent;

				                     SignatureLayout.LayoutParameters = SignatureLayoutParams;
				                     SignatureLayout.BringToFront();
				                     SignatureLayout.StartAnimation( SignatureLayoutPullInLeft );

				                     SignaturePoints   = await Signature.CaptureSignature();
				                     CustomerSignature = !string.IsNullOrWhiteSpace( SignaturePoints.Points ) ? $"{SignaturePoints.Height}^{SignaturePoints.Width}^{SignaturePoints.Points}" : "";

				                     SignatureLayout.StartAnimation( SignatureLayoutPushOutLeft );
				                     UpdateTripButton.Enabled = EnableUpdateTrip();
			                     #if ALLOW_ROTATE
				                    Orientation = ScreenOrientation.Unspecified;
			                     #else
				                     Orientation = SaveOrientation;
			                     #endif
			                     };

			TripDetailsLayout = FindViewById<LinearLayout>( Resource.Id.tripDetailsLayout )!;

			DetailsLayout.FindViewById<Button>( Resource.Id.detailsBackButton )!.Click += DetailsBackButtonClick;

			UpdateTripButton        =  DetailsLayout.FindViewById<Button>( Resource.Id.updateTripButton )!;
			UpdateTripButton!.Click += UpdateTripButtonOnClick;

			SignatureLayout       = FindViewById<LinearLayout>( Resource.Id.signatureLayout )!;
			SignatureLayoutParams = SignatureLayout!.LayoutParameters!;

			SignatureLayoutPullInLeft  = AnimationUtils.LoadAnimation( Globals.Debug.Activity, Resource.Animation.PullInLeft )!;
			SignatureLayoutPushOutLeft = AnimationUtils.LoadAnimation( Globals.Debug.Activity, Resource.Animation.PushOutLeft )!;

			SignatureLayoutPushOutLeft!.AnimationEnd += ( _, _ ) =>
			                                            {
				                                            SignatureLayoutParams.Width      = 0;
				                                            SignatureLayout.LayoutParameters = SignatureLayoutParams;
			                                            };

			// ReSharper disable once PossibleNullReferenceException
			FindViewById<LinearLayout>( Resource.Id.pickupLayout )!.LongClick += PickupOnClick;

			// ReSharper disable once PossibleNullReferenceException
			FindViewById<LinearLayout>( Resource.Id.deliveryLayout )!.LongClick += DeliveryOnClick;

			RunOnUiThread( () =>
			               {
				               DetailsLayout.Visibility = ViewStates.Gone;
				               CancelBtn.Visibility     = ViewStates.Gone;

				               DetailsLayoutParams            = DetailsLayout.LayoutParameters!;
				               DetailsLayoutParams!.Width     = 0;
				               DetailsLayout.LayoutParameters = DetailsLayoutParams;

				               DetailsLayoutPushOutLeft = AnimationUtils.LoadAnimation( Globals.Debug.Activity, Resource.Animation.PushOutLeft )!;

				               Timer? EndTimer = null;

				               void AnimationEnd()
				               {
					               RunOnUiThread( () =>
					                              {
						                              var Lp = DetailsLayoutParams;
						                              Lp.Width = 0;
						                              var Dl = DetailsLayout;
						                              Dl.LayoutParameters      = Lp;
						                              DetailsLayout.Visibility = ViewStates.Gone;
						                              DetailsLayout.Enabled    = true;
						                              MainLayout.Visibility    = ViewStates.Visible;
						                              MainLayout.BringToFront();
						                              InAnimation = false;
					                              } );
				               }

				               DetailsLayoutPushOutLeft!.AnimationStart += ( _, _ ) =>
				                                                           {
					                                                           InAnimation = true;
					                                                           MainLayout.BringToFront();

					                                                           UndeliverableLayout.Visibility = ViewStates.Gone;
					                                                           DetailsLayout.Visibility       = ViewStates.Gone;

					                                                           EndTimer = new Timer( _ =>
					                                                                                 {
						                                                                                 AnimationEnd();

						                                                                                 // ReSharper disable once AccessToModifiedClosure
						                                                                                 EndTimer?.Dispose();
						                                                                                 EndTimer = null;
					                                                                                 }, null, 370, int.MaxValue );
				                                                           };

				               DetailsLayoutPushOutLeft.AnimationEnd += ( _, _ ) => { AnimationEnd(); };

				               DetailsLayoutPullInLeft = AnimationUtils.LoadAnimation( Globals.Debug.Activity, Resource.Animation.PullInLeft )!;

				               DetailsLayoutPullInLeft!.AnimationEnd += ( _, _ ) =>
				                                                        {
					                                                        PodName.RequestFocus();
					                                                        UpdateTripButton.Enabled = EnableUpdateTrip();
				                                                        };

				               SignatureLayoutParams.Width      = 0;
				               SignatureLayoutParams.Height     = 0;
				               SignatureLayout.LayoutParameters = SignatureLayoutParams;
			               } );

			ListView = FindViewById<ListView>( Resource.Id.tripListView )!;

			ListView.FastScrollEnabled = true;

			ShowScannedTotalOnly = false;

			lock( this )
			{
				TripListAdapter  = GetPickupDeliveriesAdapter;
				ListView.Adapter = TripListAdapter;
			}

			LookForTrips = true;
		}

		protected virtual string XlateTripId( string tripId ) => tripId;

		protected override async void OnExecute( object? arg = null )
		{
			if( WaitingForTrips )
			{
				Globals.DisableNotifications = true;

				try
				{
					await Task.Run( () =>
					                {
						                RunOnUiThread( () =>
						                               {
							                               MainLayout.Visibility            = ViewStates.Gone;
							                               WaitingForTripsLayout.Visibility = ViewStates.Visible;
						                               } );

						                do
							                Thread.Sleep( 1000 );
						                while( WaitingForTrips && Executing );
					                } );

					if( !Executing )
						return;
				}
				finally
				{
					Globals.DisableNotifications = false;
				}
			}

			RunOnUiThread( () =>
			               {
				               MainButtonLayout.Visibility      = PickupDeliveryByPieces ? ViewStates.Visible : ViewStates.Gone;
				               MainLayout.Visibility            = ViewStates.Visible;
				               WaitingForTripsLayout.Visibility = ViewStates.Gone;
			               } );

			UpdateDisplayFromDatabase();

			if( LocationOrPieces )
			{
				try
				{
					if( PickupDeliveryByPieces ) // Used if scan location barcode is on or off
						await LocationWithPieces();
					else
						await LocationWithoutPieces();
				}
				finally
				{
					Scanner.Abort();
				}
			}
			else
				RunOnUiThread( () => { ScannerLayout.Visibility = ViewStates.Gone; } );
		}

		protected virtual List<Trip> OnFilterTrips( List<Trip> trips ) => trips;

		protected void UpdateDisplayFromDatabase( List<Trip> trips )
		{
			if( !DisableUpdateDisplayFromDatabase )
			{
				if( !InUpdate )
				{
					InUpdate = true;

					try
					{
						trips = ( from T in trips
						          where T.Status < STATUS.VERIFIED
						          select T ).ToList();

						RunOnUiThread( () =>
						               {
							               TripCount = trips.Count;
							               TripListAdapter.UpdateTrips( trips );
						               } );

						if( UseLocationBarcodes )
						{
							Locations.Clear();

							foreach( var Trip in trips )
							{
								AddCompanyBarcode( Trip.PickupCompanyName, Trip, false );
								AddCompanyBarcode( Trip.DeliveryCompanyName, Trip, true );
							}
						}

						HaveTrips = trips.Count > 0;
					}
					finally
					{
						InUpdate = false;
					}
				}
			}
		}

		protected void UpdateDisplayFromDatabase()
		{
			UpdateDisplayFromDatabase( OnFilterTrips( Globals.Database.Trips ) );
		}

		[Obsolete]
		protected override void OnTripsChanged( int totalSummaryTrips, bool hasNew, bool hasDeleted, bool hasChanged )
		{
			if( hasNew || hasDeleted || hasChanged )
				UpdateDisplayFromDatabase();
			else
			{
				var Trips = OnFilterTrips( Globals.Database.Trips );

				var Lc = TripListAdapter.Count;

				if( Lc != Trips.Count )
					UpdateDisplayFromDatabase( Trips );
			}

			if( hasDeleted )
				Globals.Sounds.PlayDeletedTripSound();

			if( hasNew )
				Globals.Sounds.PlayNewTripSound();
		}

		public DriverPickup() : base( Resource.Layout.Fragment_PickupDelivery )
		{
		}

		public DriverPickup( int resourceId ) : base( resourceId )
		{
		}

	#region Title
		private string _Title = "Pickups / Deliveries";

		private Color _TitleColor = Color.White;

		protected virtual string Title
		{
			get => _Title;

			set
			{
				_Title = value;

				RunOnUiThread( () =>
				               {
					               MainTitle.SetTextColor( _TitleColor );
					               MainTitle.Text = value;
				               } );
			}
		}

		protected virtual Color TitleColor
		{
			get => _TitleColor;
			set
			{
				_TitleColor = value;
				RunOnUiThread( () => { MainTitle.SetTextColor( value ); } );
			}
		}
	#endregion
	}
}