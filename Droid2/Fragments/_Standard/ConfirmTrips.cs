﻿#nullable enable

using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments._Standard
{
	[Activity( Theme                 = "@android:style/Theme.Translucent.NoTitleBar",
	           Label                 = "ClaimsPiecesActivity",
	           AlwaysRetainTaskState = true )]
	public class ConfirmTrips : Activity
	{
		public enum STATUS
		{
			BUSY,
			YES,
			NO
		}


		internal class TripAdapter : BaseAdapter<Trip>
		{
			public override int Count => Trips.Count;

			private readonly List<Trip> Trips;

			public override long GetItemId( int position ) => position;

			public override View? GetView( int position, View? convertView, ViewGroup? parent )
			{
				var Trip = Trips[ position ];

				if( Trip is not null && convertView is not null )
				{
					convertView.FindViewById<TextView>( Resource.Id.tripViewServiceLevel )!.Text = Trip.ServiceLevel;
					convertView.FindViewById<TextView>( Resource.Id.tripViewPieces )!.Text       = Trip.Pieces.ToString( "####" );
					convertView.FindViewById<TextView>( Resource.Id.tripViewPackageType )!.Text  = Trip.PackageType;

					convertView.FindViewById<TextView>( Resource.Id.tripViewWeight )!.Text         = Trip.Weight.ToString( Trip.Weight >= 100 ? "####" : "##.##" );
					convertView.FindViewById<TextView>( Resource.Id.tripViewDueTime )!.Text        = Trip.DueTime.ToString( "t" );
					convertView.FindViewById<TextView>( Resource.Id.tripViewCompanyName )!.Text    = Trip.DeliveryCompanyName;
					convertView.FindViewById<TextView>( Resource.Id.tripViewCompanyAddress )!.Text = Trip.DeliveryAddressLine1;

					convertView.FindViewById<TextView>( Resource.Id.tripId )!.Text = Trip.TripId;
				}

				return convertView;
			}

			public TripAdapter( List<Trip> trips )
			{
				Trips = trips;
			}

			public override Trip this[ int position ] => Trips[ position ];
		}

		private static ConfirmTrips? Instance;

		private ListView? TripListView;
		private TextView? TotalDeliveries;


		private STATUS Status = STATUS.BUSY;

		protected override void OnCreate( Bundle? savedInstanceState )
		{
			base.OnCreate( savedInstanceState );

			SetContentView( Resource.Layout.Activity_ConfirmTrips );

			TripListView    = FindViewById<ListView>( Resource.Id.tripListView );
			TotalDeliveries = FindViewById<TextView>( Resource.Id.totalDeliveries );

			FindViewById<Button>( Resource.Id.okButton )!.Click += ( _, _ ) =>
			                                                       {
				                                                       Status = STATUS.YES;
				                                                       Finish();
			                                                       };

			FindViewById<Button>( Resource.Id.cancelButton )!.Click += ( _, _ ) =>
			                                                           {
				                                                           Status = STATUS.NO;
				                                                           Finish();
			                                                           };
			Instance = this;
		}

		public static Task<STATUS> Confirm( List<Trip> trips )
		{
			Instance = null;

			var Activity = Globals.Debug.Activity;

			var Intent = new Intent( Activity, typeof( ConfirmTrips ) );
			Activity.StartActivity( Intent );

			return Task.Run( () =>
			                 {
				                 while( Instance is null )
					                 Thread.Sleep( 1 );

				                 return Instance.DoConfirm( trips );
			                 } );
		}

		private STATUS DoConfirm( List<Trip> trips )
		{
			Instance = null;

			if( TripListView is not null )
				TripListView.Adapter = new TripAdapter( trips );

			if( TotalDeliveries is not null )
				TotalDeliveries.Text = trips.Count.ToString( "####" );

			while( Status == STATUS.BUSY )
				Thread.Sleep( 250 );

			return Status;
		}
	}
}