
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Signature;
using SignatureResult = Droid2.Signature.SignatureResult;

namespace Droid2.Fragments.Standard.Claims.CrossDockFragment
{
	[Activity( Theme                 = "@android:style/Theme.Black.NoTitleBar",
	           Label                 = "ClaimPiecesFinaliseActivity",
	           AlwaysRetainTaskState = true,
	           ConfigurationChanges  = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Keyboard
	         )]
	public class CrossDockFinaliseActivity : Activity
	{
		public enum FINALISE_ACTION
		{
			BUSY,
			CANCEL,
			FINALISE
		}

		public static CrossDockFinaliseActivity Instance;

		public static FINALISE_ACTION Action;
		public static SignatureResult SignaturePoints;
		public static string PodName;

		public static List<ScannedTrip> ScannedTrips,
		                                UnScannedTrips;

		private LinearLayout SignatureLayout,
		                     MainLayout;

		private SignatureFragment Signature;

		private Animation SignatureLayoutPullInLeft,
		                  SignatureLayoutPushOutLeft;

		private ViewGroup.LayoutParams SignatureLayoutParams;

        [System.Obsolete]
        protected override void OnCreate( Bundle savedInstanceState )
		{
			base.OnCreate( savedInstanceState );

			Instance = this;

			SignaturePoints = null;
			PodName         = "";

			SetContentView( Resource.Layout.Activity_CrossDockFinialise );

			MainLayout            = FindViewById<LinearLayout>( Resource.Id.mainLayout );
			MainLayout.Visibility = ViewStates.Visible;

			FindViewById<Button>( Resource.Id.signButton ).Click += ( sender, args ) =>
			                                                        {
			                                                        };
			FindViewById<TextView>( Resource.Id.totalTrips ).Text = ScannedTrips.Count.ToString( "####0" );

			var TotalPieces = ScannedTrips.Sum( scannedTrip => scannedTrip.Pieces );
			FindViewById<TextView>( Resource.Id.totalItems ).Text = TotalPieces.ToString( "####0" );

			var     TotalUnScanned       = 0;
			decimal TotalUnScannedPieces = 0;

			foreach( var Trip in UnScannedTrips )
			{
				TotalUnScanned       += 1;
				TotalUnScannedPieces += Trip.Balance;
			}

			FindViewById<TextView>( Resource.Id.undeliverableTrips ).Text = TotalUnScanned.ToString( "####0" );
			FindViewById<TextView>( Resource.Id.undeliverableItems ).Text = TotalUnScannedPieces.ToString( "####0" );

			FindViewById<Button>( Resource.Id.cancelButton ).Click +=
				( sender, args ) =>
				{
					Task.Run( () =>
					          {
						          if( Globals.Dialogues.Confirm( "Are you sure that you wish to\r\ncancel the entire claim?", true, this ) )
						          {
							          RunOnUiThread( () =>
							                         {
								                         Action = FINALISE_ACTION.CANCEL;
								                         Finish();
							                         } );
						          }
					          } );
				};

			FindViewById<Button>( Resource.Id.finaliseButton ).Click += ( sender, args ) =>
			                                                            {
				                                                            PodName = FindViewById<TextView>( Resource.Id.podName ).Text.Trim();
				                                                            Action  = FINALISE_ACTION.FINALISE;
				                                                            Finish();
			                                                            };

			Signature = new SignatureFragment();
			var Trans = FragmentManager.BeginTransaction();
			Trans.Add( Resource.Id.signatureLayout, Signature );
			Trans.CommitAllowingStateLoss();

			SignatureLayout                  = FindViewById<LinearLayout>( Resource.Id.signatureLayout );
			SignatureLayoutParams            = SignatureLayout.LayoutParameters;
			SignatureLayoutParams.Width      = 0;
			SignatureLayout.LayoutParameters = SignatureLayoutParams;

			SignatureLayoutPullInLeft  = AnimationUtils.LoadAnimation( this, Resource.Animation.PullInLeft );
			SignatureLayoutPushOutLeft = AnimationUtils.LoadAnimation( this, Resource.Animation.PushOutLeft );

			SignatureLayoutPushOutLeft.AnimationEnd += ( sender, args ) =>
			                                           {
				                                           SignatureLayoutParams.Width      = 0;
				                                           SignatureLayout.LayoutParameters = SignatureLayoutParams;
			                                           };

			var SignButton = FindViewById<Button>( Resource.Id.signButton );

			SignButton.Click += ( sender, args ) =>
			                    {
				                    RunOnUiThread( async () =>
				                                   {
					                                   RequestedOrientation = ScreenOrientation.Landscape;

					                                   SignatureLayoutParams.Width  = ViewGroup.LayoutParams.MatchParent;
					                                   SignatureLayoutParams.Height = ViewGroup.LayoutParams.MatchParent;

					                                   SignatureLayout.LayoutParameters = SignatureLayoutParams;
					                                   SignatureLayout.StartAnimation( SignatureLayoutPullInLeft );
					                                   SignatureLayout.BringToFront();

					                                   Globals.Keyboard.Hide = true;
					                                   SignaturePoints       = await Signature.CaptureSignature();

					                                   RequestedOrientation = ScreenOrientation.Unspecified;
					                                   SignatureLayout.StartAnimation( SignatureLayoutPushOutLeft );
				                                   } );
			                    };
		}
	}
}