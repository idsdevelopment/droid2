using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Signature;
using Service.Interfaces;
using Utils;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard.Claims.CrossDockFragment
{
	public class CrossDock : AFragment
	{
		private Login LoginFragment;

		private ListView             ScannedListView;
		private ClaimsPiecesAdapter  ListAdapter;
		private LinearLayout         MainLayout;
		private FrameLayout          LoginLayout;
		private BarcodeInputFragment Scanner;
		private Button               NextButton;

		private readonly List<ScannedTrip> ScannedTrips = new();

		private volatile bool Completed;

		private string Driver = "";

		private string SaveAuthToken = "";

		private void EnableNext( bool enable )
		{
			RunOnUiThread( () => { NextButton.Enabled = enable; } );
		}

		private void EnableNext()
		{
			EnableNext( ( ScannedTrips.Count > 0 ) && !ListAdapter.HasOverscan );
		}

        [Obsolete]
        protected override void OnCreateDynamicFragments()
		{
			LoginFragment = new Login { IsAlternateLogin = true };
			AddDynamicFragment( Resource.Id.loginFrame, LoginFragment );

			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLayout, Scanner );
		}

		protected override void OnCreateFragment()
		{
			LoginLayout = FindViewById<FrameLayout>( Resource.Id.loginFrame );
			MainLayout  = FindViewById<LinearLayout>( Resource.Id.mainLayout );

			RunOnUiThread( () => { MainLayout.Visibility = ViewStates.Gone; } );

			ScannedListView         = FindViewById<ListView>( Resource.Id.claimsListView );
			ListAdapter             = new ClaimsPiecesAdapter( Activity, ScannedListView );
			ScannedListView.Adapter = ListAdapter;

			NextButton = FindViewById<Button>( Resource.Id.nextButton );
		}

		protected override void OnTripsChanged( int totalSummaryTrips, bool hasNew, bool hasDeleted, bool hasChanged )
		{
			if( Driver.IsNullOrWhiteSpace() )
				Globals.Settings.GetLoginAlternateDefaults( out _, out Driver );

			var Trips = ( from T in Globals.Database.Trips
			              where T.Driver.Compare( Driver, StringComparison.OrdinalIgnoreCase ) == 0
			              select T ).ToList();

			ListAdapter.Update( Trips );
			TripCount = Trips.Count;
		}

        [Obsolete]
        protected override async void OnExecute( object arg = null )
		{
			SaveAuthToken = Globals.Database.AuthTokenAsString;

#pragma warning disable 4014
			Task.Run( () =>
			          {
				          Globals.Database.DeleteAllLocalTripsAsync();
				          Globals.Database.GetZones();
			          } );
#pragma warning restore 4014

			Globals.Fragment.CurrentFragment = LoginFragment;

			ExecuteFragment( LoginFragment, Resource.Id.loginFrame );
			LoginFragment.Abort();
			LoginFragment.KillFragment();
			LoginFragment = null;

			TripStatusFilterSet = new[] { STATUS.PICKED_UP };

			LookForTrips = false;

			RunOnUiThread( () =>
			               {
				               LoginLayout.Visibility = ViewStates.Gone;
				               MainLayout.Visibility  = ViewStates.Visible;
			               } );

			ListAdapter.Callback = ( trip, action, zoneFilter ) =>
			                       {
				                       if( action == PickupDeliveriesOptionsActivity.SCAN_STATUS.REMOVE )
					                       ListAdapter.Remove( trip );

				                       EnableNext();
			                       };

			EnableNext( false );

			NextButton.Click += async ( sender, args ) =>
			                    {
				                    EnableNext( false );

				                    CrossDockFinaliseActivity.ScannedTrips = ScannedTrips;

				                    CrossDockFinaliseActivity.UnScannedTrips = ( from Trip in ListAdapter.Trips
				                                                                 select Trip as ScannedTrip ).ToList();

				                    await Task.Run( () =>
				                                    {
					                                    Activity.StartActivity( new Intent( Activity, typeof( CrossDockFinaliseActivity ) ) );
					                                    Activity.OverridePendingTransition( Resource.Animation.PullInLeft, 0 );

					                                    for( CrossDockFinaliseActivity.Action = CrossDockFinaliseActivity.FINALISE_ACTION.BUSY;
					                                         CrossDockFinaliseActivity.Action == CrossDockFinaliseActivity.FINALISE_ACTION.BUSY; )
						                                    Thread.Sleep( 100 );

					                                    if( CrossDockFinaliseActivity.Action != CrossDockFinaliseActivity.FINALISE_ACTION.CANCEL )
					                                    {
						                                    var Sig = CrossDockFinaliseActivity.SignaturePoints ?? new SignatureResult();

						                                    var Pod = CrossDockFinaliseActivity.PodName;

						                                    // Make sure re-login as original driver before trying to claim trips
						                                    var Configuration = Globals.Database.Config;
						                                    Globals.Database.Service.Connect( Configuration.AccountId, Configuration.UserName, Configuration.Password );

						                                    var Deliverable = ( from Trip in ScannedTrips
						                                                        select Trip.TripId ).ToList();

						                                    Globals.Database.AddClaim( SaveAuthToken, Deliverable, Pod, Sig.Points, Sig.Height, Sig.Width, DateTime.Now );
					                                    }

					                                    Completed = true;
					                                    Scanner.Abort();
				                                    } );
			                    };

			LookForTrips                     = true;
			Globals.Fragment.CurrentFragment = this;

			try
			{
				while( Executing )
				{
					var Barcode = await Scanner.Scan();

					if( Completed )
					{
						await Globals.Database.DeleteAllTripsAsync();

						// Delay finish to remove unwanted display
						RunOnUiThread( () =>
						               {
							               CrossDockFinaliseActivity.Instance?.Finish();
							               CrossDockFinaliseActivity.Instance = null;
						               } );

						Scanner.Abort();
						Globals.Fragment.CurrentFragment = this;
						Return( Globals.OperatingMode.OPERATING_MODE.DRIVER_PICKUP );

						return;
					}

					if( !string.IsNullOrWhiteSpace( Barcode ) ) // Probably back key
					{
						LookForTrips = false;

						if( Globals.Preferences.PickupDeliveryByPieceNumber )
						{
							var P = Barcode.LastIndexOf( '-' );

							if( P > 0 )
								Barcode = Barcode[ ..P ];
						}

						var FoundTrip = ListAdapter.Trips.FirstOrDefault( trip => trip.TripId == Barcode );

						if( FoundTrip != null )
						{
							// Zone Filter Active?
							if( !string.IsNullOrWhiteSpace( PickupDeliveriesOptionsActivity.ZoneFilter ) && ( FoundTrip.DeliveryZone != PickupDeliveriesOptionsActivity.ZoneFilter ) )
							{
								Error( "Trip is not for this zone." );

								continue;
							}

							var STrip = (ScannedTrip)FoundTrip;
							STrip.Scanned += 1;
							STrip.Balance -= 1;

							if( !Globals.Preferences.PickupDeliveryByPieceNumber )
							{
								if( STrip.Balance == 0 )
								{
									ScannedTrips.Add( STrip );
									ListAdapter.Remove( FoundTrip );
								}
							}
							else if( STrip.Scanned > 0 )
							{
								if( !( from St in ScannedTrips
								       where St.TripId == STrip.TripId
								       select St ).Any() )
									ScannedTrips.Add( STrip );

								if( STrip.Balance == 0 )
									ListAdapter.Remove( FoundTrip );
							}
							ListAdapter.NotifyDataSetChanged();
						}
						else
						{
							// Maybe overscan
							var STrip1 = ScannedTrips.FirstOrDefault( trip => trip.TripId == Barcode );

							if( STrip1 != null )
							{
								STrip1.Scanned += 1;
								STrip1.Balance -= 1;

								ScannedTrips.Remove( STrip1 );
								ListAdapter.Add( STrip1 );
							}
							else //Unknown Trip
							{
								STrip1 = new ScannedTrip( new Trip
								                          {
									                          TripId = Barcode
								                          } );

								ListAdapter.AddUnknownTrip( STrip1 );
							}

							Globals.Sounds.PlayBadScanSound();
						}

						EnableNext();
					}
				}
			}
			finally
			{
				// Make sure re-login as original driver before trying to claim trips
				var Config = Globals.Database.Config;
				var (Status, AuthToken, _) = await Globals.Database.Service.Connect( Config.AccountId, Config.UserName, Config.Password );

				if( Status == CONNECTION_STATUS.OK )
					Globals.Database.Service.AuthToken = AuthToken;

				await Globals.Database.DeleteAllTripsAsync();
			}

			Return();
		}

		protected override void OnDestroyFragment()
		{
			Scanner.Abort();
			Globals.Login.ReLogin();
		}

		public CrossDock() : base( Resource.Layout.Fragment_CrossDock )
		{
		}
	}
}