using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content.PM;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Signature;
using SignatureResult = Droid2.Signature.SignatureResult;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Standard.Claims.StandardFragment
{
	public class ClaimsStandard : AFragment
	{
		protected bool LandscapeMode
		{
			get
			{
				var Temp = Globals.Settings.ClaimsLandscape;
				Orientation = Temp ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;

				return Temp;
			}

			set
			{
				Orientation                      = value ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;
				Globals.Settings.ClaimsLandscape = value;
			}
		}

		protected virtual string Title => "Claims (Standard)";

		private SignatureFragment Signature;
		private FrameLayout SignatureLayout;
		private ViewGroup.LayoutParams SignatureLayoutParams;

		private Button FinaliseButton;
		private LinearLayout FinaliseLayout;
		private ViewGroup.LayoutParams FinaliseLayoutParams;

		private Animation FinaliseLayoutPullInRight;
		private Animation FinaliseLayoutPushOutRight;

		private Animation SignatureLayoutPullInLeft;
		private Animation SignatureLayoutPushOutLeft;
		protected BaseTripAdapter Adapter;

		protected ListView ClaimsListView;

		protected Button NextButton,
		                 AlternateNextButton;

		protected EditText PodName;
		protected BarcodeInputFragment Scanner;

		protected SignatureResult SignaturePoints;

		private void GoHome()
		{
			RunOnUiThread( () =>
			               {
				               FinaliseLayout.StartAnimation( FinaliseLayoutPushOutRight );
			               } );
		}

		protected virtual bool GetEnableNext()
		{
			return Adapter.Trips.Count > 0;
		}

		protected virtual bool GetEnableFinalise()
		{
			return true;
		}

		protected virtual void EnableNext()
		{
			RunOnUiThread( () =>
			               {
				               var Enabled = GetEnableNext();
				               NextButton.Enabled          = Enabled;
				               AlternateNextButton.Enabled = Enabled;
			               } );
		}

		protected void EnableFinalise()
		{
			RunOnUiThread( () =>
			               {
				               FinaliseButton.Enabled = GetEnableFinalise();
			               } );
		}

		protected virtual string XlateTripId( string tripId )
		{
			return tripId;
		}

		protected virtual BaseTripAdapter GeClaimsAdapter()
		{
			return new ClaimsStandardAdapter( Activity,
			                                  ClaimsListView,
			                                  trip =>
			                                  {
				                                  Globals.Dialogues.Confirm( $"Remove trip:\r\n{Adapter.CurrentTrip.TripId}",
				                                                             () =>
				                                                             {
					                                                             Adapter.Remove( trip );
					                                                             EnableNext();
				                                                             }, null, false );
			                                  } );
		}

		protected void DoNextBtn( object sender, EventArgs args )
		{
			SignaturePoints = null;
			EnableFinalise();

			RunOnUiThread( () =>
			               {
				               FinaliseLayout.Visibility = ViewStates.Visible;

				               FinaliseLayoutParams.Width  = ViewGroup.LayoutParams.MatchParent;
				               FinaliseLayoutParams.Height = ViewGroup.LayoutParams.MatchParent;

				               FinaliseLayout.LayoutParameters = FinaliseLayoutParams;

				               FinaliseLayout.StartAnimation( FinaliseLayoutPullInRight );
			               } );
		}

		public virtual void AddClaim( string authToken, IList<string> tripIds, string pod, string signature, int signatureHeight, int signatureWidth, DateTime pickupTime )
		{
			Globals.Database.AddClaim( Globals.Database.AuthTokenAsString, tripIds, pod, signature, signatureHeight, signatureWidth, pickupTime );
		}

		protected virtual void FinialiseClaim( List<Trip> trips )
		{
			var TripIds = trips.Select( trip => trip.TripId ).ToList();

			RunOnUiThread( () =>
			               {
				               NextButton.Enabled          = false;
				               AlternateNextButton.Enabled = false;

				               Adapter.Clear();

				               GoHome();

				               var PName = PodName.Text;
				               PodName.Text = "";

				               var Sig = SignaturePoints ?? new SignatureResult();

				               AddClaim( Globals.Database.AuthTokenAsString, TripIds, PName, Sig.Points, Sig.Height, Sig.Width, DateTime.Now );
				               EnableNext();
			               } );
		}

		protected override void OnCreateFragment()
		{
			NextButton       =  FindViewById<Button>( Resource.Id.nextButton );
			NextButton.Click += DoNextBtn;

			AlternateNextButton            =  FindViewById<Button>( Resource.Id.alternateNextButton );
			AlternateNextButton.Click      += DoNextBtn;
			AlternateNextButton.Visibility =  ViewStates.Gone;

			FindViewById<Button>( Resource.Id.backButton ).Click += ( sender, args ) =>
			                                                        {
				                                                        GoHome();
			                                                        };

			ClaimsListView         = FindViewById<ListView>( Resource.Id.claimsListView );
			ClaimsListView.Adapter = Adapter = GeClaimsAdapter();

			FinaliseLayout = FindViewById<LinearLayout>( Resource.Id.finaliseLayout );

			RunOnUiThread( () =>
			               {
				               FinaliseLayout.Visibility = ViewStates.Gone;
			               } );
			FinaliseLayoutParams       = FinaliseLayout.LayoutParameters;
			FinaliseLayoutPullInRight  = AnimationUtils.LoadAnimation( Activity, Resource.Animation.PullInRight );
			FinaliseLayoutPushOutRight = AnimationUtils.LoadAnimation( Activity, Resource.Animation.PushOutRight );

			FinaliseLayoutPushOutRight.AnimationEnd += ( sender, args ) =>
			                                           {
				                                           FinaliseLayoutParams.Width      = 0;
				                                           FinaliseLayout.LayoutParameters = FinaliseLayoutParams;
				                                           FinaliseLayout.Visibility       = ViewStates.Gone;
			                                           };

			FinaliseLayout.Touch += ( sender, args ) =>
			                        {
				                        args.Handled = true;
			                        };

			SignatureLayout = FindViewById<FrameLayout>( Resource.Id.signatureLayout );

			RunOnUiThread( () =>
			               {
				               SignatureLayout.Visibility = ViewStates.Gone;
			               } );
			SignatureLayoutParams      = SignatureLayout.LayoutParameters;
			SignatureLayoutPullInLeft  = AnimationUtils.LoadAnimation( Activity, Resource.Animation.PullInLeft );
			SignatureLayoutPushOutLeft = AnimationUtils.LoadAnimation( Activity, Resource.Animation.PushOutLeft );

			SignatureLayoutPushOutLeft.AnimationEnd += ( sender, args ) =>
			                                           {
				                                           SignatureLayoutParams.Width      = 0;
				                                           SignatureLayout.LayoutParameters = SignatureLayoutParams;
				                                           SignatureLayout.Visibility       = ViewStates.Gone;
			                                           };

			FindViewById<Button>( Resource.Id.signButton ).Click += async ( sender, args ) =>
			                                                        {
				                                                        Activity.RequestedOrientation = ScreenOrientation.Landscape;

				                                                        SignatureLayout.Visibility   = ViewStates.Visible;
				                                                        SignatureLayoutParams.Width  = ViewGroup.LayoutParams.MatchParent;
				                                                        SignatureLayoutParams.Height = ViewGroup.LayoutParams.MatchParent;

				                                                        SignatureLayout.LayoutParameters = SignatureLayoutParams;
				                                                        SignatureLayout.StartAnimation( SignatureLayoutPullInLeft );

				                                                        SignaturePoints = await Signature.CaptureSignature();

				                                                        Activity.RequestedOrientation = ScreenOrientation.Unspecified;
				                                                        SignatureLayout.StartAnimation( SignatureLayoutPushOutLeft );

				                                                        EnableFinalise();
			                                                        };

			PodName = FindViewById<EditText>( Resource.Id.podName );

			PodName.TextChanged += ( sender, args ) =>
			                       {
				                       EnableFinalise();
			                       };

			FinaliseButton = FindViewById<Button>( Resource.Id.finaliseButton );

			FinaliseButton.Click += ( sender, args ) =>
			                        {
				                        FinialiseClaim( Adapter.Trips );
			                        };

			FindViewById<TextView>( Resource.Id.claimsTitle ).Text = Title;
		}

		protected override void OnDestroyFragment()
		{
			Scanner?.Abort();
			Scanner = null;
		}

        [Obsolete]
        protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLoadFrame, Scanner );

			Signature = new SignatureFragment();
			AddDynamicFragment( Resource.Id.signatureLayout, Signature );
		}

		protected virtual bool ValidateTripId( string tripId )
		{
			return true;
		}

		protected virtual void AddScannedTripId( string tripId )
		{
			( (ClaimsStandardAdapter)Adapter ).Add( tripId ).ScrollToTripId( tripId );
		}

		protected override async void OnExecute( object arg = null )
		{
			while( Executing )
			{
				EnableNext();
				Scanner.Placeholder = "Barcode";
				var TripId = await Scanner.Scan();
				TripId = TripId.Trim();

				if( !string.IsNullOrWhiteSpace( TripId ) )
				{
					if( Globals.Preferences.PickupDeliveryByPieceNumber )
					{
						var P = TripId.LastIndexOf( '-' );

						if( P > 0 )
							TripId = TripId[ ..P ];
					}

					if( ValidateTripId( TripId ) )
						AddScannedTripId( TripId );
				}
			}
		}

		public ClaimsStandard() : base( Resource.Layout.Fragment_ClaimsStandard )
		{
		}
	}
}