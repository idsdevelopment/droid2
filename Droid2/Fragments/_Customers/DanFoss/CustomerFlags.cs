#nullable enable

using Droid2.Fragments.Standard.Claims.CrossDockFragment;
using Droid2.Fragments.Standard.Claims.StandardFragment;
using Droid2.Fragments.Standard.PickupDeliveries;

namespace Droid2
{
	public static partial class Globals
	{
		public static partial class Modifications
		{
			private static readonly ProgramEntry[] DamFossPrograms =
			{
				new DriverPickupSelector
				{
					Mode        = OperatingMode.OPERATING_MODE.DRIVER_PICKUP,
					Program     = typeof( DriverPickup ),
					RadioButton = Resource.Id.driverPickupButton
				},
				new()
				{
					      Mode        = OperatingMode.OPERATING_MODE.CLAIMS_STANDARD,
					      Program     = typeof( ClaimsStandard ),
					      RadioButton = Resource.Id.claimsStandardButton
				      },
				new()
				{
					      Mode        = OperatingMode.OPERATING_MODE.CROSS_DOCK,
					      Program     = typeof( CrossDock ),
					      RadioButton = Resource.Id.crossDockButton
				      }
			};

			public static Property IsDanFoss = new( "Foss", DEFAULT_LAYOUT, "DanFoss",
			                                        new PreferenceOverrides(),
			                                        DamFossPrograms,
			                                        OnLoggedIn );
		}
	}
}