using System;
using Droid2.Fragments._Customers.Pml;
using Droid2.Fragments._Customers.Pml.StarTrack;
using Droid2.Fragments._Standard_Customers.Pml.StarTrack;
using Droid2.Fragments.Customers.Pml;
using Droid2.Fragments.Standard.Claims.CrossDockFragment;
using Droid2.Fragments.Standard.Claims.StandardFragment;
using Utils;

namespace Droid2
{
	public static partial class Globals
	{
		public static partial class Modifications
		{
			private const string PML_TEST = "PMLTest",
			                     PML = "PML",
			                     STAR_TRACK = "uplift";

			public static Property IsPmlTest = new( PML_TEST, Resource.Id.pmlOptions, PML_TEST,
			                                        new PreferenceOverrides
			                                        {
				                                        PickupDeliveryByPieces            = true,
				                                        UseLocationBarcodes               = true,
				                                        ScanBarcodeForUndeliverableReason = true,
				                                        RequiresPodName                   = true,
				                                        RequiresSignature                 = true,
				                                        HidePiecesAndWeight               = true,
				                                        EditPiecesWeightReference         = false,
				                                        HideReference                     = false,
			                                        #if !DEBUG
				                                        EnableFilter = true
			                                        #endif
			                                        },
			                                        new[]
			                                        {
				                                        new ProgramEntry
				                                        {
					                                        Mode        = OperatingMode.OPERATING_MODE.PML_RETURNS,
					                                        Program     = typeof( PmlDsdDeliveries ),
					                                        RadioButton = Resource.Id.pmlDsdReturnsButton,
					                                        IsDefault   = true
				                                        },
				                                        new ProgramEntry
				                                        {
					                                        Mode        = OperatingMode.OPERATING_MODE.PML_AUDIT,
					                                        Program     = typeof( PmlAudit ),
					                                        RadioButton = Resource.Id.pmlAuditButton
				                                        },
				                                        new ProgramEntry
				                                        {
					                                        Mode        = OperatingMode.OPERATING_MODE.PML_CLAIMS,
					                                        Program     = typeof( PmlClaims ),
					                                        RadioButton = Resource.Id.pmlClaimsButton
				                                        },
				                                        new ProgramEntry
				                                        {
					                                        Mode        = OperatingMode.OPERATING_MODE.PML_CROSS_DOCK,
					                                        Program     = typeof( CrossDock ),
					                                        RadioButton = Resource.Id.pmlCrossDockButton
				                                        },
				                                        new ProgramEntry
				                                        {
					                                        Mode        = OperatingMode.OPERATING_MODE.PML_STAR_TRACK,
					                                        Program     = typeof( StarTrack ),
					                                        RadioButton = Resource.Id.pmlStarTrackButton,
					                                        Filter      = STAR_TRACK
				                                        },
				                                        new ProgramEntry
				                                        {
					                                        Mode        = OperatingMode.OPERATING_MODE.PML_STAR_TRACK_PICKUPS,
					                                        Program     = typeof( StarTrackPickups ),
					                                        RadioButton = Resource.Id.hiddenButton,
					                                        Filter      = ALWAYS_HIDDEN
				                                        },
				                                        new ProgramEntry
				                                        {
					                                        Mode        = OperatingMode.OPERATING_MODE.PML_STAR_TRACK_AUDIT,
					                                        Program     = typeof( StarTrackAudit ),
					                                        RadioButton = Resource.Id.hiddenButton,
					                                        Filter      = ALWAYS_HIDDEN
				                                        }
			                                        }
			                                        ,
			                                        OnLogin
			                                               );

			public static Property IsPml = new( PML, Resource.Id.pmlOptions, PML,
			                                    new PreferenceOverrides
			                                    {
				                                    PickupDeliveryByPieces            = true,
				                                    UseLocationBarcodes               = true,
				                                    ScanBarcodeForUndeliverableReason = true,
				                                    RequiresPodName                   = true,
				                                    RequiresSignature                 = true,
				                                    HidePiecesAndWeight               = true,
				                                    EditPiecesWeightReference         = false,
				                                    HideReference                     = false,
			                                    #if !DEBUG
				                                    EnableFilter = true
			                                    #endif
			                                    },
			                                    new[]
			                                    {
				                                    new ProgramEntry
				                                    {
					                                    Mode        = OperatingMode.OPERATING_MODE.PML_RETURNS,
					                                    Program     = typeof( PmlDsdDeliveries ),
					                                    RadioButton = Resource.Id.pmlDsdReturnsButton,
					                                    IsDefault   = true
				                                    },
				                                    new ProgramEntry
				                                    {
					                                    Mode        = OperatingMode.OPERATING_MODE.PML_AUDIT,
					                                    Program     = typeof( PmlAudit ),
					                                    RadioButton = Resource.Id.pmlAuditButton
				                                    },
				                                    new ProgramEntry
				                                    {
					                                    Mode        = OperatingMode.OPERATING_MODE.PML_CLAIMS,
					                                    Program     = typeof( ClaimsStandard ),
					                                    RadioButton = Resource.Id.pmlClaimsButton
				                                    },
				                                    new ProgramEntry
				                                    {
					                                    Mode        = OperatingMode.OPERATING_MODE.PML_CROSS_DOCK,
					                                    Program     = typeof( CrossDock ),
					                                    RadioButton = Resource.Id.pmlCrossDockButton
				                                    },
				                                    new ProgramEntry
				                                    {
					                                    Mode        = OperatingMode.OPERATING_MODE.PML_STAR_TRACK,
					                                    Program     = typeof( StarTrack ),
					                                    RadioButton = Resource.Id.pmlStarTrackButton,
					                                    Filter      = STAR_TRACK
				                                    },
				                                    new ProgramEntry
				                                    {
					                                    Mode        = OperatingMode.OPERATING_MODE.PML_STAR_TRACK_PICKUPS,
					                                    Program     = typeof( StarTrackPickups ),
					                                    RadioButton = Resource.Id.hiddenButton,
					                                    Filter      = ALWAYS_HIDDEN
				                                    },
				                                    new ProgramEntry
				                                    {
					                                    Mode        = OperatingMode.OPERATING_MODE.PML_STAR_TRACK_AUDIT,
					                                    Program     = typeof( StarTrackAudit ),
					                                    RadioButton = Resource.Id.hiddenButton,
					                                    Filter      = ALWAYS_HIDDEN
				                                    }
			                                    }
			                                           );

			private static void OnLogin()
			{
				if( Filter.Compare( STAR_TRACK, StringComparison.OrdinalIgnoreCase ) == 0 )
				{
					var Pgms = IsPml.AllowedPrograms;
					Pgms[ 0 ].IsDefault = false;
					Pgms[ 4 ].IsDefault = true;
				}
			}
		}
	}
}