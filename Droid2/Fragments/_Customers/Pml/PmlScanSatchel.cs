using System;
using System.Threading.Tasks;
using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using Droid2.Barcode;
using Utils;

// ReSharper disable AccessToModifiedClosure

namespace Droid2.Fragments.Customers.Pml
{
	[Activity( Theme                 = "@android:style/Theme.Translucent.NoTitleBar",
	           Label                 = "PmlSatchel",
	           AlwaysRetainTaskState = true,
	           NoHistory             = true
	         )]
	public class PmlScanSatchel : Activity
	{
		public enum STATUS
		{
			BUSY,
			OK,
			CANCELED
		}

		public static STATUS Status;

		public static string Satchel = "";

		private BarcodeInputFragment SatchelScanner;

		protected virtual bool IsSatchelIdValid( string txt ) => txt.StartsWith( "AUDR" );

		[Obsolete]
		protected override async void OnCreate( Bundle savedInstanceState )
		{
			Status  = STATUS.BUSY;
			Satchel = "";

			base.OnCreate( savedInstanceState );
			SetContentView( Resource.Layout.Pml_Activity_Satchel );

			var ScannerLoadFrame = FindViewById<FrameLayout>( Resource.Id.scannerLoadFrame );

			ScannerLoadFrame?.SetBackgroundColor( Color.Black );
			SatchelScanner = new BarcodeInputFragment();

			var FragManager = FragmentManager?.BeginTransaction();

			// The fragment will have the ID of Resource.Id.fragment_container.
			if( FragManager is not null )
			{
				FragManager.Add( Resource.Id.scannerLoadFrame, SatchelScanner );

				// Commit the transaction.
				FragManager.Commit();
			}

			FindViewById<Button>( Resource.Id.cancelButton )!.Click += ( _, _ ) =>
			                                                           {
				                                                           SatchelScanner.Abort();
				                                                           Status = STATUS.CANCELED;
			                                                           };

			Satchel = await Task.Run( async () =>
			                          {
				                          try
				                          {
					                          while( Status == STATUS.BUSY )
					                          {
						                          var BCode = await SatchelScanner.Scan();
						                          BCode = BCode.Trim();

						                          if( BCode.IsNotNullOrEmpty() )
						                          {
							                          if( ( Status == STATUS.BUSY ) && !IsSatchelIdValid( BCode ) )
								                          Globals.Dialogues.Error( "Not a Satchel Barcode", true, this, () => { SatchelScanner.Abort(); } );
							                          else
							                          {
								                          Status = STATUS.OK;

								                          return BCode;
							                          }
						                          }
					                          }

					                          return "";
				                          }
				                          finally
				                          {
					                          RunOnUiThread( Finish );
				                          }
			                          } );
		}
	}
}