﻿using Android.Widget;
using Droid2.Fragments.Standard.Claims.StandardFragment;

namespace Droid2.Fragments._Customers.Pml
{
	public class PmlClaims : ClaimsStandard
	{
		private CheckBox Landscape;

		protected override int OverrideLayoutId( int id )
		{
			return Resource.Layout.Pml_Activity_ClaimsStandard;
		}

		protected override void OnExecute( object arg = null )
		{
			base.OnExecute( arg );
			Landscape = FindViewById<CheckBox>( Resource.Id.landscape );

			Landscape.Click += ( sender, args ) =>
			                   {
				                   LandscapeMode = Landscape.Checked;
			                   };

			Landscape.Checked = LandscapeMode;
		}
	}
}