﻿#nullable enable

using Android.Views;
using Droid2.Fragments.Customers.Pml;
using Service.Interfaces;

namespace Droid2.Fragments._Customers.Pml.StarTrack
{
	public class StarTrackAudit : PmlAudit
	{
		private const string STARTRACK_PICKED_UP = "Startrackpu";


		protected override void BeforeUpdateRemoteTrip( ITrip t )
		{
			t.Driver = STARTRACK_PICKED_UP;
			t.Status = STATUS.PICKED_UP;
		}

		protected virtual Globals.OperatingMode.OPERATING_MODE OnExit()
		{
			return Globals.OperatingMode.OPERATING_MODE.PML_STAR_TRACK;
		}

		protected override void OnCreateFragment()
		{
			base.OnCreateFragment();
			ExitButton.Visibility = ViewStates.Visible;

			ExitButton.Click += ( sender, args ) => { Return( OnExit() ); };
		}
	}
}