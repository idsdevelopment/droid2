﻿#nullable enable

using System.Threading;
using Android.Content.PM;
using Android.Widget;
using Droid2.Fragments.Standard;

namespace Droid2.Fragments._Standard_Customers.Pml.StarTrack
{
	public class StarTrack : AFragment
	{
		protected override void OnExecute( object? arg = null )
		{
			Orientation = ScreenOrientation.Unspecified;

			while( Executing )
				Thread.Sleep( 250 );
		}

		protected virtual Globals.OperatingMode.OPERATING_MODE OnScan1()
		{
			return Globals.OperatingMode.OPERATING_MODE.PML_STAR_TRACK_PICKUPS;
		}

		protected virtual Globals.OperatingMode.OPERATING_MODE OnScan2()
		{
			return Globals.OperatingMode.OPERATING_MODE.PML_STAR_TRACK_AUDIT;
		}

		protected override void OnCreateFragment()
		{
			var Scan1 = FindViewById<Button>( Resource.Id.scan1Button );

			if( Scan1 is not null )
				Scan1.Click += ( sender, args ) => { Return( OnScan1() ); };

			var Scan2 = FindViewById<Button>( Resource.Id.scan2Button );

			if( Scan2 is not null )
				Scan2.Click += ( sender, args ) => { Return( OnScan2() ); };
		}

		protected override void OnDestroyFragment()
		{
		}

		protected override void OnCreateDynamicFragments()
		{
		}

		public StarTrack() : base( Resource.Layout.Pml_Activity_Startrack )
		{
		}
	}
}