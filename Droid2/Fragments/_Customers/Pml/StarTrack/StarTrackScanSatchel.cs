﻿using Android.App;
using Droid2.Fragments.Customers.Pml;

namespace Droid2.Fragments._Customers.Pml.StarTrack
{
	[Activity( Theme                 = "@android:style/Theme.Translucent.NoTitleBar",
	           Label                 = "PmlSatchel",
	           AlwaysRetainTaskState = true,
	           NoHistory             = true
	         )]
	public class StarTrackScanSatchel : PmlScanSatchel
	{
		protected override bool IsSatchelIdValid( string txt )
		{
			return true;
		}
	}
}