﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.OS;
using Android.Views;
using Droid2.Adapters;
using Droid2.Adapters._Customers.Pml;
using Droid2.Adapters._Customers.Pml.StarTrack;
using Droid2.Fragments.Customers.Pml;
using Service.Interfaces;
using Utils;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments._Customers.Pml.StarTrack
{
	public class StarTrackPickups : PmlDsdDeliveries
	{
		public const string STARTRACK = "startrack",
		                    STARTRACK_SCAN1 = "Startrackscan1";

		protected override string Title => "Dispatch Scan";

		protected override Type ScanSatchelIntentType => typeof( StarTrackScanSatchel );

		protected override bool WaitingForTrips => false;

		protected override PmlClaimsPiecesAdapter GetPmlAdapter => new StarTrackClaimsPiecesAdapter( Activity,
		                                                                                             ListView );

		private List<Trip> TripsToDelete = new();

		protected override Task<AFTER_SATCHEL_SCAN_STATUS> AfterSatchelScan( ITrip trip, bool isExamine, bool finalising, bool showing )
		{
			return Task.FromResult( AFTER_SATCHEL_SCAN_STATUS.COMPLETED );
		}

		protected override void AfterCompletedStatus()
		{
			Globals.Dialogues.Inform( "Completed", () => { base.AfterCompletedStatus(); } );
		}

		protected override ScannedTrip MatchTrip( List<Trip> scannerTrips, string scannedId )
		{
			if( GetPmlAdapter.GetBarcodeDetails( scannedId ).Found )
			{
				var Trip = ( from T in scannerTrips
				             where IsStarTrack( T )
				             select T as ScannedTrip ).FirstOrDefault();

				if( Trip is not null )
					return Trip;
			}

			return base.MatchTrip( scannerTrips, scannedId );
		}

		public static bool IsStarTrack( ITrip t )
		{
			return t.ServiceLevel.Compare( STARTRACK, StringComparison.OrdinalIgnoreCase ) == 0;
		}

		protected override void BeforeUpDateSatchel( Trip satchel )
		{
			foreach( var Td in TripsToDelete )
			{
				Td.Status = STATUS.DELETED;
				Globals.Database.UpdateTrip( Td );
			}

			satchel.Status = STATUS.CREATE_NEW_TRIP_AS_DISPATCHED;
			satchel.Driver = STARTRACK_SCAN1;
		}

		protected override void BeforeUpdateNewTrip( Trip oldTrip, Trip newTrip )
		{
			oldTrip.Status       = STATUS.DELETED;
			newTrip.ServiceLevel = STARTRACK;
			newTrip.Driver       = STARTRACK_SCAN1;
			newTrip.Status       = STATUS.CREATE_NEW_TRIP_AS_DISPATCHED;
		}

		public override void OnActivityCreated( Bundle savedInstanceState )
		{
			base.OnActivityCreated( savedInstanceState );
			ExitButton.Visibility = ViewStates.Visible;

			ExitButton.Click += ( sender, args ) => { Return( Globals.OperatingMode.OPERATING_MODE.PML_STAR_TRACK ); };
		}

		protected override bool ValidateLocation( string locationCode, out LocationCompany? location )
		{
			location = null;

			var Trips = Globals.Database.Service.GetTripsAtLocation( locationCode ).Result;

			if( Trips is null || ( Trips.Count == 0 ) )
				Error( "No Trips For This Location" );
			else
			{
				var Driver = Globals.Login.UserName;
				var Now    = DateTime.Now;

				foreach( var T in Trips )
				{
					if( IsStarTrack( T ) )
					{
						T.Driver     = Driver;
						T.Status     = STATUS.DISPATCHED;
						T.PickupTime = Now;
						Globals.Database.UpdateTrip( T );
						AddCompanyBarcode( T.PickupCompanyName, new Trip( T ), false );
					}
				}

				CheckForTrips();

				var Ok = base.ValidateLocation( locationCode, out location );

				if( Ok )
				{
					TripsToDelete = ( from T in location.Trips
					                  where IsStarTrack( T ) && ( T.PackageType.Compare( UPLIFT, StringComparison.OrdinalIgnoreCase ) == 0 )
					                  select T ).ToList();

					RunOnUiThread( () => { ExitButton.Visibility = ViewStates.Gone; } );
				}

				return Ok;
			}

			return false;
		}

		protected override bool TryGetLocation( out LocationCompany location, out string locationCode )
		{
			RunOnUiThread( () => { ExitButton.Visibility = ViewStates.Visible; } );

			return base.TryGetLocation( out location, out locationCode );
		}
	}
}