using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Adapters._Customers.Pml;
using Droid2.Barcode;
using Droid2.Fragments.Standard;
using Droid2.Signature;
using Service.Interfaces;
using Utils;
using Exception = Java.Lang.Exception;
using Math = Java.Lang.Math;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

// ReSharper disable AccessToModifiedClosure

namespace Droid2.Fragments.Customers.Pml
{
	[Activity( Theme                 = "@android:style/Theme.Translucent.NoTitleBar",
	           Label                 = "PmlAudit",
	           AlwaysRetainTaskState = true,
	           NoHistory             = true
	         )]
	public class PmlAudit : AFragment
	{
		private bool LandscapeMode
		{
			get
			{
				var Temp = Globals.Settings.PmlAuditLandscape;
				Orientation = Temp ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;

				return Temp;
			}

			set
			{
				Orientation                        = value ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;
				Globals.Settings.PmlAuditLandscape = value;
			}
		}

		// ReSharper disable once InconsistentNaming
		private enum ACCEPT_STATE
		{
			PROCESSING,
			ACCEPT,
			FINALISE
		}

		private ACCEPT_STATE AcceptState;


		private PmlAuditAdapter Adapter;
		private bool Cancel;

		private string CustomerSignature = "";

		private LinearLayout NavigationLayout,
		                     BarcodeLayout,
		                     PodSignature;

		private Button NextButton,
		               CancelButton,
		               AcceptButton;

		private TextView PodName,
		                 SatchelText;

		private BarcodeInputFragment Scanner;

		private CheckBox ShowHidden,
		                 Landscape;

		private SignatureFragment Signature;

		protected Button ExitButton;

		public string Satchel = "",
		              SatchelId = "";

		private ListView ListView;

		private void Process()
		{
			var Trips = new List<Trip>( Adapter.Trips );
			var Pod   = PodName.Text.Trim();
			var Sig   = CustomerSignature;
			var Now   = DateTime.Now;

			Globals.Database.DeleteAllLocalTrips();

			Task.Run( async () =>
			          {
				          while( true )
				          {
					          try
					          {
						          var (Trp, Ok) = await Globals.Database.GetRemoteTrip( SatchelId );

						          if( Ok )
						          {
							          Trp.Status              = STATUS.VERIFIED;
							          Trp.PodName             = Pod;
							          Trp.Signature           = Sig;
							          Trp.DeliveryArrivalTime = Now;
							          Trp.DeliveryTime        = Now;
							          BeforeUpdateRemoteTrip( Trp );
							          Globals.Database.UpdateRemoteTrip( Trp );
						          }

						          foreach( var Trip in Trips )
						          {
							          if( Trip is ScannedTrip T )
							          {
								          T.Status              = STATUS.VERIFIED;
								          T.PodName             = Pod;
								          T.Signature           = Sig;
								          T.DeliveryArrivalTime = Now;
								          T.DeliveryTime        = Now;
								          BeforeUpdateRemoteTrip( T );
								          Globals.Database.UpdateRemoteTrip( T );
							          }
						          }
					          }
					          catch( Exception Exception )
					          {
						          Console.WriteLine( Exception );
					          }

					          return;
				          }
			          } );
		}

		private void ProcessManually()
		{
			string Text;

			var HasOver  = Adapter.HasOverscan;
			var HasUnder = Adapter.HasUnderScan;

			if( !HasUnder && !HasOver )
				Text = "All Ok - Process Manually\r\n";
			else
			{
				var OverText = HasOver ? "Extra stock" : "";

				var UnderText = HasUnder ? "Missing stock" : "";

				var JoinText = HasOver && HasUnder ? " and " : "";
				Text = $"{OverText}{JoinText}{UnderText}\r\n";
			}

			var Trips = new List<Trip>( Adapter.Trips );

			Task.Run( async () =>
			          {
				          var (Trp, Ok) = await Globals.Database.GetRemoteTrip( SatchelId );

				          if( Ok )
				          {
					          Trp.DeliveryNote = $"{Text}{Trp.DeliveryNote}";
					          Trp.Status       = STATUS.DELETED;
					          BeforeUpdateRemoteTrip( Trp );
					          Globals.Database.UpdateRemoteTrip( Trp );
				          }

				          foreach( var Trip in Trips )
				          {
					          if( Trip is ScannedTrip T )
					          {
						          string Txt;

						          if( T.Balance != 0 )
						          {
							          Txt = T.Balance < 0 ? "Overscan" : "Underscan";
							          Txt = $"{Txt} : {Math.Abs( (float)T.Balance ):N}\r\n";
						          }
						          else
							          Txt = "";

						          Trip.DeliveryNote = $"{Text}{Txt}{Trip.DeliveryNote}";
						          T.Status          = STATUS.DELETED;
						          BeforeUpdateRemoteTrip( T );
						          Globals.Database.UpdateTrip( T, true );
					          }
				          }
			          } );
		}

		protected virtual void BeforeUpdateRemoteTrip( ITrip t )
		{
		}

		protected virtual void ShowExit( bool show )
		{
			RunOnUiThread( () =>
			               {
				               ExitButton.Visibility = show ? ViewStates.Visible : ViewStates.Gone;
			               } );
		}

		protected override async void OnExecute( object arg = null )
		{
			Landscape.Checked = LandscapeMode;

			while( Executing )
			{
				ShowExit( true );

				RunOnUiThread( () =>
				               {
					               Scanner.Placeholder         = "Barcode";
					               NavigationLayout.Visibility = ViewStates.Gone;
					               ShowHidden.Visibility       = ViewStates.Gone;
					               Landscape.Visibility        = ViewStates.Visible;
					               ShowHidden.Checked          = false;
					               Adapter.ShowHidden          = false;
					               AcceptState                 = ACCEPT_STATE.PROCESSING;
					               NextButton.Text             = "Next";
					               PodSignature.Visibility     = ViewStates.Gone;
					               BarcodeLayout.Visibility    = ViewStates.Visible;
					               SatchelText.Visibility      = ViewStates.Visible;
					               SatchelText.Text            = "Satchel";
				               } );

				Satchel = ( await Scanner.Scan() ).Trim();

				if( !Satchel.IsNullOrEmpty() )
				{
					var (Status, ContainerId) = await Globals.Database.GetTripsInContainer( Satchel );

					switch( Status )
					{
					case CONTAINER_STATUS.OK:
						RunOnUiThread( () =>
						               {
							               Adapter.Add( Globals.Database.Trips );
							               ListView.Visibility = ViewStates.Visible;

							               CancelButton.Visibility     = ViewStates.Visible;
							               NextButton.Visibility       = ViewStates.Visible;
							               NextButton.Enabled          = false;
							               ShowHidden.Visibility       = ViewStates.Visible;
							               NavigationLayout.Visibility = ViewStates.Visible;
							               Landscape.Visibility        = ViewStates.Gone;

							               SatchelText.Text = "Item Barcode";
						               } );

						Cancel    = false;
						SatchelId = ContainerId;

						ShowExit( false );

						while( Executing )
						{
							var Barcode = ( await Scanner.Scan() ).Trim();

							var Found = false;

							if( !Barcode.IsNullOrEmpty() )
							{
								foreach( var Trip in Adapter.Trips )
								{
									if( Trip is PmlAuditScannedTrip ScannedTrip )
									{
										if( ScannedTrip.Reference == Barcode )
										{
											Adapter.UpdateCount( ScannedTrip.TripId, ScannedTrip );

											RunOnUiThread( () =>
											               {
												               NextButton.Enabled = !Adapter.HasBalance;
											               } );

											Found = true;

											break;
										}
									}
								}
							}
							else if( Cancel )
								break;

							if( !Found && Executing )
								Globals.Sounds.PlayBadScanSound();
						}

						break;

					case CONTAINER_STATUS.DELETED:
						Globals.Dialogues.Error( "Satchel deleted.\r\nReused satchel id?", true, Activity );

						break;

					case CONTAINER_STATUS.EMPTY:
						Globals.Dialogues.Error( "Empty satchel.\r\nSatchel contains no items", true, Activity );

						break;

					case CONTAINER_STATUS.BAD:
						break;

					default:
						Globals.Dialogues.Error( "Satchel not found.", true, Activity );

						break;
					}
				}
				else
					Globals.Sounds.PlayBadScanSound();
			}
		}

		protected override void OnCreateFragment()
		{
			ExitButton            = FindViewById<Button>( Resource.Id.exitButton );
			ExitButton.Visibility = ViewStates.Gone;

			var MainLayout = FindViewById<LinearLayout>( Resource.Id.mainLayout );
			MainLayout.Visibility = ViewStates.Visible;

			NavigationLayout = FindViewById<LinearLayout>( Resource.Id.navigationButtonLayout );
			var CloseLayout = FindViewById<LinearLayout>( Resource.Id.closeLayout );

			ShowHidden            = FindViewById<CheckBox>( Resource.Id.showHidden );
			ShowHidden.Visibility = ViewStates.Gone;

			Landscape = FindViewById<CheckBox>( Resource.Id.landscape );

			var InLandscape = false;

			Landscape.Click += ( sender, args ) =>
			                   {
				                   if( !InLandscape )
				                   {
					                   InLandscape = true;

					                   try
					                   {
						                   LandscapeMode = Landscape.Checked;
					                   }
					                   finally
					                   {
						                   InLandscape = false;
					                   }
				                   }
			                   };

			ShowHidden.Click += ( sender, args ) =>
			                    {
				                    Adapter.ShowHidden = ShowHidden.Checked;
			                    };

			void CloseCloseLayout()
			{
				CloseLayout.Visibility      = ViewStates.Gone;
				MainLayout.Visibility       = ViewStates.Visible;
				NavigationLayout.Visibility = ViewStates.Visible;
			}

			void Abort()
			{
				RunOnUiThread( () =>
				               {
					               AcceptState = ACCEPT_STATE.PROCESSING;
					               Adapter.Clear();
					               Cancel = true;
					               Scanner.Abort();
					               ShowHidden.Visibility = ViewStates.Gone;
					               ShowHidden.Checked    = false;
					               CloseCloseLayout();
					               NavigationLayout.Visibility = ViewStates.Gone;
					               BarcodeLayout.Visibility    = ViewStates.Visible;
					               PodSignature.Visibility     = ViewStates.Gone;
				               } );
			}

			CloseLayout.Visibility = ViewStates.Gone;

			FindViewById<Button>( Resource.Id.discardButton ).Click += ( sender, args ) =>
			                                                           {
				                                                           RunOnUiThread( Abort );
			                                                           };

			FindViewById<Button>( Resource.Id.manualButton ).Click += ( sender, args ) =>
			                                                          {
				                                                          ProcessManually();
				                                                          Abort();
			                                                          };

			FindViewById<Button>( Resource.Id.cancelCancelButton ).Click += ( sender, args ) =>
			                                                                {
				                                                                CloseCloseLayout();
			                                                                };

			BarcodeLayout = FindViewById<LinearLayout>( Resource.Id.barcodeLayout );
			var PodLayout = FindViewById<LinearLayout>( Resource.Id.podLayout );

			SatchelText = FindViewById<TextView>( Resource.Id.satchelText );

			var SignatureLayout = FindViewById<FrameLayout>( Resource.Id.signatureLoadFrame );
			SignatureLayout.Visibility = ViewStates.Gone;

			PodSignature            = FindViewById<LinearLayout>( Resource.Id.podSignature );
			PodSignature.Visibility = ViewStates.Gone;

			ListView = FindViewById<ListView>( Resource.Id.itemListView );

			Adapter = new PmlAuditAdapter( Activity, ListView )
			          {
				          ShowMinimalOptions = true,
				          AutoShow           = false
			          };
/*
			Adapter.OnRowLongClick += ( view, o, arg3 ) =>
			                          {
				                          Scanner.InjectBarcode( Adapter.CurrentTrip.Reference );
			                          };
*/
			ListView.Adapter = Adapter;

			void OnAcceptButtonOnClick( object sender, EventArgs args )
			{
				Process();
				Abort();
			}

			NextButton = FindViewById<Button>( Resource.Id.nextButton );

			NextButton.Click += ( sender, args ) =>
			                    {
				                    SatchelText.Visibility   = ViewStates.Gone;
				                    BarcodeLayout.Visibility = ViewStates.Gone;

				                    switch( AcceptState )
				                    {
				                    case ACCEPT_STATE.PROCESSING:
					                    AcceptState        = ACCEPT_STATE.ACCEPT;
					                    Adapter.ShowHidden = true;
					                    NextButton.Text    = "Accept Delivery";

					                    break;

				                    case ACCEPT_STATE.ACCEPT:
					                    AcceptState             = ACCEPT_STATE.FINALISE;
					                    ListView.Visibility     = ViewStates.Gone;
					                    PodSignature.Visibility = ViewStates.Visible;
					                    NextButton.Visibility   = ViewStates.Gone;
					                    AcceptButton.Enabled    = false;

					                    OnAcceptButtonOnClick( sender, args );

					                    break;
				                    }
			                    };

			NextButton.Visibility = ViewStates.Gone;

			CancelButton = FindViewById<Button>( Resource.Id.cancelButton );

			CancelButton.Click += ( sender, args ) =>
			                      {
				                      Globals.Keyboard.Hide  = true;
				                      CloseLayout.Visibility = ViewStates.Visible;
				                      MainLayout.Visibility  = ViewStates.Gone;
			                      };

			CancelButton.Visibility = ViewStates.Gone;

			AcceptButton         = FindViewById<Button>( Resource.Id.acceptButton );
			AcceptButton.Enabled = false;

			AcceptButton.Click += OnAcceptButtonOnClick;

			PodName = FindViewById<TextView>( Resource.Id.podName );

			void EnableAccept()
			{
				RunOnUiThread( () =>
				               {
					               AcceptButton.Enabled = !CustomerSignature.IsNullOrEmpty() && !PodName.Text.Trim().IsNullOrEmpty();
				               } );
			}

			PodName.TextChanged += ( sender, args ) =>
			                       {
				                       EnableAccept();
			                       };

			var ButtonsLayout = FindViewById<LinearLayout>( Resource.Id.buttonsLayout );

			FindViewById<Button>( Resource.Id.signButton ).Click += async ( sender, args ) =>
			                                                        {
				                                                        PodName.RequestFocus(); // Make sure something has the focus so we can loose the keyboard
				                                                        Globals.Keyboard.Hide = true;

				                                                        NavigationLayout.Visibility = ViewStates.Gone;
				                                                        ButtonsLayout.Visibility    = ViewStates.Gone;

				                                                        PodLayout.Visibility       = ViewStates.Gone;
				                                                        SignatureLayout.Visibility = ViewStates.Visible;
				                                                        Orientation                = ScreenOrientation.Landscape;
				                                                        SignatureLayout.BringToFront();

				                                                        var SignaturePoints = await Signature.CaptureSignature();
				                                                        CustomerSignature = !string.IsNullOrWhiteSpace( SignaturePoints.Points ) ? $"{SignaturePoints.Height}^{SignaturePoints.Width}^{SignaturePoints.Points}" : "";

				                                                        EnableAccept();

#if ALLOW_ROTATE
				                                                        Orientation = ScreenOrientation.Unspecified;
#else
				                                                        Orientation = ScreenOrientation.Portrait;
#endif

				                                                        SignatureLayout.Visibility  = ViewStates.Gone;
				                                                        PodLayout.Visibility        = ViewStates.Visible;
				                                                        ButtonsLayout.Visibility    = ViewStates.Visible;
				                                                        NavigationLayout.Visibility = ViewStates.Visible;
			                                                        };
		}

		protected override void OnDestroyFragment()
		{
			Scanner.Abort();
		}

        [Obsolete]
        protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();

			Scanner.OnInitialised = () =>
			                        {
				                        Scanner.OnAccept += s =>
				                                            {
					                                            Satchel = s;
				                                            };

				                        Scanner.OnCancel += () =>
				                                            {
					                                            Satchel = "";
				                                            };
			                        };

			AddDynamicFragment( Resource.Id.scannerLoadFrame, Scanner );

			Signature = new SignatureFragment();
			AddDynamicFragment( Resource.Id.signatureLoadFrame, Signature );
		}

		public PmlAudit() : base( Resource.Layout.Pml_Activity_Audit )
		{
		}
	}
}