using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Widget;

// ReSharper disable AccessToModifiedClosure

namespace Droid2.Fragments.Customers.Pml
{
	[Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
		Label = "PmlSelectReason",
		AlwaysRetainTaskState = true )]
	public class PmlSelectReasonActivity : Activity
	{
		private static readonly Dictionary<REASONS, string> Reasons = new()
		                                                              {
			                                                                    { REASONS.NONE, "??????" },
			                                                                    { REASONS.STORE_NOT_READY, "Store Not Ready" },
			                                                                    { REASONS.NOT_IDENTICAL, "Stock not identical to return" },
			                                                                    { REASONS.NO_STOCK, "No Stock To Collect" },
			                                                                    { REASONS.AUSTRALIAN_STOCK, "Australian Stock" },
			                                                                    { REASONS.OPEN_STOCK, "Open Stock" },
			                                                                    { REASONS.OTHER, "Other" }
		                                                                    };

		public static REASONS Reason = REASONS.NONE;

		public static STATUS Status;

		public static string ReasonAsText => Reasons[ Reason ];
		private Button OkButton;

		public enum REASONS
		{
			NONE,
			STORE_NOT_READY,
			NOT_IDENTICAL,
			NO_STOCK,
			AUSTRALIAN_STOCK,
			OPEN_STOCK,
			OTHER
		}
		public enum STATUS
		{
			BUSY,
			OK,
			CANCELED
		}

		protected override void OnCreate( Bundle savedInstanceState )
		{
			Status = STATUS.BUSY;

			base.OnCreate( savedInstanceState );
			SetContentView( Resource.Layout.Pml_Activity_SelectReason );

			OkButton = FindViewById<Button>( Resource.Id.okButton );
			OkButton.Click += ( sender, args ) =>
			                  {
				                  Status = STATUS.OK;
				                  Finish();
			                  };

			RunOnUiThread( () =>
			               {
				               OkButton.Enabled = false;
			               } );

			FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
			                                                          {
				                                                          Status = STATUS.CANCELED;
				                                                          Finish();
			                                                          };

			FindViewById<RadioButton>( Resource.Id.radioButton1 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.STORE_NOT_READY;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton2 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NOT_IDENTICAL;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton3 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NO_STOCK;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton4 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.AUSTRALIAN_STOCK;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton5 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.OPEN_STOCK;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton6 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.OTHER;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton7 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NONE;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton8 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NONE;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton9 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NONE;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton10 ).Click += ( sender, args ) =>
			                                                                {
				                                                                Reason = REASONS.NONE;
				                                                                OkButton.Enabled = true;
			                                                                };
		}
	}
}