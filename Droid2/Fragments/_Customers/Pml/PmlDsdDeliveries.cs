﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using CommsDb.Database;
using Droid2.Adapters;
using Droid2.Adapters._Customers.Pml;
using Droid2.Adapters.Customers.Metro;
using Droid2.Fragments.Customers.Metro;
using IdsService;
using IdsService.org.internetdispatcher.jbtest2;
using Java.Lang;
using Service.Interfaces;
using Utils;
using Thread = System.Threading.Thread;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Customers.Pml
{
	public class PmlDsdDeliveries : MetroDsdDeliveries
	{
		internal const string SATCHEL = "Satchel";

		public const string UPLIFT          = "UPLIFT",
		                    UPLIFTED        = "Uplifted",
		                    UPPACK          = "UPPACK",
		                    UPPC            = "UPPC",
		                    UPTOTAL         = "UPTOTAL",
		                    UPLIFT_COMPLETE = "UpliftComplete";


		private static string _PmlGroupNumber;
		public static  bool   RegenerateGroupNumber;

		public static string PmlGroupNumber
		{
			get
			{
				if( RegenerateGroupNumber )
				{
					RegenerateGroupNumber = false;

					_PmlGroupNumber =
						$"{Globals.Login.UserName} - {DateTime.UtcNow:s}";
				}

				return _PmlGroupNumber;
			}
		}

		protected override string Title => "Uplifts / Returns";

		protected virtual PmlClaimsPiecesAdapter GetPmlAdapter => new( Activity,
		                                                               ListView );

		protected override BaseTripAdapter GetClaimsPiecesAdapter
		{
			get
			{
				Adp                = GetPmlAdapter;
				Adp.OnRowClick     = OnRowClick;
				Adp.OnRowLongClick = OnRowClick;
			#if DEBUG
				Adp.OnRowLongClick = ( view, o, arg3 ) =>
				                     {
					                     if( !IgnoreAdapterClicks )
						                     InjectScanner();
				                     };
			#endif
				Adp.OnRowClick       = AcceptAdapterOnRowClick;
				ScannedCount         = 0;
				ShowScannedTotalOnly = true;

				return Adapter = Adp;
			}
		}

		protected virtual Type ScanSatchelIntentType => typeof( PmlScanSatchel );

		private PmlClaimsPiecesAdapter Adp;

		private bool HadSatchelTrip,
		             IgnoreSatchel;

		private bool IgnoreAdapterClicks;

		private string PickupAddressId,
		               DeliveryAddressId,
		               AccountId,
		               PickupCompanyName,
		               DeliveryCompanyName,
		               Driver;

		private string     Satchel = "";
		private List<Trip> SaveAdapterTrips;

		private string SavePickupEmailAddress   = "",
		               SaveDeliveryEmailAddress = "",
		               SaveCallerEmailAddress   = "";

		private   bool   Futile;
		protected Button ExitButton;

		protected CheckBox Landscape;

		private async void AcceptAdapterOnRowClick( View view, object o, EventArgs arg3 )
		{
			if( IgnoreAdapterClicks )
				return;
			bool Finished;

			ScannedTrip Trp;

			if( !Futile )
			{
				PmlDriverVerificationOptionsActivity.Status = PmlDriverVerificationOptionsActivity.STATUS.BUSY;

				PmlDriverVerificationOptionsActivity.SelectedTrip = Adp.Trips[ (int)view.Tag ] as MetroScannedTrip;
				Trp                                               = PmlDriverVerificationOptionsActivity.SelectedTrip;

				if( Trp is null )
					return;

				if( IsUpTotal( Trp ) || string.IsNullOrWhiteSpace( Trp.ServiceLevel ) )
				{
					Globals.Sounds.PlayBadScanSound();

					return;
				}

				var Intent = new Intent( Activity, typeof( PmlDriverVerificationOptionsActivity ) );
				Activity.StartActivity( Intent );

				Finished = await Task.Run( () =>
				                           {
					                           while( PmlDriverVerificationOptionsActivity.Status == PmlDriverVerificationOptionsActivity.STATUS.BUSY )
						                           Thread.Sleep( 100 );

					                           return true;
				                           } );
			}
			else
			{
				Finished = true;

				if( Adp.Trips.Count == 0 )
					return;

				PmlDriverVerificationOptionsActivity.SelectedTrip = Adp.Trips[ 0 ] as MetroScannedTrip;
				Trp                                               = PmlDriverVerificationOptionsActivity.SelectedTrip;

				PmlDriverVerificationOptionsActivity.Status = PmlDriverVerificationOptionsActivity.STATUS.CANNOT_PICKUP;
			}

			if( Trp is null )
				return;

			if( Finished )
			{
				switch( PmlDriverVerificationOptionsActivity.Status )
				{
				case PmlDriverVerificationOptionsActivity.STATUS.CANNOT_PICKUP:
					var ReasonAsText = PmlSelectReasonActivity.ReasonAsText;

					var Status = PmlSelectReasonActivity.Reason switch
					             {
						             PmlSelectReasonActivity.REASONS.STORE_NOT_READY => STATUS.ACTIVE,
						             PmlSelectReasonActivity.REASONS.OTHER           => STATUS.ACTIVE,
						             _                                               => STATUS.DELETED
					             };

					//		var Aurt = ( (PmlScannedTrip)Trp ).AurtNumber;
					var Now  = DateTime.Now.ToString( "g" );
					var Trps = Adapter.Trips;

					foreach( var Trip in Trps )
					{
						if( Trip is PmlScannedTrip T )
						{
							T.Status             = Status;
							T.CannotPickupReason = ReasonAsText;

							T.PickupNote    = $"Pickup Cancelled {T.CannotPickupReason} {Now}\r\n{T.PickupNote}";
							T.CannotPickup  = true;
							T.Balance       = T.Pieces;
							T.Scanned       = 0;
							T.Hidden        = true;
							T.Undeliverable = false;
						}
					}

					Adp.NotifyDataSetChanged();
					Adp.DoScanCompleted();
					await OnUpdateTrip();
					Finish();

					break;

				case PmlDriverVerificationOptionsActivity.STATUS.RESET_SCAN:
					Trp.Balance            = Trp.Pieces;
					Trp.Scanned            = 0;
					Trp.Hidden             = false;
					Trp.Undeliverable      = false;
					Trp.CannotPickup       = false;
					Trp.NotForThisLocation = false;

					if( IsUpLift( Trp ) )
					{
						var Trips = Adapter.Trips;
						var Ndx   = Adp.IndexOf( Trp.TripId );

						if( Ndx >= 0 )
						{
							var ToRemove = new List<Trip> { Trp };

							while( ++Ndx < Trips.Count )
							{
								if( Trips[ Ndx ] is PmlScannedTrip T && string.IsNullOrWhiteSpace( T.ServiceLevel ) )
									ToRemove.Add( T );
								else
									break;
							}

							Adp.Remove( ToRemove );
						}
					}
					else
						Adp.Remove( Trp );

					foreach( var AdapterTrip in SaveAdapterTrips )
					{
						if( AdapterTrip is PmlScannedTrip T && ( T.TripId == Trp.TripId ) )
						{
							T.Balance = Trp.Balance;
							T.Scanned = Trp.Scanned;

							if( IsUpLift( T ) )
								T.ProductTypes.Clear();
						}
					}

					Adp.NotifyDataSetChanged();
					Adp.DoScanCompleted();

					break;

				case PmlDriverVerificationOptionsActivity.STATUS.CANCEL:
					break;

				case PmlDriverVerificationOptionsActivity.STATUS.BUSY:
					break;

				default:
					throw new ArgumentOutOfRangeException();
				}
			}
		}

		private void HideButtons()
		{
			ExitButton.Visibility   = ViewStates.Gone;
			AcceptButton.Visibility = ViewStates.Gone;
			CancelBtn.Visibility    = ViewStates.Gone;
		}

		protected void Finish()
		{
			RunOnUiThread( () =>
			               {
				               CurrentTrip    = null;
				               CancelDelivery = true;
				               Scanner.Abort();
				               OnCancelButtonClick();
				               ShowScannedTotalOnly = false;
				               Futile               = false;

				               HideButtons();
			               } );
		}


		protected override int OverrideLayoutId( int id )
		{
			return Resource.Layout.Pml_Activity_PickupDelivery;
		}

		public static bool IsUpliftedTrip( Trip t )
		{
			return string.Compare( t.ServiceLevel.ToUpper(), UPLIFTED, StringComparison.OrdinalIgnoreCase ) == 0;
		}

		protected override void OnExecute( object arg = null )
		{
			Landscape         = FindViewById<CheckBox>( Resource.Id.landscape );
			Landscape.Checked = LandscapeMode;

			Landscape.Click += ( sender, args ) => { LandscapeMode = Landscape.Checked; };

			base.OnExecute( arg );
		}

		public override void OnActivityCreated( Bundle savedInstanceState )
		{
			ExitButton = FindViewById<Button>( Resource.Id.exitButton );

			HideButtons();

			base.OnActivityCreated( savedInstanceState );
			FutileButton.Visibility = ViewStates.Visible;

			FutileButton.Click += async ( sender, args ) =>
			                      {
				                      Futile = await SelectReason();

				                      if( Futile )
				                      {
					                      Scanner.Abort();
					                      AcceptAdapterOnRowClick( null, null, null );
				                      }
			                      };
		}

		protected override void ShowOnlyReturns( List<Trip> trips )
		{
		}

		protected override STATUS[] GetTripFilterSet()
		{
			return Db<Client, AuthToken,
				remoteTrip>.DefaultStatusFilter;
		}

		protected override void OnCancelButtonClick()
		{
			Adp.ShowHidden = false;
			AfterUpdatePieceCount();
			ShowScannedTotalOnly = true;
			IgnoreAdapterClicks  = false;

			if( SaveAdapterTrips is not null )
			{
				Adapter.Replace( SaveAdapterTrips );
				SaveAdapterTrips = null;
			}

			AfterUpdatePieceCount();
		}

		protected override List<Trip> GetTripsForAcceptDisplay()
		{
			ShowScannedTotalOnly = false;
			Adp.ShowHidden       = true;
			IgnoreAdapterClicks  = false;
			var RetVal = new List<Trip>();
			SaveAdapterTrips = Adapter.Trips;

			decimal TotalScanned = 0;

			foreach( var AdapterTrip in Adapter.Trips )
			{
				if( AdapterTrip is PmlScannedTrip Trip )
				{
					if( Trip.Scanned > 0 )
					{
						RetVal.Add( new PmlScannedTrip( Trip ) );

						if( IsUpLift( Trip ) )
						{
							foreach( var (Key, Scanned) in Trip.ProductTypes )
							{
								if( Scanned > 0 )
								{
									TotalScanned += Scanned;

									RetVal.Add( new PmlScannedTrip( Trip )
									            {
										            Status = STATUS.CREATE_NEW_TRIP_AS_PICKED_UP,

										            TripId = $"{Trip.TripId}~", // Add suffix for display ordering

										            Reference = Key,
										            Scanned   = Scanned,

										            ServiceLevel = "",
										            PackageType  = ""
									            } );
								}
							}
						}
						else
							TotalScanned += Trip.Scanned;
					}
				}
			}

			ScannedCount = (int)TotalScanned;
			Adapter.DoScanCompleted();

			return RetVal;
		}

// Used in debug mode
		protected virtual void InjectScanner()
		{
			Scanner.InjectBarcode( Adapter.CurrentTrip.Reference );
		}

		protected override decimal AfterUpdatePieceCount()
		{
			var Retval = (int)BaseAfterUpdatePieceCount();
			ScannedCount = Retval;

			return Retval;
		}

		protected override ScannedTrip MatchTrip( List<Trip> scannerTrips, string scannedId )
		{
			var Trip = base.MatchTrip( scannerTrips, scannedId );

			foreach( var ScannerTrip in scannerTrips )
			{
				if( IsUpLift( ScannerTrip ) )
					return new ScannedTrip( ScannerTrip );
			}

			return Trip;
		}

		protected override List<Trip> OnFilterTrips( List<Trip> trips )
		{
			return ( from T in trips
			         where ( T.Status == STATUS.PICKED_UP )
			               || IsReturn( T ) || IsUpTotal( T ) || IsUpPack( T ) || IsUpLift( T ) || IsUppc( T )
			         select T ).ToList();
		}

		protected override void OnScanComplete( bool completed )
		{
			if( completed )
			{
				RunOnUiThread( () =>
				               {
					               AcceptButton.Visibility = ViewStates.Visible;
					               AcceptButton.Enabled    = true;
				               } );
			}
		}

		protected override bool IsReturn( Trip t )
		{
			return ( t.Status == STATUS.DISPATCHED ) || ( ( t.Status == STATUS.PICKED_UP ) && IsSatchel( t ) );
		}

		protected async Task<bool> SelectReason()
		{
			PmlSelectReasonActivity.Status =
				PmlSelectReasonActivity.STATUS.BUSY;

			var Intent = new Intent( Activity, typeof( PmlSelectReasonActivity ) );

			Activity.StartActivity( Intent );

			return await Task.Run( () =>
			                       {
				                       while( PmlSelectReasonActivity.Status == PmlSelectReasonActivity.STATUS.BUSY )
					                       Thread.Sleep( 100 );

				                       return PmlSelectReasonActivity.Status == PmlSelectReasonActivity.STATUS.OK;
			                       } );
		}

		protected override bool IsTripPickup(
			ScannedTrip t )
		{
			return t.Status != STATUS.PICKED_UP;
		}

		protected override bool AllowReferenceEdit( bool allowEdit )
		{
			return true;
		}

		protected override bool PodValid( string pod, out string validText )
		{
			var VText = new StringBuilder();

			var Ok = true;

			foreach( var C in pod )
			{
				if( ( C >= '0' ) && ( C <= '9' ) )
					Ok = false;
				else
					VText.Append( C );
			}

			validText = VText.ToString();

			return Ok;
		}

		protected enum AFTER_SATCHEL_SCAN_STATUS
		{
			FALSE,
			TRUE,
			COMPLETED
		}

		protected virtual async Task<AFTER_SATCHEL_SCAN_STATUS> AfterSatchelScan( ITrip trip, bool isExamine, bool finalising, bool showing )
		{
			return await base.OnShowDetail( trip, isExamine, true, true ) ? AFTER_SATCHEL_SCAN_STATUS.TRUE : AFTER_SATCHEL_SCAN_STATUS.FALSE;
		}

		protected virtual void AfterCompletedStatus()
		{
			Finish();
		}

		protected override async Task<bool> OnShowDetail( ITrip trip, bool isExamine, bool finalising, bool showing )
		{
			if( finalising && showing )
			{
				var AllSatchels = true;

				foreach( var Trip in Adapter.Trips )
				{
					if( !IsSatchel( Trip ) )
					{
						AllSatchels = false;

						break;
					}
				}

				if( AllSatchels )
					return await base.OnShowDetail( trip, isExamine, true, true );

				PmlScanSatchel.Status = PmlScanSatchel.STATUS.BUSY;

				var Intent = new Intent( Activity, ScanSatchelIntentType );
				Activity.StartActivity( Intent );

				Satchel = await Task.Run( () =>
				                          {
					                          while( PmlScanSatchel.Status == PmlScanSatchel.STATUS.BUSY )
						                          Thread.Sleep( 100 );

					                          return PmlScanSatchel.Satchel;
				                          } );
				var RetVal = false;

				if( !string.IsNullOrEmpty( Satchel ) && ( PmlScanSatchel.Status == PmlScanSatchel.STATUS.OK ) )
				{
					RunOnUiThread( async () =>
					               {
						               RegenerateGroupNumber = true;

						               switch( await AfterSatchelScan( trip, isExamine, true, true ) )
						               {
						               case AFTER_SATCHEL_SCAN_STATUS.TRUE:
							               RetVal = true;

							               break;

						               case AFTER_SATCHEL_SCAN_STATUS.FALSE:
							               RetVal = false;

							               break;

						               case AFTER_SATCHEL_SCAN_STATUS.COMPLETED:
							               await OnUpdateTrip();
							               AfterCompletedStatus();

							               break;
						               }

						               ;
					               } );
				}

				return RetVal;
			}
			else
			{
				RunOnUiThread( () => { UpdateTripButton.Visibility = ViewStates.Gone; } );
			}

			RunOnUiThread( () => { WeightLayout.Visibility = ViewStates.Gone; } );

			return true;
		}

		protected override void ShowDetails( bool hideUndeliverable = false )
		{
			base.ShowDetails( true );
		}

		public static bool IsSatchel( ITrip t )
		{
			return string.Equals( t.PackageType, SATCHEL, StringComparison.CurrentCultureIgnoreCase );
		}

		public static bool IsUpTotal( ITrip t )
		{
			return t.ServiceLevel.ToUpper() == UPTOTAL;
		}

		public static bool IsUppc( ITrip t )
		{
			return t.ServiceLevel.ToUpper() == UPPC;
		}

		public static bool IsUpPack( ITrip t )
		{
			return t.ServiceLevel.ToUpper() == UPPACK;
		}

		public static bool IsUpLift( ITrip t )
		{
			return t.ServiceLevel.ToUpper() == UPLIFT;
		}

        [Obsolete]
        public override void OnViewCreated( View view, Bundle savedInstanceState )
		{
			base.OnViewCreated( view, savedInstanceState );

			CancelDeliveryButton =
				FindViewById<Button>( Resource.Id.cancelDeliveryButton );
		}

		protected override List<Trip> OnFilterPickupDeliveryTrips( List<Trip> trips )
		{
			RunOnUiThread( () =>
			               {
				               CancelDeliveryButton.Visibility =
					               ViewStates.Visible;
			               } );
			PmlScannedTrip.ResetLimits();
			var Trips = new List<Trip>();

			foreach( var Trip in trips )
			{
				if( IsReturn( Trip ) )
					Trips.Add( Trip );
			}

			foreach( var Trip in Trips )
			{
				if( IsUpLift( Trip ) || IsUpPack( Trip ) )
					Trip.Pieces = 9999;

				if( IsUpTotal( Trip ) )
					PmlScannedTrip.UpperScanLimit = Trip.Pieces;
			}

			return Trips;
		}

		protected override void UpdateNotes( Trip trip )
		{
			base.UpdateNotes( trip );

			if( trip is ScannedTrip STrip )
			{
				if( IsUpliftedTrip( trip ) ) // Sometimes they seek through
					trip.ServiceLevel = UPLIFT_COMPLETE;

				if( trip.Pieces <= 0 )
					trip.Status = STATUS.DELETED;

				if( !STrip.CannotPickup )
				{
					AccountId = STrip.AccountId;

					PickupAddressId = STrip.OriginalPickupAddressId;

					DeliveryAddressId = STrip.OriginalDeliveryAddressId;
					PickupCompanyName = STrip.PickupCompanyName;

					DeliveryCompanyName = STrip.DeliveryCompanyName;
					Driver              = STrip.Driver;

					if( ( STrip.Status <= STATUS.PICKED_UP ) || ( STrip.Status == STATUS.DELETED ) )
					{
						STrip.Pallets  = PmlGroupNumber;
						HadSatchelTrip = true;
					}
				}
			}
		}

		protected virtual void BeforeUpdateNewTrip( Trip oldTrip, Trip newTrip )
		{
		}

		protected override void BeforeUpdateTrips()
		{
			if( Futile )
				return;

/*
 NotifyOutFifoEmpty = () =>
 
			                     {
				                     LookForTrips = true;
			                     };
*/
			var Now =
				DateTimeExtensions.NowKindUnspecified;

			var Trips = SaveAdapterTrips;
			Adp.Replace( Trips );

			if( Trips is not null )
			{
				for( var I = Trips.Count; --I >= 0; )
				{
					if( Trips[ I ] is PmlScannedTrip T )
					{
						T.PodName              = PodName.Text.Trim();
						T.Signature            = CustomerSignature ?? "";
						SavePickupEmailAddress = T.PickupEmailAddress;

						SaveDeliveryEmailAddress =
							T.DeliveryEmailAddress;
						SaveCallerEmailAddress = T.CallerEmailAddress;

						if( ( T.Status == STATUS.DELETED ) || T.CannotPickup )
							continue;

						void UpdateTrip( string reference, string packageType )
						{
							var Trp = new Trip
							          {
								          Status              = STATUS.CREATE_NEW_TRIP_AS_PICKED_UP,
								          TripId              = T.TripId,
								          AccountId           = T.AccountId,
								          Driver              = T.Driver,
								          BillingNote         = T.BillingNote,
								          PickupCompanyName   = T.PickupCompanyName,
								          Pallets             = PmlGroupNumber,
								          PickupNote          = PmlGroupNumber,
								          DeliveryCompanyName = T.DeliveryCompanyName,
								          PodName             = PodName.Text.Trim(),
								          Signature           = CustomerSignature ?? "",

								          OriginalPickupAddressId   = T.OriginalPickupAddressId,
								          OriginalDeliveryAddressId = T.OriginalDeliveryAddressId,

								          ServiceLevel = UPLIFT_COMPLETE,
								          PackageType  = packageType,

								          Reference = reference,

								          PickupTime = Now,
								          ReadyTime  = Now,
								          DueTime    = Now,
								          Pieces     = T.Scanned,
								          Weight     = 1,

								          PickupEmailAddress   = T.PickupEmailAddress,
								          DeliveryEmailAddress = T.DeliveryEmailAddress,
								          CallerEmailAddress   = T.CallerEmailAddress
							          };

							BeforeUpdateNewTrip( T, Trp );
							Globals.Database.UpdateTrip( T );
							Globals.Database.UpdateTrip( Trp );
						}

						void UpdateStatusAndNotes()
						{
							T.Status = STATUS.DELETED;

							T.DeliveryNote = $"{UPLIFT_COMPLETE}\r\n{T.DeliveryNote}";

							T.BillingNote = $"{PmlGroupNumber}\r\n{T.BillingNote}";
						}

						if( IsSatchel( T ) )
						{
							T.Status      = STATUS.DELETED;
							IgnoreSatchel = true;
						}
						else if( T.Status == STATUS.CREATE_NEW_TRIP_AS_PICKED_UP ) // Uplift
						{
							UpdateStatusAndNotes();
							UpdateTrip( T.TripIdAsText, "" );
						}
						else if( !IsReturnTrip( T ) )
						{
							UpdateStatusAndNotes();

							if( !IsUpTotal( T ) && !IsUpLift( T ) && ( T.Scanned > 0 ) )
								UpdateTrip( T.Reference, T.PackageType );
							T.Status = STATUS.DELETED;
						}
					}
				}
			}
		}

		protected virtual void BeforeUpDateSatchel( Trip satchel )
		{
		}

		protected override void AfterUpdateTrips() // Create the satchel record
		{
			if( HadSatchelTrip && !IgnoreSatchel )
			{
				HadSatchelTrip = false;

				var Now =
					DateTimeExtensions.NowKindUnspecified;

				var S = new Trip
				        {
					        Status                    = STATUS.CREATE_NEW_TRIP_AS_PICKED_UP,
					        AccountId                 = AccountId,
					        Driver                    = Driver,
					        Reference                 = Satchel,
					        Pallets                   = PmlGroupNumber,
					        PackageType               = SATCHEL,
					        ServiceLevel              = SATCHEL,
					        PickupTime                = Now,
					        ReadyTime                 = Now,
					        DueTime                   = Now,
					        Pieces                    = 1,
					        Weight                    = 1,
					        OriginalPickupAddressId   = PickupAddressId,
					        OriginalDeliveryAddressId = DeliveryAddressId,
					        DeliveryCompanyName       = DeliveryCompanyName,
					        PickupCompanyName         = PickupCompanyName,

					        PickupEmailAddress   = SavePickupEmailAddress,
					        DeliveryEmailAddress = SaveDeliveryEmailAddress,
					        CallerEmailAddress   = SaveCallerEmailAddress
				        };
				BeforeUpDateSatchel( S );
				Globals.Database.UpdateTrip( S );
			}

			SaveCallerEmailAddress = SavePickupEmailAddress = SaveDeliveryEmailAddress = "";
			IgnoreAdapterClicks    = false;
			IgnoreSatchel          = false;
		}
	}
}