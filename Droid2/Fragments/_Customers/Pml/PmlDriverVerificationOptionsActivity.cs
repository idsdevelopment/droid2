using Android.App;
using Android.OS;
using Android.Widget;
using Droid2.Adapters;

namespace Droid2.Fragments.Customers.Metro
{
	[Activity( Theme                 = "@android:style/Theme.Translucent.NoTitleBar",
	           Label                 = "ClaimsPiecesActivity",
	           AlwaysRetainTaskState = true )]
	public class PmlDriverVerificationOptionsActivity : Activity
	{
		// ReSharper disable once InconsistentNaming
		public enum STATUS
		{
			CANCEL,
			CANNOT_PICKUP,
			RESET_SCAN,
			BUSY // For Thread
		}

		public static STATUS Status = STATUS.BUSY;
		public static ScannedTrip SelectedTrip;

		protected override void OnCreate( Bundle savedInstanceState )
		{
			Status = STATUS.BUSY;
			base.OnCreate( savedInstanceState );

			if( SelectedTrip == null )
				Finish();
			else
			{
				var Trip = SelectedTrip;

				SetContentView( Resource.Layout.Pml_Activity_DsdDriVerificationOptions );

				FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
				                                                          {
					                                                          Status = STATUS.CANCEL;
					                                                          Finish();
				                                                          };

				RunOnUiThread( () => { FindViewById<TextView>( Resource.Id.tripId ).Text = Trip.TripId; } );

/*
				FindViewById<Button>( Resource.Id.cannotPickupButton ).Click += ( sender, args ) =>
				                                                                {
					                                                                Status = STATUS.CANNOT_PICKUP;
					                                                                Finish();
				                                                                };
*/
				FindViewById<Button>( Resource.Id.resetScanButton ).Click += ( sender, args ) =>
				                                                             {
					                                                             Status = STATUS.RESET_SCAN;
					                                                             Finish();
				                                                             };
			}
		}
	}
}