using Droid2.Fragments._Customers.ConLog;
using Droid2.Fragments.Standard.Claims.CrossDockFragment;
using Droid2.Fragments.Standard.Claims.StandardFragment;

namespace Droid2
{
	public static partial class Globals
	{
		public static partial class Modifications
		{
			private static readonly ProgramEntry[] ConLogPrograms =
			{
				new()
				{
					      Mode        = OperatingMode.OPERATING_MODE.DRIVER_PICKUP,
					      Program     = typeof( ConLogDriverPickup ),
					      RadioButton = Resource.Id.driverPickupButton
				      },
				new()
				{
					      Mode        = OperatingMode.OPERATING_MODE.CLAIMS_STANDARD,
					      Program     = typeof( ClaimsStandard ),
					      RadioButton = Resource.Id.claimsStandardButton
				      },
				new()
				{
					      Mode        = OperatingMode.OPERATING_MODE.CROSS_DOCK,
					      Program     = typeof( CrossDock ),
					      RadioButton = Resource.Id.crossDockButton
				      }
			};

			public static Property IsConLog = new( "ConLog", DEFAULT_LAYOUT, "ConLog",
			                                       new PreferenceOverrides(),
			                                       ConLogPrograms,
			                                       OnLoggedIn );
		}
	}
}