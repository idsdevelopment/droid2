﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using Droid2.Adapters;
using Droid2.Fragments._Standard;
using Droid2.Fragments.Standard.PickupDeliveries;
using Service.Interfaces;
using Utils;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;


namespace Droid2.Fragments._Customers.ConLog
{
	public class ConLogDriverPickup : DriverPickup
	{
		protected override PickupDeliveriesAdapter GetPickupDeliveriesAdapter => new(Activity, ListView, new List<Trip>(), OnTripClick, OnTripLongClick);

		private string FilterAddress = "",
		               FilterCity    = "";

		protected override (List<Trip> Trips, bool IsPickup, string CompanyName, string Barcode) FilterLocation( Trip currentTrip, List<Trip> trips ) =>
			CurrentTrip!.Status != STATUS.PICKED_UP
				? base.FilterLocation( currentTrip, trips )
				: ( FilterTrips( currentTrip ), true, CurrentTrip.DeliveryAddressLine1, CurrentTrip.DeliveryBarcode );

		protected override List<Trip> OnFilterTrips( List<Trip> trips )
		{
			if( FilterAddress.IsNotNullOrWhiteSpace() || FilterCity.IsNotNullOrWhiteSpace() )
			{
				return ( from T in trips
				         where ( T.Status == STATUS.PICKED_UP ) && ( PackUpper( T.DeliveryAddressLine1 ) == FilterAddress ) && ( PackUpper( T.DeliveryCity ) == FilterCity )
				         select T ).ToList();
			}

			return trips;
		}

		private static string PackUpper( string str ) => str.Pack().ToUpper();

		private async void OnTripLongClick( Trip trip )
		{
			if( trip.Status == STATUS.PICKED_UP )
			{
				var Trips = FilterTrips( trip );

				if( await ConfirmTrips.Confirm( Trips ) == ConfirmTrips.STATUS.YES )
				{
					CurrentTrip = trip;
					UpdateTripButtonOnClick( this, EventArgs.Empty );
				}
			}
		}

		private List<Trip> FilterTrips( ITrip currentTrip )
		{
			FilterAddress = PackUpper( currentTrip.DeliveryAddressLine1 );
			FilterCity    = PackUpper( currentTrip.DeliveryCity );

			var Trips = OnFilterTrips( TripListAdapter.Trips );

			FilterCity    = "";
			FilterAddress = "";

			return Trips;
		}
	}
}