﻿using System.Collections.Generic;

namespace Droid2.Fragments._Customers.Metro
{
	public class DestructionTrailer : MetroLoad
	{
		public const string TRAILER = "Trailer";

		protected override string HeaderText => TRAILER;

		protected override void OnCreateFragment()
		{
			base.OnCreateFragment();
			Title = "Destruction Trailer";
		}

		protected override bool VerifyBin( string bin )
		{
			return true;
		}

		protected override void OnUpdate( string proBill, string pop, int sigHeight, int sigWidth, string signature, List<string> references )
		{
			Globals.Database.RbhUpdateDestructionTrailer( proBill, pop, sigHeight, sigWidth, signature, references );
		}
	}
}