#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Adapters.Customers.Metro;
using Droid2.Barcode;
using Droid2.Fragments.Standard.PickupDeliveries;
using Service.Interfaces;
using Utils;
using Timer = System.Timers.Timer;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

// ReSharper disable InconsistentNaming

namespace Droid2.Fragments.Customers.Metro
{
	public class MetroDsdDeliveries : DriverPickup
	{
		public const string RETURN           = "RETURN",
		                    CWD_SHIPMENT     = "CWDSHIPMENT",
		                    TOTE_ACCOUNT     = "Metro SCG",
		                    TOTE_ASSET       = "asset",
		                    TOTE_COMPANY     = "(HubCA44) Rothmans Benson&Hedges Inc",
		                    TOTE_ADDRESS     = "1500 Don Mills Road",
		                    TOTE_CITY        = "1500 Don Mills Road",
		                    TOTE_REGION      = "Ontario",
		                    TOTE_POST_CODE   = "M5X 1A4",
		                    TOTE_ADDRESS_ID  = "15b8d6ea75061c4f01750f49cae04155",
		                    TOTE_RESELLER_ID = "metro",
		                    TOTE_ACCOUNT_ID  = "rbandh_metro",
		                    ASSET            = "ASSET";


		public static bool DoingReturns;


		//        protected override bool EnablePickupDeliver => IsDelivery;
		protected override bool NeedPodOrSignatureOnPickup => true;

		public override UNDELIVERABLE_MODE UndeliverableMode => UNDELIVERABLE_MODE.STATUS_CHANGE;

		private Button ToteButton       = null!,
		               ToteCancelButton = null!,
		               ToteOkButton     = null!;

		private ListView ToteListView = null!;

		private LinearLayout ToteLayout = null!;


		private StringAdapter ToteAdapter = null!;

		private bool  JustScannedLocationCode;
		private Trip? ReturnTrip;
		private bool  GettingReturnCode;

		private decimal TotalPieces;

		private bool AbortTotes;

		private BarcodeInputFragment ToteScanner = null!;

		private LocationCompany? Location;

		protected BaseTripAdapter? Adapter;

		protected override List<Trip> GetTripsForAcceptDisplay()
		{
			var AssetDeliveries = ( from A in SavedAssetTrips
			                        where A.Status is STATUS.PICKED_UP && IsAssetServiceLevel( A )
			                        select A ).ToList();

			var Trips = ( from T in ClaimsPiecesAdapter.Trips
			              where T is ScannedTrip
			              select T ).ToList();

			AssetDeliveries.AddRange( Trips );
			return AssetDeliveries;
		}

		protected override void OnScanComplete( bool completed )
		{
			RunOnUiThread( () => { UpdateTripButton.Visibility = completed ? ViewStates.Visible : ViewStates.Gone; } );

			if( completed && DoingReturns && Adapter is not null )
			{
				DoingReturns = false;

				SavedAssetTrips.AddRange( Adapter.Trips );
				Adapter.Replace( SavedAssetTrips );

				foreach( var Trip in Adapter.Trips )
					Trip.Hidden = IsReturn( Trip ) || IsAsset( Trip );

				RunOnUiThread( () =>
				               {
					               Adapter.NotifyDataSetChanged();
					               AcceptButton.Visibility = ViewStates.Visible;
				               } );
			}
			else
				base.OnScanComplete( completed );

			if( completed )
				ToteButton.Visibility = ViewStates.Gone;
		}


		protected override bool TryGetLocation( out LocationCompany location, out string locationCode )
		{
			SavedAssetTrips = new List<Trip>();

			var Temp = base.TryGetLocation( out Location, out locationCode );
			location = Location;

			State = END_OF_PRECESSING_STATE.NORMAL;

			if( Temp && IsRequired() ) // Asset pickup Driver
			{
				foreach( var Trip in location.Trips )
				{
					if( string.Compare( Trip.ServiceLevel, ASSET, StringComparison.OrdinalIgnoreCase ) == 0 )
					{
						State = END_OF_PRECESSING_STATE.ASSET;
						UpdateTitle();
						break;
					}
				}
			}
			return JustScannedLocationCode = Temp;
		}


		protected override decimal AfterUpdatePieceCount()
		{
			var Temp = base.AfterUpdatePieceCount();
			RunOnUiThread( () => { AcceptButton.Enabled = Temp == TotalPieces; } );
			return Temp;
		}

		protected virtual void ShowOnlyReturns( List<Trip> trips )
		{
			foreach( var Trip in trips )
			{
				if( Trip is ScannedTrip T )
					T.Hidden = !T.IsReturn;
			}

			Adapter?.NotifyDataSetChanged();
		}

		protected override void UpdateTripTimes( STATUS originalStatus, DateTimeOffset originalPickupTime, DateTimeOffset originalDeliveryTime, DateTimeOffset currentTime, Trip trip )
		{
			var SaveStatus = trip.Status;

			try
			{
				trip.Status = originalStatus;

				if( IsCWDTrip( trip ) )
				{
					trip.DeliveryTime      = originalDeliveryTime;
					trip.PickupArrivalTime = currentTime;
					trip.PickupTime        = currentTime.AddMinutes( 2 );
				}
			}
			finally
			{
				trip.Status = SaveStatus;
			}
		}

		protected override void UpdateNotes( Trip trip )
		{
			if( trip is MetroScannedTrip Trip )
			{
				if( Trip.BrokenSeal )
					Trip.BillingNote = $"Broken Seal\r\n{Trip.BillingNote}";

				if( IsCWD( Trip ) )
					Trip.Status = STATUS.PICKED_UP;
			}
		}

		protected void OnRowClick( View view1, object o1, EventArgs eventArgs )
		{
			if( Adapter is not null )
			{
				var T = Adapter.CurrentTrip;

				if( ( DoingReturns && IsReturn( T ) ) || IsCWDTrip( T ) )
				{
					GettingReturnCode   = true;
					ReturnTrip          = T;
					Scanner.Placeholder = "Return Tote Code";
				}
			}
		}

		protected override ScannedTrip? MatchTrip( List<Trip> scannerTrips, string scannedId )
		{
			if( GettingReturnCode )
			{
				GettingReturnCode = false;

				if( ReturnTrip is not null )
				{
					ReturnTrip.Reference = scannedId;
					return ( ReturnTrip as ScannedTrip )!;
				}
			}

			scannedId = scannedId.Trim();

			return ( from ScannerTrip in scannerTrips
			         let St = ScannerTrip as ScannedTrip
			         where St is not null && ( ( St.TripId.Trim() == scannedId ) || ( St.Reference.Trim() == scannedId ) )
			         select St ).FirstOrDefault();
		}

		protected virtual STATUS[] GetTripFilterSet()
		{
			return new[] { STATUS.DISPATCHED, STATUS.PICKED_UP };
		}

		protected override (string CompanyName, string Barcode, bool Ok) GetBarcodeFromCompanyName( string companyName )
		{
			var Temp = base.GetBarcodeFromCompanyName( companyName );

			if( !Temp.Ok )
				Temp.Barcode = companyName;

			Temp.Ok = true;

			return Temp;
		}

		protected override void OnExecute( object? arg = null )
		{
			TripListAdapter.OnTripLongClick = trip =>
			                                  {
				                                  if( IsCWDTrip( trip ) )
				                                  {
					                                  var (_, Barcode, Ok) = GetBarcodeFromCompanyName( trip.PickupCompanyName );

					                                  if( Ok )
						                                  Scanner.InjectBarcode( Barcode );
				                                  }
			                                  };
			base.OnExecute( arg );
			TripStatusFilterSet = GetTripFilterSet();
			Globals.Database.DeleteAllLocalTripsAsync();
			LookForTrips = true;
		}

		protected override bool OnUndeliverable( Trip trip )
		{
			UndeliverableReasonActivity.SelectedTrip = trip;
			UndeliverableReasonActivity.ScanAction   = UndeliverableReasonActivity.SCAN_ACTION.BUSY;

			var Intent = new Intent( Activity, typeof( UndeliverableReasonActivity ) );
			StartActivity( Intent );

			while( UndeliverableReasonActivity.ScanAction == UndeliverableReasonActivity.SCAN_ACTION.BUSY )
				Thread.Sleep( 100 );

			if( UndeliverableReasonActivity.ScanAction == UndeliverableReasonActivity.SCAN_ACTION.OK )
			{
				var Reason = UndeliverableReasonActivity.Reason;
				trip.UndeliverableReason = Reason;
				trip.BillingNote         = $"Reason: {Reason}\r\n{trip.BillingNote}";
				return true;
			}
			return false;
		}
/*
		private bool HasMissingPieces() => Adapter is not null && ( from T in Adapter.Trips
		                                                            let S = (ScannedTrip)T
		                                                            where S.Pieces != S.Scanned
		                                                            select S ).Any();
*/

	#region Assets
		private List<Trip> SavedAssetTrips = new();

		public static bool IsCTT( ITrip t ) => t.Status is STATUS.PICKED_UP && t.TripId.StartsWith( "CTT", StringComparison.OrdinalIgnoreCase ) && IsRequired();

		private static bool HasCTTDeliveries( IEnumerable<Trip> trips ) => ( from T in trips
		                                                                     where IsCTT( T )
		                                                                     select T ).Any();

		public static bool IsRequired() => string.Compare( Globals.Modifications.Fax, "Required", StringComparison.OrdinalIgnoreCase ) == 0; // Asset pickup Driver

		protected override string OnCancelBtnGetText( bool _ ) => base.OnCancelBtnGetText( false );

		protected override void OnCancelButtonClick()
		{
			InAcceptDelivery = State switch
			                   {
				                   END_OF_PRECESSING_STATE.FINALISE => false,
				                   _                                => InAcceptDelivery
			                   };
			base.OnCancelButtonClick();
		}

		private enum END_OF_PRECESSING_STATE
		{
			ASSET,
			NORMAL,
			BARCODES,
			FINALISE
		}

		private END_OF_PRECESSING_STATE State = END_OF_PRECESSING_STATE.NORMAL;

		protected override bool BeforeAcceptButton()
		{
			var LocTrips = Location!.Trips;

			try
			{
				switch( State )
				{
				case END_OF_PRECESSING_STATE.ASSET:
					State           = END_OF_PRECESSING_STATE.NORMAL;
					SavedAssetTrips = new List<Trip>( Adapter!.Trips );

					JustScannedLocationCode = true;

					var MetroTrips = ( from T in LocTrips
					                   where !IsAsset( T )
					                   select (Trip)new MetroScannedTrip( T ) ).ToList();

					var Trips = FilterLocationTrips( MetroTrips );

					List<Trip> FinalDisplayTrips = new();

					FinalDisplayTrips.AddRange( Trips );
					Adapter.Replace( FinalDisplayTrips );
					break;

				case END_OF_PRECESSING_STATE.NORMAL:
					if( HasCTTDeliveries( LocTrips ) )
					{
						InAcceptDelivery = false;
						State            = END_OF_PRECESSING_STATE.BARCODES;
						StartScanTotes();
					}
					else
					{
						State = END_OF_PRECESSING_STATE.FINALISE;
						return true;
					}
					break;

				case END_OF_PRECESSING_STATE.BARCODES:
					State = END_OF_PRECESSING_STATE.FINALISE;
					EndScanTotes();
					return true;
				}
			}
			finally
			{
				UpdateTitle();
			}
			return false;
		}

	#region Scan Totes
		private void CloseTote()
		{
			AbortTotes               = true;
			ScannerLayout.Visibility = ViewStates.Visible;
			Scanner.Abort();
			PodLayout.Visibility     = ViewStates.Visible;
			DetailsLayout.Visibility = ViewStates.Visible;
			ToteLayout.Visibility    = ViewStates.Gone;
			AcceptEventFunc( null, null );
		}


		private void StartScanTotes()
		{
			ToteAdapter.Clear();
			Scanner.Abort();

			DetailsLayout.Visibility = ViewStates.Gone;
			ScannerLayout.Visibility = ViewStates.Gone;
			ToteLayout.Visibility    = ViewStates.Visible;
			ToteLayout.BringToFront();

			ScanTotes();
		}

		private void EndScanTotes()
		{
			DetailsLayout.Visibility = ViewStates.Visible;
			ScannerLayout.Visibility = ViewStates.Visible;
			ToteLayout.Visibility    = ViewStates.Gone;
			ToteLayout.BringToFront();
		}


		private void ScanTotes()
		{
			AbortTotes = false;

			void ShowError( string err )
			{
				RunOnUiThread( () => { Globals.Dialogues.Error( err ); } );
			}

			RunOnUiThread( async () =>
			               {
				               while( !AbortTotes )
				               {
					               ToteScanner.Placeholder = "Tote Barcode";

					               var Timer = new Timer( 200 ) { AutoReset = false };

					               Timer.Elapsed += ( _, _ ) =>
					                                {
						                                RunOnUiThread( () => { ToteScanner.Focus(); } );
						                                Timer.Dispose();
						                                Timer = null;
					                                };
					               Timer.Start();

					               var Code = ( await ToteScanner.Scan() ).Trim();

					               ToteScanner.Abort();

					               if( Code.IsNotNullOrWhiteSpace() )
					               {
						               switch( Code[ 0 ] )
						               {
						               case 'T':
						               case 'L':
							               if( ( from T in ToteAdapter.StringList
							                     where T == Code
							                     select T ).Any() )
							               {
								               ShowError( "Already scanned." );
								               continue;
							               }

							               RunOnUiThread( () =>
							                              {
								                              ToteAdapter.Add( Code );
								                              ToteOkButton.Enabled = true;
							                              } );
							               continue;
						               }
						               ShowError( "Not a valid tote." );
					               }
				               }
			               } );
		}
	#endregion
	#endregion

	#region Trip Update
		protected override bool IgnoreTrip( ScannedTrip trip ) => IsAssetServiceLevel( trip );

		protected override void BeforeUpdateTrips()
		{
			base.BeforeUpdateTrips();
			var CTTTripId = "";

			if( Adapter!.Count > 0 )
			{
				foreach( var Trip in Adapter.Trips )
				{
					if( IsAsset( Trip ) )
					{
						Trip.Status = Trip.Status is STATUS.PICKED_UP ? STATUS.VERIFIED : STATUS.PICKED_UP;
						Globals.Database.UpdateTrip( Trip );
					}
					else if( IsCTT( Trip ) )
						CTTTripId = Trip.TripId;
				}

				var Totes = ToteAdapter.StringList;

				if( ( Totes.Count > 0 ) && CTTTripId.IsNotNullOrWhiteSpace() )
				{
					var AccountId = Adapter!.Trips[ 0 ].AccountId;
					Globals.Database.RbhUpdateAssets( AccountId, CTTTripId, Totes );
				}
			}
		}
	#endregion

	#region Filter Trips
		protected override List<Trip> OnFilterTrips( List<Trip> trips ) => ( from T in trips
		                                                                     where T.Status is STATUS.PICKED_UP
		                                                                           || ( T.Status is STATUS.DISPATCHED && IsAssetServiceLevel( T ) )
		                                                                           || IsReturnOrCWD( T )
		                                                                     select T ).ToList();


		protected override List<Trip> OnFilterPickupDeliveryTrips( List<Trip> trips )
		{
			trips = ( from T in trips
			          where !( ( State == END_OF_PRECESSING_STATE.ASSET ) ^ IsAsset( T ) )
			          select T ).ToList();

			TotalPieces = 0;

			foreach( var Trip1 in trips )
				TotalPieces += Trip1.Pieces;

			return trips;
		}

		// By Pieces
		protected override List<Trip> FilterLocationTrips( List<Trip> trips )
		{
			if( JustScannedLocationCode )
			{
				JustScannedLocationCode = false;
				DoingReturns            = false;
				GettingReturnCode       = false;

				foreach( var Trip in trips )
				{
					if( Trip is ScannedTrip T && IsReturn( Trip ) )
					{
						T.IsReturn   = true;
						DoingReturns = true;
					}
				}

				RunOnUiThread( () => { CancelBtn.Visibility = ViewStates.Visible; } );

				if( DoingReturns )
				{
					RunOnUiThread( () =>
					               {
						               AcceptButton.Visibility = ViewStates.Gone;
						               ShowOnlyReturns( trips );
					               } );
				}
			}

			return trips;
		}
	#endregion

	#region Trip Type Tests
		protected override bool IsTripPickup( ScannedTrip t ) => IsReturn( t );

		private static bool IsAssetServiceLevel( ITrip t ) => string.Compare( t.ServiceLevel.Trim(), ASSET, StringComparison.OrdinalIgnoreCase ) == 0;

		private static bool IsAsset( ITrip t ) => t.Status is STATUS.DISPATCHED or STATUS.PICKED_UP && IsAssetServiceLevel( t );

		public static bool IsReturnTrip( Trip t ) => t.Status is STATUS.DISPATCHED or STATUS.PICKED_UP && ( t.ServiceLevel.ToUpper() == RETURN );

		public static bool IsCWD( string txt ) => txt.ToUpper() == CWD_SHIPMENT;

		public static bool IsCWD( Trip t ) => IsCWD( t.ServiceLevel );

		public static bool IsCWDTrip( Trip t ) => t.Status is STATUS.DISPATCHED or STATUS.PICKED_UP && IsCWD( t );

		public static bool IsReturnOrCWD( Trip t ) => IsReturnTrip( t ) || IsCWDTrip( t );

		public static bool IsReturnAndNotCWD( Trip t ) => IsReturnTrip( t ) && !IsCWDTrip( t );

		protected virtual bool IsReturn( Trip t ) => IsReturnTrip( t );
	#endregion

	#region Creation
		protected override BaseTripAdapter GetClaimsPiecesAdapter
		{
			get
			{
				return Adapter = new MetroClaimsPiecesAdapter( Activity, ListView )
				                 {
					                 OnRowLongClick = OnRowClick
				                 };
			}
		}

		protected override string Title => State switch
		                                   {
			                                   END_OF_PRECESSING_STATE.ASSET    => "Pick-up Tote (Asset)",
			                                   END_OF_PRECESSING_STATE.BARCODES => "Asset Drop-off",
			                                   _                                => "DSD Deliveries"
		                                   };

		protected override PickupDeliveriesAdapter GetPickupDeliveriesAdapter => new MetroTripListAdapter( Activity, ListView, new List<Trip>(), OnTripClick );


		public MetroDsdDeliveries() : base( Resource.Layout.Metro_Fragment_PickupDelivery )
		{
		}

		[Obsolete]
		protected override void OnCreateDynamicFragments()
		{
			base.OnCreateDynamicFragments();
			ToteScanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.toteScannerLayout, ToteScanner );
		}

		[Obsolete]
		protected override void OnCreateFragment()
		{
			base.OnCreateFragment();
			var WaitingLayout = FindViewById<RelativeLayout>( Resource.Id.waitingTripsLayout )!;
			WaitingLayout.Visibility = ViewStates.Visible;

			UpdateTripButton.Visibility = ViewStates.Gone;

			ToteLayout            = FindViewById<LinearLayout>( Resource.Id.toteLayout )!;
			ToteLayout.Visibility = ViewStates.Gone;

			ToteButton            = FindViewById<Button>( Resource.Id.toteBarcodeButton )!;
			ToteButton.Visibility = ViewStates.Gone;

			ToteButton.Click += ( _, _ ) => { StartScanTotes(); };

			ToteOkButton       =  FindViewById<Button>( Resource.Id.toteOkButton )!;
			ToteOkButton.Click += ( _, _ ) => { CloseTote(); };

			ToteCancelButton = FindViewById<Button>( Resource.Id.toteCancelButton )!;

			ToteCancelButton.Click += ( _, _ ) =>
			                          {
				                          ToteAdapter.Clear();
				                          CloseTote();
			                          };

			ToteListView         = FindViewById<ListView>( Resource.Id.toteListView )!;
			ToteAdapter          = new StringAdapter( Activity );
			ToteListView.Adapter = ToteAdapter;
		}

		protected override Task<bool> OnShowDetail( ITrip trip, bool isExamine, bool finalising, bool showing )
		{
			UpdateTripButton.Visibility = finalising ? ViewStates.Visible : ViewStates.Gone;
/*
			ToteButton.Visibility = finalising ? ViewStates.Visible : ViewStates.Gone;
			ToteButton.Enabled    = !HasMissingPieces();
*/
			return base.OnShowDetail( trip, isExamine, finalising, showing );
		}
	#endregion
	}
}