﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Content.PM;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Fragments.Standard;
using Droid2.Signature;
using Utils;
using SignatureResult = Droid2.Signature.SignatureResult;

namespace Droid2.Fragments._Customers.Metro
{
	public class MetroLoad : AFragment
	{
		public const string PRO_NUMBER = "Pro Number",
		                    SCAN_BINS = "Scan Bins",
		                    SCAN_BIN = "Scan Bin";

		private StringAdapter Adapter { get; set; }

		private ListView ListView { get; set; }

		protected string Title
		{
			set
			{
				RunOnUiThread( () =>
				               {
					               _Title.Text = value;
				               } );
			}
		}

		protected virtual string HeaderText => PRO_NUMBER;

		private BarcodeInputFragment Scanner;

		private SignatureFragment Signature;
		private SignatureResult SignaturePoints;
		private string CustomerSignature;

		private TextView ProNumber,
		                 Header,
		                 _Title;

		private Button CancelButton,
		               NextButton,
		               BackButton,
		               SignButton,
		               FinialiseButton;

		private LinearLayout ScanLayout,
		                     PodAndSignLayout,
		                     SignatureLayout,
		                     MainLayout;

		private EditText PopName;

		private bool Cancelling,
		             Back;

		private void EnableFinalise()
		{
			RunOnUiThread( () =>
			               {
				               FinialiseButton.Enabled = CustomerSignature.IsNotNullOrWhiteSpace() && PopName.Text.Trim().IsNotNullOrWhiteSpace();
			               } );
		}

		protected virtual bool VerifyBin( string bin )
		{
			return MetroBuild.IsPIMPOrPDOM( bin );
		}

		protected override async void OnExecute( object arg = null )
		{
			void Error( string txt )
			{
				if( Executing )
				{
					Task.Run( () =>
					          {
						          Globals.Dialogues.Error( txt );
					          } );
				}
			}

			try
			{
				while( Executing )
				{
					RunOnUiThread( () =>
					               {
						               Adapter.Clear();
						               Header.Text          = HeaderText;
						               CancelButton.Enabled = false;
						               NextButton.Enabled   = false;
						               ProNumber.Text       = "";
					               } );

					Scanner.Placeholder = HeaderText;
					var Pro = await Scanner.Scan();

					if( Pro.IsNotNullOrWhiteSpace() )
					{
						RunOnUiThread( () =>
						               {
							               // ReSharper disable once AccessToModifiedClosure
							               ProNumber.Text       = Pro;
							               Header.Text          = SCAN_BINS;
							               CancelButton.Enabled = true;
						               } );

						while( Executing )
						{
							ReScan:

							Scanner.Placeholder = SCAN_BIN;
							var Bin = await Scanner.Scan();

							if( !Cancelling && !Back )
							{
								if( VerifyBin( Bin ) )
								{
									foreach( var Item in Adapter.StringList )
									{
										if( Item == Bin )
										{
											Error( "Already Scanned" );

											goto ReScan;
										}
									}

									RunOnUiThread( () =>
									               {
										               Adapter.StringList.Add( Bin );
										               NextButton.Enabled = true;
									               } );
								}
								else
									Error( "Invalid Bin Number" );
							}
							else if( Back )
								Back = false;
							else
							{
								Cancelling = false;

								break;
							}
						}
					}
				}
			}
			finally
			{
				Scanner.Abort();
			}
		}

		protected virtual void OnUpdate( string proBill, string pop, int sigHeight, int sigWidth, string signature, List<string> references )
		{
			Globals.Database.RbhUpdateLoadingTrailer( proBill,
			                                          pop,
			                                          sigHeight,
			                                          sigWidth,
			                                          signature,
			                                          references );
		}

		protected override void OnCreateFragment()
		{
			MainLayout = FindViewById<LinearLayout>( Resource.Id.mainLayout );

			ScanLayout            = FindViewById<LinearLayout>( Resource.Id.scanLayout );
			ScanLayout.Visibility = ViewStates.Visible;

			PodAndSignLayout            = FindViewById<LinearLayout>( Resource.Id.podAndSign );
			PodAndSignLayout.Visibility = ViewStates.Gone;

			_Title = FindViewById<TextView>( Resource.Id.title );

			ProNumber      = FindViewById<TextView>( Resource.Id.proNumber );
			ProNumber.Text = "";

			CancelButton         = FindViewById<Button>( Resource.Id.cancelButton );
			CancelButton.Enabled = false;

			CancelButton.Click += ( sender, args ) =>
			                      {
				                      Cancelling = true;
				                      Scanner.Abort();
			                      };

			NextButton         = FindViewById<Button>( Resource.Id.nextButton );
			NextButton.Enabled = false;

			NextButton.Click += ( sender, args ) =>
			                    {
				                    PodAndSignLayout.Visibility = ViewStates.Visible;
				                    ScanLayout.Visibility       = ViewStates.Gone;
			                    };

			Header = FindViewById<TextView>( Resource.Id.header );

			Adapter          = new StringAdapter( Activity );
			ListView         = FindViewById<ListView>( Resource.Id.returnListView );
			ListView.Adapter = Adapter;

			BackButton = FindViewById<Button>( Resource.Id.backButton );

			BackButton.Click += ( sender, args ) =>
			                    {
				                    CustomerSignature = "";
				                    PopName.Text      = "";
				                    EnableFinalise();

				                    PodAndSignLayout.Visibility = ViewStates.Gone;
				                    ScanLayout.Visibility       = ViewStates.Visible;
				                    Back                        = true;
				                    Scanner.Abort();
			                    };

			PopName = FindViewById<EditText>( Resource.Id.popName );

			PopName.TextChanged += ( sender, args ) =>
			                       {
				                       EnableFinalise();
			                       };

			SignatureLayout            = FindViewById<LinearLayout>( Resource.Id.signatureLayout );
			SignatureLayout.Visibility = ViewStates.Gone;

			SignButton = FindViewById<Button>( Resource.Id.signButton );

			SignButton.Click += async ( sender, args ) =>
			                    {
				                    Globals.Keyboard.Hide = true;
				                    Orientation = ScreenOrientation.Landscape;
				                    MainLayout.Visibility      = ViewStates.Gone;
				                    SignatureLayout.Visibility = ViewStates.Visible;
				                    SignatureLayout.BringToFront();
				                    SignaturePoints   = await Signature.CaptureSignature();
				                    CustomerSignature = !string.IsNullOrWhiteSpace( SignaturePoints.Points ) ? $"{SignaturePoints.Height}^{SignaturePoints.Width}^{SignaturePoints.Points}" : "";
				                    EnableFinalise();
				                    SignatureLayout.Visibility = ViewStates.Gone;
				                    MainLayout.Visibility      = ViewStates.Visible;
				                    MainLayout.BringToFront();
				                    Orientation = ScreenOrientation.Portrait;
			                    };

			FinialiseButton = FindViewById<Button>( Resource.Id.finaliseButton );

			FinialiseButton.Click += ( sender, args ) =>
			                         {
				                         OnUpdate( ProNumber.Text,
				                                   PopName.Text,
				                                   SignaturePoints.Height,
				                                   SignaturePoints.Width,
				                                   CustomerSignature,
				                                   Adapter.StringList );

				                         CustomerSignature = "";
				                         PopName.Text      = "";

				                         PodAndSignLayout.Visibility = ViewStates.Gone;
				                         ScanLayout.Visibility       = ViewStates.Visible;

				                         Cancelling = true;
				                         Scanner.Abort();
			                         };

			FinialiseButton.Enabled = false;
		}

		protected override void OnDestroyFragment()
		{
		}

        [System.Obsolete]
        protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLayout, Scanner );

			Signature = new SignatureFragment();
			AddDynamicFragment( Resource.Id.signatureLayout, Signature );
		}


		public MetroLoad() : base( Resource.Layout.Metro_Fragment_Load )
		{
		}
	}
}