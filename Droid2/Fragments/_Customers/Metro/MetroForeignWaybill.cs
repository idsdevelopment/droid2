﻿using System;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Fragments.Standard;
using Utils;

namespace Droid2.Fragments._Customers.Metro
{
	public class MetroForeignWaybill : AFragment
	{
		private const string SCAN_BINS = "Scan Bins",
		                     SCAN_FWB = "Scan FWB";

		private bool InDelete
		{
			get => _InDelete;
			set
			{
				_InDelete = value;

				RunOnUiThread( () =>
				               {
					               var Enab = !value;

					               NextButton.Enabled   = Enab;
					               DeleteButton.Enabled = Enab;
				               } );
			}
		}

		private TextView Header;

		private RelativeLayout ScanLayout;
		private BarcodeInputFragment Scanner;
		private ListView FwbListView;

		private StringAdapter Adapter,
		                      TwoBinsAdapter;

		private Button DeleteButton,
		               NextButton,
		               CancelButton;

		private bool _InDelete,
		             InCancel,
		             InScanBins;

		protected override async void OnExecute( object arg = null )
		{
			try
			{
				while( Executing )
				{
					RunOnUiThread( () =>
					               {
						               Header.Text          = SCAN_FWB;
						               NextButton.Enabled   = false;
						               NextButton.Text      = SCAN_BINS;
						               DeleteButton.Enabled = false;
						               TwoBinsAdapter.Clear();
						               Adapter.Clear();
						               FwbListView.Adapter       = Adapter;
						               Scanner.ForceManualAccept = false;
					               } );

					InCancel   = false;
					InScanBins = false;

					while( Executing && !InCancel )
					{
						ReScan:

						Scanner.Placeholder = InDelete ? "Delete" : InScanBins ? "Bin" : "FWB";

						var Waybill = await Scanner.Scan();

						if( Waybill.IsNotNullOrWhiteSpace() )
						{
							StringAdapter Adp;

							if( InScanBins )
							{
								if( !MetroBuild.IsPIMPOrPDOM( Waybill ) )
								{
									Error( "Not A Valid Bin" );

									goto ReScan;
								}

								Adp = TwoBinsAdapter;
							}
							else
							{
								var P = Waybill.LastIndexOf( "RET", StringComparison.OrdinalIgnoreCase );

								if( P < 0 )
								{
									Error( "Not A Valid Waybill" );

									goto ReScan;
								}

								Waybill = Waybill[ P.. ];
								Adp     = Adapter;
							}

							if( InDelete )
							{
								InDelete = false;

								if( !Adp.StringList.Remove( Waybill ) )
								{
									Error( "Item Not Scanned" );

									goto ReScan;
								}
							}
							else
							{
								foreach( var Item in Adp.StringList )
								{
									if( Item == Waybill )
									{
										Error( "Already Scanned" );

										goto ReScan;
									}
								}

								Adp.Add( Waybill );
							}

							RunOnUiThread( () =>
							               {
								               NextButton.Enabled        = !InScanBins || ( TwoBinsAdapter.Count == 1 );
								               DeleteButton.Enabled      = true;
								               Scanner.ForceManualAccept = InScanBins;
							               } );
						}
					}
				}
			}
			finally
			{
				Scanner.Abort();
			}
		}

		protected override void OnCreateFragment()
		{
			Header = FindViewById<TextView>( Resource.Id.header );

			ScanLayout            = FindViewById<RelativeLayout>( Resource.Id.scannerLayout );
			ScanLayout.Visibility = ViewStates.Visible;

			FwbListView         = FindViewById<ListView>( Resource.Id.fwbListView );
			TwoBinsAdapter      = new StringAdapter( Activity );
			Adapter             = new StringAdapter( Activity );
			FwbListView.Adapter = Adapter;

			DeleteButton = FindViewById<Button>( Resource.Id.deleteButton );

			DeleteButton.Click += ( sender, args ) =>
			                      {
				                      InDelete = true;
				                      Scanner.Abort();
			                      };

			void DoCancel()
			{
				DeleteButton.Enabled = true;
				NextButton.Enabled   = false;

				InCancel = true;
				Scanner.Abort();
			}

			NextButton = FindViewById<Button>( Resource.Id.nextButton );

			NextButton.Click += ( sender, args ) =>
			                    {
				                    if( InScanBins ) // Finalise
				                    {
					                    Globals.Database.RbhUpdateForeignWaybill( Adapter.StringList, TwoBinsAdapter.StringList );
					                    DoCancel();
				                    }
				                    else
				                    {
					                    Header.Text     = SCAN_BINS;
					                    NextButton.Text = "Finalise";

					                    FwbListView.Adapter = TwoBinsAdapter;
					                    InScanBins          = true;
					                    Scanner.Abort();
					                    Scanner.ForceManualAccept = true;
				                    }
			                    };

			CancelButton = FindViewById<Button>( Resource.Id.cancelButton );

			CancelButton.Click += ( sender, args ) =>
			                      {
				                      DoCancel();
			                      };
		}

		protected override void OnDestroyFragment()
		{
		}

        [Obsolete]
        protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLayout, Scanner );
		}

		public MetroForeignWaybill() : base( Resource.Layout.Metro_Fragment_ForeignWaybill )
		{
		}
	}
}