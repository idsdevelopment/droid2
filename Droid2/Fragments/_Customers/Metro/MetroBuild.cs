﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Adapters.Customers.Metro;
using Droid2.Barcode;
using Droid2.Fragments.Standard;
using IdsService;
using Service.Abstracts;

// ReSharper disable InconsistentNaming

namespace Droid2.Fragments._Customers.Metro
{
	public class MetroBuild : AFragment
	{
		private const string PDOM = "PDOM",
		                     PIMP = "PIMP",
		                     RDOM = "RDOM",
		                     RIMP = "RIMP";

		public const string SCAN_BIN      = "Scan Bin",
		                    SCAN_RETURN   = "Scan Return",
		                    DELETE_RETURN = "Delete Return";

		protected string Title
		{
			set { RunOnUiThread( () => { _Title.Text = value; } ); }
		}

		private StringAdapter           Adapter        = null!;
		private MetroMissingListAdapter MissingAdapter = null!;

		private ListView ListView = null!;

		private BarcodeInputFragment Scanner = null!;

		private TextView BinNumber = null!,
		                 Header    = null!,
		                 Missing   = null!,
		                 _Title    = null!;

		private Button CancelButton   = null!,
		               CheckButton    = null!,
		               AddButton      = null!,
		               DeleteButton   = null!,
		               FinaliseButton = null!;

		private LinearLayout FinaliseLayout = null!,
		                     CheckLayout    = null!;

		private bool Cancelling,
		             DoingDelete;

		private string Bin = "";


		public static bool IsPDOM( string txt ) => txt.StartsWith( PDOM, StringComparison.OrdinalIgnoreCase );

		public static bool IsPIMP( string txt ) => txt.StartsWith( PIMP, StringComparison.OrdinalIgnoreCase );

		public static bool IsPIMPOrPDOM( string txt ) => IsPIMP( txt ) || IsPDOM( txt );


		protected override async void OnExecute( object? arg = null )
		{
			void Err( string txt )
			{
				if( Executing )
					Task.Run( () => { Globals.Dialogues.Error( txt ); } );
			}

			try
			{
				while( Executing )
				{
					RunOnUiThread( () =>
					               {
						               Adapter.Clear();
						               BinNumber.Text       = "";
						               Header.Text          = SCAN_BIN;
						               DeleteButton.Enabled = false;
					               } );

					Scanner.Placeholder = SCAN_BIN;
					Bin                 = await Scanner.Scan();

					if( IsPIMPOrPDOM( Bin ) )
					{
						RunOnUiThread( () =>
						               {
							               BinNumber.Text       = Bin;
							               CancelButton.Enabled = true;
						               } );

						while( Executing )
						{
							ReScan:

							RunOnUiThread( () => { Header.Text = DoingDelete ? DELETE_RETURN : SCAN_RETURN; } );

							Scanner.Placeholder = DoingDelete ? DELETE_RETURN : SCAN_RETURN;

							var ProcessDelete = DoingDelete;
							DoingDelete = false;

							var Return = await Scanner.Scan();

							if( !DoingDelete )
							{
								if( !Cancelling )
								{
									if( ProcessDelete )
									{
										if( Adapter.StringList.Remove( Return ) )
											continue;

										Err( "Item Not Found" );
										DoingDelete = true;

										continue;
									}

									if( IsRIMPOrRDOM( Return ) )
									{
										if( IsPDOMAndRDOM( Bin, Return ) || IsPIMPAndRIMP( Bin, Return ) )
										{
											foreach( var Item in Adapter.StringList )
											{
												if( Item == Return )
												{
													Err( "Already Scanned" );

													goto ReScan;
												}
											}

											RunOnUiThread( () =>
											               {
												               CheckButton.Enabled  = true;
												               CancelButton.Enabled = true;
												               DeleteButton.Enabled = true;
												               Adapter.Add( Return );
											               } );
										}
										else
											Err( "Package for the Wrong Bin" );
									}
									else
										Err( "Not A Valid Return" );
								}
								else
								{
									Cancelling = false;

									break;
								}
							}
						}
					}
					else
						Err( "Not A Valid Bin" );
				}
			}
			finally
			{
				Scanner.Abort();
			}
		}

		protected virtual void OnUpdate( string bin, List<string> references )
		{
			Globals.Database.RbhUpdateReturnsWithBin( bin, references );
		}

		protected override void OnCreateFragment()
		{
			CancelButton         = FindViewById<Button>( Resource.Id.cancelButton )!;
			CancelButton.Enabled = false;

			CheckLayout = FindViewById<LinearLayout>( Resource.Id.checkLayout )!;

			void Cancel()
			{
				FinaliseLayout.Visibility = ViewStates.Gone;
				CheckLayout.Visibility    = ViewStates.Visible;
				Cancelling                = true;
				Scanner.Abort();
			}

			CancelButton.Click += ( _, _ ) => { Cancel(); };

			FinaliseLayout            = FindViewById<LinearLayout>( Resource.Id.finaliseLayout )!;
			FinaliseLayout.Visibility = ViewStates.Gone;

			List<string> GetReferences()
			{
				return ( from R in Adapter.StringList
				         select R ).ToList();
			}

			CheckButton            = FindViewById<Button>( Resource.Id.checkButton )!;
			CheckButton.Enabled    = false;
			CheckButton.Visibility = ViewStates.Visible;

			List<MissingReference> FilterReferences( IEnumerable<AIService<Client, AuthToken>.Missing> references )
			{
				var IsPimp = IsPIMP( Bin );

				return ( from M in references
				         let R = M.Reference
				         where IsPimp ? IsRIMP( R ) : IsRDOM( R )
				         select new MissingReference
				                {
					                TripId      = M.TripId,
					                CompanyName = M.CompanyName,
					                Barcode     = M.Reference,
					                MercuryId   = M.MercuryTripId
				                } ).ToList();
			}

			CheckButton.Click += async ( _, _ ) =>
			                     {
				                     CheckButton.Enabled = false;

				                     try
				                     {
					                     var Refs = GetReferences();

					                     var Missings = await Globals.Database.RbhFindMissingShipments( Refs );

					                     var MissingReferences = FilterReferences( Missings );

					                     FinaliseLayout.Visibility = ViewStates.Visible;
					                     CheckLayout.Visibility    = ViewStates.Gone;

					                     var Ok = MissingReferences.Count == 0;
					                     FinaliseButton.Enabled = Ok;
					                     AddButton.Enabled      = !Ok;
					                     DeleteButton.Enabled   = !Ok;

					                     if( !Ok )
					                     {
						                     Missing.Visibility = ViewStates.Visible;
						                     Missing.Text       = "Missing";
						                     MissingAdapter.Clear();
						                     MissingAdapter.AddRange( MissingReferences );
						                     ListView.Adapter = MissingAdapter;
					                     }
				                     }
				                     finally
				                     {
					                     CheckButton.Enabled = true;
				                     }
			                     };

			AddButton = FindViewById<Button>( Resource.Id.addButton )!;

			AddButton.Click += ( _, _ ) =>
			                   {
				                   Missing.Visibility        = ViewStates.Invisible;
				                   FinaliseLayout.Visibility = ViewStates.Gone;
				                   CheckLayout.Visibility    = ViewStates.Visible;
				                   ListView.Adapter          = Adapter;
			                   };

			DeleteButton = FindViewById<Button>( Resource.Id.deleteButton )!;

			DeleteButton.Click += ( _, _ ) =>
			                      {
				                      DoingDelete = true;
				                      Scanner.Abort();
			                      };

			FinaliseButton = FindViewById<Button>( Resource.Id.finaliseButton )!;

			FinaliseButton.Click += ( _, _ ) =>
			                        {
				                        FinaliseButton.Enabled = false;

				                        try
				                        {
					                        OnUpdate( Bin, GetReferences() );
				                        }
				                        finally
				                        {
					                        FinaliseButton.Enabled = true;
					                        Cancel();
				                        }
			                        };
			_Title = FindViewById<TextView>( Resource.Id.title )!;

			BinNumber      = FindViewById<TextView>( Resource.Id.binNumber )!;
			BinNumber.Text = "";

			Missing            = FindViewById<TextView>( Resource.Id.missing )!;
			Missing.Visibility = ViewStates.Invisible;

			Header = FindViewById<TextView>( Resource.Id.header )!;

			MissingAdapter = new MetroMissingListAdapter( Activity );
			Adapter        = new StringAdapter( Activity );

			ListView         = FindViewById<ListView>( Resource.Id.returnListView )!;
			ListView.Adapter = Adapter;
		}

		protected override void OnDestroyFragment()
		{
		}

		[Obsolete]
		protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLayout, Scanner );
		}

		public MetroBuild() : base( Resource.Layout.Metro_Fragment_Build )
		{
		}

		private static bool IsRDOM( string txt ) => txt.StartsWith( RDOM, StringComparison.OrdinalIgnoreCase );

		private static bool IsRIMP( string txt ) => txt.StartsWith( RIMP, StringComparison.OrdinalIgnoreCase );

		private static bool IsRIMPOrRDOM( string txt ) => IsRIMP( txt ) || IsRDOM( txt );


		private static bool IsPIMPAndRIMP( string pimp, string rimp ) => IsPIMP( pimp ) && IsRIMP( rimp );

		private static bool IsPDOMAndRDOM( string pdom, string rdom ) => IsPDOM( pdom ) && IsRDOM( rdom );
	}
}