﻿using System;
using System.Collections.Generic;
using Droid2.Fragments.Standard.Claims.StandardFragment;

namespace Droid2.Fragments._Customers.Metro
{
	public class MetroRcClaim : ClaimsStandard
	{
		protected override string Title => "Rc Claims";

		public override void AddClaim( string authToken, IList<string> tripIds, string pod, string signature, int signatureHeight, int signatureWidth, DateTime pickupTime )
		{
			Globals.Database.AddRcClaim( authToken, tripIds, pod, signature, signatureHeight, signatureWidth, pickupTime );
		}
	}
}