using Android.App;
using Android.OS;
using Android.Widget;
using Droid2.Adapters;

namespace Droid2.Fragments.Customers.Metro
{
    [ Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
        Label = "ClaimsPiecesActivity",
        AlwaysRetainTaskState = true ) ]
    public class MetroDriverVerificationOptionsActivity : Activity
    {
        public enum SCAN_ACTION
        {
            CANCEL,
            UNDELIVERABLE,
            RESET_SCAN,
            REMOVE,
            BUSY // For Thread
        }

        public static volatile SCAN_ACTION ScanAction = SCAN_ACTION.CANCEL;
        public static ScannedTrip SelectedTrip;
        public static string ZoneFilter;


        protected override void OnCreate( Bundle savedInstanceState )
        {
            base.OnCreate( savedInstanceState );

            if( SelectedTrip == null )
                Finish();
            else
            {
                var Trip = SelectedTrip;

                SetContentView( Resource.Layout.Metro_Activity_DsdDriVerificationOptions );
                FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
                {
                    ScanAction = SCAN_ACTION.CANCEL;
                    Finish();
                };

                RunOnUiThread( () => { FindViewById<TextView>( Resource.Id.tripId ).Text = Trip.TripId; } );

                FindViewById<Button>( Resource.Id.undeliverableButton ).Click += ( sender, args ) =>
                {
                    ScanAction = SCAN_ACTION.UNDELIVERABLE;
                    Finish();
                };

                FindViewById<Button>( Resource.Id.resetScanButton ).Click += ( sender, args ) =>
                {
                    ScanAction = SCAN_ACTION.RESET_SCAN;
                    Finish();
                };

                FindViewById<Button>( Resource.Id.removeButton ).Click += ( sender, args ) =>
                {
                    ScanAction = SCAN_ACTION.REMOVE;
                    Finish();
                };
            }
        }
    }
}