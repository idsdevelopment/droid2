﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Fragments.Standard;
using Utils;

namespace Droid2.Fragments._Customers.Metro
{
	public class MetroCreateEdit : MetroBuild
	{
		protected override void OnCreateFragment()
		{
			base.OnCreateFragment();
			Title = "Create / Edit";
		}

		protected override void OnUpdate( string bin, List<string> references )
		{
			Globals.Database.RbhEditCreateReturnsWithBin( bin, references );
		}
	}
}