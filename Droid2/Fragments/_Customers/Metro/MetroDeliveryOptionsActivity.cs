using Android.App;
using Android.OS;
using Android.Widget;
using Droid2.Adapters.Customers.Metro;

namespace Droid2.Fragments.Customers.Metro
{
	[ Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
		Label = "ClaimsPiecesActivity",
		AlwaysRetainTaskState = true ) ]
	public class MetroOptionsActivity : Activity
	{
		public enum SCAN_ACTION
		{
			CANCEL,
			RESET_SCAN,
			BROKEN_SEAL,
			BUSY // For Thread
		}

		public static volatile SCAN_ACTION ScanAction = SCAN_ACTION.CANCEL;
		public static MetroScannedTrip SelectedTrip;
		public static string ZoneFilter;

		protected override void OnCreate( Bundle savedInstanceState )
		{
			base.OnCreate( savedInstanceState );

			if( SelectedTrip == null )
				Finish();
			else
			{
				var Trip = SelectedTrip;

				SetContentView( Resource.Layout.Metro_Activity_DeliveryOptions );
				FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
				{
					ScanAction = SCAN_ACTION.CANCEL;
					Finish();
				};

				RunOnUiThread( () => { FindViewById<TextView>( Resource.Id.tripId ).Text = Trip.TripId; } );


				FindViewById<Button>( Resource.Id.resetScanButton ).Click += ( sender, args ) =>
				{
					ScanAction = SCAN_ACTION.RESET_SCAN;
					Finish();
				};

				FindViewById<Button>( Resource.Id.brokenSealButton ).Click += ( sender, args ) =>
				{
					ScanAction = SCAN_ACTION.BROKEN_SEAL;
					Finish();
				};
			}
		}
	}
}