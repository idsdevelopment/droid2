﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Fragments.Standard;
using Utils;

namespace Droid2.Fragments._Customers.Metro
{
	public class MetroUnload : AFragment
	{
		private StringAdapter Adapter;

		private ListView ListView;

		private BarcodeInputFragment Scanner;

		private TextView ProNumber,
		                 Header;

		private Button FinaliseButton,
		               CancelButton;

		private bool Cancelling;

		private List<string> BinNumbers;

		protected override async void OnExecute( object arg = null )
		{
			void Error( string txt )
			{
				if( Executing )
				{
					Task.Run( () =>
					          {
						          Globals.Dialogues.Error( txt );
					          } );
				}
			}

			try
			{
				while( Executing )
				{
					BinNumbers = new List<string>();
					Cancelling = false;

					RunOnUiThread( () =>
					               {
						               Header.Text         = MetroLoad.PRO_NUMBER;
						               Scanner.Placeholder = MetroLoad.PRO_NUMBER;
						               ProNumber.Text      = "";

						               FinaliseButton.Enabled = false;
						               Adapter.StringList.Clear();
					               } );

					var Pro = await Scanner.Scan();

					if( Pro.IsNotNullOrWhiteSpace() )
					{
						RunOnUiThread( () =>
						               {
							               ProNumber.Text = Pro;
						               } );

						BinNumbers = await Globals.Database.RbhUnloadTrailerFindBins( Pro );

						if( BinNumbers is null || ( BinNumbers.Count == 0 ) )
						{
							Error( "Probill Number Not Found." );

							RunOnUiThread( () =>
							               {
								               ProNumber.Text = "";
							               } );

							continue;
						}

						RunOnUiThread( () =>
						               {
							               Adapter.StringList.AddRange( BinNumbers );
							               CancelButton.Enabled = true;
						               } );

						while( Executing && !Cancelling )
						{
							RunOnUiThread( () =>
							               {
								               Header.Text         = MetroLoad.SCAN_BINS;
								               Scanner.Placeholder = MetroLoad.SCAN_BIN;
							               } );

							var Bin = await Scanner.Scan();

							if( Bin.IsNullOrWhiteSpace() )
								continue;

							if( MetroBuild.IsPIMPOrPDOM( Bin ) )
							{
								if( Adapter.StringList.Remove( Bin ) )
								{
									RunOnUiThread( () =>
									               {
										               FinaliseButton.Enabled = Adapter.StringList.Count == 0;
									               } );

									continue;
								}
							}

							Error( "Invalid Bin Number" );
						}
					}
				}
			}
			finally
			{
				Scanner.Abort();
			}
		}

		protected override void OnCreateFragment()
		{
			ListView         = FindViewById<ListView>( Resource.Id.returnListView );
			Adapter          = new StringAdapter( Activity );
			ListView.Adapter = Adapter;

			Header         = FindViewById<TextView>( Resource.Id.header );
			ProNumber      = FindViewById<TextView>( Resource.Id.proNumber );
			ProNumber.Text = "";

			FinaliseButton = FindViewById<Button>( Resource.Id.finaliseButton );

			FinaliseButton.Click += ( sender, args ) =>
			                        {
				                        Globals.Database.RbhUpdateRbhUnLoadTrailer( ProNumber.Text, BinNumbers );
				                        Cancelling = true;
				                        Scanner.Abort();
			                        };

			FinaliseButton.Enabled = false;

			CancelButton = FindViewById<Button>( Resource.Id.cancelButton );

			CancelButton.Click += ( sender, args ) =>
			                      {
				                      CancelButton.Enabled = false;
				                      Cancelling           = true;
				                      Scanner.Abort();
			                      };

			CancelButton.Enabled = false;
		}

		protected override void OnDestroyFragment()
		{
		}

        [System.Obsolete]
        protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLayout, Scanner );
		}


		public MetroUnload() : base( Resource.Layout.Metro_Fragment_UnLoad )
		{
		}
	}
}