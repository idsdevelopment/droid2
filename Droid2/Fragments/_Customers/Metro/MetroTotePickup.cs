﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Content.PM;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Fragments.Customers.Metro;
using Droid2.Fragments.Standard;
using Droid2.Fragments.Standard.PickupDeliveries;
using Droid2.Signature;
using Java.Lang;
using Service.Interfaces;
using Utils;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

#pragma warning disable 612

namespace Droid2.Fragments._Customers.Metro
{
	internal class MetroTotePickup : AFragment
	{
		private LinearLayout ToteScanLayout  = null!,
		                     DetailsLayout   = null!,
		                     LocationLayout  = null!,
		                     SignatureLayout = null!;

		private BarcodeInputFragment Scanner = null!;

		private StringAdapter ToteAdapter = null!;

		private ListView ToteListView = null!;

		private bool ToteCancel;

		private Button ToteOkButton      = null!,
		               ToteCancelButton  = null!,
		               PickupButton      = null!,
		               SignButton        = null!,
		               DetailsBackButton = null!;

		private TextView LocationText = null!;

		private bool   DoingPodSig;
		private string LocationCode = "";

		private SignatureFragment Signature = null!;
		private EditText          PodText   = null!;

		protected override async void OnExecute( object? arg = null )
		{
			static void ShowError( string err )
			{
				Task.Run( () => { Globals.Dialogues.Error( err ); } );
			}

			while( Executing )
			{
				var ScanningLocation = true;
				DoingPodSig = false;
				ToteCancel  = false;

				RunOnUiThread( () =>
				               {
					               ToteAdapter.Clear();

					               LocationLayout.Visibility = ViewStates.Gone;
					               DetailsLayout.Visibility  = ViewStates.Gone;
					               ToteScanLayout.Visibility = ViewStates.Visible;

					               ToteOkButton.Enabled     = false;
					               ToteCancelButton.Enabled = false;

					               LocationCode      = "";
					               LocationText.Text = "";
				               } );

				while( Executing && !ToteCancel )
				{
					if( !DoingPodSig )
					{
						Scanner.Placeholder = ScanningLocation ? "Location Code" : "Tote Barcode";
						var Code = ( await Scanner.Scan() ).Trim();

						if( Code.IsNotNullOrWhiteSpace() )
						{
							if( ScanningLocation )
							{
								ScanningLocation = false;
								LocationCode     = Code;

								RunOnUiThread( () =>
								               {
									               LocationText.Text         = Code;
									               LocationLayout.Visibility = ViewStates.Visible;
									               ToteCancelButton.Enabled  = true;
								               } );
							}
							else
							{
								switch( Code[ 0 ] )
								{
								case 'T':
								case 'L':
									if( ( from T in ToteAdapter.StringList
									      where T == Code
									      select T ).Any() )
									{
										ShowError( "Already scanned." );
										continue;
									}

									ToteAdapter.Add( Code );
									ToteOkButton.Enabled = true;
									continue;

								default:
									ShowError( "Not a valid tote." );
									continue;
								}
							}
						}
					}
					else
						Thread.Sleep( 100 );
				}
			}
		}

		protected override void OnCreateFragment()
		{
			ToteScanLayout           = FindViewById<LinearLayout>( Resource.Id.toteScanLayout )!;
			DetailsLayout            = FindViewById<LinearLayout>( Resource.Id.detailsLayout )!;
			DetailsLayout.Visibility = ViewStates.Gone;

			ToteListView         = FindViewById<ListView>( Resource.Id.toteListView )!;
			ToteAdapter          = new StringAdapter( Activity );
			ToteListView.Adapter = ToteAdapter;

			ToteOkButton = FindViewById<Button>( Resource.Id.toteOkButton )!;

			ToteOkButton.Click += ( _, _ ) =>
			                      {
				                      DoingPodSig               = true;
				                      DetailsLayout.Visibility  = ViewStates.Visible;
				                      ToteScanLayout.Visibility = ViewStates.Gone;
				                      Scanner.Abort();
				                      PodText.Text = "";
				                      EnablePickup();
			                      };

			ToteCancelButton = FindViewById<Button>( Resource.Id.toteCancelButton )!;

			ToteCancelButton.Click += ( _, _ ) =>
			                          {
				                          Globals.Dialogues.Confirm( DriverPickup.CANCEL_PICKUP, () =>
				                                                                                 {
					                                                                                 ToteCancel = true;
					                                                                                 Scanner.Abort();
				                                                                                 }, () => { } );
			                          };

			LocationLayout = FindViewById<LinearLayout>( Resource.Id.locationLayout )!;
			LocationText   = FindViewById<TextView>( Resource.Id.locationText )!;

			SignatureLayout            = FindViewById<LinearLayout>( Resource.Id.signatureLayout )!;
			SignatureLayout.Visibility = ViewStates.Gone;
			SignButton                 = FindViewById<Button>( Resource.Id.signButton )!;

			SignButton.Click += async ( _, _ ) =>
			                    {
				                    SignatureLayout.Visibility = ViewStates.Visible;
				                    Orientation                = ScreenOrientation.Landscape;
				                    SignatureLayout.BringToFront();

				                    await Signature.CaptureSignature();

				                    Orientation = ScreenOrientation.Portrait;
				                    DetailsLayout.BringToFront();
				                    SignatureLayout.Visibility = ViewStates.Gone;

				                    EnablePickup();
			                    };
			SignButton.Visibility = ViewStates.Gone;

			PickupButton = FindViewById<Button>( Resource.Id.pickupButton )!;

			PickupButton.Click += ( _, _ ) =>
			                      {
				                      if( ToteAdapter.Count > 0 )
				                      {
					                      var Now    = DateTimeExtensions.NowKindUnspecified;
					                      var Driver = Globals.Login.UserName;

					                      var ToteIds = new List<string>( ToteAdapter.StringList );

					                      Task.Run( () =>
					                                {
						                                foreach( var ToteId in ToteIds )
						                                {
							                                var Trip = new Trip
							                                           {
								                                           UseUpdateTripDetailedQuickForNew = true,

								                                           Status = STATUS.CREATE_NEW_TRIP,
								                                           TripId = ToteId,

								                                           DeliveryResellerId = MetroDsdDeliveries.TOTE_RESELLER_ID,
								                                           PickupResellerId   = MetroDsdDeliveries.TOTE_RESELLER_ID,
								                                           DeliveryAccountId  = MetroDsdDeliveries.TOTE_ACCOUNT_ID,
								                                           PickupAccountId    = MetroDsdDeliveries.TOTE_ACCOUNT_ID,

								                                           AccountId = MetroDsdDeliveries.TOTE_ACCOUNT,
								                                           Driver    = Driver,
								                                           Reference = LocationCode,
								                                           PodName   = PodText.Text?.Trim() ?? "",

								                                           //                  Signature                 = CustomerSignature,
								                                           Pallets                   = "",
								                                           PackageType               = MetroDsdDeliveries.TOTE_ASSET,
								                                           ServiceLevel              = MetroDsdDeliveries.TOTE_ASSET,
								                                           PickupTime                = Now,
								                                           ReadyTime                 = Now,
								                                           DueTime                   = Now,
								                                           Pieces                    = 1,
								                                           Weight                    = 1,
								                                           OriginalPickupAddressId   = "",
								                                           OriginalDeliveryAddressId = "",

								                                           DeliveryAddressId    = MetroDsdDeliveries.TOTE_ADDRESS_ID,
								                                           DeliveryCompanyName  = MetroDsdDeliveries.TOTE_COMPANY,
								                                           DeliveryAddressLine1 = MetroDsdDeliveries.TOTE_ADDRESS,
								                                           DeliveryAddressLine2 = "",
								                                           DeliveryCity         = MetroDsdDeliveries.TOTE_CITY,
								                                           DeliveryRegion       = MetroDsdDeliveries.TOTE_REGION,
								                                           DeliveryPostalCode   = MetroDsdDeliveries.TOTE_POST_CODE,

								                                           PickupAddressId    = MetroDsdDeliveries.TOTE_ADDRESS_ID,
								                                           PickupCompanyName  = MetroDsdDeliveries.TOTE_COMPANY,
								                                           PickupAddressLine1 = MetroDsdDeliveries.TOTE_ADDRESS,
								                                           PickupAddressLine2 = "",
								                                           PickupCity         = MetroDsdDeliveries.TOTE_CITY,
								                                           PickupRegion       = MetroDsdDeliveries.TOTE_REGION,
								                                           PickupPostalCode   = MetroDsdDeliveries.TOTE_POST_CODE
							                                           };

							                                Globals.Database.UpdateTrip( Trip );
						                                }
					                                } );
				                      }
				                      ToteCancel = true;
			                      };

			PodText = FindViewById<EditText>( Resource.Id.podName )!;

			PodText.AfterTextChanged += ( _, _ ) => { EnablePickup(); };

			DetailsBackButton = FindViewById<Button>( Resource.Id.detailsBackButton )!;

			DetailsBackButton.Click += ( _, _ ) =>
			                           {
				                           DoingPodSig               = false;
				                           DetailsLayout.Visibility  = ViewStates.Gone;
				                           ToteScanLayout.Visibility = ViewStates.Visible;
			                           };
		}

		protected override void OnDestroyFragment()
		{
		}

		protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLayout, Scanner );

			Signature = new SignatureFragment();
			AddDynamicFragment( Resource.Id.signatureLayout, Signature );
		}

		public MetroTotePickup() : base( Resource.Layout.Metro_Fragment_PickupTote )
		{
		}

		private void EnablePickup()
		{
			var EnabPod = PodText.Text is not null && ( !Globals.Preferences.RequiresPodName || ( Globals.Preferences.RequiresPodName && PodText.Text.Trim().IsNotNullOrWhiteSpace() ) );

			//		var EnabSig = !Globals.Preferences.RequiresSignature || ( Globals.Preferences.RequiresSignature && CustomerSignature.IsNotNullOrWhiteSpace() );
			PickupButton.Enabled = EnabPod /* && EnabSig */;
		}
	}
}