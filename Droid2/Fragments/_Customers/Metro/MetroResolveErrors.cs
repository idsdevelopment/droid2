using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;

namespace Droid2.Fragments.Customers.Metro
{
    [ Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
        AlwaysRetainTaskState = true,
        ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Keyboard
    ) ]
    public class MetroResolveErrors : Activity
    {
        protected override void OnCreate( Bundle savedInstanceState )
        {
            base.OnCreate( savedInstanceState );
            SetContentView( Resource.Layout.Metro_Activity_WarehouseResolve );

            var ButtonTitle = Intent.GetStringExtra( "Button" );
            var OkBtn = FindViewById<Button>( Resource.Id.okButton );
            OkBtn.Text = ButtonTitle;
            OkBtn.Click += ( sender, args ) =>
            {
                SetResult( Result.Ok );
                Finish();
            };

            FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
            {
                SetResult( Result.Canceled );
                Finish();
            };
        }
    }
}