using Droid2.Fragments._Customers.Metro;
using Droid2.Fragments.Customers.Metro;
using Droid2.Fragments.Standard.Claims.StandardFragment;

namespace Droid2
{
	public static partial class Globals
	{
		public static partial class Modifications
		{
			private const string METRO = "Metro",
			                     CAGE = "Cage";

			public static Property IsMetro = new( METRO, Resource.Id.metroOptions, METRO,
			                                      new PreferenceOverrides
			                                      {
				                                      PickupDeliveryByPieces            = true,
				                                      UseLocationBarcodes               = true,
				                                      ScanBarcodeForUndeliverableReason = true,
				                                      HideUndeliverableButton           = true,
				                                      EnableFilter = false
			                                      },
			                                      new[]
			                                      {
				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_DRIVER_VERIFICATION,
					                                      Program     = typeof( MetroDriverVerification ),
					                                      RadioButton = Resource.Id.metroDsdPickupsButton
				                                      },
				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_WAREHOUSE_SCAN,
					                                      Program     = typeof( MetroWarehouseScan ),
					                                      RadioButton = Resource.Id.metroWarehouseScanButton
				                                      },
				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_DSD_DELIVERIES,
					                                      Program     = typeof( MetroDsdDeliveries ),
					                                      RadioButton = Resource.Id.metroDsdDeliveriesButton
				                                      },

				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_CLAIMS,
					                                      Program     = typeof( ClaimsStandard ),
					                                      RadioButton = Resource.Id.metroClaimsButton
				                                      },

				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_BUILD,
					                                      Program     = typeof( MetroBuild ),
					                                      RadioButton = Resource.Id.metroBuildButton
				                                      },

				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_LOAD,
					                                      Program     = typeof( MetroLoad ),
					                                      RadioButton = Resource.Id.metroLoadButton
				                                      },

				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_UNLOAD,
					                                      Program     = typeof( MetroUnload ),
					                                      RadioButton = Resource.Id.metroUnloadButton
				                                      },

				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_CREATE_EDIT,
					                                      Program     = typeof( MetroCreateEdit ),
					                                      RadioButton = Resource.Id.metroCreateEditButton,
					                                      Filter      = CAGE
				                                      },

				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_DESTRUCTION_TRAILER,
					                                      Program     = typeof( DestructionTrailer ),
					                                      RadioButton = Resource.Id.metroDestructionTrailerButton,
					                                      Filter      = CAGE
				                                      },

				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_FOREIGN_WAYBILL,
					                                      Program     = typeof( MetroForeignWaybill ),
					                                      RadioButton = Resource.Id.metroForeignWaybillButton,
					                                      Filter      = CAGE
				                                      },

				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_RC_CLAIM,
					                                      Program     = typeof( MetroRcClaim ),
					                                      RadioButton = Resource.Id.metroRcClaimButton,
					                                      Filter      = CAGE
				                                      },

				                                      new ProgramEntry
				                                      {
					                                      Mode        = OperatingMode.OPERATING_MODE.METRO_TOTE_PICKUP,
					                                      Program     = typeof( MetroTotePickup ),
					                                      RadioButton = Resource.Id.metroPickupToteButton,
				                                      }

			                                      } );
		}
	}
}