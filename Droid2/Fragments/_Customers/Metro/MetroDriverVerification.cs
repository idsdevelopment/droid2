#nullable enable

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Barcode;
using Droid2.Fragments.Standard;
using Service.Interfaces;
using Utils;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;
using TripStatus = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.TripStatus;

namespace Droid2.Fragments.Customers.Metro
{
	public class MetroDriverVerification : AFragment
	{
		private static readonly string[] NOT_ALLOWED =
		{
			MetroDsdDeliveries.CWD_SHIPMENT,
			MetroDsdDeliveries.RETURN,
			MetroDsdDeliveries.ASSET
		};

		private Button ClaimButton  = null!,
		               ClaimButton1 = null!,
		               ClaimButton2 = null!;

		private ClaimsPiecesAdapter ListAdapter = null!;

		private ListView             PickupListView = null!;
		private BarcodeInputFragment Scanner        = null!;

		private bool TripsClaimed;

		private RelativeLayout WaitingLayout = null!,
		                       MainLayout    = null!;

		[Obsolete]
		protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLayout, Scanner );
		}

		protected override void OnCreateFragment()
		{
			WaitingLayout = FindViewById<RelativeLayout>( Resource.Id.waitingLayout )!;
			MainLayout    = FindViewById<RelativeLayout>( Resource.Id.mainLayout )!;

			RunOnUiThread( () => { MainLayout.Visibility = ViewStates.Gone; } );

			ClaimButton1 = FindViewById<Button>( Resource.Id.claimNowButton1 )!;
			ClaimButton2 = FindViewById<Button>( Resource.Id.claimNowButton2 )!;

			RunOnUiThread( () =>
			               {
				               if( Globals.OperatingMode.ManualScan == Globals.OperatingMode.MANUAL_SCAN.MANUAL_SCAN )
				               {
					               ClaimButton             = ClaimButton1;
					               ClaimButton2.Visibility = ViewStates.Gone;
				               }
				               else
				               {
					               ClaimButton             = ClaimButton2;
					               ClaimButton1.Visibility = ViewStates.Gone;
				               }

				               ClaimButton.Enabled = false;
			               } );

			PickupListView         = FindViewById<ListView>( Resource.Id.claimsListView )!;
			ListAdapter            = new ClaimsPiecesAdapter( Activity, PickupListView ) { ShowMinimalOptions = true };
			PickupListView.Adapter = ListAdapter;

			FindViewById<CheckBox>( Resource.Id.showScannedTotes )!.Click += ( sender, _ ) =>
			                                                                 {
				                                                                 if( sender is CheckBox Sender )
					                                                                 ListAdapter.ShowHidden = Sender.Checked;
			                                                                 };
		}

		protected override void OnTripsChanged( int totalTrips, bool hasNew, bool hasDeleted, bool hasChanged )
		{
			try
			{
				var Trps = ListAdapter.ConvertToAdapterType( ( from T in Globals.Database.Trips
				                                               let Sl = T.ServiceLevel
				                                               where ( Sl != null ) && !NOT_ALLOWED.Contains( Sl.ToUpper() )
				                                               select T ).ToList() );
				var Tc = Trps.Count;
				var Lc = ListAdapter.Count;

				hasNew     |= Tc > Lc;
				hasDeleted |= Tc < Lc;

				var CurrentTrips = ListAdapter.Trips;

				if( hasDeleted )
				{
					var DeletedTrips =
						( from CurrentTrip in CurrentTrips
						  let Found = Trps.Any( trip => CurrentTrip.TripId == trip.TripId )
						  where !Found
						  select CurrentTrip ).ToList();
					ListAdapter.Remove( DeletedTrips );
				}

				if( hasNew )
				{
					var NewTrips = Trps.Where( trip => CurrentTrips.All( currentTrip => currentTrip.TripId != trip.TripId ) ).ToList();
					ListAdapter.Append( NewTrips );
				}

				if( hasChanged )
				{
					foreach( var Trip in Trps )
					{
						foreach( var CurrentTrip in CurrentTrips )
						{
							var STrip = (ScannedTrip)CurrentTrip;

							var SaveScanned = STrip.Scanned;

							if( STrip.TripId == Trip.TripId )
							{
								if( Trip.Pieces != STrip.Pieces )
								{
									STrip.Pieces  = Trip.Pieces;
									STrip.Balance = Trip.Pieces - SaveScanned;
									STrip.Hidden  = STrip.Balance == 0;
									STrip.Scanned = SaveScanned;

									break;
								}
							}
						}
					}
				}
			}
			finally
			{
				TripCount = ListAdapter.Count;

				if( hasDeleted || hasChanged || hasNew )
					ListAdapter.NotifyDataSetChanged();

				EnableClaim();
			}
		}

		protected override async void OnExecute( object? arg = null )
		{
			Globals.Database.DeleteAllLocalTripsAsync().Wait();

			IgnoreTripAddress   = true; //Must be before Status filter set
			TripStatusFilterSet = new[] { STATUS.DISPATCHED };
			LookForTrips        = true;

			RunOnUiThread( () =>
			               {
				               MainLayout.Visibility    = ViewStates.Gone;
				               WaitingLayout.Visibility = ViewStates.Visible;
			               } );

			await Task.Run( () =>
			                {
				                do
					                Thread.Sleep( 1000 );
				                while( ( ListAdapter.Count <= 0 ) && Executing );
			                } );

			if( !Executing )
				return;

			RunOnUiThread( () =>
			               {
				               MainLayout.Visibility    = ViewStates.Visible;
				               WaitingLayout.Visibility = ViewStates.Gone;
			               } );

			ListAdapter.Callback = ( trip, action, _ ) =>
			                       {
				                       if( action == PickupDeliveriesOptionsActivity.SCAN_STATUS.REMOVE )
				                       {
					                       ListAdapter.Remove( trip );
					                       EnableClaim();
				                       }
			                       };

			while( ClaimButton == null )
				Thread.Sleep( 10 );

			ClaimButton.Click += ( _, _ ) =>
			                     {
				                     Globals.Dialogues.Confirm( "Claim Totes?", () =>
				                                                                {
					                                                                var Now = DateTimeExtensions.NowKindUnspecified;

					                                                                Globals.Database.UpdateTripStatus( ( from T in ListAdapter.Trips
					                                                                                                     select new TripStatus { TripId = T.TripId, Status = STATUS.PICKED_UP, UpdateTime = Now } ).ToList() );
					                                                                ListAdapter.Clear();
					                                                                EnableClaim();
					                                                                TripsClaimed      = true;
					                                                                LookForTrips      = false; // Don't wait for destructor
					                                                                IgnoreTripAddress = false;
					                                                                Return( Globals.OperatingMode.OPERATING_MODE.METRO_DSD_DELIVERIES );
				                                                                }, null, false );
			                     };

		#if DEBUG
			ListAdapter.OnRowLongClick += ( view, o, arg3 ) =>
			                              {
				                              Scanner.InjectBarcode( ListAdapter.CurrentTrip.TripId );
			                              };
		#endif

			while( Executing )
			{
				Scanner.Abort();

				var Barcode = await Scanner.Scan();

				if( !Executing )
					break;

				if( !Scanner.WasBackKey )
				{
					if( !string.IsNullOrWhiteSpace( Barcode ) ) // Probably back key
					{
#pragma warning disable 4014
						Task.Run( () =>
						          {
							          try
							          {
								          if( ListAdapter.Trips.FirstOrDefault( trip => trip.TripId == Barcode ) is ScannedTrip FoundTrip )
								          {
									          if( ListAdapter.UpdateCount( Barcode, FoundTrip ) < 0 )
										          PlayBadScanSound();
								          }
								          else
									          Globals.Dialogues.Error( "Tote is not for this route." );
							          }
							          finally
							          {
								          EnableClaim();
							          }
						          } );
#pragma warning restore 4014
					}
				}
			}

			LookForTrips      = false; // Don't wait for destructor
			IgnoreTripAddress = false;

			Return();
		}

		protected override async void OnDestroyFragment()
		{
			Scanner.Abort();

			if( !TripsClaimed )
				await Globals.Database.DeleteAllTripsAsync();
		}

		public MetroDriverVerification() : base( Resource.Layout.Metro_Fragment_DsdDriverVerification )
		{
		}

		private void EnableClaim()
		{
			var Trps      = ListAdapter.Trips;
			var AllHidden = ( Trps.Count > 0 ) && Trps.Cast<ScannedTrip>().All( sTrip => ( sTrip == null ) || sTrip.Hidden );

			RunOnUiThread( () => { ClaimButton.Enabled = AllHidden; } );
		}
	}
}