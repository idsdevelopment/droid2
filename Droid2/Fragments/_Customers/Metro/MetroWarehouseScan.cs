using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Droid2.Adapters.Customers.Metro;
using Droid2.Barcode;
using Droid2.Fragments.Standard;
using Java.Lang;
using Service.Interfaces;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Customers.Metro
{
	public class MetroWarehouseScan : AFragment
	{
		private enum WAREHOUSE_STATES
		{
			WAITING_FOR_TRIPS,
			GET_CURRENT_ZONE,
			GET_DESTINATION_ZONE,
			PROCESS_TRIPS,
			PROCESS_ERRORS,
			POST_DATA,
			COMPLETED,
			ABORT
		}

		private BarcodeInputFragment Scanner;

		private MetroWarehouseListAdapter WarehouseAdapter, BadTripsAdapter;
		private ListView WarehouseListView, BadTripsListView;
		private ResidualList CompletedTrips;

		private LinearLayout WarehouseScanLayout, ProcessingLayout, BadTripsLayout;

		private string SourceZone, DestinationZone;

		private WAREHOUSE_STATES NextState;

		private ResidualTrip BadTrip;
		private int BadPosition;

		private bool InWarehouseTrips;

		private TextView ProcessingMessage;

		private Button ClaimNowButton;
		private Action OnClaimNowClick;

		private HashSet<string> CurrentZones;

		private FrameLayout ScannerLayout;

		private bool HasScannedPieces
		{
			get { return WarehouseAdapter.Trips.Any( t => t.Residual != t.Trip.Pieces ); }
		}

		public MetroWarehouseScan() : base( Resource.Layout.Metro_Fragment_WarehouseScan )
		{
		}

		protected override void OnDestroyFragment()
		{
			NextState = WAREHOUSE_STATES.ABORT;
			Scanner?.Abort();
		}

        [Obsolete]
        protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLayout, Scanner );
		}

		protected override void OnCreateFragment()
		{
			Gps = false;
#if DEBUG
			Globals.Database.DeleteAllLocalTripsAsync();
#endif
			TripStatusFilterSet = new[] { STATUS.DISPATCHED };

			LookForTrips = true;

			SourceZone = "";
			DestinationZone = "";

			ScannerLayout = FindViewById<FrameLayout>( Resource.Id.scannerLayout );

			WarehouseScanLayout = FindViewById<LinearLayout>( Resource.Id.warehouseScanLayout );
			WarehouseScanLayout.Touch += ( sender, args ) => { args.Handled = true; };

			ProcessingLayout = FindViewById<LinearLayout>( Resource.Id.processingLayout );
			ProcessingMessage = ProcessingLayout.FindViewById<TextView>( Resource.Id.message );

			ClaimNowButton = ProcessingLayout.FindViewById<Button>( Resource.Id.claimNowButton );
			ClaimNowButton.Click += ( sender, args ) => { OnClaimNowClick?.Invoke(); };

			BadTripsListView = FindViewById<ListView>( Resource.Id.badTripsListView );
			BadTripsLayout = FindViewById<LinearLayout>( Resource.Id.badTripsLayout );
			WarehouseListView = FindViewById<ListView>( Resource.Id.warehouseListView );

			var Act = Activity;
			WarehouseAdapter = new MetroWarehouseListAdapter( Act, new List<Trip>() );
			WarehouseListView.Adapter = WarehouseAdapter;

			RunOnUiThread( () =>
			{
				ScannerLayout.Visibility = ViewStates.Gone;
				BadTripsLayout.Visibility = ViewStates.Gone;
				ProcessingLayout.Visibility = ViewStates.Gone;
			} );

			BadTripsAdapter = new MetroWarehouseListAdapter( Act, new List<Trip>() )
							  {
								  Background = Resource.Drawable.RedGradient,
								  OnClick = ( trip, poistion ) =>
								  {
									  BadTrip = trip;
									  BadPosition = poistion;

									  var Intnt = new Intent( Act, typeof( MetroResolveErrors ) );
									  Intnt.PutExtra( "Button", trip.Trip.Pieces == 0 ? "Remove Item" : "Rescan Item" );
									  StartActivityForResult( Intnt, Globals.Dialogues.RESOLVE );
								  }
							  };

			BadTripsListView.Adapter = BadTripsAdapter;
		}

		protected override async void OnExecute( object arg = null )
		{
			// Run as state machine
			for( NextState = WAREHOUSE_STATES.WAITING_FOR_TRIPS; Executing && ( NextState < WAREHOUSE_STATES.ABORT ); )
			{
				RunOnUiThread( () => { BadTripsLayout.Visibility = BadTripsAdapter.Trips.Count > 0 ? ViewStates.Visible : ViewStates.Gone; } );

				ResidualList BadTrips;
				switch( NextState )
				{
				case WAREHOUSE_STATES.WAITING_FOR_TRIPS:
					if( !Globals.Database.HasTrips )
						ShowWaitingTrips( true );

					while( WaitingForTrips )
					{
						if( !Executing )
							return;

						Thread.Sleep( 100 );
					}
					CurrentZones = new HashSet<string>();

					foreach( var Trip in Globals.Database.Trips )
						CurrentZones.Add( Trip.CurrentZone.Trim() );

					RunOnUiThread( () => { ScannerLayout.Visibility = ViewStates.Visible; } );

					NextState = WAREHOUSE_STATES.GET_CURRENT_ZONE;
					break;

				case WAREHOUSE_STATES.GET_CURRENT_ZONE:
					await Task.Run( () =>
					{
						CompletedTrips = new ResidualList( new List<Trip>() );
						WarehouseAdapter.UpdateTrips( new List<Trip>() );
						BadTripsAdapter.UpdateTrips( new List<Trip>() );
					} );

					Scanner.Placeholder = "Ready spoke name";
					Scanner.OnCancel = () => { Scanner.Abort(); };
					Scanner.OnAccept = barcode =>
					{
						if( !string.IsNullOrEmpty( barcode ) )
						{
							if( CurrentZones.Contains( barcode ) )
							{
								var Trips = Globals.Database.GetDispatchedTripsByCurrentZone( barcode );

								if( Trips.Count > 0 )
								{
									SourceZone = barcode;
									NextState = WAREHOUSE_STATES.GET_DESTINATION_ZONE;
									WarehouseAdapter.UpdateTrips( Trips );
									return;
								}
								Error( "No Totes for this Spoke" );
							}
							else
								Error( "Invalid spoke name" );
						}
						else
							PlayBadScanSound();
					};
					await Scanner.Scan();
					break;

				case WAREHOUSE_STATES.GET_DESTINATION_ZONE:
					Scanner.Placeholder = "To spoke name";
					Scanner.OnCancel = () => { NextState = WAREHOUSE_STATES.GET_CURRENT_ZONE; };

					Scanner.OnAccept = barcode =>
					{
						if( !string.IsNullOrEmpty( barcode ) )
						{
							DestinationZone = barcode;

							if( DestinationZone != SourceZone )
							{
								NextState = WAREHOUSE_STATES.PROCESS_TRIPS;
								return;
							}
							Error( "To spoke same as Ready spoke" );
						}
						else
							PlayBadScanSound();
					};

					await Scanner.Scan();
					break;

				case WAREHOUSE_STATES.PROCESS_TRIPS:
					ShowEnterTotes();
					Scanner.Placeholder = "Tote  (" + SourceZone + " --> " + DestinationZone + ")";
					Scanner.OnCancel = () =>
					{
						Scanner.Abort();

						if( ( CompletedTrips.Count > 0 ) || ( BadTripsAdapter.Trips.Count > 0 ) || HasScannedPieces )
						{
							Confirm( "Scanned Totes\r\nAre you sure you wish to cancel everything?", () =>
							{
								NextState = WAREHOUSE_STATES.ABORT;
								Scanner.Abort();
							} );
						}
						else
							NextState = WAREHOUSE_STATES.GET_DESTINATION_ZONE;
					};

					Scanner.OnAccept = barcode =>
					{
						if( !string.IsNullOrEmpty( barcode ) )
						{
							var GoodTrips = WarehouseAdapter.Trips;

							foreach( var GoodTrip in GoodTrips )
							{
								if( GoodTrip.Trip.TripId == barcode )
								{
									if( --GoodTrip.Residual >= 0 )
									{
										if( GoodTrip.Residual == 0 )
										{
											GoodTrips.Remove( GoodTrip );
											CompletedTrips.Add( GoodTrip );
										}
										WarehouseAdapter.UpdateTrips( GoodTrips );

										if( GoodTrips.Count == 0 )
										{
											NextState = BadTripsAdapter.Trips.Count > 0
												? WAREHOUSE_STATES.PROCESS_ERRORS
												: WAREHOUSE_STATES.POST_DATA;

											Scanner.Abort();
										}
										return;
									}
									break;
								}
							}

							foreach( var Completed in CompletedTrips ) // Maybe overscan
							{
								if( Completed.Trip.TripId == barcode )
								{
									Completed.Residual--;
									CompletedTrips.Remove( Completed ); // Move to bad list
									var Bad = BadTripsAdapter.Trips;
									Completed.BadScanReason = "Overscan";
									Bad.Add( Completed );
									BadTripsAdapter.UpdateTrips( Bad );
									PlayBadScanSound();
									return;
								}
							}

							BadTrips = BadTripsAdapter.Trips;
							foreach( var BadTrp in BadTrips )
							{
								if( BadTrp.Trip.TripId == barcode )
								{
									BadTrp.Residual--;
									BadTripsAdapter.UpdateTrips( BadTrips );
									PlayBadScanSound();
									return;
								}
							}

							BadTripsAdapter.Add( barcode, "Not for this Spoke" );
							PlayBadScanSound();

							RunOnUiThread( () =>
							{
								if( BadTripsAdapter.Trips.Count == 1 )
									BadTripsLayout.Visibility = ViewStates.Visible;
							} );
						}
					};

					await Scanner.Scan();
					break;

				case WAREHOUSE_STATES.PROCESS_ERRORS:
					ShowResolveErrors();

					BadTrips = BadTripsAdapter.Trips;

					await Task.Run( () =>
					{
						while( ( WarehouseAdapter.Trips.Count == 0 ) && ( BadTrips.Count > 0 ) )
						{
							if( NextState == WAREHOUSE_STATES.ABORT )
								return;

							Thread.Sleep( 300 );
						}
					} );

					if( NextState != WAREHOUSE_STATES.ABORT )
						NextState = WarehouseAdapter.Trips.Count > 0 ? WAREHOUSE_STATES.PROCESS_TRIPS : WAREHOUSE_STATES.POST_DATA;

					break;

				case WAREHOUSE_STATES.POST_DATA:
					ShowProcessTotes();
					OnClaimNowClick = () =>
					{
						Globals.Database.ChangeCurrentZoneAsync( CompletedTrips.Select( t => t.Trip ).ToList(), DestinationZone );
						NextState = WAREHOUSE_STATES.COMPLETED;
					};

					await Task.Run( () =>
					{
						while( NextState == WAREHOUSE_STATES.POST_DATA )
							Thread.Sleep( 200 );

						if( NextState == WAREHOUSE_STATES.COMPLETED )
						{
							NextState = WAREHOUSE_STATES.WAITING_FOR_TRIPS;
							ShowWaitingTrips( false );
						}
					} );
					break;
				}
			}
			Return();
		}

		private void AbortWarehouseScan()
		{
			NextState = WAREHOUSE_STATES.ABORT;
			Scanner.Abort();
		}

		private void DoResolve()
		{
			var BadTrips = BadTripsAdapter.Trips;
			BadTrips.RemoveAt( BadPosition );
			if( BadTrip.Trip.Pieces != 0 ) // == 0 (not for this zone)
			{
				BadTrip.Residual = BadTrip.Trip.Pieces;
				WarehouseAdapter.Trips.Add( BadTrip );
				WarehouseAdapter.NotifyDataSetChanged();
			}
			if( BadTrips.Count <= 0 )
				RunOnUiThread( () => { BadTripsListView.Visibility = ViewStates.Gone; } );
			BadTripsAdapter.NotifyDataSetChanged();
		}

		public override void OnActivityResult( int requestCode, Result resultCode, Intent data )
		{
			DoResolve();
		}

		private void WarehouseShowProcess( bool show, string text, bool showButton )
		{
			RunOnUiThread( () =>
			{
				ClaimNowButton.Visibility = showButton ? ViewStates.Visible : ViewStates.Gone;

				if( show )
				{
					Scanner.Visibility = ViewStates.Invisible;

					WarehouseListView.Visibility = ViewStates.Gone;
					ProcessingLayout.Visibility = ViewStates.Visible;
					ProcessingMessage.Text = text;
				}
				else
				{
					Scanner.Visibility = ViewStates.Visible;

					WarehouseListView.Visibility = ViewStates.Visible;
					ProcessingLayout.Visibility = ViewStates.Gone;

					if( BadTripsAdapter.Trips.Count > 0 )
						BadTripsListView.Visibility = ViewStates.Visible;
				}
			} );
		}

		private bool WaitingForTrips;

		private void ShowWaitingTrips( bool show )
		{
			WaitingForTrips = show;
			WarehouseShowProcess( show, "Waiting for trips", false );
		}

		private void ShowProcessTotes()
		{
			WarehouseShowProcess( true, "Process Totes", true );
		}

		private void ShowEnterTotes()
		{
			WarehouseShowProcess( false, "", false );
		}

		private void ShowResolveErrors()
		{
			WarehouseShowProcess( true, "Please resolve the error(s)", false );
		}

		protected override void OnTripsChanged( int totalSummaryTrips, bool hasNew, bool hasDeleted, bool hasChanged )
		{
			WarehouseUpdateTrips( Globals.Database.Trips );
		}

		private async void WarehouseUpdateTrips( List<Trip> trips )
		{
			if( WaitingForTrips )
				ShowWaitingTrips( false );

			TripCount = trips.Count;

			if( !InWarehouseTrips && !string.IsNullOrWhiteSpace( SourceZone ) )
			{
				InWarehouseTrips = true;

				try
				{
					await Task.Run( () =>
					{
						var BadTrips = BadTripsAdapter.Trips;
						var WarehouseTrips = WarehouseAdapter.Trips;

						// Get only dispatched trips for this zone
						trips = trips.Where( trip => trip.CurrentZone == SourceZone ).ToList();

						// Find Deleted trips
						CompletedTrips.RemoveDeleted( trips );

						var ChangedWarehouse = WarehouseTrips.RemoveDeleted( trips );

						// Find new trips
						var Combined = new ResidualList();
						Combined.AddRange( CompletedTrips );
						Combined.AddRange( BadTrips );
						Combined.AddRange( WarehouseTrips );

						for( var Ndx = trips.Count - 1; Ndx >= 0; Ndx-- )
						{
							var Trip = trips[ Ndx ];

							var Found = Combined.Any( ctrip => ctrip.Trip.TripId == Trip.TripId );

							if( Found )
								trips.RemoveAt( Ndx );
						}


						// Should now only have new trips

						ChangedWarehouse |= trips.Count > 0;

						if( ChangedWarehouse )
						{
							RunOnUiThread( () =>
							{
								WarehouseNotifySound();

								if( trips.Count > 0 )
									WarehouseAdapter.AddRange( trips );

								if( ChangedWarehouse )
									WarehouseAdapter.NotifyDataSetChanged();

								if( WarehouseAdapter.Trips.Count == 0 )
								{
									var HasCompletedTrips = CompletedTrips.Count != 0;
									var HasBadTrips = BadTripsAdapter.Trips.Count != 0;

									if( HasBadTrips || HasCompletedTrips )
									{
										if( HasBadTrips )
										{
											if( NextState != WAREHOUSE_STATES.PROCESS_ERRORS )
											{
												NextState = WAREHOUSE_STATES.PROCESS_ERRORS;
												Scanner.Abort();
											}
										}
										else if( NextState != WAREHOUSE_STATES.POST_DATA )
										{
											NextState = WAREHOUSE_STATES.POST_DATA;
											Scanner.Abort();
										}
									}
									else // Now nothing to do
										AbortWarehouseScan();
								}

								// new trip appeared need to go back to scanning
								else if( NextState == WAREHOUSE_STATES.POST_DATA )
								{
									NextState = WAREHOUSE_STATES.PROCESS_TRIPS;
									WarehouseShowProcess( false, "", false );
								}
							} );
						}
					} );
				}
				finally
				{
					InWarehouseTrips = false;
				}
			}
		}
	}
}