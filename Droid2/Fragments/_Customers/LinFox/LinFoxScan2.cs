﻿#nullable enable

using Droid2.Fragments._Customers.Pml.StarTrack;

namespace Droid2.Fragments._Customers.LinFox
{
	public class LinFoxScan2 : StarTrackAudit
	{
		protected override Globals.OperatingMode.OPERATING_MODE OnExit()
		{
			return Globals.OperatingMode.OPERATING_MODE.LINFOX_SCAN1_SCAN2;
		}
	}
}