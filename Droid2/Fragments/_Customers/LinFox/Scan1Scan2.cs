﻿#nullable enable

using Droid2.Fragments._Standard_Customers.Pml.StarTrack;

namespace Droid2.Fragments._Customers.LinFox
{
	public class Scan1Scan2 : StarTrack
	{
		protected override Globals.OperatingMode.OPERATING_MODE OnScan1()
		{
			return Globals.OperatingMode.OPERATING_MODE.LINFOX_STAR_TRACK_PICKUPS;
		}

		protected override Globals.OperatingMode.OPERATING_MODE OnScan2()
		{
			return Globals.OperatingMode.OPERATING_MODE.LINFOX_STAR_TRACK_AUDIT;
		}
	}
}