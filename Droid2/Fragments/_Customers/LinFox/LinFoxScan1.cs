﻿#nullable enable

using Droid2.Adapters._Customers.LinFox;
using Droid2.Adapters._Customers.Pml;
using Droid2.Fragments._Customers.Pml.StarTrack;

namespace Droid2.Fragments._Customers.LinFox
{
	public class LinFoxScan1 : StarTrackPickups
	{
		protected override string Title => "Linfox Dispatch Scan";

		protected override PmlClaimsPiecesAdapter GetPmlAdapter => new LinFoxClaimsPiecesAdapter( Activity,
		                                                                                          ListView );
	}
}