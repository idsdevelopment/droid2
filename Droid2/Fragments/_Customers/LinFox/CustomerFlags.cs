#nullable enable

using Droid2.Fragments._Customers.LinFox;

namespace Droid2
{
	public static partial class Globals
	{
		public static partial class Modifications
		{
			private const string LINFOX = "LinFox";

			public static Property IsLinFox = new( LINFOX, Resource.Id.linFoxOptions, LINFOX,
			                                       new PreferenceOverrides
			                                       {
				                                       PickupDeliveryByPieces            = true,
				                                       UseLocationBarcodes               = true,
				                                       ScanBarcodeForUndeliverableReason = true,
				                                       RequiresPodName                   = true,
				                                       RequiresSignature                 = true,
				                                       HidePiecesAndWeight               = true,
				                                       EditPiecesWeightReference         = false,
				                                       HideReference                     = false,
				                                       EnableFilter                      = true
			                                       },
			                                       new[]
			                                       {
				                                       new ProgramEntry
				                                       {
					                                       Mode        = OperatingMode.OPERATING_MODE.LINFOX_SCAN1_SCAN2,
					                                       Program     = typeof( Scan1Scan2 ),
					                                       RadioButton = Resource.Id.linFoxScanButton
				                                       },
				                                       new ProgramEntry
				                                       {
					                                       Mode        = OperatingMode.OPERATING_MODE.LINFOX_STAR_TRACK_PICKUPS,
					                                       Program     = typeof( LinFoxScan1 ),
					                                       RadioButton = Resource.Id.hiddenButton,
					                                       Filter      = ALWAYS_HIDDEN
				                                       },
				                                       new ProgramEntry
				                                       {
					                                       Mode        = OperatingMode.OPERATING_MODE.LINFOX_STAR_TRACK_AUDIT,
					                                       Program     = typeof( LinFoxScan2 ),
					                                       RadioButton = Resource.Id.hiddenButton,
					                                       Filter      = ALWAYS_HIDDEN
				                                       }
			                                       }
			                                              );
		}
	}
}