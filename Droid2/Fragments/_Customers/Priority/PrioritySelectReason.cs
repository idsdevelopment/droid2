using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Widget;

// ReSharper disable AccessToModifiedClosure

namespace Droid2.Fragments.Customers.Priority
{
	[Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
		Label = "PmlSelectReason",
		AlwaysRetainTaskState = true )]
	public class PrioritySelectReasonActivity : Activity
	{
		private static readonly Dictionary<REASONS, string> Reasons = new()
		                                                              {
			                                                                    { REASONS.NONE, "??????" },
			                                                                    { REASONS.DELIVERED, "Delivered" },
			                                                                    { REASONS.PARTIAL, "Partial" },
			                                                                    { REASONS.REFUSED, "Refused" },
			                                                                    { REASONS.ATTEMPTED, "Attempted" },
		                                                                    };

		public static REASONS Reason = REASONS.NONE;

		public static STATUS Status;

		public static string ReasonAsText => Reasons[ Reason ];

		private Button OkButton;

		public enum REASONS
		{
			NONE,
			DELIVERED,
			PARTIAL,
			REFUSED,
			ATTEMPTED
		}
		public enum STATUS
		{
			BUSY,
			OK,
			CANCELED
		}

		protected override void OnCreate( Bundle savedInstanceState )
		{
			Status = STATUS.BUSY;

			base.OnCreate( savedInstanceState );
			SetContentView( Resource.Layout.Priority_Activity_SelectReason );

			OkButton = FindViewById<Button>( Resource.Id.okButton );
			OkButton.Click += ( sender, args ) =>
			                  {
				                  Status = STATUS.OK;
				                  Finish();
			                  };

			RunOnUiThread( () =>
			               {
				               OkButton.Enabled = false;
			               } );

			FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
			                                                          {
				                                                          Status = STATUS.CANCELED;
				                                                          Finish();
			                                                          };

			FindViewById<RadioButton>( Resource.Id.radioButton1 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.DELIVERED;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton2 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.PARTIAL;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton3 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.REFUSED;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton4 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.ATTEMPTED;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton5 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NONE;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton6 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NONE;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton7 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NONE;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton8 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NONE;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton9 ).Click += ( sender, args ) =>
			                                                               {
				                                                               Reason = REASONS.NONE;
				                                                               OkButton.Enabled = true;
			                                                               };
			FindViewById<RadioButton>( Resource.Id.radioButton10 ).Click += ( sender, args ) =>
			                                                                {
				                                                                Reason = REASONS.NONE;
				                                                                OkButton.Enabled = true;
			                                                                };
		}
	}
}