using Droid2.Fragments.Standard.Claims.StandardFragment;

namespace Droid2.Fragments.Customers.Priority
{
	internal class PriorityClaims : ClaimsStandard
	{
		protected override string XlateTripId( string tripId )
		{
			return tripId.PriorityBarcodeFilter();
		}
	}
}