﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Utils;

namespace Droid2.Fragments._Customers.Priority
{
	[Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
		Label = "SelectRouteActivity", AlwaysRetainTaskState = true )]
	public class PrioritySelectRoute : Activity
	{
		public const string ROUTE = "Route";

		private Button NextButton, ClearButton, CloseButton;
		private Spinner Pharmacy, Route, ServiceLevel;

		private string SelectedPharmacy, SelectedRoute, SelectedServiceLevel, SelectedLocation;

		private void EnableNext()
		{
			var Enab = false;

			if( !string.IsNullOrEmpty( SelectedPharmacy ) && !string.IsNullOrEmpty( SelectedServiceLevel ) )
			{
				if( ( ( SelectedServiceLevel != ROUTE ) && string.IsNullOrEmpty( SelectedRoute ) ) || !string.IsNullOrEmpty( SelectedRoute ) )
					Enab = true;
			}

			NextButton.Enabled = Enab;
		}

		public override bool DispatchKeyEvent( KeyEvent e )
		{
			return ( e.KeyCode == Keycode.Back ) || base.DispatchKeyEvent( e );
		}

		protected override void OnCreate( Bundle savedInstanceState )
		{
			Globals.Priority.Switches.SelectedPharmacy = "";
			Globals.Priority.Switches.SelectedLocation = "";
			Globals.Priority.Switches.SelectedRoute = "";
			Globals.Priority.Switches.SelectedServiceLevel = "";

			base.OnCreate( savedInstanceState );
			SetContentView( Resource.Layout.Priority_Activity_SelectRoute );

			var Pharmacies = Globals.Database.GetPharmaciesRoutes();
			var PharmacyLocations = Globals.Database.GetPharmacyLocations();

			ArrayAdapter GetAdapter( ICollection<string> items )
			{
				items.Add( "" );
				var Items = ( from I in items
				              orderby I
				              select I ).ToArray();

				var Adapter = new ArrayAdapter<string>( this, Android.Resource.Layout.SimpleSpinnerItem, Items );
				Adapter.SetDropDownViewResource( Android.Resource.Layout.SimpleSpinnerDropDownItem );
				return Adapter;
			}

			string GetSelectedItem( AdapterView spinner, AdapterView.ItemSelectedEventArgs e )
			{
				return (string)spinner.GetItemAtPosition( e.Position );
			}

			ServiceLevel = FindViewById<Spinner>( Resource.Id.serviceLevel );
			ServiceLevel.ItemSelected += ( sender, args ) =>
			                             {
				                             SelectedServiceLevel = GetSelectedItem( ServiceLevel, args );
				                             if( SelectedServiceLevel != ROUTE )
				                             {
					                             Route.Enabled = false;
					                             Route.SetSelection( 0 );
					                             SelectedRoute = "";
				                             }
				                             else
				                             {
					                             SelectedRoute = "";
					                             Route.Enabled = true;
				                             }

				                             EnableNext();
			                             };

			ServiceLevel.Adapter = GetAdapter( new List<string>
			                                   {
				                                   "Stat",
				                                   "Multistat",
				                                   "Sweep",
				                                   ROUTE
			                                   } );

			Route = FindViewById<Spinner>( Resource.Id.route );
			Route.ItemSelected += ( sender, args ) =>
			                      {
				                      SelectedRoute = GetSelectedItem( Route, args );
				                      EnableNext();
			                      };

			Route.Adapter = GetAdapter( new List<string>() );

			Pharmacy = FindViewById<Spinner>( Resource.Id.pharmacy );
			Pharmacy.Adapter = GetAdapter( ( from P in Pharmacies
			                                 select P.Key ).ToList() );

			Pharmacy.ItemSelected += ( sender, args ) =>
			                         {
				                         SelectedPharmacy = GetSelectedItem( Pharmacy, args );

				                         if( !PharmacyLocations.TryGetValue( SelectedPharmacy, out SelectedLocation ) )
					                         SelectedLocation = "";
				                         else if( !SelectedLocation.IsNullOrWhiteSpace() )
					                         SelectedLocation = $"({SelectedLocation})";
				                         else
					                         SelectedLocation = "";

				                         SelectedRoute = "";
				                         if( !string.IsNullOrEmpty( SelectedPharmacy ) )
					                         Route.Adapter = GetAdapter( Pharmacies[ SelectedPharmacy ].ToList() );
				                         EnableNext();
			                         };

			ClearButton = FindViewById<Button>( Resource.Id.clearButton );
			ClearButton.Click += ( sender, args ) =>
			                     {
				                     SelectedPharmacy = SelectedRoute = SelectedLocation = "";
				                     Pharmacy.Adapter = GetAdapter( ( from P in Pharmacies
				                                                      select P.Key ).ToList() );
				                     Route.Adapter = GetAdapter( new List<string>() );
				                     ServiceLevel.SetSelection( 0 );
				                     EnableNext();
			                     };

			NextButton = FindViewById<Button>( Resource.Id.nextButton );
			NextButton.Click += ( sender, args ) =>
			                    {
				                    Globals.Priority.Switches.SelectedServiceLevel = SelectedServiceLevel;
				                    Globals.Priority.Switches.SelectedRoute = SelectedServiceLevel != ROUTE
					                                                              ? SelectedServiceLevel
					                                                              : SelectedRoute;

				                    Globals.Priority.Switches.SelectedPharmacy = SelectedPharmacy;
				                    Globals.Priority.Switches.SelectedLocation = SelectedLocation;
				                    Finish();
			                    };

			CloseButton = FindViewById<Button>( Resource.Id.closeButton );
			CloseButton.Click += ( sender, args ) =>
			                     {
				                     Globals.Priority.Switches.ClosePickupByBarcode = true;
				                     Finish();
			                     };
		}
	}
}