using Droid2.Fragments.Customers.Priority;

namespace Droid2
{
	public static partial class Globals
	{
		public static partial class Modifications
		{
			private static readonly ProgramEntry[] PriorityPrograms =
			{
				new()
				{
					      Mode = OperatingMode.OPERATING_MODE.PRIORITY_DELIVERIES,
					      Program = typeof( PriorityDeliveries ),
					      RadioButton = Resource.Id.priorityDeliveriesButton
				      },
				new()
				{
					      Mode = OperatingMode.OPERATING_MODE.PRIORITY_PICKUP_BY_BARCODE,
					      Program = typeof( PriorityPickupByBarcode ),
					      RadioButton = Resource.Id.priorityPickupByBarcodeButton
				      },
				new()
				{
					      Mode = OperatingMode.OPERATING_MODE.CLAIMS_STANDARD,
					      Program = typeof( PriorityClaims ),
					      RadioButton = Resource.Id.priorityClaimsButton
				      }
			};

			private static readonly PreferenceOverrides PriorityPreferenceOverrides = new()
			                                                                          {
				                                                                                UseLocationBarcodes = true,
				                                                                                BatchMode = true,
				                                                                                OkString = "Yes",
				                                                                                CancelString = "No"
			                                                                                };

			private const string PRIORITY = "Priority";
			private const string PRIORITY_TEST = "PriorityTest";

			public static Property IsPriority = new( PRIORITY, Resource.Id.priorityOptions, PRIORITY, PriorityPreferenceOverrides, PriorityPrograms );

			public static Property IsPriorityTest = new( PRIORITY_TEST, Resource.Id.priorityOptions, PRIORITY_TEST, PriorityPreferenceOverrides, PriorityPrograms );
		}
	}
}