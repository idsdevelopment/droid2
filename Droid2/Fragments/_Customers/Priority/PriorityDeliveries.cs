using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Fragments.Standard.PickupDeliveries;
using Service.Interfaces;
using Utils;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Customers.Priority
{
	internal class PriorityDeliveries : DriverPickup
	{
		private const string CONF  = "Delivery Confirmation",
		                     TLIST = "Trip List";

		public override UNDELIVERABLE_MODE UndeliverableMode => UNDELIVERABLE_MODE.NEW_TRIP;

		private Color SaveColor;

		private readonly Color DeliveryColor = Color.Gold;

		private bool ProcessingSingleTrip;

		private Button ReasonButton;

		protected override bool ProcessSingleTrip( bool processMany ) => ProcessingSingleTrip || base.ProcessSingleTrip( processMany );


		protected override bool EnableUpdateTrip()
		{
			if( ReasonButton is not null )
			{
				var Enab = base.EnableUpdateTrip();

				if( Enab && ReasonButton.Text is not null )
					Enab = ReasonButton.Text.Trim().IsNotNullOrWhiteSpace();

				return Enab;
			}
			return false;
		}

		protected override void SetUpdateTripButtonText()
		{
			ReasonButton.Text     = "";
			UpdateTripButton.Text = "Finished";
		}

		protected override async Task<bool> OnShowDetail( ITrip trip, bool isExamine, bool finalising, bool showing )
		{
			var Retval = await base.OnShowDetail( trip, isExamine, finalising, showing );

			if( showing && ( trip.Status == STATUS.PICKED_UP ) )
			{
				ProcessingSingleTrip = true;
				RunOnUiThread( () => { DeliveryButtonsLayout.Visibility = ViewStates.Visible; } );
			}
			else
				ProcessingSingleTrip = false;

			return Retval;
		}

		protected override string XlateTripId( string tripId )
		{
			tripId = tripId.PriorityBarcodeFilter();

			if( !tripId.Contains( '+' ) ) // Probably special tote  (tripId in Reference)
			{
				foreach( var Trip in TripListAdapter.Trips )
				{
					if( Trip.Reference == tripId )
						return Trip.TripId;
				}
			}

			return tripId;
		}

		protected override bool IgnoreTrip( ScannedTrip trip ) => false;

		protected override void UpdateNotes( Trip trip )
		{
			if( ReasonButton.Text is not null )
				trip.DeliveryEmailAddress = ReasonButton.Text.Trim();
			base.UpdateNotes( trip );
		}

		protected override void AfterUpdateTrips()
		{
			var Save = DisableUpdateDisplayFromDatabase;

			try
			{
				DisableUpdateDisplayFromDatabase = false;
				UpdateDisplayFromDatabase();
			}
			finally
			{
				DisableUpdateDisplayFromDatabase = Save;
			}
		}

		protected override void AddCompanyBarcode( string companyName, Trip trip, bool isDelivery )
		{
			if( isDelivery && ( trip.Status < STATUS.VERIFIED ) )
			{
				var LocationCode = GetLocationCode( trip );

				if( !string.IsNullOrEmpty( LocationCode ) )
				{
					var Loc = new LocationCompany
					          {
						          Company         = companyName,
						          LocationBarcode = LocationCode
					          };

					Loc.Trips.Add( trip );
					UpdateLocation( Loc );

					var Pos = LocationCode.IndexOf( '-' );

					if( Pos > 0 )
					{
						var NewLoc = new LocationCompany( Loc )
						             {
							             LocationBarcode = LocationCode[ ..Pos ]
						             };

						UpdateLocation( NewLoc );
					}
				}
			}
		}

		protected override bool TryGetLocation( out LocationCompany location, out string locationCode )
		{
			RunOnUiThread( () =>
			               {
				               Title      = TLIST;
				               TitleColor = SaveColor;
			               } );

			var RetVal = false;
			ProcessingSingleTrip = false;

			location = Task.Run( async () =>
			                     {
				                     LocationCompany Location = null;

				                     Scanner.Placeholder       = "Delivery Station";
				                     Scanner.ForceManualAccept = true;
				                     string Loc;

				                     try
				                     {
					                     Loc = ( await Scanner.Scan() ).Trim();
				                     }
				                     finally
				                     {
					                     Scanner.ForceManualAccept = false;
				                     }

				                     if( !Scanner.WasBackKey && ( Loc != "" ) && Executing )
				                     {
					                     var WasTote = Loc.IndexOfAll( new[] { '+', '-' } );

					                     if( WasTote )
					                     {
						                     Loc = GetLocationCode( Loc ); // will include station id

						                     if( Loc is null )
							                     return null;

						                     var Pos = Loc.IndexOf( '-' );

						                     if( Pos > 0 )
							                     Loc = Loc[ ..Pos ];
					                     }
					                     else
					                     {
						                     if( !Loc.Contains( '+' ) && Loc.Contains( '-' ) ) // Special totes
						                     {
							                     var Pos = Loc.IndexOf( '-' );
							                     Pos = Loc.IndexOf( '-', Pos + 1 );

							                     if( Pos >= 0 )
								                     Loc = 'D' + Loc[ ( Pos + 1 ).. ];
						                     }
					                     }

					                     var Matches = ( from LocationCompany in Locations
					                                     let Key = LocationCompany.Value.LocationBarcode
					                                     where Key.StartsWith( Loc )
					                                     orderby Key
					                                     select LocationCompany.Value ).ToList();

					                     PrioritySelectDeliveryStationActivity.Status   = PrioritySelectDeliveryStationActivity.STATUS.BUSY;
					                     PrioritySelectDeliveryStationActivity.Stations = Matches;

					                     if( Matches.Count == 0 )
						                     Error( "No matching stations found." );
					                     else
					                     {
						                     const string ALL_STATIONS   = "(All Stations)",
						                                  MANY_LOCATIONS = "(Many Stations)";

						                     var M0 = Matches[ 0 ];

						                     if( !M0.LocationBarcode.Contains( '-' ) && !M0.LocationBarcode.Contains( ALL_STATIONS ) )
							                     M0.LocationBarcode = $"{M0.LocationBarcode}  {ALL_STATIONS}";

						                     var Intent = new Intent( Activity, typeof( PrioritySelectDeliveryStationActivity ) );
						                     Activity.StartActivity( Intent );

						                     await Task.Run( () =>
						                                     {
							                                     while( PrioritySelectDeliveryStationActivity.Status == PrioritySelectDeliveryStationActivity.STATUS.BUSY )
								                                     Thread.Sleep( 100 );
						                                     } );

						                     if( PrioritySelectDeliveryStationActivity.Status == PrioritySelectDeliveryStationActivity.STATUS.OK )
						                     {
							                     // Flatten trips
							                     var SingleLocation = new LocationCompany
							                                          {
								                                          LocationBarcode = MANY_LOCATIONS
							                                          };

							                     var First = true;

							                     var Temp = new List<Trip>();

							                     foreach( var LocationCompany in PrioritySelectDeliveryStationActivity.SelectedLocations )
							                     {
								                     if( First )
								                     {
									                     First                  = false;
									                     SingleLocation.Company = LocationCompany.Company;
								                     }

								                     Temp.AddRange( LocationCompany.Trips );
							                     }

							                     SingleLocation.Trips.AddRange( from T in Temp
							                                                    orderby T.TripId
							                                                    select T );

							                     Location = SingleLocation;

							                     RetVal = true;
						                     }
					                     }
				                     }

				                     return Location;
			                     } ).Result;

			locationCode = location?.LocationBarcode;

			if( RetVal )
			{
				RunOnUiThread( () =>
				               {
					               Title      = CONF;
					               TitleColor = DeliveryColor;
				               } );
			}

			return RetVal;
		}

		protected async Task<bool> SelectReason()
		{
			PrioritySelectReasonActivity.Status =
				PrioritySelectReasonActivity.STATUS.BUSY;

			var Intent = new Intent( Activity, typeof( PrioritySelectReasonActivity ) );

			Activity.StartActivity( Intent );

			return await Task.Run( () =>
			                       {
				                       while( PrioritySelectReasonActivity.Status == PrioritySelectReasonActivity.STATUS.BUSY )
					                       Thread.Sleep( 100 );

				                       return PrioritySelectReasonActivity.Status == PrioritySelectReasonActivity.STATUS.OK;
			                       } );
		}

		protected override void OnExecute( object arg = null )
		{
			SaveColor = TitleColor;

			Title = TLIST;

			ReasonButton = FindViewById<Button>( Resource.Id.pmlReasonButton );

			if( ReasonButton != null )
			{
				ReasonButton.Visibility = ViewStates.Visible;

				ReasonButton.Click += async ( _, _ ) =>
				                      {
					                      ReasonButton.Text        = await SelectReason() ? PrioritySelectReasonActivity.ReasonAsText : "";
					                      UpdateTripButton.Enabled = EnableUpdateTrip();
				                      };
			}

			TripListAdapter.OnTripLongClick += trip => { Scanner.InjectBarcode( trip.TripId ); };
			TripStatusFilterSet             =  new[] { STATUS.PICKED_UP };

			base.OnExecute( arg );
		}

		private static string GetLocationCode( string tripId )
		{
			var FirstPlus = tripId.IndexOf( '+' );

			if( FirstPlus >= 0 )
			{
				var LastPlus = tripId.LastIndexOf( '+' );

				if( LastPlus >= 0 )
				{
					var Len = LastPlus - FirstPlus;

					if( Len > 0 )
						return tripId.Substring( FirstPlus + 1, Len - 1 );
				}
			}

			return null;
		}

		private static string GetLocationCode( ITrip trip ) => GetLocationCode( trip.TripId );

		private void UpdateLocation( LocationCompany location )
		{
			var Key = location.LocationBarcode;

			if( !Locations.TryGetValue( Key, out var Location ) )
				Locations.Add( Key, location );
			else
				Location.Trips.AddRange( location.Trips );
		}
	}
}