using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.OS;
using Android.Widget;
using Droid2.Adapters;
using Droid2.Fragments.Standard.PickupDeliveries;

// ReSharper disable AccessToModifiedClosure

namespace Droid2.Fragments.Customers.Priority
{
	[Activity( Theme                 = "@android:style/Theme.Translucent.NoTitleBar",
	           Label                 = "DeliveryLocationActivity",
	           AlwaysRetainTaskState = true )]
	public class PrioritySelectDeliveryStationActivity : Activity
	{
		public enum STATUS
		{
			BUSY,
			OK,
			CANCELED
		}

		public static STATUS Status;

		public static List<DriverPickup.LocationCompany> Stations;
		public static List<DriverPickup.LocationCompany> SelectedLocations = new();

		private Button OkButton;

		private ListView  StationListView;
		public  List<int> SelectedIndices = new();

		protected override void OnCreate( Bundle savedInstanceState )
		{
			SelectedLocations = new List<DriverPickup.LocationCompany>();
			Status            = STATUS.BUSY;

			base.OnCreate( savedInstanceState );
			SetContentView( Resource.Layout.Priority_Activity_SelectDeliveryStation );

			FindViewById<TextView>( Resource.Id.companyName ).Text = Stations.Count > 0 ? Stations[ 0 ].Company : "????";

			OkButton = FindViewById<Button>( Resource.Id.okButton );

			OkButton.Click += ( sender, args ) =>
			                  {
				                  foreach( var SelectedIndex in SelectedIndices )
				                  {
					                  if( SelectedIndex == 0 ) // All
					                  {
						                  SelectedLocations.Add( Stations[ 0 ] );

						                  break;
					                  }

					                  SelectedLocations.Add( Stations[ SelectedIndex ] );
				                  }

				                  Status = STATUS.OK;
				                  Finish();
			                  };

			RunOnUiThread( () => { OkButton.Enabled = false; } );

			FindViewById<Button>( Resource.Id.cancelButton ).Click += ( sender, args ) =>
			                                                          {
				                                                          Status = STATUS.CANCELED;
				                                                          Finish();
			                                                          };

			var StationList = ( from Station in Stations
			                    select Station.LocationBarcode ).ToList();

			StationListView = FindViewById<ListView>( Resource.Id.stationListView );
			CheckBoxListAdapter Adapter = null;

			Adapter = new CheckBoxListAdapter( this, StationList )
			          {
				          OnSelectionChanged = ndx =>
				                               {
					                               if( Adapter != null )
					                               {
						                               SelectedIndices  = Adapter.SelectedIndices;
						                               OkButton.Enabled = SelectedIndices.Count > 0;
					                               }
				                               }
			          };

			StationListView.Adapter = Adapter;
		}
	}
}