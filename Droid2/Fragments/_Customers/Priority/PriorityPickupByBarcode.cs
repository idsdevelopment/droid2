using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.Views;
using Android.Widget;
using Droid2.Adapters._Customers.Priority;
using Droid2.Barcode;
using Droid2.Fragments.Standard;
using Droid2.Fragments._Customers.Priority;

namespace Droid2.Fragments.Customers.Priority
{
	internal static class Extensions
	{
		internal static bool IsPackageBarcode( this string str )
		{
			return str.IndexOfAny( new[] { '+', '-', '(', ')' } ) != -1;
		}
	}

	internal class PriorityPickupByBarcode : AFragment
	{
		private LinearLayout LocationLayout;
		private ListView PickupListView;
		private PriorityPickupByPiecesAdapter PriorityPickupAdapter;
		private Button ResetButton, ClaimButton;
		private BarcodeInputFragment Scanner;

		public PriorityPickupByBarcode() : base( Resource.Layout.Priority_Fragment_PickupByBarcode )
		{
		}

		protected override void OnCreateFragment()
		{
			LocationLayout = FindViewById<LinearLayout>( Resource.Id.locationLayout );
			LocationLayout.Visibility = ViewStates.Invisible;

			PriorityPickupAdapter = new PriorityPickupByPiecesAdapter( Activity );
			PickupListView = FindViewById<ListView>( Resource.Id.pickupListView );
			PickupListView.Adapter = PriorityPickupAdapter;

			ResetButton = FindViewById<Button>( Resource.Id.resetButton );
			ClaimButton = FindViewById<Button>( Resource.Id.claimButton );
		}

		protected override void OnDestroyFragment()
		{
			Scanner.Abort();
		}

        [Obsolete]
        protected override void OnCreateDynamicFragments()
		{
			Scanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.scannerLayout, Scanner );
		}

		protected override async void OnExecute( object arg = null )
		{
			var Reset = false;

			ResetButton.Click += ( sender, args ) =>
			                     {
				                     Task.Run( () =>
				                               {
					                               if( Confirm( "Are you really sure that you wish to abort the current pick-up?", false ) )
					                               {
						                               Reset = true;
						                               Scanner.Abort();
					                               }
				                               } );
			                     };

			ClaimButton.Click += ( sender, args ) =>
			                     {
				                     var WorkingList = new List<string>( PriorityPickupAdapter.StringList );
				                     var Loc = Globals.Priority.Switches.SelectedLocation;

				                     var C = WorkingList.Count;
				                     for( var I = 0; I < C; I++ )
					                     WorkingList[ I ] = $"{Loc}{WorkingList[ I ].Trim()}";

				                     var RoueText = Globals.Priority.Switches.SelectedServiceLevel == PrioritySelectRoute.ROUTE ? "Route: " : "";

				                     Task.Run( () =>
				                               {
					                               Globals.Database.CreateTripFromUpdateBarcode( WorkingList,
					                                                                             DateTimeOffset.Now,
					                                                                             $"{RoueText}{Globals.Priority.Switches.SelectedRoute}",
					                                                                             Globals.Priority.Switches.SelectedServiceLevel );
				                               } );

				                     Reset = true;
				                     Scanner.Abort();
			                     };

			Activity.StartActivity( typeof( PrioritySelectRoute ) );

			Globals.Priority.Switches.ClosePickupByBarcode = false;
			Globals.Priority.Switches.SelectedPharmacy = "";
			Globals.Priority.Switches.SelectedLocation = "";
			Globals.Priority.Switches.SelectedRoute = "";
			Globals.Priority.Switches.SelectedServiceLevel = "";

		#pragma warning disable 4014
			var Fin = false;

			Task.Run( () =>
			          {
				          do
				          {
					          Thread.Sleep( 10 );

					          Fin = Globals.Priority.Switches.ClosePickupByBarcode
					                || ( !string.IsNullOrEmpty( Globals.Priority.Switches.SelectedPharmacy )
					                     && !string.IsNullOrEmpty( Globals.Priority.Switches.SelectedRoute )
					                     && !string.IsNullOrEmpty( Globals.Priority.Switches.SelectedServiceLevel ) );
				          } while( !Fin );

				          return true;
			          } );
		#pragma warning restore 4014

			while( !Fin )
				Thread.Sleep( 10 );

			if( Globals.Priority.Switches.ClosePickupByBarcode )
			{
				Return( Globals.OperatingMode.OPERATING_MODE.PRIORITY_DELIVERIES );
				return;
			}

			while( Executing )
			{
				RunOnUiThread( () =>
				               {
					               ClaimButton.Enabled = false;
					               ResetButton.Enabled = false;
				               } );

				PriorityPickupAdapter.Clear();

				RunOnUiThread( () =>
				               {
					               LocationLayout.Visibility = ViewStates.Invisible;
				               } );
				/*
				                Scanner.Placeholder = "Location";
				                var Loc = ( await Scanner.Scan() ).PriorityBarcodeFilter();
				                if( !Loc.IsPackageBarcode() )
				                {
				                    RunOnUiThread( () =>
				                                   {
				                                       LocationTextView.Text = Loc;
				                                       LocationLayout.Visibility = ViewStates.Visible;
				                                   } );

				*/

				RunOnUiThread( () =>
				               {
					               ResetButton.Enabled = true;
				               } );

				for( Reset = false; Executing && !Reset; )
				{
					Scanner.Placeholder = "Barcode";
					var BarCode = ( await Scanner.Scan() ).PriorityBarcodeFilter();

					if( !string.IsNullOrEmpty( BarCode ) && BarCode.StartsWith( "CX" ) )
					{
						if( BarCode.IsPackageBarcode() )
						{
							if( !PriorityPickupAdapter.Add( "", BarCode ) )
								Error( "Package already scanned" );

							RunOnUiThread( () =>
							               {
								               ClaimButton.Enabled = PriorityPickupAdapter.StringList.Count > 0;
							               } );
						}
						else
							Error( "Please scan a package barcode." );
					}
					else
						PlayBadScanSound();
				}

//				}
//				else
//					Error( "Please scan a location barcode first." );
			}
		}
	}
}