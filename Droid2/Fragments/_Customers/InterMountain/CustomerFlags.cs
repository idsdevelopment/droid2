using System;
using Droid2.Fragments.Customers.Intermountain;
using Droid2.Fragments.Standard.Claims.CrossDockFragment;
using Droid2.Fragments.Standard.Claims.StandardFragment;

namespace Droid2
{
	public static partial class Globals
	{
		public class IntermountainDriverPickupSelector : Modifications.ProgramEntry
		{
			public override Type Program
			{
				get => Preferences.PickupDeliveryByPieceNumber ? typeof( IntermountainDeliveriesPieceNumber ) : typeof( IntermountainDeliveriesByPieces );
				set { }
			}
		}

		public static partial class Modifications
		{
			private static readonly ProgramEntry[] IntermountainPrograms =
			{
				new IntermountainDriverPickupSelector
				{
					Mode = OperatingMode.OPERATING_MODE.INTERMOUNTAIN_PICKUP,
					RadioButton = Resource.Id.driverPickupButton
				},
				new()
				{
					      Mode = OperatingMode.OPERATING_MODE.CLAIMS_STANDARD,
					      Program = typeof( ClaimsStandard ),
					      RadioButton = Resource.Id.claimsStandardButton
				      },
				new()
				{
					      Mode = OperatingMode.OPERATING_MODE.CROSS_DOCK,
					      Program = typeof( CrossDock ),
					      RadioButton = Resource.Id.crossDockButton
				      }
			};

			public static Property IsIntermountain = new( "IntExp", DEFAULT_LAYOUT, "InterMountain",
			                                              new PreferenceOverrides(),
			                                              IntermountainPrograms,
			                                              OnLoggedIn );
		}
	}
}