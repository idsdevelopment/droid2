﻿using Droid2.Fragments.Standard;
using Droid2.Fragments.Standard.PickupDeliveries;
using Service.Interfaces;
using Trip = CommsDb.Database.Db<IdsService.Client, IdsService.AuthToken, IdsService.org.internetdispatcher.jbtest2.remoteTrip>.Trip;

namespace Droid2.Fragments.Customers.Intermountain
{
	internal static class Filter
	{
		internal static bool PiecesTripFilter( Trip trip, bool isPickup )
		{
			var Status = trip.Status;
			return isPickup ? Status == STATUS.DISPATCHED : Status == STATUS.PICKED_UP;
		}
	}

	public class IntermountainDeliveriesPieceNumber : DeliveriesByPieceNumber
	{
		protected override string Title => "Inter-Mountain Pickup/Deliveries (PN)";

		protected override bool PiecesTripFilter( Trip trip, bool isPickup )
		{
			return Filter.PiecesTripFilter( trip, isPickup );
		}
	}

	public class IntermountainDeliveriesByPieces : DriverPickup
	{
		protected override string Title => "Inter-Mountain Pickup/Deliveries (P)";

		protected override bool PiecesTripFilter( Trip trip, bool isPickup )
		{
			return Filter.PiecesTripFilter( trip, isPickup );
		}
	}
}