using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Widget;
using Droid2.Fragments.Standard;

#pragma warning disable 612

namespace Droid2
{
	public partial class MainActivity
	{
		public void ClearLoadFrame()
		{
			LoaderLayout ??= FindViewById<LinearLayout>( Resource.Id.loaderLayout );

			if( Globals.Fragment.CurrentFragment != null )
			{
				var F = Globals.Fragment.CurrentFragment;

				// destroy dynamic fragments before destroying this
				F.Destroy();

				var Completed = false;

				RunOnUiThread( () =>
				               {
					               try
					               {
 #pragma warning disable 618
						               FragmentManager?.BeginTransaction()?.Remove( F )?.CommitAllowingStateLoss();
 #pragma warning restore 618
					               }
					               catch( Exception E )
					               {
						               Console.WriteLine( E );
					               }

					               LoaderLayout.RemoveAllViews();
					               Completed = true;
				               } );

				Task.Run( () =>
				          {
					          while( !Completed )
						          Thread.Sleep( 10 );
				          } ).Wait();

				Globals.Fragment.PreviousFragment = F;
				Globals.Fragment.CurrentFragment  = null;
			}
		}

		[Obsolete]
		public object ExecuteFragment( AFragment fragment, int loadAreaResourceId, object arg = null )
		{
			if( fragment is not null )
			{
				var Finished = false;

				if( !fragment.IsAdded )
				{
					RunOnUiThread( () =>
					               {
						               try
						               {
							               if( FragmentManager is not null )
							               {
								               FragmentManager.BeginTransaction()?.Add( loadAreaResourceId, fragment )?.CommitAllowingStateLoss();
								               FragmentManager.ExecutePendingTransactions();
							               }
						               }
						               catch
						               {
						               }
						               finally
						               {
							               Finished = true;
						               }
					               } );
				}
				else
					Finished = true;

				return Task.Run( () =>
				                 {
					                 while( !Finished || !fragment.Created )
						                 Thread.Sleep( 50 );

					                 fragment.RunFragment( arg );

					                 return fragment.Result;
				                 } ).Result;
			}

			return null;
		}

		public object ExecuteFragment( AFragment fragment, object arg = null )
		{
			ClearLoadFrame();
			Globals.Fragment.CurrentFragment = fragment;

			return ExecuteFragment( fragment, Resource.Id.loaderLayout, arg );
		}
	}
}