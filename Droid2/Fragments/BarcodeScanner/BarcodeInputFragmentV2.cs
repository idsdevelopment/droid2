using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Text;
using Android.Views;
using Android.Widget;
using ZXing.Mobile;
using Timer = System.Timers.Timer;

namespace Droid2.Barcode
{
#pragma warning disable 618
	public class BarcodeInputFragment : Fragment
#pragma warning restore 618
	{
		public static bool ScannerAbort;

		private static bool ZxingInitialised;

		public bool WasBackKey { get; private set; }

		public Action OnCancel
		{
			get => _OnCancel;
			set
			{
				_OnCancel = value;

				Globals.Debug.Activity.RunOnUiThread( () =>
				                                      {
					                                      try
					                                      {
						                                      CancelButton.Visibility = _OnCancel != null ? ViewStates.Visible : ViewStates.Gone;
					                                      }
					                                      catch( Exception Exception )
					                                      {
						                                      Console.WriteLine( Exception );
					                                      }
				                                      } );
			}
		}

		public bool ForceManualAccept
		{
			get => _ForceManualAccept;
			set
			{
				_ForceManualAccept = value;

				if( value )
				{
					SaveDisableAutoAccept = DisableAutoAccept;
					DisableAutoAccept     = true;
					SaveOnAccept          = OnAccept;

					if( SaveOnAccept == null )
						OnAccept = s => { };
				}
				else
				{
					DisableAutoAccept = SaveDisableAutoAccept;
					OnAccept          = SaveOnAccept;
					SaveOnAccept      = null;
				}
			}
		}

		public bool AutoAccept => !DisableAutoAccept && ( Globals.OperatingMode.ManualScan == Globals.OperatingMode.MANUAL_SCAN.AUTOMATIC_SCAN );

		public Action<string> OnAccept
		{
			get => _OnAccept;
			set
			{
				_OnAccept = value;
				ShowAccept();
			}
		}

		public bool IsPassword
		{
			get => _IsPassword;

			set
			{
				_IsPassword = value;

				Globals.Debug.Activity.RunOnUiThread(
				                                     () =>
				                                     {
					                                     BarcodeText.InputType = ( value ? InputTypes.TextVariationPassword : InputTypes.TextVariationNormal )
					                                                             | InputTypes.ClassText;
				                                     } );
				ShowAccept();
			}
		}

		public ViewStates Visibility
		{
			get => _Visibility;

			set
			{
				_Visibility = value;
				Globals.Debug.Activity.RunOnUiThread( () => { BaseLayout.Visibility = value; } );
			}
		}

		public string Placeholder
		{
			set { Globals.Debug.Activity.RunOnUiThread( () => { BarcodeText.Hint = value; } ); }
		}


		private readonly object KbdLockObject = new();

		private bool _ForceManualAccept;

		private bool _IsPassword;

		private Action<string> _OnAccept;

		private Action _OnCancel;

		private ViewStates _Visibility = ViewStates.Visible;

		private bool Aborting;

		private Button AcceptButton,
		               CancelButton;

		private bool AlreadyInitialised;
		private bool AutoFocusWaitingBarcode;

		private string Barcode = "";

		private EditText       BarcodeText;
		private LinearLayout   BaseLayout;
		private bool           IgnoreTextChanged;
		private bool           SaveDisableAutoAccept;
		private Action<string> SaveOnAccept;

		private Timer ScanCompleteTimer,
		              AcceptTimer;

		private MobileBarcodeScanningOptions ScannerOptions;

		private volatile bool Scanning,
		                      Destroying;

		private bool                 StartingBarcode;
		private MobileBarcodeScanner ZingScanner;
		private ImageButton          ZXingScannerButton;

		public bool   DisableAutoAccept;
		public Action OnInitialised;

		private static void PlayCameraClick()
		{
			Globals.Sounds.PlaySound( Resource.Raw.CameraClick, false );
		}

		private void ShowAccept()
		{
			try
			{
				Globals.Debug.Activity.RunOnUiThread( () =>
				                                      {
					                                      try
					                                      {
						                                      AcceptButton.Visibility = ( _OnAccept != null ) && !AutoAccept && !IsPassword ? ViewStates.Visible : ViewStates.Gone;
					                                      }
					                                      catch( Exception Exception )
					                                      {
						                                      Console.WriteLine( Exception );
					                                      }
				                                      } );
			}
			catch // Happens during Forced Shutdown
			{
			}
		}

		private void AutoFocusScanner()
		{
			if( ZingScanner != null )
			{
				AutoFocusWaitingBarcode = true;

				Task.Run( () =>
				          {
					          while( Scanning && AutoFocusWaitingBarcode && ( ZingScanner != null ) )
					          {
						          Globals.Debug.Activity.RunOnUiThread( () => { ZingScanner?.AutoFocus(); } );
						          Thread.Sleep( 4000 );
					          }

					          AutoFocusWaitingBarcode = false;
				          } );
			}
		}

		private string GetBarcodeText()
		{
			var HaveBarcode = false;
			var BCode       = "";

			if( BarcodeText != null )
			{
				Globals.Debug.Activity.RunOnUiThread( () =>
				                                      {
					                                      BCode = BarcodeText.Text;

					                                      if( !_IsPassword )
						                                      BCode = BCode.Trim();
					                                      HaveBarcode = true;
				                                      } );

				while( !HaveBarcode )
					Thread.Sleep( 1 );
			}

			return BCode;
		}

		private void AcceptButtonOnClick( object sender, EventArgs eventArgs )
		{
			var BCode = GetBarcodeText();
			Barcode = BCode;
			OnAccept?.Invoke( BCode );

			if( !IsPassword )
				Scanning = false; // Must be last
		}

		private void Clear()
		{
			var IsAborting = Aborting;

			IgnoreTextChanged = true;
			StopScan();
			ZingScanner?.Cancel();
			WasBackKey = false;
			Barcode    = "";

			// Use Globals Activity During Shutdown
			// Local Activity Becomes Invalid                
			var Act = Globals.Debug.Activity;

			if( ( Act != null ) && !Act.IsFinishing )
			{
				var WaitingClear = true;
				var ForcedAbort  = false;

				Act.RunOnUiThread( () =>
				                   {
					                   try
					                   {
						                   BarcodeText.Text = "";

						                   if( !IsAborting )
						                   {
							                   BarcodeText.RequestFocus();
							                   try
											   {
								                   StartingBarcode      = true;
								                   AcceptButton.Enabled = false;
								                   StartingBarcode      = false;

								                   while( StartingBarcode && !Act.IsFinishing && !Aborting && !ScannerAbort )
									                   Thread.Sleep( 100 );
							                   }
							                   catch // Happens During Forced Shutdown
							                   {
								                   ForcedAbort = true;
							                   }
						                   }
					                   }
					                   finally
					                   {
						                   WaitingClear = false;
					                   }
				                   } );

				while( WaitingClear && !ForcedAbort && !Destroying && !IsDetached && !ScannerAbort )
					Thread.Sleep( 100 );
			}

			IgnoreTextChanged = false;
			Scanning          = false;
		}

		private void KeyboardOnOnKeyPress( object sender, Globals.Keyboard.OnKeyPressData keyData )
		{
			Globals.Debug.Activity.RunOnUiThread( () =>
			                                      {
				                                      if( BarcodeText.HasFocus )
				                                      {
					                                      keyData.Handled  =  true;
					                                      BarcodeText.Text += keyData.Key;
				                                      }
			                                      } );
		}

		~BarcodeInputFragment()
		{
			Scanning   = false;
			Aborting   = true;
			Destroying = true;
		}

		public void InjectBarcode( string code )
		{
			Globals.Debug.Activity.RunOnUiThread( () =>
			                                      {
				                                      BarcodeText.Text = code;
				                                      AcceptButtonOnClick( null, null );
			                                      } );
		}

		public void StopScan()
		{
			try
			{
				lock( KbdLockObject )
				{
					var Tmr = ScanCompleteTimer;

					if( Tmr != null )
					{
						ScanCompleteTimer = null;
						Tmr.Stop();
						Tmr.Dispose();
					}
				}
			}
			catch
			{
			}
			finally
			{
				Scanning                = OnAccept != null; // Stay scanning if OnAccept
				AutoFocusWaitingBarcode = false;
			}
		}

		public override View OnCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
		{
			ScannerAbort = false;
			var V = inflater.Inflate( Resource.Layout.Fragment_BarcodeInputV2, container, false );
			AcceptButton = V.FindViewById<Button>( Resource.Id.acceptButton );
			BaseLayout   = V.FindViewById<LinearLayout>( Resource.Id.barcodeInputLayout );
			BarcodeText  = V.FindViewById<EditText>( Resource.Id.barcodeText );

			return V;
		}

		public override void OnDetach()
		{
			Abort();
			base.OnDetach();
		}

		public override void OnResume()
		{
			base.OnResume();

			if( AlreadyInitialised )
				return;

			AlreadyInitialised = true;

			Globals.Keyboard.OnKeyPress += KeyboardOnOnKeyPress;

			var V = View;

			V.Touch += ( sender, args ) => { args.Handled = true; };

			BarcodeText.Hint = "";

			BarcodeText.TextChanged += ( sender, args ) =>
			                           {
				                           if( !IgnoreTextChanged )
				                           {
					                           lock( KbdLockObject )
					                           {
						                           if( !AutoAccept && ( AcceptTimer == null ) )
						                           {
							                           AcceptTimer = new Timer { Interval = 400 };

							                           AcceptTimer.Elapsed += ( o, eventArgs ) =>
							                                                  {
								                                                  var Tmr = AcceptTimer;
								                                                  AcceptTimer = null;

								                                                  if( Tmr != null )
								                                                  {
									                                                  Tmr.Stop();
									                                                  Tmr.Dispose();

									                                                  var Txt = GetBarcodeText();
									                                                  Barcode = Txt;

									                                                  Globals.Debug.Activity.RunOnUiThread( () => { AcceptButton.Enabled = !string.IsNullOrEmpty( Txt ); } );
								                                                  }
							                                                  };
							                           AcceptTimer.Start();
						                           }
						                           else if( AcceptTimer != null )
						                           {
							                           AcceptTimer.Stop();
							                           AcceptTimer.Start();
						                           }

						                           if( Scanning )
						                           {
							                           if( AutoAccept || IsPassword )
							                           {
								                           if( ScanCompleteTimer == null )
								                           {
									                           ScanCompleteTimer = new Timer { Interval = 75 };

									                           ScanCompleteTimer.Elapsed += ( o, eventArgs ) =>
									                                                        {
										                                                        if( ScanCompleteTimer != null )
										                                                        {
											                                                        var Tmr = ScanCompleteTimer;
											                                                        ScanCompleteTimer = null;

											                                                        Tmr.Stop();
											                                                        Tmr.Dispose();

											                                                        IgnoreTextChanged = true;

											                                                        try
											                                                        {
												                                                        if( OnAccept != null )
													                                                        AcceptButtonOnClick( null, null );
												                                                        else
													                                                        Barcode = GetBarcodeText();
											                                                        }
											                                                        finally
											                                                        {
												                                                        IgnoreTextChanged = false;

												                                                        if( !IsPassword )
													                                                        Scanning = false;
											                                                        }
										                                                        }
									                                                        };
									                           ScanCompleteTimer.Start();
								                           }
								                           else
								                           {
									                           ScanCompleteTimer.Stop();
									                           ScanCompleteTimer.Start();
								                           }
							                           }
							                           else
							                           {
								                           if( ScanCompleteTimer != null )
								                           {
									                           ScanCompleteTimer.Stop();
									                           ScanCompleteTimer.Dispose();
									                           ScanCompleteTimer = null;
								                           }
							                           }
						                           }
					                           }
				                           }
			                           };

			BarcodeText.KeyPress += ( sender, args ) =>
			                        {
				                        lock( KbdLockObject )
				                        {
					                        if( args.KeyCode == Keycode.Back )
					                        {
						                        WasBackKey = true;

						                        Globals.Debug.Activity.RunOnUiThread( () =>
						                                                              {
							                                                              BarcodeText.Text = "";
							                                                              StopScan();
							                                                              OnCancel?.Invoke();
							                                                              Scanning = false;
						                                                              } );
					                        }

					                        args.Handled = false;
				                        }
			                        };

			AcceptButton.Enabled    =  false;
			AcceptButton.Visibility =  ViewStates.Gone;
			AcceptButton.Click      += AcceptButtonOnClick;

			BarcodeText.Visibility = ViewStates.Visible;

			if( Globals.OperatingMode.ManualScan == Globals.OperatingMode.MANUAL_SCAN.MANUAL_SCAN )
				OnAccept = s => { };

			CancelButton            = V.FindViewById<Button>( Resource.Id.cancelButton );
			CancelButton.Visibility = ViewStates.Gone;

			CancelButton.Click += ( sender, args ) =>
			                      {
				                      _OnCancel?.Invoke();
				                      Scanning = false; // Must be last
			                      };

			ZXingScannerButton = V.FindViewById<ImageButton>( Resource.Id.zxingScannerButton );

			ZXingScannerButton.Click += ( sender, args ) =>
			                            {
				                            Task.Run( async () =>
				                                      {
					                                      if( !ZxingInitialised )
					                                      {
						                                      ZxingInitialised = true;
						                                      MobileBarcodeScanner.Initialize( Activity.Application );
					                                      }

					                                      ZingScanner    = new MobileBarcodeScanner();
					                                      ScannerOptions = new MobileBarcodeScanningOptions();

					                                      AutoFocusScanner();
					                                      string BarCode;

					                                      try
					                                      {
						                                      var BCode = await ZingScanner.Scan( ScannerOptions );
						                                      BarCode = BCode?.Text;
					                                      }
					                                      finally
					                                      {
						                                      AutoFocusWaitingBarcode = false;
						                                      ZingScanner             = null;
					                                      }

					                                      if( !string.IsNullOrEmpty( BarCode ) )
					                                      {
						                                      var Stopped = false;

						                                      PlayCameraClick();

						                                      Globals.Debug.Activity.RunOnUiThread( () =>
						                                                                            {
							                                                                            if( !_IsPassword )
								                                                                            BarCode = BarCode.Trim();

							                                                                            BarcodeText.Text = BarCode;

							                                                                            StopScan();
							                                                                            Stopped = true;
						                                                                            } );

						                                      while( !Stopped )
							                                      Thread.Sleep( 10 );

						                                      Barcode = BarCode;
						                                      OnAccept?.Invoke( BarCode );

						                                      Scanning = false;
					                                      }
					                                      else
						                                      Scanning = false;
				                                      } );
			                            };
			OnInitialised?.Invoke();
		}

		public void Focus()
		{
			Globals.Debug.Activity.RunOnUiThread( () => { BarcodeText.RequestFocus(); } );
		}

		public void Abort()
		{
			Aborting = true;

			try
			{
				Clear();
			}
			finally
			{
				Aborting = false;
			}
		}

		public async Task<string> Scan()
		{
			Abort();
			Scanning = true;

			Globals.Debug.Activity.RunOnUiThread( () =>
			                                      {
				                                      BarcodeText.Enabled = true;
				                                      BarcodeText.RequestFocus();
			                                      } );

			return await Task.Run( () =>
			                       {
				                       while( Scanning )
					                       Thread.Sleep( 100 );

				                       Globals.Debug.Activity.RunOnUiThread( () => { BarcodeText.Enabled = false; } );

				                       return Barcode;
			                       } );
		}
	}
}