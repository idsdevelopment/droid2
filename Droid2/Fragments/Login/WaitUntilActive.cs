using System.Threading;
using Android.Widget;

namespace Droid2.Fragments.Standard
{
	public class WaitUntilActive : AFragment
	{
		private TextView CountDown;

		public WaitUntilActive() : base( Resource.Layout.Fragment_WaitUntilActive )
		{
		}

		protected override void OnCreateFragment()
		{
			CountDown = FindViewById<TextView>( Resource.Id.countDown );
		}

		protected override void OnExecute( object arg = null )
		{
			var Config = Globals.Database.Config;

			while( !Globals.Settings.InActivePeriod( Config, out var TimeRemaining ) && Executing && !Activity.IsFinishing )
			{
				var Days = TimeRemaining.Days;
				var Text = Days > 0
					? $"{Days}.{TimeRemaining.Hours:D2}:{TimeRemaining.Minutes:D2}:{TimeRemaining.Seconds:D2}"
					: $"{TimeRemaining.Hours:D2}:{TimeRemaining.Minutes:D2}:{TimeRemaining.Seconds:D2}";

				RunOnUiThread( () => { CountDown.Text = Text; } );
				Thread.Sleep( 1000 );
			}
			Return();
		}

		protected override void OnDestroyFragment()
		{
		}

		protected override void OnCreateDynamicFragments()
		{
		}
	}
}