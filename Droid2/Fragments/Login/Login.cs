using System;
using System.Timers;
using Android.App;
using Android.Views;
using Android.Widget;
using Droid2.Barcode;
using Service.Interfaces;

namespace Droid2.Fragments.Standard
{
	public class Login : AFragment
	{
		public bool IsAlternateLogin
		{
			get => _IsAlternateLogin;
			set
			{
				_IsAlternateLogin = value;

				if( MainLayout != null )
				{
					RunOnUiThread( () => { MainLayout.Visibility = value ? ViewStates.Gone : ViewStates.Visible; } );
				}
			}
		}

		private bool _IsAlternateLogin;

		private EditText AccountId,
		                 UserName;

		private LinearLayout MainLayout;

		private string               Pass = "";
		private BarcodeInputFragment PasswordScanner;

		private Button SignInButton;

		private void EnableSignIn()
		{
			if( SignInButton != null )
			{
				RunOnUiThread( () =>
				               {
					               SignInButton.Enabled = Online
					                                      && !string.IsNullOrWhiteSpace( AccountId.Text )
					                                      && !string.IsNullOrWhiteSpace( UserName.Text )
					                                      && ( Pass.Length > 0 );
				               } );
			}
		}

		protected override void OnCreateFragment()
		{
			SignInButton = FindViewById<Button>( Resource.Id.signInButton );
			MainLayout   = FindViewById<LinearLayout>( Resource.Id.mainLayout );

			AccountId = FindViewById<EditText>( Resource.Id.accountId );

			AccountId.KeyPress += ( sender, args ) =>
			                      {
				                      args.Handled = false;

				                      if( args.KeyCode != Keycode.Del )
					                      EnableSignIn();
			                      };

			UserName = FindViewById<EditText>( Resource.Id.userName );

			UserName.KeyPress += ( sender, args ) =>
			                     {
				                     args.Handled = false;

				                     if( args.KeyCode != Keycode.Del )
					                     EnableSignIn();
			                     };

			// Maybe Initialised before created
			IsAlternateLogin = IsAlternateLogin;
		}

        [Obsolete]
        protected override void OnCreateDynamicFragments()
		{
			PasswordScanner = new BarcodeInputFragment();
			AddDynamicFragment( Resource.Id.barcodeScannerContainer, PasswordScanner );
			PasswordScanner.DisableAutoAccept = true;
		}

		public void Abort()
		{
			if( PasswordScanner != null )
			{
				PasswordScanner.Abort();
				PasswordScanner = null;
			}
		}

		protected override void OnDestroyFragment()
		{
			Abort();
		}

		protected override void OnPing( bool online )
		{
			EnableSignIn();
		}

        [Obsolete]
        protected override async void OnExecute( object arg = null )
		{
			if( Globals.Database.Config.UseTestingServer )
			{
				RunOnUiThread( () =>
				               {
					               var Header = FindViewById<TextView>( Resource.Id.header );
					               Header.SetBackgroundResource( Resource.Color.Orange );
					               Header.Text = "  Login to IDS TESTING SERVER as driver";
				               } );
			}

			PasswordScanner.IsPassword = true;

			Timer AutoSignInTimer = null;

			PasswordScanner.OnAccept = barcode =>
			                           {
				                           Pass = barcode;

				                           EnableSignIn();

				                           if( AutoSignInTimer != null )
				                           {
					                           AutoSignInTimer.Stop();
					                           AutoSignInTimer.Start();
				                           }
				                           else
				                           {
					                           if( Pass.Contains( "^" ) )
					                           {
						                           AutoSignInTimer = new Timer( 500 );

						                           AutoSignInTimer.Elapsed += ( sender, args ) =>
						                                                      {
							                                                      AutoSignInTimer.Dispose();
							                                                      AutoSignInTimer = null;

							                                                      RunOnUiThread( () => { SignInButton.CallOnClick(); } );
						                                                      };

						                           AutoSignInTimer?.Start();
					                           }
				                           }
			                           };

			string CarrierId,
			       UName;

			if( !IsAlternateLogin )
				Globals.Settings.GetLoginDefaults( out CarrierId, out UName );
			else
				Globals.Settings.GetLoginAlternateDefaults( out CarrierId, out UName );

			RunOnUiThread( () =>
			               {
				               AccountId.Text       = CarrierId;
				               UserName.Text        = UName;
				               SignInButton.Enabled = false;
			               } );

			SignInButton.Click += async ( sender, args ) =>
			                      {
				                      PasswordScanner.Abort();

				                      var CarrierId1 = AccountId.Text;
				                      var UName1     = UserName.Text;

				                      // Combined user name and password
				                      if( Pass.Contains( "^" ) )
				                      {
					                      var Parts = Pass.Split( new[] { '^' }, StringSplitOptions.RemoveEmptyEntries );

					                      if( Parts.Length >= 2 )
					                      {
						                      UName1 = Parts[ 0 ];
						                      Pass   = Parts[ 1 ];

						                      var Name1 = UName1;

						                      RunOnUiThread( () => { UserName.Text = Name1; } );
					                      }
				                      }

				                      static bool Dragons( string s )
				                      {
					                      return string.Compare( s.Trim(), "There be dragons here", StringComparison.OrdinalIgnoreCase ) == 0;
				                      }

				                      var Config = Globals.Database.Config;

				                      if( Dragons( CarrierId1 ) || Dragons( UName1 ) || Dragons( Pass ) )
				                      {
					                      Config.UseTestingServer = !Config.UseTestingServer;
					                      Globals.Database.Config = Config;
					                      Globals.Database.DeleteAllTripsAsync().Wait();
					                      Globals.Android.Restart();
					                      Return();

					                      return;
				                      }

				                      var (Status, User) = await Globals.RemoteService.Login( CarrierId1, UName1, Pass );

				                      if( Status != CONNECTION_STATUS.OK )
				                      {
					                      var Builder = new AlertDialog.Builder( ( (Fragment)this ).Activity );
					                      Builder.SetMessage( "Invalid credentials" );

					                      Builder.SetPositiveButton( "Retry", ( s, e ) => { } );

					                      RunOnUiThread( () => { Builder.Show(); } );

					                      await PasswordScanner.Scan();
				                      }
				                      else
				                      {
					                      CarrierId1 = User.CarrierId;
					                      UName1     = User.UserName;

					                      if( ( CarrierId1 != Config.AccountId ) || ( UName != UName1 ) )
						                      await Globals.Database.DeleteAllTripsAsync();

					                      if( !IsAlternateLogin )
					                      {
						                      var Fax = User.Fax.Trim();
						                      Globals.Modifications.Filter = Fax;
						                      Globals.Modifications.Fax    = Fax;

						                      Globals.OperatingMode.Mode = Globals.OperatingMode.OPERATING_MODE.UNDEFINED;

						                      Globals.Settings.PollingInterval = User.PollingInterval;
						                      Globals.Settings.SaveLogin( CarrierId1, UName1, Pass, User.PollingInterval, User.StartHour, User.EndHour, User.ActiveDays );
						                      Globals.Login.LoggedIn = true;
					                      }
					                      else
						                      Globals.Settings.SaveAlternateLogin( UName1 );

					                      Globals.Database.PollingIntervalInSeconds = User.PollingInterval;

					                      Return( User );
				                      }
			                      };
			await PasswordScanner.Scan();
		}

		public Login() : base( Resource.Layout.Fragment_Login )
		{
		}
	}
}