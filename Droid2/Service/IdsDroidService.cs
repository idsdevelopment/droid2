using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.App;
using Android.Util;
using Object = Java.Lang.Object;

namespace Droid2
{
	[Service]
	public class IdsDroidService : Android.App.Service
	{
		public IBinder Binder { get; private set; }

		public bool ServiceStarted { get; protected internal set; }

		public OnTripsChangedEvent OnTripsChanged
		{
			get => _OnTripsChanged;
			set
			{
				_OnTripsChanged = value;

				if( value is not null )
					Globals.Database.OnNotifyTripChange = ( tripCount, hasNew, hasModify, hasDelete ) => { _OnTripsChanged?.Invoke( tripCount, hasNew, hasDelete, hasModify ); };
				else
					Globals.Database.OnNotifyTripChange = null;
			}
		}

		private LocationProvider Locations;

		private Intent Intent;

		private OnTripsChangedEvent _OnTripsChanged;

		internal Timer EventRunTimer;

		public Action<bool> OnPing;

		public delegate void OnTripsChangedEvent( int totalTrips, bool hasNew, bool hasDeleted, bool hasChanged );

		// This is any integer value unique to the application.
		public const int SERVICE_RUNNING_NOTIFICATION_ID = 375093756;

		//private NotificationChannel NotificationChannel;

		public override StartCommandResult OnStartCommand( Intent intent, StartCommandFlags flags, int startId )
		{
			Intent = intent;

			Locations = new LocationProvider( this )
			            {
				            OnPositionChanged = ( time, latitude, longitude ) => { Globals.Database.AddGpsPoint( time, latitude, longitude ); }
			            };
/*
			if( Build.VERSION.SdkInt >= BuildVersionCodes.O )
			{
				if( NotificationChannel is null )
				{
					NotificationChannel = new NotificationChannel( "ZZZZ", "ZZZZ", NotificationImportance.Default ) { Description = "ZZZZ" };

					Notification notification = new NotificationCompat.Builder( this, NotificationChannel.Id )
					                            .SetContentTitle( "AndroidMonks Sticker" )
					                            .SetTicker( "AndroidMonks Sticker" )
					                            .SetContentText( "Example" )
					                            .SetSmallIcon( Resource.Drawable.Icon )
					                            .SetOngoing( true ).Build();

					// Enlist this instance of the service as a foreground service
					StartForeground( SERVICE_RUNNING_NOTIFICATION_ID, notification );
				}

			}
*/
			return StartCommandResult.Sticky;
		}

		public void StopService()
		{
			StopService( Intent );
			ServiceStarted = false;
		}

		public override void OnDestroy()
		{
			Locations?.Dispose();
			Locations = null;
			base.OnDestroy();
		}

		public override IBinder OnBind( Intent intent )
		{
			return Binder = new IdsDroidServiceBinder( this );
		}
	}

	public class IdsDroidServiceBinder : Binder
	{
		public IdsDroidService Service { get; }

		public IdsDroidServiceBinder( IdsDroidService service )
		{
			Service = service;
		}
	}

	public class IdsDroidServiceConnection : Object, IServiceConnection
	{
		private readonly MainActivity Activity;

		public Action<IdsDroidService> OnConnected;

		public IdsDroidServiceConnection( MainActivity activity )
		{
			Activity = activity;
		}

		public void OnServiceConnected( ComponentName name, IBinder service )
		{
			if( service is IdsDroidServiceBinder ServiceBinder )
			{
				Activity.Binder  = ServiceBinder;
				Activity.IsBound = true;

				if( !ServiceBinder.Service.ServiceStarted )
				{
					ServiceBinder.Service.ServiceStarted = true;
					Application.Context.StartService( Globals.Background.ServiceIntent );
					OnConnected?.Invoke( ServiceBinder.Service );

					var Service = ServiceBinder.Service;
					var InCycle = false; // Mainly for debugging

					Service.EventRunTimer = new Timer( state =>
					                                   {
						                                   if( !InCycle )
						                                   {
							                                   InCycle = true;

							                                   try
							                                   {
								                                   Globals.RemoteService.CycleEventQueue();
							                                   }
							                                   finally
							                                   {
								                                   InCycle = false;
							                                   }
						                                   }
					                                   }, null, 0, 10 );

					Globals.RemoteService.StartPing( 15, online =>
					                                     {
						                                     if( !online )
							                                     Globals.Database.LastOutFifoUpdate = DateTime.UtcNow;

						                                     Service.OnPing?.Invoke( online );
					                                     } );
				}
			}
		}

		public void OnServiceDisconnected( ComponentName name )
		{
			Activity.IsBound = false;
		}
	}
}