using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Locations;
using Android.OS;
using Android.Provider;
using Utils;
using Object = Java.Lang.Object;

namespace Droid2
{
	public class LocationProvider : Object, ILocationListener
	{
		public delegate void PositionChanged( DateTimeOffset time, double latitude, double longitude );

		private readonly Criteria CriteriaForLocationService = new()
		                                                       {
			                                                             Accuracy = Accuracy.Fine
		                                                             };

		private LocationManager Manager;

		public PositionChanged OnPositionChanged;
		private volatile string Provider = "";

		public LocationProvider()
		{
		}

		public LocationProvider( Context context )
		{
			Manager = (LocationManager)context.GetSystemService( Context.LocationService );

			GetProvider();

			// If GPS is off open up GPS settings
			if( string.IsNullOrEmpty( Provider ) || !Manager.IsProviderEnabled( Provider ) )
			{
				Globals.Debug.Activity.RunOnUiThread( () =>
				                                      {
					                                      var GpsSettingsIntent = new Intent( Settings.ActionLocationSourceSettings );
					                                      GpsSettingsIntent.SetFlags( ActivityFlags.NewTask );
					                                      context.StartActivity( GpsSettingsIntent );
				                                      } );
			}

			WaitProvider();
		}

		// ILocationListener Interface routines
		public void OnLocationChanged( Location location )
		{
			if( Globals.GpsEnabled )
			{
				double Lat = location.Latitude,
				       Long = location.Longitude;

				if( Globals.Distance.IsDistanceOk( Lat, Long ) )
					OnPositionChanged?.Invoke( DateTimeOffset.UtcNow, Lat, Long );
			}
		}

		public void OnProviderDisabled( string provider )
		{
		}

		public void OnProviderEnabled( string provider )
		{
		}

		public void OnStatusChanged( string provider, Availability status, Bundle extras )
		{
		}

		private void GetProvider()
		{
			if( Globals.Android.VersionAsInt <= 412 )
				Provider = LocationManager.GpsProvider;
			else
			{
				var AcceptableLocationProviders = Manager.GetProviders( CriteriaForLocationService, true );
				Provider = AcceptableLocationProviders.Any() ? AcceptableLocationProviders.First() : string.Empty;
			}
		}

		private void WaitProvider()
		{
			Task.Run( () =>
			          {
				          while( true )
				          {
					          GetProvider();

					          if( string.IsNullOrWhiteSpace( Provider ) || !Manager.IsProviderEnabled( Provider ) )
						          Thread.Sleep( 100 );
					          else
						          break;
				          }

				          var Act = Globals.Debug.Activity;
				          while( Act == null )
				          {
					          Thread.Sleep( 100 );
					          Act = Globals.Debug.Activity;
				          }

				          Act.RunOnUiThread( () => { Manager.RequestLocationUpdates( Provider, 1500, 1, this ); } );
			          } );
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing && ( Manager != null ) )
			{
				Manager.RemoveUpdates( this );
				Manager.Dispose();
				Manager = null;
			}

			base.Dispose( disposing );
		}
	}
}