using Android.App;
using Android.Widget;

namespace Droid2
{
    public partial class MainActivity : Activity
    {
        private TextView DebugTextView;

        private void InitDebug()
        {
            Globals.Debug.Activity = this;
            DebugTextView = FindViewById<TextView>( Resource.Id.debugText );
            DebugText = "";
        }

        private string _DebugText;

        public string DebugText
        {
            get => _DebugText;
            set
            {
                _DebugText = value;
                RunOnUiThread( () => { DebugTextView.Text = value; } );
            }
        }
    }
}