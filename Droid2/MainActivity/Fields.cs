﻿using Android.Views;
using Android.Views.Animations;
using Android.Widget;

namespace Droid2
{
	public partial class MainActivity
	{
		private TextView AppTitle;
		private TextView UpdateText, CurrentVersion;

		private ImageView OptionsImage;
		public LinearLayout OptionsMenuLayout;
		public ViewGroup.LayoutParams OptionsMenuParams;
		private Animation OptionsMenuLayoutPullInRight, OptionsMenuLayoutPushOutRight;

		private LinearLayout LoaderLayout;

		private ImageView PingIcon;

		public LinearLayout LoginLayout;

		public IdsDroidServiceBinder Binder;
		public bool IsBound;
	}
}