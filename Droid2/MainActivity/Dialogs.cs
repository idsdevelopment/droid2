using Android.App;
using Android.Content;

namespace Droid2
{
	public partial class MainActivity
	{
		protected override void OnActivityResult( int requestCode, Result resultCode, Intent data )
		{
			if( resultCode == Result.Ok )
			{
				switch( requestCode )
				{
				case Globals.Dialogues.SORT:
					DoSortOptions();
					break;

				case Globals.Dialogues.VISIBILITY:
					SetVisibilityButton();
					break;
				}
			}
		}
	}
}