﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using AzureRemoteService;
using Droid2.Fragments.Standard;
using Utils;
using Uri = Android.Net.Uri;

// ReSharper disable InconsistentNaming

namespace Droid2
{
	[Activity( Theme                 = "@android:style/Theme.Black.NoTitleBar",
	           Label                 = "Ids2 Droid",
	           MainLauncher          = true, Icon = "@drawable/icon",
	           AlwaysRetainTaskState = true,
	           ClearTaskOnLaunch     = true,
	           ConfigurationChanges  = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden
	         )]
	public partial class MainActivity
	{
		private const string AZURE_USER_NAME = "Special Droid Login",
		                     AZURE_PASSWORD  = "Do Not Change";

		private const int OFFLINE = Resource.Drawable.offline32x32;
		private const int ON_LINE = Resource.Drawable.online32x32;


		public static bool Online;

		private enum PERMISSIONS
		{
			LOCATION_PERMISSION_REQUEST,
			STORAGE_REQUEST,
			INSTALL_REQUEST,
			NO_DOZE
		}

		private bool ForceIconUpdate;

		private bool Paused;

		private Action NextPermissionCallback;

		private bool ServiceStarted;

		private void AskPermission( string[] permission, PERMISSIONS requestCode, Action completedCallback )
		{
			NextPermissionCallback = completedCallback;

			// Here, thisActivity is the current activity
			if( ContextCompat.CheckSelfPermission( this, permission[ 0 ] ) != Permission.Granted )
			{
				if( !ShouldShowRequestPermissionRationale( permission[ 0 ] ) )
				{
					RequestPermissions( permission, (int)requestCode );

					return;
				}
			}

			completedCallback?.Invoke();
		}

		private void DoService()
		{
			Task.Run( () =>
			          {
				          Globals.Background.ServiceIntent = new Intent( this, typeof( IdsDroidService ) );

				          Globals.Background.Connection = new IdsDroidServiceConnection( this )
				                                          {
					                                          OnConnected = service =>
					                                                        {
						                                                        Globals.DroidService.Service = service;
						                                                        ServiceStarted               = true;

						                                                        /*
						                                                         if( Build.VERSION.SdkInt >= BuildVersionCodes.O )
																					StartForegroundService( Globals.Background.ServiceIntent );
																				else
																					StartService( Globals.Background.ServiceIntent );
						                                                        */

						                                                        StartService( Globals.Background.ServiceIntent );

						                                                        service.OnPing = online =>
						                                                                         {
							                                                                         if( ( online != Online ) || ( Globals.Fragment.CurrentFragment?.Online != online ) || ForceIconUpdate )
							                                                                         {
								                                                                         ForceIconUpdate = false;
								                                                                         Online          = online;

								                                                                         if( Globals.Fragment.CurrentFragment != null )
									                                                                         Globals.Fragment.CurrentFragment.Online = online;

								                                                                         RunOnUiThread( () => { PingIcon.SetImageResource( online ? ON_LINE : OFFLINE ); } );
							                                                                         }
						                                                                         };
					                                                        }
				                                          };

				          // Keep active across configuration changes use ApplicationContext
				          ApplicationContext.BindService( Globals.Background.ServiceIntent, Globals.Background.Connection, Bind.AutoCreate );
			          } );
		}

		private void ShowErrorDialogue( string text )
		{
			var Alert = new AlertDialog.Builder( this );
			Alert.SetTitle( "Cannot continue" );
			Alert.SetMessage( $"Unable to continue without {text} permission." );

			Alert.SetPositiveButton( "Ok", ( sender, args ) => { Finish(); } );

			var Dialog = Alert.Create();
			Dialog.Show();
		}

		private void GetPermissions()
		{
			var MajorVersion = Globals.Android.MajorVersion;

			void Android_6_Permissions()
			{
				AskPermission( new[] { Manifest.Permission.WriteExternalStorage, Manifest.Permission.ReadExternalStorage },
				               PERMISSIONS.STORAGE_REQUEST,
				               () =>
				               {
					               AskPermission( new[] { Manifest.Permission.AccessFineLocation, Manifest.Permission.AccessCoarseLocation },
					                              PERMISSIONS.LOCATION_PERMISSION_REQUEST,
					                              DoService );
				               } );
			}

			void Android_7_Permissions()
			{
				if( MajorVersion == 7 )
				{
					AskPermission( new[] { Manifest.Permission.RequestInstallPackages },
					               PERMISSIONS.INSTALL_REQUEST,
					               Android_6_Permissions );
				}
				else
					Android_6_Permissions();
			}

			async void Android_8_Permissions()
			{
				if( MajorVersion >= 8 )
				{
					if( !PackageManager.CanRequestPackageInstalls() )
					{
						var SettingsIntent = new Intent( Settings.ActionManageUnknownAppSources );
						SettingsIntent.SetData( Uri.Parse( "package:" + PackageName ) );
						StartActivity( SettingsIntent );

						var Ok = await Task.Run( () =>
						                         {
							                         while( !Paused )
								                         Thread.Sleep( 1000 );

							                         while( Paused ) // Wait for Settings Dialog To Close
								                         Thread.Sleep( 1000 );

							                         return PackageManager.CanRequestPackageInstalls();
						                         } );

						if( !Ok )
						{
							ShowErrorDialogue( "Install" );

							return;
						}
					}
				}

				Android_7_Permissions();
			}

			void Android_9_Permissions()
			{
				if( MajorVersion >= 6 )
				{
					AskPermission( new[] { Manifest.Permission.RequestIgnoreBatteryOptimizations },
					               PERMISSIONS.NO_DOZE, Android_8_Permissions );
				}
				else
					Android_8_Permissions();
			}

			Android_9_Permissions();
		}

		protected override void OnDestroy()
		{
			try
			{
				base.OnDestroy();

				if( IsBound )
				{
					IsBound = false;
					ApplicationContext.UnbindService( Globals.Background.Connection );
				}
			}
			catch
			{
			}
		}

		public override void OnRequestPermissionsResult( int requestCode, string[] permissions, Permission[] grantResults )
		{
			if( ( grantResults.Length <= 0 ) || ( grantResults[ 0 ] == Permission.Denied ) )
			{
				string Text;

				switch( (PERMISSIONS)requestCode )
				{
				case PERMISSIONS.LOCATION_PERMISSION_REQUEST:
					Text = "GPS";

					break;

				case PERMISSIONS.STORAGE_REQUEST:
					Text = "Storage";

					break;

				case PERMISSIONS.INSTALL_REQUEST:
					Text = "Install";

					break;

				case PERMISSIONS.NO_DOZE:
					Text = "Disable Battery Optimisation";

					break;

				default:
					return;
				}

				ShowErrorDialogue( Text );
			}
			else
				NextPermissionCallback?.Invoke();
		}

		public void ShowUpdateAvailable()
		{
			RunOnUiThread( () => { UpdateText.Visibility = ViewStates.Visible; } );
		}

		protected override void OnPause()
		{
			Paused = true;
			base.OnPause();
		}

		protected override void OnResume()
		{
			Paused = false;
			base.OnResume();
		}

		protected override async void OnCreate( Bundle bundle )
		{
			base.OnCreate( bundle );

			SetContentView( Resource.Layout._Main );

			CurrentVersion = FindViewById<TextView>( Resource.Id.currentVersion );

			RunOnUiThread( () => { CurrentVersion.Text = $"Vsn: 1.0.0.{Globals.Version.CURRENT_VERSION}"; } );
			UpdateText            = FindViewById<TextView>( Resource.Id.updateText );
			UpdateText.Visibility = ViewStates.Gone;

			AppTitle = FindViewById<TextView>( Resource.Id.appTitle );

			LoginLayout = FindViewById<LinearLayout>( Resource.Id.loginLayout );

			PingIcon = FindViewById<ImageView>( Resource.Id.pingIcon );
			PingIcon.SetImageResource( ON_LINE );
			Online          = true;
			ForceIconUpdate = true;

			IdsService.Globals.EndPoints.UseTestingServer = Globals.Database.Config.UseTestingServer;
			Globals.Database.ConnectService( Globals.RemoteService );
			InitDebug();
			PreInitOptionsMenu();

			// Now must explicitly request permissions
			GetPermissions();

			Globals.Version.DoUpdateIfNeeded();

			await Task.Run( () =>
			                {
				                while( !ServiceStarted )
					                Thread.Sleep( 10 );

				                Online = false;

				                if( Globals.Login.Execute() )
				                {
					                InitOptionsMenu();
					                ShowOptionsMenuButton();

					                Globals.Database.IsMetro = Globals.Modifications.IsMetro;

					                if( Globals.Modifications.IsPriority )
					                {
						                try
						                {
							                Task.Run( async () =>
							                          {
#if DEBUG
								                          Azure.Slot = Azure.SLOT.ALPHA_TRW;
#else
								                          Azure.Slot = Azure.SLOT.PRODUCTION;
#endif
								                          string Err = null;

								                          switch( await Azure.LogIn( "Droid", "priority", AZURE_USER_NAME, AZURE_PASSWORD ) )
								                          {
								                          case Azure.AZURE_CONNECTION_STATUS.OK:
									                          var PharmacyRoutes = Azure.Client.RequestPriorityGetRouteNamesForDevice().Result;
									                          Globals.Database.AddPharmacyRouteNames( PharmacyRoutes );

									                          break;

								                          case Azure.AZURE_CONNECTION_STATUS.TIME_ERROR:
									                          Err = "Please correct the time.";

									                          break;

								                          default:
									                          Err = "Error communicating to Azure.";

									                          break;
								                          }

								                          if( Err.IsNotNullOrWhiteSpace() )
									                          Globals.Dialogues.Inform( Err, Finish );
							                          } ).Wait();
						                }
						                catch
						                {
						                }
					                }

					                Globals.Version.CheckForUpdates = true;

					                if( !Globals.Settings.InActivePeriod() )
						                ExecuteFragment( new WaitUntilActive() );

					                Task.Run( () => { RunMenuOption(); } );
				                }
			                } );
		}
	}
}