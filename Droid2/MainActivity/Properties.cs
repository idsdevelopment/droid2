using System.Globalization;

namespace Droid2
{
	public partial class MainActivity
	{
		public int TripCount
		{
			set => DoCount( "Trips", value );
		}

		public int PieceCount
		{
			set => DoCount( "Pieces", value );
		}

		public int ScannedCount
		{
			set => DoCount("Scanned", value);
		}

		private void DoCount( string text, int count )
		{
			if( AppTitle != null )
			{
				var DroidTitle = Globals.Login.UserName;

				if( count >= 0 )
					DroidTitle += $"    {text}: {count.ToString( CultureInfo.InvariantCulture )}";

				RunOnUiThread( () => { AppTitle.Text = DroidTitle.Trim(); } );
			}
		}
	}
}