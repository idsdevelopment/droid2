#nullable enable

using System;
using System.Timers;
using Android.Views;

namespace Droid2
{
	public partial class MainActivity
	{
		public static    bool    AllowBackButton = true;
		public static    Action? OnBackKey;
		private volatile Timer?  BackDebounceTimer;

		public override bool DispatchKeyEvent( KeyEvent? e )
		{
			if( e is not null )
			{
				if( e.KeyCode == Keycode.Back )
				{
					// Sometimes gives multiple events
					if( AllowBackButton && ( BackDebounceTimer == null ) )
					{
						BackDebounceTimer = new Timer( 500 );

						BackDebounceTimer.Elapsed += ( _, _ ) =>
						                             {
							                             BackDebounceTimer?.Dispose();
							                             BackDebounceTimer = null;
						                             };
						BackDebounceTimer.Start();

						// Close Options Menu
						if( OptionsMenuLayout.LayoutParameters!.Width != 0 )
							OptionsImage_Click( null, null );
						else
							OnBackKey?.Invoke();
					}

					return true;
				}

				if( e.Action == KeyEventActions.Down )
				{
					var Key = (char)e.GetUnicodeChar( e.MetaState );

					if( Key is >= ' ' and <= '~' )
					{
						var Kd = new Globals.Keyboard.OnKeyPressData { Key = Key };

						Globals.Keyboard._OnKeyPress.Raise( this, Kd );

						if( Kd.Handled )
							return false;
					}
				}
			}

			return base.DispatchKeyEvent( e );
		}
	}
}