using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;

namespace Droid2.OptionsMenu.VisibilityActivity
{
    [ Activity( Theme = "@android:style/Theme.Translucent.NoTitleBar",
        Label = "Visibility",
        AlwaysRetainTaskState = true,
        ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize
    ) ]
    public class Visibility : Activity
    {
        protected override void OnCreate( Bundle savedInstanceState )
        {
            base.OnCreate( savedInstanceState );

            SetContentView( Resource.Layout.Activity_Visibility );

            var AllBtn = FindViewById<RadioButton>( Resource.Id.allButton );
            AllBtn.Click += ( sender, args ) =>
            {
                Globals.Filters.Visibility = Globals.Filters.VISIBILITY.ALL;
                SetResult( Result.Ok );
                Finish();
            };

            var PickupBtn = FindViewById<RadioButton>( Resource.Id.puButton );
            PickupBtn.Click += ( sender, args ) =>
            {
                Globals.Filters.Visibility = Globals.Filters.VISIBILITY.PICKUP;
                SetResult( Result.Ok );
                Finish();
            };

            var DeliveryBtn = FindViewById<RadioButton>( Resource.Id.delButton );
            DeliveryBtn.Click += ( sender, args ) =>
            {
                Globals.Filters.Visibility = Globals.Filters.VISIBILITY.DELIVERY;
                SetResult( Result.Ok );
                Finish();
            };

            var NewBtn = FindViewById<RadioButton>( Resource.Id.newButton );
            NewBtn.Click += ( sender, args ) =>
            {
                Globals.Filters.Visibility = Globals.Filters.VISIBILITY.NEW;
                SetResult( Result.Ok );
                Finish();
            };

            switch( Globals.Filters.Visibility )
            {
            case Globals.Filters.VISIBILITY.ALL:
                AllBtn.Checked = true;
                break;
            case Globals.Filters.VISIBILITY.PICKUP:
                PickupBtn.Checked = true;
                break;
            case Globals.Filters.VISIBILITY.DELIVERY:
                DeliveryBtn.Checked = true;
                break;
            case Globals.Filters.VISIBILITY.NEW:
                NewBtn.Checked = true;
                break;
            }
        }
    }
}