using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;

namespace Droid2.OptionsMenu.SortActivity
{
	[Activity( Theme                 = "@android:style/Theme.Translucent.NoTitleBar",
	           Label                 = "Sort",
	           AlwaysRetainTaskState = true,
	           ConfigurationChanges  = ConfigChanges.Orientation | ConfigChanges.ScreenSize
	         )]
	public class Sort : Activity
	{
		private Globals.Filters.OPTION SortOption;

		private void SetOption( Globals.Filters.SORT option )
		{
			if( SortOption == Globals.Filters.OPTION.PRIMARY_SORT )
				Globals.Filters.PrimarySort = option;
			else
				Globals.Filters.SecondarySort = option;

			SetResult( Result.Ok );
			Finish();
		}

		protected override void OnCreate( Bundle savedInstanceState )
		{
			base.OnCreate( savedInstanceState );

			SetContentView( Resource.Layout.Activity_Sort );

			var StatusBtn = FindViewById<RadioButton>( Resource.Id.statusButton );

			StatusBtn.Click += ( sender, args ) =>
			                   {
				                   SetOption( Globals.Filters.SORT.STATUS );
			                   };

			var IdBtn = FindViewById<RadioButton>( Resource.Id.idButton );

			IdBtn.Click += ( sender, args ) =>
			               {
				               SetOption( Globals.Filters.SORT.ID );
			               };

			var DueBtn = FindViewById<RadioButton>( Resource.Id.duelButton );

			DueBtn.Click += ( sender, args ) =>
			                {
				                SetOption( Globals.Filters.SORT.DUE );
			                };

			var AccountIdBtn = FindViewById<RadioButton>( Resource.Id.accIdButton );

			AccountIdBtn.Click += ( sender, args ) =>
			                      {
				                      SetOption( Globals.Filters.SORT.ACCOUNT_ID );
			                      };

			var AccountNunBtn = FindViewById<RadioButton>( Resource.Id.accNumButton );

			AccountNunBtn.Click += ( sender, args ) =>
			                       {
				                       SetOption( Globals.Filters.SORT.ACCOUNT_NUMBER );
			                       };

			var CompanyBtn = FindViewById<RadioButton>( Resource.Id.compButton );

			CompanyBtn.Click += ( sender, args ) =>
			                    {
				                    SetOption( Globals.Filters.SORT.COMPANY );
			                    };

			var ReferenceBtn = FindViewById<RadioButton>( Resource.Id.refButton );

			ReferenceBtn.Click += ( sender, args ) =>
			                      {
				                      SetOption( Globals.Filters.SORT.REFERENCE );
			                      };

			var NewBtn = FindViewById<RadioButton>( Resource.Id.newButton );

			NewBtn.Click += ( sender, args ) =>
			                {
				                SetOption( Globals.Filters.SORT.NEW );
			                };

			var ServiceLevelBtn = FindViewById<RadioButton>( Resource.Id.serviceLevelButton );

			ServiceLevelBtn.Click += ( sender, args ) =>
			                         {
				                         SetOption( Globals.Filters.SORT.SERVICE_LEVEL );
			                         };

			var SortTitle = FindViewById<TextView>( Resource.Id.sortTitle );
			SortOption = (Globals.Filters.OPTION)Intent.GetIntExtra( "SortOption", 0 );

			Globals.Filters.SORT SrtOpt;

			if( SortOption == Globals.Filters.OPTION.PRIMARY_SORT )
			{
				SortTitle.Text = "Primary Sort";
				SrtOpt         = Globals.Filters.PrimarySort;
			}
			else
			{
				SortTitle.Text = "Secondary Sort";
				SrtOpt         = Globals.Filters.SecondarySort;
			}

			switch( SrtOpt )
			{
			case Globals.Filters.SORT.STATUS:
				StatusBtn.Checked = true;

				break;

			case Globals.Filters.SORT.ID:
				IdBtn.Checked = true;

				break;

			case Globals.Filters.SORT.DUE:
				DueBtn.Checked = true;

				break;

			case Globals.Filters.SORT.ACCOUNT_ID:
				AccountIdBtn.Checked = true;

				break;

			case Globals.Filters.SORT.ACCOUNT_NUMBER:
				AccountNunBtn.Checked = true;

				break;

			case Globals.Filters.SORT.COMPANY:
				CompanyBtn.Checked = true;

				break;

			case Globals.Filters.SORT.REFERENCE:
				ReferenceBtn.Checked = true;

				break;

			case Globals.Filters.SORT.SERVICE_LEVEL:
				ServiceLevelBtn.Checked = true;

				break;

			case Globals.Filters.SORT.NEW:
				NewBtn.Checked = true;

				break;
			}
		}
	}
}