using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Droid2.Fragments.Standard;
using Droid2.OptionsMenu.SortActivity;
using Droid2.OptionsMenu.VisibilityActivity;

namespace Droid2
{
	public partial class MainActivity
	{
		private bool InOptionsClick;

		private Globals.Filters.OPTION RequestedOption;

		public volatile bool Stopping,
		                     StoppingAll; // Releases thread

		internal async void RunMenuOption( AFragment prog, object args = null )
		{
			if( prog != null )
			{
				await Task.Run( () =>
				                {
					                Stopping = true;

					                try
					                {
						                do
						                {
							                ShowOptionsMenuButton();
							                ExecuteFragment( prog, args );
						                }
						                while( !IsFinishing && !Stopping && !StoppingAll && !prog.Destroyed );
					                }
					                finally
					                {
						                Stopping = false; // Reset by Previous Thread Exiting
					                }
				                } );
			}
		}

		internal void RunMenuOption( object args = null )
		{
			RunMenuOption( Globals.OperatingMode.Program, args );
		}

		internal void RunMenuOption( Globals.OperatingMode.OPERATING_MODE mode, object args = null )
		{
			CollapseOptionsMenu();

			if( Globals.OperatingMode.Mode != mode )
			{
				Globals.OperatingMode.Mode = mode;
				RunMenuOption( args );
			}
		}

		private void PreInitOptionsMenu()
		{
			Globals.Modifications.HideAll();

			RunOnUiThread( () =>
			               {
				               OptionsImage             = FindViewById<ImageView>( Resource.Id.optionsImage );
				               OptionsImage!.Visibility = ViewStates.Invisible;

				               OptionsMenuLayout                  = FindViewById<LinearLayout>( Resource.Id.optionsMenuLayout );
				               OptionsMenuParams                  = OptionsMenuLayout!.LayoutParameters;
				               OptionsMenuParams!.Width           = 0;
				               OptionsMenuLayout.LayoutParameters = OptionsMenuParams;
			               } );
		}

		private void InitOptionsMenu()
		{
			Globals.Modifications.EnableModifications();

			RunOnUiThread( () =>
			               {
				               FindViewById<Button>( Resource.Id.visibilityButton )!.Click    += VisibilityButton_Click;
				               FindViewById<Button>( Resource.Id.primarySortButton )!.Click   += PrimarySortButton_Click;
				               FindViewById<Button>( Resource.Id.secondarySortButton )!.Click += SecondarySortButton_Click;
				               FindViewById<Button>( Resource.Id.signOutButton )!.Click       += SignOutOnClick;

				               OptionsMenuLayoutPullInRight = AnimationUtils.LoadAnimation( this, Resource.Animation.PullInRight );

				               OptionsMenuLayoutPushOutRight = AnimationUtils.LoadAnimation( this, Resource.Animation.PushOutRight );

				               OptionsMenuLayoutPushOutRight!.AnimationEnd += ( _, _ ) =>
				                                                              {
					                                                              OptionsMenuParams.Width            = 0;
					                                                              OptionsMenuLayout.LayoutParameters = OptionsMenuParams;
				                                                              };

				               OptionsMenuLayout.Touch += ( _, args ) => { args.Handled = true; };
				               OptionsImage.Click      += OptionsImage_Click;

				               var ManualBarcodeInput = FindViewById<CheckBox>( Resource.Id.manualBarcodeInputCheckBox );
			               #if !DEBUG
				               if( Globals.Modifications.IsMetro )
				               {
					               var Layout = FindViewById<LinearLayout>( Resource.Id.manualBarcodeLayout );

					               if( Layout is not null )
						               Layout.Visibility = ViewStates.Gone;

					               if( ManualBarcodeInput is not null )
						               ManualBarcodeInput.Visibility = ViewStates.Gone;

					               Globals.OperatingMode.ManualScan = Globals.OperatingMode.MANUAL_SCAN.AUTOMATIC_SCAN;
				               }
				               else
			               #endif
				               {
					               if( ManualBarcodeInput is not null )
					               {
						               ManualBarcodeInput.Checked = Globals.OperatingMode.ManualScan == Globals.OperatingMode.MANUAL_SCAN.MANUAL_SCAN;

						               ManualBarcodeInput.Click += ( _, _ ) =>
						                                           {
							                                           Globals.OperatingMode.ManualScan = ManualBarcodeInput.Checked
								                                                                              ? Globals.OperatingMode.MANUAL_SCAN.MANUAL_SCAN
								                                                                              : Globals.OperatingMode.MANUAL_SCAN.AUTOMATIC_SCAN;
						                                           };
					               }
				               }

				               var UseTestingServer = FindViewById<CheckBox>( Resource.Id.useTestingServerCheckBox );

				               if( UseTestingServer is not null )
				               {
					               UseTestingServer.Checked = Globals.Database.Config.UseTestingServer;

					               UseTestingServer.Click += ( _, _ ) =>
					                                         {
						                                         const string WARNING = "Are you really sure that you wish to switch between the\r\n"
						                                                                + "LIVE server to the TESTING server?\r\n\r\n"
						                                                                + "DO THIS AT YOUR OWN PERIL!";

						                                         Globals.Dialogues.Warning( WARNING, () =>
						                                                                             {
							                                                                             var UseTesting = UseTestingServer.Checked;
							                                                                             var Config     = Globals.Database.Config;

							                                                                             Config.UseTestingServer = UseTesting;
							                                                                             Config.UserName         = "";
							                                                                             Config.Password         = "";

							                                                                             Globals.Database.Config                       = Config;
							                                                                             IdsService.Globals.EndPoints.UseTestingServer = UseTesting;
							                                                                             Globals.Database.DeleteAllTripsAsync().Wait();
							                                                                             Globals.Android.Restart();
						                                                                             } );
					                                         };
				               }

				               SetVisibilityButton();
				               SetPrimarySortButton();
				               SetSecondarySortButton();

				               HideOptionMenu();
			               } );
		}

		private void SignOutOnClick( object sender, EventArgs eventArgs )
		{
			Globals.Dialogues.AskExitIds( () =>
			                              {
				                              Globals.Login.LoggedIn = false;
				                              Online                 = false;

				                              StoppingAll = true;
				                              StopService( Globals.Background.ServiceIntent );
				                              RunOnUiThread( Finish );

				                              var Service = Binder.Service;
				                              Service.StopService();

				                              while( Service.ServiceStarted )
					                              Thread.Sleep( 100 );
			                              } );
		}

		private void CollapseOptionsMenu()
		{
			RunOnUiThread( () =>
			               {
				               OptionsMenuParams.Width            = 0;
				               OptionsMenuLayout.LayoutParameters = OptionsMenuParams;
			               } );
		}

		private void HideOptionMenu()
		{
			RunOnUiThread( () =>
			               {
				               CollapseOptionsMenu();
				               OptionsImage.Visibility = ViewStates.Invisible;
			               } );
		}

		private void ShowOptionsMenuButton()
		{
			RunOnUiThread( () => { OptionsImage.Visibility = ViewStates.Visible; } );
		}

		private void OptionsImage_Click( object sender, EventArgs e )
		{
			if( !InOptionsClick )
			{
				InOptionsClick = true;

				try
				{
					Globals.Keyboard.Hide = true;

					Animation Ani;

					if( OptionsMenuLayout.LayoutParameters is { Width: 0 } )
					{
						OptionsMenuParams.Width = Globals.Android.ConvertDpToPixels( 250 );

						OptionsMenuLayout.LayoutParameters = OptionsMenuParams;
						Ani                                = OptionsMenuLayoutPullInRight;
						OptionsMenuLayout.Visibility       = ViewStates.Visible;
					}
					else
						Ani = OptionsMenuLayoutPushOutRight;

					OptionsMenuLayout.BringToFront();
					OptionsMenuLayout.StartAnimation( Ani );
				}
				finally
				{
					InOptionsClick = false;
				}
			}
		}

		private void SetVisibilityButton()
		{
			var VisibilityBtn = FindViewById<Button>( Resource.Id.visibilityButton );

			if( VisibilityBtn is not null )
			{
				VisibilityBtn.Text = Globals.Filters.Visibility switch
				                     {
					                     Globals.Filters.VISIBILITY.ALL      => "ALL",
					                     Globals.Filters.VISIBILITY.DELIVERY => "DEL",
					                     Globals.Filters.VISIBILITY.NEW      => "NEW",
					                     Globals.Filters.VISIBILITY.PICKUP   => "PU",
					                     _                                   => VisibilityBtn.Text
				                     };
			}
		}

		private static void SetSortButton( TextView button, Globals.Filters.SORT sortOption )
		{
			button.Text = sortOption switch
			              {
				              Globals.Filters.SORT.ACCOUNT_ID     => "Avv Id",
				              Globals.Filters.SORT.NEW            => "NEW",
				              Globals.Filters.SORT.ACCOUNT_NUMBER => "Acc Num",
				              Globals.Filters.SORT.COMPANY        => "Comp",
				              Globals.Filters.SORT.DUE            => "Due",
				              Globals.Filters.SORT.REFERENCE      => "Ref",
				              Globals.Filters.SORT.ID             => "ID",
				              Globals.Filters.SORT.STATUS         => "Status",
				              Globals.Filters.SORT.SERVICE_LEVEL  => "Sv Level",
				              _                                   => button.Text
			              };
		}

		private void DoSortOptions()
		{
			switch( RequestedOption )
			{
			case Globals.Filters.OPTION.VISABILITY:
				SetVisibilityButton();

				break;

			case Globals.Filters.OPTION.PRIMARY_SORT:
				SetPrimarySortButton();

				break;

			case Globals.Filters.OPTION.SECONDARY_SORT:
				SetSecondarySortButton();

				break;
			}
		}

		private void SetPrimarySortButton()
		{
			SetSortButton( FindViewById<Button>( Resource.Id.primarySortButton ), Globals.Filters.PrimarySort );
		}

		private void SetSecondarySortButton()
		{
			SetSortButton( FindViewById<Button>( Resource.Id.secondarySortButton ), Globals.Filters.SecondarySort );
		}

		private void VisibilityButton_Click( object sender, EventArgs e )
		{
			StartActivityForResult( typeof( Visibility ), Globals.Dialogues.VISIBILITY );
		}

		private void DoSortMenu( Globals.Filters.OPTION option )
		{
			RequestedOption = option;
			var Intent1 = new Intent( this, typeof( Sort ) );
			Intent1.PutExtra( "SortOption", (int)option );
			StartActivityForResult( Intent1, Globals.Dialogues.SORT );
		}

		private void PrimarySortButton_Click( object sender, EventArgs e )
		{
			DoSortMenu( Globals.Filters.OPTION.PRIMARY_SORT );
		}

		private void SecondarySortButton_Click( object sender, EventArgs e )
		{
			DoSortMenu( Globals.Filters.OPTION.SECONDARY_SORT );
		}
	}
}