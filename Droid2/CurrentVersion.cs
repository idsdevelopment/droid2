// ReSharper disable RedundantUsingDirective

#nullable enable

using System.IO;
#if !DEBUG || DEBUG_INSTALL
using System;
using System.Net.Http;
using System.Text;
#endif
using System.Threading;
using Android.Content;
using Android.Support.V4.Content;
using Environment = Android.OS.Environment;
using File = Java.IO.File;
using Uri = Android.Net.Uri;

namespace Droid2
{
	public static partial class Globals
	{
		public static class Version
		{
			public const uint CURRENT_VERSION = 505;

			private const string PACKAGE_NAME = "Ids.Droid2-Signed.apk";

			private static Timer? UpdateTimer;

			private static File FileName
			{
				get
				{
					var Downloads = Environment.GetExternalStoragePublicDirectory( Environment.DirectoryDownloads )!.AbsolutePath;
					return new File( Path.Combine( Downloads!, PACKAGE_NAME ) );
				}
			}

			public static bool CheckForUpdates
			{
				get { return UpdateTimer != null; }

				// ReSharper disable once ValueParameterNotUsed
				set
				{
				#if !DEBUG || DEBUG_INSTALL
					if( UpdateTimer != null )
					{
						var Temp = UpdateTimer;
						UpdateTimer = null;
						Temp.Dispose();
					}

					if( value )
					{
						var Url         = "http://" + DOWNLOAD_URL.Replace( "~LOCATION~", Modifications.UpdateLocation ).Replace( "//", "/" );
						var VersionFile = Url + VERSION_FILE;
						var PackageFile = Url + PACKAGE_NAME;
						var Client      = new HttpClient { Timeout = new TimeSpan( 0, 0, 15, 0 ) };

						UpdateTimer = new Timer( async _ =>
						                         {
							                         try
							                         {
								                         if( !Database.Config.InstallNeeded )
								                         {
									                         var Bytes   = await Client.GetByteArrayAsync( VersionFile );
									                         var Version = Encoding.ASCII.GetString( Bytes ).Trim();

									                         if( uint.TryParse( Version, out var ServerVersion ) )
									                         {
										                         if( ServerVersion > CURRENT_VERSION )
										                         {
											                         var Package = await Client.GetByteArrayAsync( PackageFile );
											                         var FName   = FileName.AbsolutePath;

											                         if( FName is not null )
											                         {
												                         System.IO.File.Delete( FName );
												                         System.IO.File.WriteAllBytes( FName, Package );
											                         }
											                         else
												                         return;

											                         var Config = Database.Config;
											                         Config.InstallNeeded = true;
											                         Database.Config      = Config;

											                         Debug.Activity.ShowUpdateAvailable();
										                         }
									                         }
								                         }
							                         }
							                         catch( Exception E )
							                         {
								                         Console.WriteLine( E.Message );
							                         }
						                         }, null, 10 * 1000, POLLING_INTERVAL_IN_MINUTES * 60 * 1000 );
					}
				#endif
				}
			}

			public static void DoUpdateIfNeeded()
			{
				var Config = Database.Config;

				if( Config.InstallNeeded )
				{
					if( System.IO.File.Exists( FileName.AbsolutePath ) )
						Dialogues.Warning( "Installing a software update.", DoInstall );
					else
					{
						Config.InstallNeeded = false;
						Database.Config      = Config;
					}
				}
			}

			private static void DoInstall()
			{
				var Config = Database.Config;
				Config.InstallNeeded = false;
				Database.Config      = Config;

				Intent InstallIntent;
				var    F = FileName;
				Uri    U;

				if( Android.MajorVersion >= 7 )
				{
					InstallIntent = new Intent( Intent.ActionInstallPackage );
					U             = FileProvider.GetUriForFile( Debug.Activity.ApplicationContext, "com.idsroute.provider", FileName );
				}
				else
				{
					InstallIntent = new Intent( Intent.ActionView );
					U             = Uri.FromFile( F )!;
				}

				InstallIntent.SetDataAndType( U, "application/vnd.android.package-archive" );
				InstallIntent.SetFlags( ActivityFlags.NewTask | ActivityFlags.GrantReadUriPermission | ActivityFlags.GrantWriteUriPermission );
				Debug.Activity.StartActivity( InstallIntent );
			}

		#if !DEBUG || DEBUG_INSTALL
			private const int POLLING_INTERVAL_IN_MINUTES = 60;

			private const string DOWNLOAD_URL = "jbtest.internetdispatcher.org/html5/Droid/~LOCATION~/";
			private const string VERSION_FILE = "CurrentVersion.txt";
		#endif
		}
	}
}